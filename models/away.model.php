<?php
class multitender_model_away extends multitender_model {

    function  __construct() {
        parent::__construct();
        $this->db_person = & $this->conf['dbs']['person'];
        $this->db_tenders = & $this->conf['dbs']['tenders'];
    }
    
    public function click($site_id) {
        $sql = "INSERT IGNORE INTO away VALUES (?,?)";
        $this->db_person->Execute($sql, array(
            $site_id,
            0
        ));
        $sql = "UPDATE away SET cnt=cnt+1 WHERE site_id=?";
        $this->db_person->Execute($sql, $site_id);
    }
    
    public function get_statistic() {
        
    }
}