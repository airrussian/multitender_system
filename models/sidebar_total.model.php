<?php

class multitender_model_sidebar_total extends multitender_model {

    function  __construct() {
        parent::__construct();
        $this->db     = & $this->conf['dbs']['tenders'];
        $this->db1    = & $this->conf['dbs']['person'];
    }


    function get_info() {

        $info = array();

        $info['tenders']['count'] = $this->db->GetOne("SELECT count(*) FROM item");

        $date = date('Y-m-d', strtotime('-21 day'));

        $info['tenders']['actual'] = $this->db->GetRow("SELECT count(*) as count, SUM(price)/(1000*1000*1000) as summa FROM item WHERE date > '$date'");

        $info['customers']['count'] = $this->db->GetOne("SELECT count(*) FROM customer");

        $info['distributors']['count'] = $this->db->GetOne("SELECT count(id) FROM distributor");

        $info['users']['count'] = $this->db1->GetOne("SELECT COUNT(*) as count FROM users");
        $info['firms']['count'] = $this->db1->GetOne("SELECT COUNT(*) as count FROM firms");

        return $info;
        
    }

    function getRegName() {
        if (!isset($_COOKIE['tc_region'])) {
            return 'вашего региона';
        } else {
            return $this->db->GetOne("SELECT name_rp FROM region WHERE id=" . (int)$_COOKIE['tc_region']);
        }
    }


}
