<?php

class multitender_model_email extends multitender_model {

    /**
     * Отправка письма
     * @param string $subject - заголовок письма
     * @param string $html - текст письма / html
     * @param string $to - адресат
     * @param string $from - откуда
     * @param array $attache - массив файлов для отправки
     * @param string $repty-to - ответ на ...
     */
    function send($subject, $html, $to, $from, $attaches=NULL, $reptyto=NULL) {

        $bound=md5(time())."-".crc32($subject);
        
        $header = "From: $from \nSubject: $subject \nMIME-Version: 1.0 \n";
        if (!is_null($reptyto)) { $header.="Reply-To: $reptyto\n"; }

        if (is_null($attaches)) {
            $header.="Content-type: text/html; charset=UTF-8\n";
            return mail($to, $subject, $html, $header);
        }

        $header.="Content-Type: multipart/mixed; boundary=$bound\n";
	$body="--$bound\n";
	$body.="Content-type: text/html; charset=UTF-8\n";
	$body.="Content-transfer-encoding: quoted-printable\n\n";
	$body.=$html . "\n\n";
        
        foreach ($attaches as $attache) {
            if (!file_exists($attache['file'])) { continue; }
            $type = isset($attache['type']) ? $attache['type'] : "application/octet-stream"; 
            $file=fopen($attache['file'],"rb");
            $body.="--$bound\n";
            $body.="Content-Type: $type; name=\"".$attache['name']."\"\n";
            $body.="Content-Transfer-Encoding:base64\n";
            $body.="Content-Disposition:attachment\n\n";
            $body.=base64_encode(fread($file,filesize($attache['file'])));
        }
        mail($to, $subject, $body, $header);
    }

}

?>
