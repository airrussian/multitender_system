<?php
class multitender_model_subscribe_customer extends multitender_model {
    
    private $_tableName = "email_subscribe_customer";

    function  __construct() {
        parent::__construct();
        $this->db      = & $this->conf['dbs']['person'];
        if (is_null($this->db)) {
            trigger_error('Where DB person?');
            exit;
        }
    }
    
    public function GetList($user_id) {        
        if (!is_numeric($user_id)) { return false; }        
        $sql = "SELECT * FROM {$this->_tableName} WHERE user_id = ?";
        return $this->db->GetAll($sql, array($user_id));        
    }
    
    public function check($user_id, $customer_inn) {
        $sql = "SELECT * FROM {$this->_tableName} WHERE user_id = ? AND customer_inn = ?";
        $row = $this->db->GetRow($sql, array($user_id, $customer_inn));
        return empty($row) ? FALSE : TRUE;
    }

    public function add($user_id, $customer_inn) {
        if (!is_numeric($user_id)) { return false; }

        if (!$this->check($user_id, $customer_inn)) {
            $sql = "INSERT INTO {$this->_tableName}(`user_id`, `customer_inn`, `addDate`) VALUES(?,?,?)";
            if ($this->db->Execute($sql, array($user_id, $customer_inn, date("Y-m-d H:i:s")))) {
                return true;
            } else {
                return false;
            }            
        } else {
            return false;
        }
    }

    public function del($user_id, $customer_inn) {
        if (!is_numeric($user_id)) { return false; }

        $sql="DELETE FROM {$this->_tableName} WHERE user_id=? AND customer_inn=? LIMIT 1";
        if ($this->db->Execute($sql, array($user_id, $customer_inn))) {
            return true;
        } else {
            return false;
        }
    }
    
    public function active($user_id, $status) {
        $sql = "UPDATE {$this->_tableName} SET active = ? WHERE user_id = ?";
        if ($this->db->Execute($sql, array($status, $user_id))) {
            return true;
        } else {
            return false;
        }
    }
    
    public function change($user_id, $customer_inn, $actual=1, $freg=1) {
        if (($active > 1) || ($active < 0)) { $active = 1; }
        if ($freg > 7) { $freg = 7; }
        if ($freg < 1) { $freg = 1; }
        $sql = "UPDATE {$this->_tableName} SET `freg`=?, `active`=? WHERE user_id = ? AND customer_id = ?";
        $arr = array($freg, $active, $user_id, $customer_inn);
        return $this->db->Execute($sql, $arr);
    }

}