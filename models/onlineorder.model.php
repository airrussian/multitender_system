<?php

class multitender_model_onlineorder extends multitender_model {

    function  __construct() {
        parent::__construct();
        $this->db      = & $this->conf['dbs']['person'];
        if (is_null($this->db)) {
            trigger_error('Where DB person?');
            exit;
        }
        $this->db1     = & $this->conf['dbs']['tenders'];
        if (is_null($this->db1)) {
            trigger_error('Where DB tenders?');
            exit;
        }
    }

    function addrequest($apps) {
        $sql = "INSERT INTO online_requests(user_id, item_id, url, fio_person, telephone, name_organization, details_organization, email) ";
        $sql.= " VALUES(?,?,?,?,?,?,?,?)";

        $arr = array($apps['user_id'], (int) $apps['form']['item_id'], $apps['page']['action'], $apps['form']['contact'], $apps['form']['telephone'], $apps['form']['name'], $apps['form']['other'], $apps['form']['email']);

        if ($this->db->Execute($sql, $arr)) {
            return true;
        } else {
            return false;
        }
    }

    function listrequest($offset, $count) {
        $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM online_requests ORDER BY datetime DESC LIMIT $offset, $count";
        if ($res = $this->db->GetAll($sql)) {
            $res['total'] = $this->db->GetOne("SELECT FOUND_ROWS()");
            return $res;
        } else {
            return false;
        }
    }

    function delrequest($id) {
        $sql = "DELECT FROM online_requests WHERE id=?";
        if ($this->db->Execute($sql, array($id))) {
            return true;
        } else {
            return false;
        }
    }

    function get_item($item_id) {
        $sql = "SELECT item.*, type.name as type_name FROM item, type WHERE item.id=? AND item.type_id = type.id LIMIT 1";
        if ($item = $this->db1->GetRow($sql, array($item_id))) {
            return $item;
        } else {
            return false;
        }
    }

    function get_id_request($user_id, $item_id) {
        $sql = "SELECT id FROM online_requests WHERE user_id=? AND item_id=? AND user_id>0";
        if ($id_request = $this->db->GetOne($sql, array($user_id, $item_id))) {
            return $id_request;
        } else {
            return false;
        }
    }

    function gozexpress_addrequest($data) {
        $sql = "INSERT INTO online_requests(user_id, item_id, url, fio_person, telephone, name_organization, details_organization, email) ";
        $sql.= " VALUES(?,?,?,?,?,?,?,?)";

        $arr = array($data['user_id'], 0, $data['page'], $data['fio1'], $data['tel1'], $data['name_org1'], $data['dop_info1'], $data['email1']);
        if ($this->db->Execute($sql, $arr)) {
            return true;
        } else {
            return false;
        }

    }

}