<?php

class multitender_model_comments_moder extends multitender_model {

    function __construct() {
        parent::__construct();
        if (is_null($this->db_person = & $this->conf['dbs']['person'])) {
            trigger_error('Where DB person?');
            exit;
        }
    }

   function select_comments($offset=0, $rectopg=10, $filter=NULL) {
        $sql = "SELECT SQL_CALC_FOUND_ROWS comments.id, comments.user_id, comments.personal_flag, comments.text, comments.date_time, comments.tender_id, users.name FROM comments INNER JOIN users ON comments.user_id = users.id ";
        if (!empty($filter)) {
            $names = explode(' ', $filter);
            $sql .= "AND (MATCH(users.name) AGAINST('";
            foreach ($names as $n) {
                $sql .=" +".$n."* ";
            }
            $sql .= "' IN BOOLEAN MODE) OR MATCH(comments.text) AGAINST('";
            foreach ($names as $n) {
                $sql .=" +".$n."* ";
            }
            $sql .= "' IN BOOLEAN MODE) OR MATCH(comments.tender_id) AGAINST('";
            foreach ($names as $n) {
                $sql .=" +".$n."* ";
            }
            $sql .= "' IN BOOLEAN MODE)) ";
        }
        $sql .= "ORDER BY comments.id DESC LIMIT $offset, $rectopg";
        $comments = $this->db_person->GetAll($sql, array());
        $comments['total'] = $this->db_person->GetOne("SELECT FOUND_ROWS()");
        return $comments;
    }

    function del_rec($id) {
        $sql = "DELETE FROM comments WHERE id = ?";
        $this->db_person->Execute($sql, array((int)$id));
        return;
    }
}