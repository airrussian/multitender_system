<?php

class multitender_model_example extends multitender_model {

    private $bads=array("Возможно вы ввели слишком длиный запрос.");

    public function get_rnd_tips() {
        $count=$this->db->GetOne("SELECT COUNT(*) FROM tip");
        $id=(int)mt_rand(1,$count);
        $text=$this->db->GetOne("SELECT text FROM tip WHERE id=".$id);
        return $text;
    }

    public function get_rnd_best() {
        $count=$this->db->GetOne("SELECT COUNT(*) FROM example");
        $id=(int)mt_rand(1,$count);
        $text=$this->db->GetOne("SELECT text FROM example WHERE id=".$id);
        return $text;
    }

    public function get_rnd_bads() {
        return $this->bads[rand(0,count($this->bads)-1)];
    }

}
