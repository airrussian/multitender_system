<?php

class multitender_model_moder_interface extends multitender_model {

    function  __construct() {
        parent::__construct();
        $this->db      = & $this->conf['dbs']['person'];
        // Коннект для krasnoyarsk.tenders
        $this->db1 = ADONewConnection( $this->conf['db_conf']['tenders_write']['dsn'] );
        $this->db1->debug = $this->conf['db_conf']['tenders_write']['debug'];
        $this->db1->Execute('set names utf8');
        // $this->db1     = & $this->conf['dbs']['tenders'];
    }

    function get_no_confirm_item($filter, $offset=0, $count=10, $moder_id=0) {
        $sql="SELECT SQL_CALC_FOUND_ROWS * FROM item WHERE source='moderator' ";
        switch ($filter) {
            case 'con': $sql.="AND status=1  "; break;
            case 'can': $sql.="AND status=-1 "; break;
            case 'ncn': $sql.="AND status=0  "; break;
        }
        if ($moder_id) {
            $item_ids = $this->get_item_moder($moder_id, $offset, $count);
            if ($item_ids['total']) {
                $total = $item_ids['total'];
                unset($item_ids['total']);
                $sql.="AND id IN (".implode($item_ids,",").") ";
            } else {
                $sql.="AND id=0 ";
            }
            $sql.="ORDER BY status ";
        } else {
            $sql.="ORDER BY status, id ";
            $sql.="DESC LIMIT $offset, $count";
        }

        $item=$this->db1->GetAll($sql);

        if ($moder_id) {
            $item['total'] = @$total?$total:0;
        } else {
            $item['total'] = $this->db1->GetOne("SELECT FOUND_ROWS()");
        }

        return $item;
    }

    function get_item_moder($moder_id, $offset=0, $count=10) {
        if (!$moder_id) { return false; exit; }

        $sql="SELECT SQL_CALC_FOUND_ROWS item_id FROM moder_item WHERE user_id=? ORDER BY item_id LIMIT $offset, $count";
        $item = $this->db->GetCol($sql, array($moder_id));
        $item['total'] = $this->db->GetOne("SELECT FOUND_ROWS()");
        return $item;
    }

    function get_moder_items($ids) {
        if (!empty($ids)) {
            $sql = "SELECT item_id, user_id FROM moder_item ";
            $sql.= "WHERE  item_id IN (".implode($ids, ",").")";
            $mod_item = $this->db->GetAll($sql);
            return $mod_item;
        } else {
            return false;
        }
    }

    function add_pages($page_url, $user_id) {
        if (!$page_url_crc = crc_p($page_url)) {
            return false;
            exit();
        }

        $sql="SELECT id FROM moder_pages WHERE page_url_crc=?";
        if (!$id=$this->db->GetOne($sql, array($page_url_crc))) {
            $sql="INSERT INTO moder_pages(page_url_crc, page_url, user_id) VALUES(?, ?, ?)";
            $arr = array($page_url_crc, $page_url, $user_id);
            $this->db->Execute($sql, $arr);
            $id = $this->db->Insert_ID();

            $sql="INSERT INTO moder_pages_content(id) VALUES(?)";
            $this->db->Execute($sql, array($id));
            return $this->db->Insert_ID();
        } else {
            return $id;
        }
    }

    function del_pages($page_id) {
        $this->db->Execute("DELETE FROM moder_pages WHERE id=?", array($page_id));
        $this->db->Execute("DELETE FROM moder_pages_content WHERE id=?", array($page_id));
    }

    function nochange_pages($page_id) {
        if (is_null($this->db)) {
            return false;
        }

        $this->db->Execute("UPDATE moder_pages SET page_change=0 WHERE id=?", array($page_id));
    }

    function get_list_pages($offset=0, $count=10) {
        $sql="SELECT SQL_CALC_FOUND_ROWS * FROM moder_pages ORDER BY id DESC LIMIT $offset, $count";
        if ($pages=$this->db->GetAll($sql)) {
            $total = $this->db->GetOne("SELECT FOUND_ROWS()");
            $pages['total'] = $total;
            return $pages;
        } else {
            return false;
        }
    }

    function get_page($page_id) {
        $sql = "SELECT moder_pages.*, moder_pages_content.content, moder_pages_content.text_change FROM moder_pages, moder_pages_content ";
        $sql.= "WHERE moder_pages.id=moder_pages_content.id ";
        $sql.= "AND moder_pages.id=? LIMIT 1";
        if ($page = $this->db->GetRow($sql, array($page_id))) {
            return $page;
        } else {
            return false;
        }
    }

    function get_list_pages_by_ids($page_ids) {
        $sql = "SELECT moder_pages.*, moder_pages_content.text_change FROM moder_pages, moder_pages_content ";
        $sql.= "WHERE moder_pages.id=moder_pages_content.id ";
        $sql.= "AND moder_pages.id IN (".implode($page_ids,",").")";

        if ($pages = $this->db->GetAll($sql)) {
            return $pages;
        } else {
            return false;
        }
    }

    function get_list_pages_change($user_id=0, $offset=0, $count=10) {
        $sql="SELECT SQL_CALC_FOUND_ROWS * FROM moder_pages ";
        if ($user_id) {
            $sql.="WHERE user_id=$user_id ";
        }
        $sql.="ORDER BY page_change DESC, id LIMIT $offset, $count";

        if ($pages=$this->db->GetAll($sql)) {
            $total = $this->db->GetOne("SELECT FOUND_ROWS()");
            foreach ($pages as $page) {
                $ch_pg_ids[] = $page['id'];
            }

            $sql="SELECT * FROM moder_pages_content WHERE id IN (".implode($ch_pg_ids, ",").")";
            $content = $this->db->GetAll($sql);

            foreach ($pages as &$page) {
                foreach ($content as $cont) {
                    if ($page['id']==$cont['id']) {
                        $page['cont']['last'] = $cont['content'];
                        $page['cont']['chgs'] = $cont['text_change'];
                    }
                }
            }
            $pages['total'] = $total;
            return $pages;
        } else {
            return false;
        }
    }

    function get_item_form_page($page_id, $offset=0, $count=20) {
        $page_id = (int) $page_id;
        $offset = (int) $offset;
        $count = (int) $count;

        $sql="SELECT SQL_CALC_FOUND_ROWS * FROM moder_item WHERE moder_pages_id=? ORDER BY item_id DESC LIMIT $offset, $count";
        if ($r = $this->db->GetAll($sql, array($page_id))) {
            $total = $this->db->GetOne("SELECT FOUND_ROWS()");
            $r['total'] = $total;
            return $r;
        } else {
            return false;
        }
    }

    function add_item($item, $user_id) {
        if ($item['begin']) {
            list($d, $m, $y) = explode('.', $item['begin']);
            $beg = $y."-".$m."-".$d;
        } else {
            $beg = NULL;
        }

        if ($item['end']) {
            list($d, $m, $y) = explode('.', $item['end']);
            $end = $y."-".$m."-".$d;
        } else {
            $end = NULL;
        }

        $date_add = $this->db1->DBTimeStamp(time());

        $crc = crc_p($item['name'].$end.$item['link_source']);

        $sql="SELECT id FROM item WHERE internal_id=?";
        if (!$id = $this->db1->GetOne($sql, array($crc))) {
            $sql="INSERT INTO item(name, type_id, region_id, date_publication, date_end, price, detail_link, customer, source, date_add, internal_id, status, site_id)
                            VALUES (?,?,?,?,?,?,?,?,'moderator', $date_add, $crc, 0, 990001)";
            $this->db1->Execute($sql, array($item['name'], $item['type'], $item['region'], $beg, $end, $item['summa'], $item['link_source'], $item['customer']));
            $item_id = $this->db1->Insert_ID();

            $sql="INSERT INTO moder_item(item_id, user_id, moder_pages_id) VALUES(?,?,?)";
            $this->db->Execute($sql, array($item_id, $user_id, $item['id']));
            return (-1)*$item_id;
        } else {
            return $id;
        }
    }

    function get_items($item_ids) {
        $sql="SELECT * FROM item WHERE id IN (".implode($item_ids,",").") ORDER BY FIELD(id,".implode($item_ids,",").")";
        $names=$this->db1->GetAll($sql);
        return $names;
    }

    function edit_item($item, $item_id) {
        if ($item['begin']) {
            list($d, $m, $y) = explode('.', $item['begin']);
            $beg = $y."-".$m."-".$d;
        } else {
            $beg = NULL;
        }

        if ($item['end']) {
            list($d, $m, $y) = explode('.', $item['end']);
            $end = $y."-".$m."-".$d;
        } else {
            $end = NULL;
        }
        
        $sql="UPDATE item SET name=?, type_id=?, region_id=?, date_publication=?, date_end=?, price=?, detail_link=?, customer=? WHERE id=?";
        if ($this->db1->Execute($sql, array($item['name'], $item['type'], $item['region'], $beg, $end, $item['summa'], $item['link_source'], $item['customer'], $item_id))) {
            return true;
        } else {
            return false;
        }

    }

    function get_types() {
        $sql="SELECT * FROM type";
        $types = $this->db1->GetAll($sql);

        return $types;

    }

    function get_regions() {
        $sql="SELECT * FROM region";
        $region = $this->db1->GetAll($sql);

        return $region;
    }

    function get_item_from_page($page_id) {
        $sql = "SELECT item_id FROM moder_item WHERE moder_pages_id=? ORDER BY item_id DESC";
        if ($id = $this->db->GetOne($sql, array($page_id))) {
            return $id;
        } else {
            return false;
        }
    }


    function get_last_item($page_id) {
        // region_id

        $id = $this->get_item_from_page($page_id);

        if ($id) {
            $sql = "SELECT * FROM item WHERE id=? LIMIT 1";
            $item = $this->db1->GetRow($sql, array($id));
            return $item;
        } else {
            return false;
        }
    }

    function item_confirm($id, $n) {
        $sql="UPDATE item SET status=? WHERE id=?";
        if ($this->db1->Execute($sql, array($n, $id))) {
            return true;
        } else {
            return false;
        }
    }

    function change_moderator($page_id, $moder_id) {
        $page_id = (int) $page_id;
        $moder_id = (int) $moder_id;

        $sql="UPDATE moder_pages SET user_id=? WHERE id=?";

        if ($this->db->Execute($sql, array($moder_id, $page_id))) {
            return true;
        } else {
            return false;
        }

    }

}
