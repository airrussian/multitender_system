<?php

class multitender_model_tag_cloud extends multitender_model {

    function  __construct() {
        parent::__construct();
    }

    public function get_weight_type($count) {

        if (is_null($this->db)) {
            return false;
        }

        // Запрос на кол-во типов
        if (!$wt = $this->db->GetAll("SELECT type_id, count(*) as type_count " .
                "FROM item WHERE type_id > 0 GROUP BY type_id ORDER BY type_count " .
                "DESC LIMIT ?", array($count))) {
            return false;
        }

        if (!$tnm = $this->db->GetAll("SELECT * FROM type")) {
            return false;
        }

        // Считаем общее кол-во и прикручиваем названия
        $total_count = 0;
        foreach ($wt as &$c) {
            foreach ($tnm as $t) {
                if ($t['id']==$c['type_id']) {
                    $c['name'] = $t['name'];
                    $c['id'] = $c['type_id'];
                }
            }
            $total_count += $c['type_count'];
        }
        if (!$total_count) {
            return false;
        }
        foreach ($wt as &$c) {
            $c['weight'] = $c['type_count'] / $total_count;
        }

        return $wt;
    }

    public function get_weight_regs($count) {

        if (is_null($this->db)) {
            return false;
        }

        // Запрос на кол-во
        if (!$wr = $this->db->GetAll("SELECT region_id, count(*) as region_count " .
                "FROM item WHERE region_id > 0 GROUP BY region_id " .
                "ORDER BY region_count DESC LIMIT ?", array($count))) {
            return false;
        }

        if (!$rnm = $this->db->GetAll("SELECT * FROM region")) {
            return false;
        }

        // Считаем общее кол-во и прикручиваем названия
        $total_count = 0;
        foreach ($wr as &$c) {
            foreach ($rnm as $t) {
                if ($t['id']==$c['region_id']) {
                    $c['name'] = $t['sname'];
                    $c['id'] = $c['region_id'];
                }
            }
            $total_count += $c['region_count']; 			// 100%
        }
        if (!$total_count) {
            return false;
        }
        foreach ($wr as &$c) {
            $c['weight'] = $c['region_count'] / $total_count;
        }

        return $wr;
    }

    public function get_weight_quers($count) {

        $this->db = & $this->conf['dbs']['person'];

        if (is_null($this->db)) {
            return false;
        }

        $sql = "SELECT search.main as name, count(last_search.search_id) as query_count " .
                "FROM last_search, search " .
                "WHERE last_search.search_id=search.id " .
                "AND search.main IS NOT NULL AND search.region = 0 AND search.okrug = 0 " .
                "GROUP BY last_search.search_id " .
                "ORDER BY query_count DESC " .
                "LIMIT ?";

        if (!$wq = $this->db->GetAll($sql, array($count))) {
            return false;
        }

        $total_count = 0;
        foreach ($wq as &$c) {
            $total_count += $c['query_count'];
        }
        if (!$total_count) {
            return false;
        }
        foreach ($wq as &$c) {
            $c['weight'] = $c['query_count'] / $total_count;
        }

        return $wq;

    }
}
