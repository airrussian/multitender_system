<?php

class multitender_model_favorite_queries extends multitender_model {

    function  __construct() {
        parent::__construct();
        $this->db      = & $this->conf['dbs']['person'];
        if (is_null($this->db)) {
            trigger_error('Where DB person?');
            exit;
        }
    }

    function add_favorite($user_id, $search_id) {
        if (!$user_id) { return false; }
        
        $user_id = (int) $user_id;
        $search_id = (int) $search_id;

        if ($id=$this->check($user_id, $search_id)) { return $id;  }

        $sql="INSERT INTO favorite_queries(user_id, search_id) VALUES(?, ?)";
        if ($this->db->Execute($sql, array($user_id, $search_id))) {
            return $this->db->Insert_ID();
        } else {
            return false;
        }
    }

    function del_favorite($user_id, $search_id) {
        $user_id = (int) $user_id;
        $search_id = (int) $search_id;

        $sql="DELETE FROM favorite_queries WHERE user_id=? AND search_id=? LIMIT 1";
        return $this->db->Execute($sql, array($user_id, $search_id));
    }

    function get_favorite_byid($user_id, $ids) {
        if (!$ids) { return false; }

        $user_id = (int) $user_id;

        $sql="SELECT search_id FROM favorite_queries WHERE user_id=? AND search_id IN (".implode(",", $ids).") ORDER BY FIELD(id,".implode(",", $ids).")";
        $ids=$this->db->GetCol($sql, array($user_id));

        if ($ids) {
            return $ids;
        } else {
            return false;
        }
    }

    function get_favorities($user_id, $offset=0, $count=10) {
        $user_id = (int) $user_id;
        $count = (int) $count;

        $sql="SELECT search_id FROM favorite_queries WHERE user_id=? ORDER BY id DESC LIMIT ?, ?";
        $ids=$this->db->GetCol($sql, array($user_id, $offset, $count));

        if ($ids) {
            return $ids;
        } else {
            return false;
        }
    }

    function get_total_by_date($ids, $date = '2010-08-01') {
        $ids['kostil'] = -1;
        $sql  = " SELECT * FROM (
                    SELECT search_id, total_items
                    FROM email_update_searches
                    WHERE search_id IN (" . implode(",", $ids) . ") AND date_update<'$date'
                    ORDER BY id DESC) as eus
                  GROUP BY search_id ";
        $return = array();
        foreach ($this->db->GetAll($sql) as $v) {
            $return[(int)$v['search_id']] = (int)$v['total_items'];
        }

        return $return;
    }

    function get_stat($ids, $from_date = '2010-08-01') {
        $ids['kostil'] = -1;
        $sql  = " SELECT search_id, CAST(date_update AS DATE) as date, MAX(total_items) as total_items ";
        $sql .= " FROM email_update_searches ";
        $sql .= "  WHERE search_id IN (" . implode(",", $ids) . ") ";
        $sql .= "  AND date_update > '$from_date' ";
        $sql .= " GROUP BY search_id, date ORDER BY date DESC";

        $return = array();
        foreach ($this->db->GetAll($sql) as $v) {
            $return[(int)$v['search_id']][$v['date']] = (int)$v['total_items'];
        }
        return $return;
    }



    function get_favorities_total($user_id) {
        $sql = "SELECT count(search_id) FROM favorite_queries WHERE user_id=?";
        return $this->db->GetOne($sql, array($user_id));
    }

    function check($user_id, $search_id) {
        $user_id = (int) $user_id;
        $search_id = (int) $search_id;

        $sql="SELECT id FROM favorite_queries WHERE user_id=? AND search_id=?";
        if ($id=$this->db->GetOne($sql, array($user_id, $search_id))) {
            return $id;
        } else {
            return false;
        }
    }
}