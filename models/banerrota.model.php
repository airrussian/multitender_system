<?php

class multitender_model_banerrota extends multitender_model {

    function __construct() {
        parent::__construct();
        $this->db      = & $this->conf['dbs']['person'];
        if (is_null($this->db)) {
            trigger_error('Where DB person?');
            exit;
        }
    }

    function get_rand_baner_horiz($reg_id) {
        /*if ($_SERVER["REQUEST_URI"] != '/') {
            $onlymain = true;
        } else {
            $onlymain = false;
        }
        $our = 0;
        $sql = 'SELECT COUNT(*) FROM baners WHERE published = 1 AND type >=0 AND type<=1 AND our = ' . $our .' AND region = ' . $reg_id;
        if ($onlymain == true) {
            $sql .= ' AND onlymain = 0';
        }
        $count = $this->db->GetOne($sql);

        if ($count == 0) {
            $our = 1;
            $sql = 'SELECT COUNT(*) FROM baners WHERE published = 1 AND type >=0 AND type<=1 AND our = ' . $our . ' AND region = ' . $reg_id;
            if ($onlymain == true) {
                $sql .= ' AND onlymain = 0';
            }
            $count = $this->db->GetOne($sql);
        }

        if ($count == 0) {//если нет банеров для региона
            $reg_id = 0;
            $our = 0;
            $sql = 'SELECT COUNT(*) FROM baners WHERE published = 1 AND type >=0 AND our = ' . $our . ' AND type<=1 AND region = ' . $reg_id;
            if ($onlymain == true) {
                $sql .= ' AND onlymain = 0';
            }
            $count = $this->db->GetOne($sql);
            if ($count == 0) {
                $our = 1;
                $sql = 'SELECT COUNT(*) FROM baners WHERE published = 1 AND type >=0 AND our = ' . $our . ' AND type<=1 AND region = ' . $reg_id;
                if ($onlymain == true) {
                    $sql .= ' AND onlymain = 0';
                }
                $count = $this->db->GetOne($sql);
            }
            if ($count == 0) {//если нет банеров для всех
                return array(array('pathway'=>0),'type'=>1);
            }
        }

        $rand_row = floor(rand(0,$count-1));

        $string = "SELECT id,pathway,type,link,our FROM baners WHERE published = 1 AND our = $our AND type >=0 AND type<=1 AND region=$reg_id ";
        if ($onlymain == true) {
            $string .= 'AND onlymain = 0 ';
        }
        $string .= "LIMIT " . $rand_row . ', 1';
        $path = $this->db->GetAll($string);
        if ($path[0]['type']==1) {
            return array(array('pathway'=>$path[0]['pathway'],'link'=>$path[0]['link'],'id'=>$path[0]['id'],'our'=>$path[0]['our']),'type'=>1);
        } else {
            $our = 0;
            $sql = "SELECT COUNT(*) FROM baners WHERE published = 1 AND type = 0 AND our = $our AND id != " . $path[0]['id'] . " AND region = $reg_id";
            if ($onlymain == true) {
                $sql .= ' AND onlymain = 0';
            }
            $count = $this->db->GetOne($sql);

            if ($count == 0) {
                $our = 1;
                $sql = "SELECT COUNT(*) FROM baners WHERE published = 1 AND type = 0 AND our = $our AND id != " . $path[0]['id'] . " AND region = $reg_id";
                if ($onlymain == true) {
                    $sql .= ' AND onlymain = 0';
                }
                $count = $this->db->GetOne($sql);
            }

            if ($count != 0) {
                $rand_row = floor(rand(0,$count-1));
                $string = "SELECT id,pathway,link,our FROM baners WHERE published = 1 AND our=$our AND type = 0 AND id != " . $path[0]['id'] . " AND region = $reg_id ";
                if ($onlymain == true) {
                    $string .= 'AND onlymain = 0';
                }
                $string .=" LIMIT " . $rand_row . ', 1';
                $path2 = $this->db->GetAll($string);
                if ($path2) {
                    return array(array('pathway'=>$path[0]['pathway'],'link'=>$path[0]['link'],'id'=>$path[0]['id'],'our'=>$path[0]['our']), array('pathway'=>$path2[0]['pathway'],'link'=>$path2[0]['link'],'id'=>$path2[0]['id'],'our'=>$path2[0]['our']),'type' =>0);
                } else {
                    return array(array('pathway'=>$path[0]['pathway'],'link'=>$path[0]['link'],'id'=>$path[0]['id'],'our'=>$path[0]['our']), array('pathway'=>0),'type' =>0);
                }
            } else {
                return array(array('pathway'=>$path[0]['pathway'],'link'=>$path[0]['link'],'id'=>$path[0]['id'],'our'=>$path[0]['our']), array('pathway'=>0),'type' =>0);
            }
        }*/
        if ($_SERVER["REQUEST_URI"] != '/') {
            $onlymain = true;
        } else {
            $onlymain = false;
        }
        $sql = 'SELECT * FROM baners WHERE published = 1 AND freq > 0 AND type = 0 AND region = ' . $reg_id;
        if ($onlymain == true) {
            $sql .= ' AND onlymain = 0';
        }
        $path = $this->db->GetAll($sql);

        if (count($path)==0) {
            $sql = 'SELECT * FROM baners WHERE published = 1 AND freq > 0 AND type = 0 AND region = 0';
            if ($onlymain == true) {
                $sql .= ' AND onlymain = 0';
            }
            $path = $this->db->GetAll($sql);
        }
 
        $sql = 'SELECT text FROM direct WHERE id = 2';
        $text_wide = $this->db->GetAll($sql);
        if (count($path)>0) {
            $sql= 'SELECT * FROM banerpos WHERE id=0';
            $direct = $this->db->GetAll($sql);
            $sum = 0;
            if (count($direct)>0 && $direct[0]['freq']>0) {
                $sum = $sum + $direct[0]['freq'];
            }
            foreach ($path as $elem) {
                $sum = $sum + $elem['freq'];
            }
            $rand = floor(rand(1,$sum));

            $sum=0;
            if ($rand <= $direct[0]['freq'] && $direct[0]['freq']>0) {
                return array(array('text'=>$text_wide[0]['text']), 'type'=>0);
            } else {
                if ($direct[0]['freq']>0) {
                    $sum = $direct[0]['freq'];
                } else {
                    $sum = 0;
                }
                foreach ($path as $elem) {
                    if (($rand >= $sum) && ($rand <= ($sum+$elem['freq']))) {
                        return array(array('pathway'=>$elem['pathway'],'link'=>$elem['id']), 'type'=>0);
                    }
                    $sum = $sum+$elem['freq'];
                }
            }
        } else {
            $flag = false;
            $sql = 'SELECT * FROM baners WHERE published = 1 AND freq > 0 AND type = 1 AND region = ' . $reg_id;
            if ($onlymain == true) {
                $sql .= ' AND onlymain = 0';
            }
            $path = $this->db->GetAll($sql);

            if (count($path)==0) {
                $sql = 'SELECT * FROM baners WHERE published = 1 AND freq > 0 AND type = 1 AND region = 0';
                if ($onlymain == true) {
                    $sql .= ' AND onlymain = 0';
                }
                $path = $this->db->GetAll($sql);
            }

            $sql = 'SELECT text FROM direct WHERE id = 1';
            $text = $this->db->GetAll($sql);
            if (count($path)>0) {
                $sql= 'SELECT * FROM banerpos WHERE id=1';
                $direct = $this->db->GetAll($sql);
                $sum = 0;
                if (count($direct)>0 && $direct[0]['freq']>0) {
                    $sum = $sum + $direct[0]['freq'];
                }
                foreach ($path as $elem) {
                    $sum = $sum + $elem['freq'];
                }
                $rand = floor(rand(1,$sum));

                $sum = 0;
                if ($rand <= $direct[0]['freq'] && $direct[0]['freq']>0) {
                    $flag = true;
                    
                    $return[0] = array('text'=>$text[0]['text']);
                    $return['type']=1;
                } else {
                    if ($direct[0]['freq']>0) {
                        $sum = $direct[0]['freq'];
                    } else {
                        $sum = 0;
                    }
                    foreach ($path as $elem) {
                        if (($rand >= $sum) && ($rand <= ($sum+$elem['freq']))) {
                            $return[0]=array('pathway'=>$elem['pathway'],'link'=>$elem['id']);
                            $return['type']=1;
                            break;
                        }
                        $sum = $sum+$elem['freq'];
                    }
                }
            } else {
                $flag = true;
                $return[0] = array('text'=>$text[0]['text']);
                $return['type']=1;
            }
                $sql = 'SELECT * FROM baners WHERE published = 1 AND freq > 0 AND type = 2 AND region = ' . $reg_id;
                if ($onlymain == true) {
                    $sql .= ' AND onlymain = 0';
                }
                $path = $this->db->GetAll($sql);

                if (count($path)==0) {
                    $sql = 'SELECT * FROM baners WHERE published = 1 AND freq > 0 AND type = 2 AND region = 0';
                    if ($onlymain == true) {
                        $sql .= ' AND onlymain = 0';
                    }
                    $path = $this->db->GetAll($sql);
                }
                $sql= 'SELECT * FROM banerpos WHERE id=1';
                $direct = $this->db->GetAll($sql);
            if (count($path)>0) {
                $sum = 0;
                if (count($direct)>0 && $direct[0]['freq']>0) {
                    $sum = $sum + $direct[0]['freq'];
                }
                foreach ($path as $elem) {
                    $sum = $sum + $elem['freq'];
                }
                $rand = floor(rand(1,$sum));
                $sum=0;
                if ($rand <= $direct[0]['freq'] && $direct[0]['freq']>0) {
                    if ($flag == false) {
                        $return[1] = array('text'=>$text[0]['text']);
                        return $return;
                    } else {
                        return array(array('text'=>$text_wide[0]['text']), 'type'=>0);
                    }
                } else {
                    if ($direct[0]['freq']>0) {
                        $sum = $direct[0]['freq'];
                    } else {
                        $sum = 0;
                    }
                    foreach ($path as $elem) {
                        if (($rand >= $sum) && ($rand <= ($sum+$elem['freq']))) {
                            $return[1]=array('pathway'=>$elem['pathway'],'link'=>$elem['id']);
                            $return['type'] = 1;
                            return $return;
                        }
                        $sum = $sum+$elem['freq'];
                    }
                }
            } else {
                if ($flag == false) {
                    $return[1] = array('text'=>$text[0]['text']);
                    return $return;
                } else {
                    return array(array('text'=>$text_wide[0]['text']), 'type'=>0);
                }
            }
        }
    }

    function get_rand_baner_vert($reg_id) {
        /*$our = 0;
        $sql = 'SELECT COUNT(*) FROM baners WHERE published = 1 AND our = ' . $our . ' AND type >=2 AND type<=3 AND region = ' . $reg_id;
        $count = $this->db->GetOne($sql);
        if ($count == 0) {
            $our = 1;
            $sql = 'SELECT COUNT(*) FROM baners WHERE published = 1 AND our = ' . $our . ' AND type >=2 AND type<=3 AND region = ' . $reg_id;
            $count = $this->db->GetOne($sql);
        }
        if ($count == 0) {//если нет банеров для региона
            $reg_id = 0;
            $our = 0;
            $sql = 'SELECT COUNT(*) FROM baners WHERE published = 1 AND our = ' . $our . ' AND type >=2 AND type<=3 AND region = ' . $reg_id;
            $count = $this->db->GetOne($sql);
            if ($count ==0) {
                $our = 1;
                $sql = 'SELECT COUNT(*) FROM baners WHERE published = 1 AND our = ' . $our . ' AND type >=2 AND type<=3 AND region = ' . $reg_id;
                $count = $this->db->GetOne($sql);
            }
            if ($count == 0) {//если нет банеров для всех
                return array(array('pathway'=>0),'type'=>2);
            }
        }

        $rand_row = floor(rand(0,$count-1));

        $string = "SELECT id,pathway,type,link,our FROM baners WHERE our = $our AND published = 1 AND type >=2 AND type<=3 AND region=$reg_id LIMIT " . $rand_row . ', 1';
        $path = $this->db->GetAll($string);
        if ($path[0]['type']==2) {
            return array(array('pathway'=>$path[0]['pathway'],'link'=>$path[0]['link'],'id'=>$path[0]['id'],'our'=>$path[0]['our']),'type'=>2);
        } else {
            $our = 0;
            $sql = "SELECT COUNT(*) FROM baners WHERE published = 1 AND our = $our AND type = 3 AND id != '" . $path[0]['id'] . "' AND region = $reg_id";
            $count = $this->db->GetOne($sql);
            if ($count == 0) {
                $our = 1;
                $sql = "SELECT COUNT(*) FROM baners WHERE published = 1 AND our = $our AND type = 3 AND id != '" . $path[0]['id'] . "' AND region = $reg_id";
                $count = $this->db->GetOne($sql);
            }
            if ($count != 0) {
                $rand_row = floor(rand(0,$count-1));
                $string = "SELECT id,pathway,link,our FROM baners WHERE our = $our AND published = 1 AND type = 3 AND id != " . $path[0]['id'] . "' AND region = $reg_id LIMIT " . $rand_row . ', 1';
                $path2 = $this->db->GetAll($string);
                if ($path2) {
                    return array(array('pathway'=>$path[0]['pathway'],'link'=>$path[0]['link'], 'id'=>$path[0]['id'],'our'=>$path[0]['our']), array('pathway'=>$path2[0]['pathway'],'link'=>$path2[0]['link'],'id'=>$path2[0]['id'],'our'=>$path2[0]['our']),'type' =>3);
                } else {
                    return array(array('pathway'=>$path[0]['pathway'],'link'=>$path[0]['link'],'id'=>$path[0]['id'],'our'=>$path[0]['our']), array('pathway'=>0),'type' =>3);
                }
            } else {
                return array(array('pathway'=>$path[0]['pathway'],'link'=>$path[0]['link'],'id'=>$path[0]['id'],'our'=>$path[0]['our']), array('pathway'=>0),'type' =>3);
            }
        }*/
        if ($_SERVER["REQUEST_URI"] != '/') {
            $onlymain = true;
        } else {
            $onlymain = false;
        }
        $sql = 'SELECT * FROM baners WHERE published = 1 AND freq > 0  AND type = 3 AND region = ' . $reg_id;
        if ($onlymain == true) {
            $sql .= ' AND onlymain = 0';
        }
        $path = $this->db->GetAll($sql);

        if (count($path)==0) {
            $sql = 'SELECT * FROM baners WHERE published = 1 AND freq > 0 AND type = 3 AND region = 0';
            if ($onlymain == true) {
                $sql .= ' AND onlymain = 0';
            }
            $path = $this->db->GetAll($sql);
        }

        $sql = 'SELECT text FROM direct WHERE id = 3';
        $text_wide = $this->db->GetAll($sql);
        if (count($path)>0) {
            $sql= 'SELECT * FROM banerpos WHERE id=3';
            $direct = $this->db->GetAll($sql);
            $sum = 0;
            if (count($direct)>0 && $direct[0]['freq']>0) {
                $sum = $sum + $direct[0]['freq'];
            }
            foreach ($path as $elem) {
                $sum = $sum + $elem['freq'];
            }
            $rand = floor(rand(1,$sum));

            $sum=0;
            if ($rand <= $direct[0]['freq'] && $direct[0]['freq']>0) {
                return array(array('text'=>$text_wide[0]['text']), 'type'=>2);
            } else {
                if ($direct[0]['freq']>0) {
                    $sum = $direct[0]['freq'];
                } else {
                    $sum = 0;
                }
                foreach ($path as $elem) {
                    if (($rand >= $sum) && ($rand <= ($sum+$elem['freq']))) {
                        return array(array('pathway'=>$elem['pathway'],'link'=>$elem['id']), 'type'=>2);
                    }
                    $sum = $sum+$elem['freq'];
                }
            }
        } else {
            $flag = false;
            $sql = 'SELECT * FROM baners WHERE published = 1 AND freq > 0 AND type = 4 AND region = ' . $reg_id;
            if ($onlymain == true) {
                $sql .= ' AND onlymain = 0';
            }
            $path = $this->db->GetAll($sql);

            if (count($path)==0) {
                $sql = 'SELECT * FROM baners WHERE published = 1 AND freq > 0 AND type = 4 AND region = 0';
                if ($onlymain == true) {
                    $sql .= ' AND onlymain = 0';
                }
                $path = $this->db->GetAll($sql);
            }

            $sql = 'SELECT text FROM direct WHERE id = 4';
            $text = $this->db->GetAll($sql);
            if (count($path)>0) {
                $sql= 'SELECT * FROM banerpos WHERE id=4';
                $direct = $this->db->GetAll($sql);
                $sum = 0;
                if (count($direct)>0 && $direct[0]['freq']>0) {
                    $sum = $sum + $direct[0]['freq'];
                }
                foreach ($path as $elem) {
                    $sum = $sum + $elem['freq'];
                }
                $rand = floor(rand(1,$sum));
                $sum = 0;
                if ($rand <= $direct[0]['freq'] && $direct[0]['freq']>0) {
                    $flag = true;
                    $return = array(array('text'=>$text[0]['text']), 'type'=>1);
                } else {
                    if ($direct[0]['freq']>0) {
                        $sum = $direct[0]['freq'];
                    } else {
                        $sum = 0;
                    }
                    foreach ($path as $elem) {
                        if (($rand >= $sum) && ($rand <= ($sum+$elem['freq']))) {
                            $return=array(array('pathway'=>$elem['pathway'],'link'=>$elem['id']), 'type'=>1);
                            break;
                        }
                        $sum = $sum+$elem['freq'];
                    }
                }
            } else {
                $flag = true;
                $return = array(array('text'=>$text[0]['text']), 'type'=>1);
            }
                $sql = 'SELECT * FROM baners WHERE published = 1 AND freq > 0 AND type = 5 AND region = ' . $reg_id;
                if ($onlymain == true) {
                    $sql .= ' AND onlymain = 0';
                }
                $path = $this->db->GetAll($sql);

                if (count($path)==0) {
                    $sql = 'SELECT * FROM baners WHERE published = 1 AND freq > 0 AND type = 5 AND region = 0';
                    if ($onlymain == true) {
                        $sql .= ' AND onlymain = 0';
                    }
                    $path = $this->db->GetAll($sql);
                }
                $sql= 'SELECT * FROM banerpos WHERE id=5';
                $direct = $this->db->GetAll($sql);
            if (count($path)>0) {
                $sum = 0;
                if (count($direct)>0 && $direct[0]['freq']>0) {
                    $sum = $sum + $direct[0]['freq'];
                }
                foreach ($path as $elem) {
                    $sum = $sum + $elem['freq'];
                }
                $rand = floor(rand(1,$sum));
                $sum=0;
                if ($rand <= $direct[0]['freq'] && $direct[0]['freq']>0) {
                    if ($flag == false) {
                        $return[1] = array('text'=>$text[0]['text']);
                        return $return;
                    } else {
                        return array(array('text'=>$text_wide[0]['text']), 'type'=>2);
                    }
                } else {
                    if ($direct[0]['freq']>0) {
                        $sum = $direct[0]['freq'];
                    } else {
                        $sum = 0;
                    }
                    foreach ($path as $elem) {
                        if (($rand >= $sum) && ($rand <= ($sum+$elem['freq']))) {
                            $return[1]=array('pathway'=>$elem['pathway'],'link'=>$elem['id']);
                            return $return;
                        }
                        $sum = $sum+$elem['freq'];
                    }
                }
            } else {
                if ($flag == false) {
                    $return[1] = array('text'=>$text[0]['text']);
                    return $return;
                } else {
                    return array(array('text'=>$text_wide[0]['text']), 'type'=>2);
                }
            }
        }
    }

    public function updateShows($id) {
        if (isset($id) && $id != '') {
            $sql = 'INSERT INTO shows (`id`,`date`) VALUES (' . $id . ', ' . ((time()-345600)/604800) . ')';
            $this->db->Execute($sql);
        }
    }
}