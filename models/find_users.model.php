<?php

class multitender_model_find_users extends multitender_model {

    function  __construct() {
        parent::__construct();
        $this->db_per = & $this->conf['dbs']['person'];
        $this->db_joo = ADONewConnection($this->conf['db_conf']['joomla']['dsn']);
        $this->db_joo->debug = $this->conf['db_conf']['joomla']['debug'];
        $this->db_joo->Execute('set names utf8');
    }

    function get_tender_users($search) {

        if (is_null($this->db_per)) {
            return false;
        }

        $sql = "SELECT users.joomla_id FROM firms, users WHERE users.Id=firms.user_id AND ";
        $sql.= " (   firms.name             LIKE '%".$search."%' ";
        $sql.= " OR  firms.INN              LIKE '%".$search."%' ";
        $sql.= " OR  firms.KPP              LIKE '%".$search."%' ";
        $sql.= " OR  firms.domicile_index   LIKE '%".$search."%' ";
        $sql.= " OR  firms.domicile_region  LIKE '%".$search."%' ";
        $sql.= " OR  firms.domicile_town    LIKE '%".$search."%' ";
        $sql.= " OR  firms.domicile         LIKE '%".$search."%' ";
        $sql.= " OR  firms.address_index    LIKE '%".$search."%' ";
        $sql.= " OR  firms.address_region   LIKE '%".$search."%' ";
        $sql.= " OR  firms.address_town     LIKE '%".$search."%' ";
        $sql.= " OR  firms.address          LIKE '%".$search."%' ";
        $sql.= " OR  firms.telephone        LIKE '%".$search."%' ";
        $sql.= " OR  firms.contact          LIKE '%".$search."%' ) ";
        $sql.= " GROUP BY firms.user_id ";
        if ($s_ids = $this->db_per->GetCol($sql)) {
            return $s_ids;
        } else {
            return false;
        }
    }

    function get_joomla_user($search) {
        if (is_null($this->db_per)) {
            return false;
        }

        $sql = "SELECT joomla_id FROM users WHERE users.id=?";
        if ($id = $this->db_per->GetOne($sql, $search)) {
            return $id;
        } else {
            return false;
        }
    }

    function get_joomla_users($search, $s_ids, $offset=0, $count=25) {
        if (is_null($this->db_joo)) {
            return false;
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM ten_users ";

        if ($search) {
            $sql.= " WHERE username LIKE '%".$search."%' OR email LIKE '%".$search."%' ";

            if (!empty($s_ids)) {
                $sql.=" OR id IN (".implode(",", $s_ids).") ";
            }
            // + поиск по ID (Joomla)
            $sql.= " OR id = '$search' ";
            $sql.= " GROUP BY id";
        }

        $sql.= " LIMIT $offset, $count ";
        
        if ($users = $this->db_joo->GetAll($sql)) {
            $users['total'] = $this->db_joo->GetOne("SELECT FOUND_ROWS()");
            return $users;
        } else {
            return false;
        }
    }

    function get_ids_users ($j_ids) {

        if (is_null($this->db_per)) {
            return false;            
        }

        if (empty($j_ids)) {
            return false;
        }

        $sql = "SELECT Id, joomla_id FROM users WHERE joomla_id IN (".implode(",", $j_ids).")";
        if ($t_users = $this->db_per->GetAll($sql)) {
            return $t_users;
        } else {
            return false;
        }
    }

    function get_firms($t_ids) {
        if (is_null($this->db_per)) {
            return false;
        }

        if (empty($t_ids)) {
            return false;
        }

        $sql = "SELECT * FROM firms WHERE user_id IN (".implode(",", $t_ids).")";
        if ($firms = $this->db_per->GetAll($sql)) {
            return $firms;
        } else {
            return false;
        }
    }

}
