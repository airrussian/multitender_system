<?php

class multitender_model_quality_analyzer extends multitender_model {

    function  __construct() {
        parent::__construct();
        $this->db      = & $this->conf['dbs']['person'];
        if (is_null($this->db)) {
            trigger_error('Where DB person?');
            exit;
        }
    }

    function get_search($filter, $type, $offset=0, $count=10) {

        switch ($type) {
            case 'email':
                $sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT necessary_searches_id FROM email_necessary_searches_income WHERE income='email' LIMIT $offset, $count";
                break;
            case 'popul':
                $sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT necessary_searches_id FROM email_necessary_searches_income WHERE income<>'email' LIMIT $offset, $count";
                break;
            default:
                $sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT necessary_searches_id FROM email_necessary_searches_income LIMIT $offset, $count";
                break;
        }
        $ids = $this->db->GetCol($sql);
        $total = $this->db->GetOne("SELECT FOUND_ROWS()");
       

        $sql = "SELECT * FROM email_necessary_searches";
        if (!empty($filter)) {
            $sql.=", search ";
            $sql.= " WHERE search.id=email_necessary_searches.search_id ";
            $names = explode(' ', $filter);
            $total = count($names);
            $sql .= "AND MATCH(search.main) AGAINST('";
            foreach ($names as $n) {
                $sql .=" ".$n."* ";
            }
            $sql .= "' IN BOOLEAN MODE) ";
            $sql .= " AND ";
        } else {
            $sql.= " WHERE ";
        }
        $sql.= " email_necessary_searches.search_id IN (".implode(",",$ids).")";

        if (!$searches = $this->db->GetAll($sql)) {
            return false;
        }

        if (empty($filter)) {
            $ids= array();
            foreach ($searches as $s) {
                $ids[] = $s['search_id'];
            }

            $sql = "SELECT id, search, main FROM search WHERE id IN (".implode(",", $ids).")";
            if (!$srch = $this->db->GetAll($sql)) {
                return false;
            }

            foreach ($srch as $sch) {
                foreach ($searches as &$s) {
                    if ($sch['id']==$s['search_id']) {
                        $s['search'] = $sch['search'];
                        $s['main'] = $sch['main'];
                    }
                }
            }
        }

        $searches['total'] = $total;
        return $searches;
    }

    function get_update_searcher($ids, $dates) {

        $sql = " SELECT search_id, CAST(date_update AS DATE) as date, MAX(new_items) as new_items, MAX(total_items) as total_items ";
        $sql.= " FROM email_update_searches ";
        $sql.= "  WHERE search_id IN (".implode(",", $ids).") ";
        $sql.= "  AND CAST(date_update AS DATE) IN (".implode(",", $dates).") ";
        $sql.= " GROUP BY search_id, date ";

        if (!$searches = $this->db->GetAll($sql)) {
            return false;
        }

        return $searches;
    }

}