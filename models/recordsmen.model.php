<?php

class multitender_model_recordsmen extends multitender_model {
    
    function __construct() {
        parent::__construct();
        if (is_null($this->db_person = & $this->conf['dbs']['person'])) {
            trigger_error('Where DB person?');
            exit;
        }
    }

    public function getCommentsRecord () {
        $sql = 'SELECT COUNT(user_id) as cnt, user_id FROM comments GROUP BY user_id ORDER BY cnt DESC LIMIT 10';
        $comments = $this->db_person->GetAll($sql, array());
        return $comments;
    }

    public function getUsersByIds($ids) {
        $string = implode(',', $ids);
        $sql = 'SELECT id, name FROM users WHERE Id IN (' . $string . ')';
        $users = $this->db_person->GetAll($sql, array());
        return $users;
    }

    public function getLastSearch($date) {
        $sql = "SELECT user_id, COUNT(user_id) as cnt FROM last_search WHERE user_id IS NOT NULL";
        if ($date != NULL) {
            $sql .= " AND date>'" . $date  . "'";
        }

        $sql .= " GROUP BY user_id ORDER BY cnt DESC LIMIT 15";
        $mass = $this->db_person->GetAll($sql,array());
        return $mass;
    }

    public function getUserTenders($date) {
        $sql = "SELECT user_id, COUNT(user_id) as cnt FROM users_tenders WHERE date_purches IS NOT NULL";
        if ($date != NULL) {
            $sql .= " AND date_last_viewing>'" . $date . "'";
        }
        $sql .= " GROUP BY user_id ORDER BY cnt DESC LIMIT 10";
        $mass = $this->db_person->GetAll($sql,array());
        return $mass;
    }

    public function getEmailNews() {
        $sql = 'SELECT user_id,COUNT(user_id) as cnt FROM email_newsletter GROUP BY user_id ORDER BY cnt DESC LIMIT 10';
        $mass = $this->db_person->GetAll($sql,array());
        return $mass;
    }

    public function GetFavoriteTenders() {
        $sql = 'SELECT user_id,COUNT(user_id) as cnt FROM favorite_tenders GROUP BY user_id ORDER BY cnt DESC LIMIT 10';
        $mass = $this->db_person->GetAll($sql,array());
        return $mass;
    }

    public function GetFavoriteQueries() {
        $sql = 'SELECT user_id,COUNT(user_id) as cnt FROM favorite_queries GROUP BY user_id ORDER BY cnt DESC LIMIT 10';
        $mass = $this->db_person->GetAll($sql,array());
        return $mass;
    }

    public function GetPayments0() {
        $sql = 'SELECT user_id,COUNT(user_id) as cnt FROM payments WHERE paid = 0 GROUP BY user_id ORDER BY cnt DESC LIMIT 10';
        $mass = $this->db_person->GetAll($sql,array());
        return $mass;
    }

    public function GetPayments1() {
        $sql = 'SELECT user_id,COUNT(user_id) as cnt FROM payments WHERE paid = 1 GROUP BY user_id ORDER BY cnt DESC LIMIT 10';
        $mass = $this->db_person->GetAll($sql,array());
        return $mass;
    }

    public function GetPaymentsSumma() {
        $sql = 'SELECT user_id,SUM(summa) as sum FROM payments WHERE paid = 1 GROUP BY user_id ORDER BY sum DESC LIMIT 10';
        $mass = $this->db_person->GetAll($sql,array());
        return $mass;
    }

    public function GetUserIps() {
        $sql = 'SELECT id as user_id, name FROM users ORDER BY LENGTH(ip) DESC LIMIT 15';
        $mass = $this->db_person->GetAll($sql,array());
        return $mass;
    }

    public function GetUserAgents() {
        $sql = 'SELECT id as user_id, name FROM users ORDER BY LENGTH(user_agent) DESC LIMIT 15';
        $mass = $this->db_person->GetAll($sql,array());
        return $mass;
    }
}