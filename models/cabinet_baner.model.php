<?php

class multitender_model_cabinet_baner extends multitender_model {

    function __construct() {
        parent::__construct();
        if (is_null($this->db_person = & $this->conf['dbs']['person'])) {
            trigger_error('Where DB person?');
            exit;
        }

        if (is_null($this->db = & $this->conf['dbs']['tenders'])) {
            trigger_error('Where DB tenders?');
            exit;
        }
    }

    function show_all() {
        $sql = 'SELECT * FROM baners';
        $all = $this->db_person->GetAll($sql);
        return $all;
    }

    function get_regions() {
        $sql = 'SELECT const, name FROM geoip_region';
        $all = $this->db->GetAll($sql);
        return $all;
    }

    function rem_record($id) {
        $sql = "DELETE FROM baners WHERE id = $id";
        $this->db_person->Execute($sql);
        $sql = 'DELETE FROM clicks WHERE id = ' . $id;
        $this->db_person->Execute($sql);
        return;
    }

    function add($pathway, $region, $type, $onlymain, $link,$published,$freq) {
        $sql = "INSERT INTO `baners` (`region`, `pathway`, `published`, `type`, `link`, `onlymain`,`freq`) VALUE ('$region', '$pathway', '$published', '$type', '$link', '$onlymain','$freq')";
        $this->db_person->Execute($sql);
        return;
    }

    function edit($id, $region, $type, $onlymain, $link,$published, $freq) {
        $sql = "UPDATE `baners` SET region='$region', published='$published', type='$type', link='$link', onlymain='$onlymain', freq='$freq' WHERE id = $id";
        $this->db_person->Execute($sql);
        return;
    }

    function direct($one,$two,$three, $four) {
        $sql = "UPDATE `direct` SET text='" . mysql_real_escape_string($one) . "' WHERE id = 1";
        $this->db_person->Execute($sql);
        $sql = "UPDATE `direct` SET text='" . mysql_real_escape_string($two) . "' WHERE id = 2";
        $this->db_person->Execute($sql);
        $sql = "UPDATE `direct` SET text='" . mysql_real_escape_string($three) . "' WHERE id = 3";
        $this->db_person->Execute($sql);
        $sql = "UPDATE `direct` SET text='" . mysql_real_escape_string($four) . "' WHERE id = 4";
        $this->db_person->Execute($sql);
        return;
    }

    function getdirect() {
        $sql = 'SELECT * FROM direct';
        $all = $this->db_person->GetAll($sql);
        return $all;
    }

    function directfreq($hleft, $hright, $hwide, $vtop,$vbot, $vwide) {
        if (isset($hleft)) {
            $sql = "UPDATE `banerpos` SET freq='" . mysql_real_escape_string($hleft) . "' WHERE id = 1";
            $this->db_person->Execute($sql);
        }
        if (isset($hright)) {
            $sql = "UPDATE `banerpos` SET freq='" . mysql_real_escape_string($hright) . "' WHERE id = 2";
            $this->db_person->Execute($sql);
        }
        if (isset($hwide)) {
            $sql = "UPDATE `banerpos` SET freq='" . mysql_real_escape_string($hwide) . "' WHERE id = 0";
            $this->db_person->Execute($sql);
        }
        if (isset($vtop)) {
            $sql = "UPDATE `banerpos` SET freq='" . mysql_real_escape_string($vtop) . "' WHERE id = 4";
            $this->db_person->Execute($sql);
        }
        if (isset($vbot)) {
            $sql = "UPDATE `banerpos` SET freq='" . mysql_real_escape_string($vbot) . "' WHERE id = 5";
            $this->db_person->Execute($sql);
        }
        if (isset($vwide)) {
            $sql = "UPDATE `banerpos` SET freq='" . mysql_real_escape_string($vwide) . "' WHERE id = 3";
            $this->db_person->Execute($sql);
        }
    }

    function getdirectfreq() {
        $sql = 'SELECT * FROM banerpos';

        $all = $this->db_person->GetAll($sql);
        $temp = array();
        foreach ($all as $elem) {
            if ($elem['id'] == 0) {
                $temp['hwide'] = $elem['freq'];
            }
            if ($elem['id'] == 1) {
                $temp['hleft'] = $elem['freq'];
            }
            if ($elem['id'] == 2) {
                $temp['hright'] = $elem['freq'];
            }
            if ($elem['id'] == 3) {
                $temp['vwide'] = $elem['freq'];
            }
            if ($elem['id'] == 4) {
                $temp['vtop'] = $elem['freq'];
            }
            if ($elem['id'] == 0) {
                $temp['vbot'] = $elem['freq'];
            }
        }
        return $temp;
    }
}