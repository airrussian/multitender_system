<?php

class multitender_model_customer extends multitender_model {

    function __construct() {
        parent::__construct();
        if (is_null($this->db)) {
            trigger_error('Where DB tenders?');
            exit;
        }
    }

    function get_date_region($region_id) {
        $sql = "SELECT id, name, name_rp FROM region WHERE id = ? LIMIT 1";
        $region = $this->db->GetRow($sql, array($region_id));
        return $region;
    }

    function get_regions_customers() {
        $sql = "SELECT COUNT(*) as count, region.name, sname, region.id as id FROM region, customer WHERE customer.region_id = region.id GROUP BY customer.region_id ORDER BY region.name";
        $regcus = $this->db->GetAll($sql);
        return $regcus;
    }

    function get_customers_by($region_id, $offset=0, $rectopg=10, $filter=NULL) {
        $sql = "SELECT SQL_CALC_FOUND_ROWS id, name FROM customer WHERE region_id=? ";
        if (!empty($filter)) {
            $names = explode(' ', $filter);
            $sql .= "AND MATCH(name) AGAINST('";
            foreach ($names as $n) {
                $sql .=" +".$n."* ";
            }
            $sql .= "' IN BOOLEAN MODE) ";
        }       
        $sql .= "LIMIT $offset, $rectopg";
        $customers = $this->db->GetAll($sql, array($region_id));
        $customers['total'] = $this->db->GetOne("SELECT FOUND_ROWS()");
        return $customers;
    }

    function get_items_by_ids($ids) {
        if (empty($ids)) {
            return false;
        }
        $sql = "SELECT COUNT(*) as count, customer_id FROM item WHERE customer_id IN (".implode($ids, ",").") GROUP BY customer_id";
        $items = $this->db->GetAll($sql);
        return $items;
    }
}
