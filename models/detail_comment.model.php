<?php
class multitender_model_detail_comment extends multitender_model {

    function __construct() {
        parent::__construct();
        if (is_null($this->db_person = & $this->conf['dbs']['person'])) {
            trigger_error('Where DB person?');
            exit;
        }
    }

    function add_comment($array) {
        $sql=" INSERT INTO `comments` (`id` ,`user_id` ,`personal_flag` ,`text` ,`date_time` ,`tender_id`) VALUES (NULL , ?, ?, ?, NULL, ?)";

        if ($this->db_person->Execute($sql, $array)) {
            return true;
        } else {
            return false;
        }
    }

    function select_comments($array, $offset=0, $rectopg=10) {
        $sql = "SELECT SQL_CALC_FOUND_ROWS comments.id, comments.user_id, comments.personal_flag, comments.text, comments.date_time, users.name FROM comments INNER JOIN users ON comments.user_id = users.id WHERE comments.tender_id = ? AND (comments.personal_flag = 0 OR (comments.personal_flag = 1 AND comments.user_id = ?)) ORDER BY comments.personal_flag DESC, comments.id DESC ";
        $sql .= "LIMIT $offset, $rectopg";
        $comments = $this->db_person->GetAll($sql, $array);
        $comments['total'] = $this->db_person->GetOne("SELECT FOUND_ROWS()");
        return $comments;
    }

    function select_edited($id) {
        $sql = "SELECT user_id, personal_flag, text FROM comments WHERE id = ?";
        $comment = $this->db_person->GetRow($sql, array((int)$id));
        return $comment;
    }

    function update_edited($array) {
        $sql = "UPDATE comments SET text = ? WHERE id = ? AND user_id = ?";
        $comment = $this->db_person->GetRow($sql, $array);
    }

    function del_rec($del_key, $user_id) {
        $sql = "DELETE FROM comments WHERE id = ? AND user_id = ? AND personal_flag = 1";
        $this->db_person->Execute($sql, array((int)$del_key, (int)$user_id));
        return;
    }

}
