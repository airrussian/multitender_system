<?php
class multitender_model_feedback extends multitender_model {

    function __construct() {
        parent::__construct();
        if (is_null($this->db_person = & $this->conf['dbs']['person'])) {
            trigger_error('Where DB person?');
            exit;
        }
    }

    function get_result () {
        $sql = "SELECT * FROM `feedback`";
        $result = $this->db_person->GetAll($sql,array());
        return $result;
    }

    function add_record($array) {
        $sql=" INSERT INTO `feedback` (`id` ,`ip` ,`wha_are_you` ,`why_dont_use` ,`other_services` ,`interest` ,`other_region` ,`why_dont_use_text`, `other_services_text`)
VALUES (NULL , ?, ?, ?, ?, ?, ?, ?, ?) ";

        if ($this->db_person->Execute($sql, $array)) {
            return true;
        } else {
            return false;
        }
    }
}