<?php

class multitender_model_list_sites extends multitender_model {

    function  __construct() {
        parent::__construct();
        if (is_null($this->db)) {
            trigger_error('Where DB tenders?');
            exit;
        }
    }

    function get_item_count_sites() {
        $sql = "SELECT site_id, count(site_id) as total FROM item WHERE site_id <> 990001 GROUP BY site_id";
        return $this->db->GetAll($sql);
    }

    function get_sites($ids, $offset, $count) {
        $sql = "SELECT id, site.desc as name, region_id, url FROM site ";
        $sql.= "WHERE id IN (".implode($ids, ",").") ";
        $sql.= "ORDER BY region_id ";
        $sql.= "LIMIT $offset, $count";
        return $this->db->GetAll($sql);
    }

    function get_sites_count($ids) {
        $sql = "SELECT count(*) FROM site WHERE id IN (".implode($ids, ",").")";
        return $this->db->GetOne($sql);
    }

    function get_regions() {
        $sql = "SELECT id, sname FROM region";
        return $this->db->GetAll($sql);
    }
    
}
