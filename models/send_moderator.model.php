<?php

class multitender_model_send_moderator extends multitender_model {

    function  __construct() {
        parent::__construct();
        $this->db      = & $this->conf['dbs']['person'];
        if (is_null($this->db)) {
            trigger_error('Where DB person?');
            exit;
        }
    }

    function bad_item($user_id, $item_id, $comment) {

        $sql = "INSERT INTO errors_tenders(user_id, item_id, comment) VALUES(?,?,?)";

        if ($this->db->Execute($sql, array($user_id, $item_id, $comment))) {
            return true;
        } else {
            return false;
        }
    }

}
