<?php
/**
 * Получение региона по IP
 */

class multitender_model_geoip extends multitender_model {

    public $number = 5;
    private $sphinx_big_number = 1000000000;
    private $cache_ip;

    function  __construct() {
        parent::__construct();
        $this->sphinx = ADONewConnection($this->conf['db_conf']['sphinx']['dsn']);
    }
    /**
     * @param  string ip
     * @return int long ip
     */
    public function get_ip( $ip = null ) {
        if (is_null($ip) && isset($this->cache_ip) ) {
            return $this->cache_ip;
        }

        if (is_null($ip)) {
            $ip = $_SERVER['REMOTE_ADDR'];
            // FIXME переписать на обработку массива возможных HTTP_
            // в порядке приоритета
            if (isset($_SERVER['HTTP_X_REAL_IP'])) {
                if ( $ip !==  $_SERVER['HTTP_X_REAL_IP'] ) {
                    $ip = array_shift(preg_split('/,|\s/', $_SERVER['HTTP_X_REAL_IP']));
                }
            }
            //[HTTP_X_FORWARDED_FOR] => 93.159.243.202, 217.107.219.180
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                if ( $ip !==  $_SERVER['HTTP_X_FORWARDED_FOR'] ) {
                    $ip = array_shift(preg_split('/,|\s/', $_SERVER['HTTP_X_FORWARDED_FOR']));
                }
            }
            $long = ip2long($ip);
            $long = ($long > 1 ) ? $long : false;
            $this->cache_ip = $long;
        } else {
            $long = ip2long($ip);
        }
        return ($long > 1 ) ? $long : false;
    }

    /**
     * @param  string ip
     * @return int long ip
     */
    public function get_region( $ip = null ) {
        if (!($long = $this->get_ip($ip))) {
            return false;
        }
        $ar_geoip = $this->db->GetRow("SELECT * FROM geoip WHERE start <= $long AND end >= $long LIMIT 1");
        if( ! $ar_geoip ) {
            return false;
        }
        $return = (int) $ar_geoip['const'];
        return $return ? $return : false;
    }

    /**
     * @param <type> $ip
     * @return array
     */
    public function get_region_info( $id ) {
	$id = (int) $id;

        $data      = multitender_model_data_common::singleton()->get_data();

        if (isset($data['region'][$id]) ) {
            $info = $data['region'][$id];
        } else {
            $info = array(
                'name'         => 'Россия',
                'name_rp'      => 'России',
                'ya_widget_id' => '12082',
            );
        }

        return $info ? $info : false;
    }

    /**
     * @param int $id регион
     * @return array
     */
    public function get_last_items( $id ) {
        $where = '';
        if ( $id ) {
            $where = "WHERE region_id = $id";
        }
        
//        $last = $this->db->GetArray(
//            "SELECT item.id, item.name, item.date_add, item.date_conf, item.date_publication, item.date_end, item.price " .
//            "FROM item $where ORDER BY id DESC LIMIT $this->number" );

        $sql = "SELECT * FROM item_old, item_fresh, item_delta $where ORDER BY id ASC LIMIT 0, ?";
        $last = $this->sphinx->GetAll($sql, $this->number);
        foreach ($last as &$v) {
            $v['id'] = $this->sphinx_big_number - $v['id']; // in Sphinx (M-id)
            $v['name'] = $this->getItemName($v['id']);
            $v['link'] = $this->conf['pref']['link_detail'] . $v['id'];
        }

        return $last ? $last : false;
    }
    private function getItemName($id) {
        return $this->db->GetOne("SELECT name FROM item WHERE id=$id");
    }
}
