<?php

class multitender_model_change_tenders extends multitender_model {

    function  __construct() {
        parent::__construct();
        $this->db      = & $this->conf['dbs']['person'];
        $this->db1     = & $this->conf['dbs']['tenders'];
    }

    function add_fav_tender($user_id,$item_id) {
        if (!$user_id) {
            return false;
            exit;
        }

        if (is_null($this->db)) {
            return false;
        }
        //if ($id=$this->tender_to_favorite($user_id, $item_id)) {
        //    return $id;
        //    exit;
        //}

        $user_id = (int) $user_id;
        $item_id = (int) $item_id;

        $sql="INSERT IGNORE INTO favorite_tenders(user_id, items_id) VALUES(?, ?)";
        if ($this->db->Execute($sql, array($user_id, $item_id))) {
            return $this->db->Insert_ID();
        } else {
            return false;
        }
    }

    function del_fav_tender($user_id, $item_id) {
        if (is_null($this->db)) {
            return false;
        }

        $user_id = (int) $user_id;
        $item_id = (int) $item_id;

        $sql="DELETE FROM favorite_tenders WHERE user_id=? AND items_id=?";
        return $this->db->Execute($sql, array($user_id, $item_id));
    }

    function get_count_fav_tenders($user_id) {
        if (is_null($this->db)) {
            return false;
        }
        $sql="SELECT count(*) FROM favorite_tenders WHERE user_id=?";
        $count=$this->db->GetOne($sql, array($user_id));
        return $count;
    }

    function get_fav_tenders($user_id, $offset=0, $count=10) {
        if (is_null($this->db)) {
            return false;
        }

        $user_id = (int) $user_id;
        $offset  = (int) $offset;
        $count   = (int) $count;

        $sql="SELECT items_id FROM favorite_tenders WHERE user_id=? ORDER BY id DESC LIMIT ?, ?";
        $ids=$this->db->GetCol($sql, array($user_id, $offset, $count));
        return $ids;
    }

    function tender_to_favorite($user_id, $item_id) {
        if (is_null($this->db)) {
            return false;
        }

        $user_id = (int) $user_id;
        $item_id = (int) $item_id;

        $sql="SELECT id FROM favorite_tenders WHERE user_id=? AND items_id=?";
        if ($id=$this->db->GetOne($sql, array($user_id, $item_id))) {
            return $id;
        } else {
            return false;
        }
    }

    function tenders_to_favorite($user_id, $tenders) {
        if (is_null($this->db)) {
            return false;
        }

        $user_id = (int) $user_id;

        if (empty($tenders)) { return false; }

        $sql="SELECT items_id FROM favorite_tenders WHERE user_id=? AND items_id IN (".implode($tenders,",").") LIMIT ".count($tenders);
        if ($ft=$this->db->GetCol($sql,array($user_id))) {
            return $ft;
        } else {
            return false;
        }
    }

    function get_title($item_ids) {
        if (is_null($this->db1)) {
            return false;
        }

        if ( empty($item_ids) ) {
            return array();
        }

        $sql="SELECT id, name, date_publication, date_end, type_id, region_id, price FROM item WHERE id IN (".implode($item_ids,",").") ORDER BY FIELD(id,".implode($item_ids,",").")";
        $names=$this->db1->GetAll($sql);
        // Сортируем в порядке добавления 
        return $names;
    }

}