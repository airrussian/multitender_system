<?php

class multitender_model_email_newsletter extends multitender_model {

    function __construct() {
        parent::__construct();
        $this->db = & $this->conf['dbs']['person'];
        if (is_null($this->db)) {
            trigger_error('Where DB person?');
            exit;
        }
    }

    function check_newsletter($user_id, $search_id) {
        if (!$user_id || !$search_id) {
            return false;
        }

        $user_id = (int) $user_id;
        $search_id = (int) $search_id;                

        $sql = "SELECT * FROM email_subscribe WHERE user_id=? AND search_id=?";
        
        $row = $this->db->GetRow($sql, array($user_id, $search_id));
        if (!empty($row)) {
            return true;
        } else {
            return false;
        }
    }

    function add_newsletter($user_id, $search_id, $freq = 1, $limit = 10) {
        if (!$user_id || !$search_id) {
            return false;
        }

        $user_id = (int) $user_id;
        $search_id = (int) $search_id;

        $active = $this->get_status($user_id);

        if (!$this->check_newsletter($user_id, $search_id)) {
            $sql = "INSERT INTO email_subscribe(`user_id`, `search_id`, `freq`, `limit`, `active`, `dateadd`) VALUES(?, ?, ?, ?, ?, ?)";
            if ($this->db->Execute($sql, array($user_id, $search_id, $freq, $limit, $active, date('Y-m-d H:i:s')))) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    function del_newsletter($user_id, $search_id) {

        if (!$user_id || !$search_id) {
            return false;
        }

        $user_id = (int) $user_id;
        $search_id = (int) $search_id;

        $sql = "DELETE FROM email_subscribe WHERE user_id=? AND search_id=?";
        return $this->db->Execute($sql, array($user_id, $search_id));
    }

    function get_list_newsletter($user_id, $offset = 0, $count = 30) {

        $user_id = (int) $user_id;
        if (!$user_id) {
            return false;
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM email_subscribe WHERE user_id=? LIMIT ?,?";
        if ($list = $this->db->GetAll($sql, array($user_id, $offset, $count))) {
            $total = $this->db->GetArray("SELECT FOUND_ROWS()");
            $total = $total[0][0];
            $list['total'] = $total;
            return $list;
        } else {
            return false;
        }
    }

    function change_freq($freq, $user_id, $search_id) {
        if (!$user_id) {
            return false;
        }

        $sql = "UPDATE email_subscribe SET freq=? WHERE user_id=? AND search_id=?";
        if (!$this->db->Execute($sql, array($freq, $user_id, $search_id))) {
            return false;
        }
        return true;
    }

    function change_limit($limit, $user_id, $search_id) {
        if (!$user_id || !$search_id) {
            return false;
        }

        $sql = "UPDATE email_subscribe SET `limit`=? WHERE user_id=? AND search_id=?";
        if (!$this->db->Execute($sql, array($limit, $user_id, $search_id))) {
            return false;
        }
        return true;
    }

    function get_freq($user_id, $search_id) {
        if (!$user_id) {
            return false;
        }

        if ($search_id) {
            $sql = "SELECT get_freq FROM email_subscribe WHERE user_id=? AND search_id=?";
            return $this->db->GetOne($sql, array($user_id, $search_id));
        }
    }

    function change_status($user_id, $active) {
        $user_id = (int) $user_id;
        if (!$user_id) {
            return false;
        }

        if (!$this->db->Execute("UPDATE email_subscribe SET active=$active WHERE user_id=$user_id")) {
            return false;
        }
        return true;
    }

    function get_status($user_id) {
        $user_id = (int) $user_id;
        if (!$user_id) {
            return false;
        }

        $active = $this->db->GetOne("SELECT active FROM email_subscribe WHERE user_id=$user_id");
        if (is_numeric($active)) {
            return $active;
        } else {
            return 1;
        }
    }

    function set_time_newsletter($user_id, $time) {
        $user_id = (int) $user_id;
        $time = (int) $time;

        $sql = "UPDATE users SET time_send_newsletter=? WHERE id=? LIMIT 1";
        return $this->db->Execute($sql, array($time, $user_id));
    }

    function get_time_newsletter($user_id) {
        $user_id = (int) $user_id;

        $sql = "SELECT time_send_newsletter FROM users WHERE id=?";
        if ($id = $this->db->GetOne($sql, array($user_id))) {
            return $id;
        } else {
            return false;
        }
    }

}
