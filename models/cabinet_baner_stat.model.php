<?php

class multitender_model_cabinet_baner_stat extends multitender_model {

    function __construct() {
        parent::__construct();
        if (is_null($this->db_person = & $this->conf['dbs']['person'])) {
            trigger_error('Where DB person?');
            exit;
        }

        if (is_null($this->db = & $this->conf['dbs']['tenders'])) {
            trigger_error('Where DB tenders?');
            exit;
        }
    }

    function getStat($id) {
        $sql = 'SELECT COUNT(*) as cnt_click, date FROM clicks WHERE id = ' . $id . ' GROUP BY date ORDER BY date ASC';
        $list = $this->db_person->GetAll($sql);
        $sql = 'SELECT COUNT(*) as cnt_shows, date FROM shows WHERE id = ' . $id . ' GROUP BY date ORDER BY date ASC';
        $list2 = $this->db_person->GetAll($sql);
        $res = array();
        for($i=0;$i<count($list);$i++) {
            for($j=0;$j<count($list2);$j++) {
                if ($list[$i]['date'] == $list2[$j]['date'] && $list[$i]['date'] != -1) {
                    $res[] = array('date'=>$list[$i]['date'], 'cnt_click' => $list[$i]['cnt_click'], 'cnt_shows'=> $list2[$j]['cnt_shows']);
                    $list[$i]['date'] = -1;
                    $list2[$j]['date'] = -1;
                }
            }
        }
        $h = count($list);
        for($i=0;$i<$h;$i++) {
            if ($list[$i]['date']==-1) {
                unset($list[$i]);
            }
        }
        $h = count($list2);
        for($i=0;$i<$h;$i++) {
            if ($list2[$i]['date']==-1) {
                unset($list2[$i]);
            }
        }

        foreach($list as $el) {
            $res[] = array('date'=>$el['date'], 'cnt_click' => $el['cnt_click'], 'cnt_shows'=> $el['cnt_click']);
        }

        foreach($list2 as $el) {
            $res[] = array('date'=>$el['date'], 'cnt_click' => 0, 'cnt_shows'=> $el['cnt_shows']);
        }

        $temp = array();
        foreach ($res as $el) {
            $temp[] = $el['date'];
        }

        array_multisort($temp, SORT_ASC, $res);

        return $res;
    }
}