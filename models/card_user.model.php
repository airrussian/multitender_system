<?php

class multitender_model_card_user extends multitender_model {

    function  __construct() {
        parent::__construct();
        $this->db_ten = & $this->conf['dbs']['tenders'];
        $this->db_per = & $this->conf['dbs']['person'];
        $this->db_joo = & $this->conf['dbs']['joomla'];
        $this->db_joo = ADONewConnection($this->conf['db_conf']['joomla']['dsn']);
        $this->db_joo->Execute('set names utf8');
    }

    function get_user($user_id) {
        $sql = "SELECT * FROM users WHERE Id=?";

        if ($u = $this->db_per->GetRow($sql, $user_id)) {
            return $u;
        } else {
            return false;
        }
    }


    function get_firm($user_id) {
        $sql = "SELECT * FROM firms WHERE user_id=? LIMIT 1";

        if ($rows = $this->db_per->GetRow($sql, array($user_id))) {
            return $rows;
        } else {
            return false;
        }
    }

    function get_user_joomla($user_id) {
        $sql = "SELECT joomla_id FROM users WHERE id=?";
        if (!$joomla_id = $this->db_per->GetOne($sql, array($user_id))) {
            return false;
        }
        $sql = "SELECT * FROM ten_users WHERE id=? LIMIT 1";
        if ($us = $this->db_joo->GetRow($sql, array($joomla_id))) {
            return $us;
        } else {
            return false;
        }
    }

    function get_access($user_id) {
        $sql = "SELECT * FROM access WHERE user_id=?";
        if ($access = $this->db_per->GetRow($sql, array($user_id))) {
            return $access;
        } else {
            return false;
        }
    }

    function get_count_payments($user_id, $all=true) {

        $sql = "SELECT count(*) as count, SUM(summa) as summa FROM payments WHERE user_id=?";
        if (!$all) {
            $sql.=" AND paid=1 ";
        }
        $sql.=" GROUP BY user_id";
        if ($cp = $this->db_per->GetRow($sql, $user_id)) {
            return $cp;
        } else {
            return array('count' => 0 , 'summa' => 0);
        }
    }

    function get_tenders($user_id) {

        $tenders = array();

        $sql = "SELECT count(*) FROM users_tenders WHERE user_id=?";
        if ($ct = $this->db_per->GetOne($sql, $user_id)) {
            $tenders['count'] = $ct;
        }
        $sql = "SELECT count(*) FROM users_tenders WHERE user_id=? AND kind_rates_id=0";
        if ($ck = $this->db_per->GetOne($sql, $user_id)) {
            $tenders['buy'] = $ct-$ck;
        }
        
        return $tenders;
    }

    function get_favorites($user_id) {
        $fav = array();
        $sql = "SELECT count(*) FROM favorite_tenders WHERE user_id=?";
        if ($ft = $this->db_per->GetOne($sql, $user_id)) {
            $fav['tenders'] = $ft;
        } else {
            $fav['tenders'] = 0;
        }
        $sql = "SELECT count(*) FROM favorite_queries WHERE user_id=?";
        if ($fq = $this->db_per->GetOne($sql, $user_id)) {
            $fav['search'] = $fq;
        } else {
            $fav['search'] = 0;
        }

        return $fav;
    }

    function get_count_search($user_id) {
        $sql = "SELECT count(*) FROM last_search WHERE user_id=?";
        if ($cs = $this->db_per->GetOne($sql, $user_id)) {
            return $cs;
        } else {
            return 0;
        }
    }

    function get_email_subscribe_count($user_id) {
        $sql = "SELECT count(*) FROM email_subscribe WHERE user_id=?";
        if ($cen = $this->db_per->GetOne($sql, $user_id)) {
            return $cen;
        } else {
            return 0;
        }
    }
    
    function get_email_subscribe_last_send($user_id) {
        $sql = "SELECT datetime FROM email_subscribe WHERE user_id=? AND datetime IS NOT NULL ORDER BY datetime DESC";
        if ($datetime = $this->db_per->GetOne($sql, $user_id)) {
            return $datetime;
        } else {
            return false;
        }
    }

    function get_email_subscribe_list($user_id) {
        $sql = "SELECT * FROM email_subscribe WHERE user_id=? AND active=1";
        $list = $this->db_per->GetAll($sql, $user_id);

        if (empty($list)) {
            return false;
        }

        foreach ($list as $row) {
            if ($row['last_item_id']) {
                $tenders[] = $row['last_item_id'];
            }
            $searchs[] = $row['search_id'];
        }

        $sql = "SELECT * FROM search WHERE id IN (".implode(",", $searchs).")";
        $searchs = $this->db_per->GetAll($sql);
        $tmp = array();
        foreach ($searchs as $row) { $tmp[$row['id']] = $row; }
        $searchs = $tmp;

        if (!empty($tendes)) {
            $sql = "SELECT * FROM item WHERE id IN (".implode(",", $tenders).")";
            $tenders = $this->db_ten->GetAll($sql);
            $tmp = array();
            foreach ($tenders as $row) { $tmp[$row['id']] = $row; }
            $tenders = $tmp;

            foreach ($list as &$row) {
                if ($row['last_item_id'] && isset($tenders[$row['last_item_id']])) {
                    $row['last_tender'] = $tenders[$row['last_item_id']];
                }
                if (isset($searchs[$row['search_id']])) {
                    $s_obj = new multitender_model_search();
                    $s_obj->SetSearch(unserialize($searchs[$row['search_id']]['search']));
                    $row['search'] = $searchs[$row['search_id']];
                    $row['search']['info'] = $s_obj->to_info();
                }
            }
        }

        return $list;
    }

    function get_last_query($user_id, $count=5) {
        $sql = "SELECT * FROM last_search WHERE user_id=? ORDER BY date DESC LIMIT ?";
        if ($query = $this->db_per->GetAll($sql, array($user_id, $count))) {
            
            foreach ($query as $q) {
                $ids[] = $q['search_id'];
            }
            $search = new multitender_model_list_queries();
            $s_obj =  new multitender_model_search();
            $search = $search->getList($ids);
            foreach ($query as &$q) {
                foreach ($search as $s) {
                    if ($q['search_id']==$s['id']) {
                        $q['search'] = $s;                        
                        $s_obj->SetSearch( unserialize($s['search']));
                        $q['search']['info'] = $s_obj->to_info();
                    }
                }
            }
            return $query;
        } else {
            return false;
        }
    }

}