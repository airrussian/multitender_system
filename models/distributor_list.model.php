<?php

class multitender_model_distributor_list extends multitender_model {

    function __construct() {
        parent::__construct();
        if (is_null($this->db)) {
            trigger_error('Where DB tenders?');
            exit;
        }
    }

    function get_graph_info($id) {
        if (empty($id)) {
            return false;
        }
        $sql = "SELECT id_contract FROM link_contracts_with_distributors WHERE id_distributor = ?";
        $contr = $this->db->GetAll($sql, array((int)$id));
        $list = array();
        foreach ($contr as $ids) {
            $list[] = $ids['id_contract'];
        }
        $array=implode($list, ",");
        if (strlen($array)>0) {
            $sql = "SELECT SUM(contract_price) AS dohod, YEAR(contract_date) AS year_gr FROM contract WHERE id IN (".$array.") ";
            $sql .= "GROUP BY year_gr";
            $contracts = $this->db->GetAll($sql, array());
            return $contracts;
        } else {
            return FALSE;
        }
    }

    function get_date_region($region_id) {
        $sql = "SELECT id, name, name_rp FROM region WHERE id = ? ORDER BY NULL LIMIT 1";
        $region = $this->db->GetRow($sql, array((int)$region_id));
        return $region;
    }

    function get_regions_distributors() {
        $sql = "SELECT COUNT(*) as count, region.name, sname, region.id as id FROM region, distributor WHERE distributor.region_id = region.id GROUP BY distributor.region_id ORDER BY region.name";
        $regcus = $this->db->GetAll($sql);
        return $regcus;
    }

    function get_distributors_list($region_id, $offset=0, $rectopg=10, $filter=NULL, $complaints) {
        $sql = "SELECT SQL_CALC_FOUND_ROWS id, Name, INN, qu_of_complaints FROM distributor WHERE region_id=? ";
        if ($complaints == 'only') {
            $sql .= "AND qu_of_complaints > 0 ";
        }
        if (!empty($filter)) {
            $names = explode(' ', $filter);
            $sql .= "AND (MATCH(Name) AGAINST('";
            foreach ($names as $n) {
                $sql .=" +".$n."* ";
            }
            $sql .= "' IN BOOLEAN MODE) OR MATCH(INN) AGAINST('";
            foreach ($names as $n) {
                $sql .=" +".$n."* ";
            }
            $sql .= "' IN BOOLEAN MODE)) ORDER BY NULL ";
        }
        $sql .= "LIMIT $offset, $rectopg";
        $customers = $this->db->GetAll($sql, array((int)$region_id));
        $customers['total'] = $this->db->GetOne("SELECT FOUND_ROWS()");
        return $customers;
    }

    function get_items_by_ids($ids) {
        if (empty($ids)) {
            return false;
        }
        $sql = "SELECT COUNT(*) as count, id_distributor FROM link_contracts_with_distributors WHERE id_distributor IN (".implode($ids, ",").") GROUP BY id_distributor ORDER BY NULL ";
        $items = $this->db->GetAll($sql);
        return $items;
    }

    function get_distributor_info_by_id($id) {
        if (empty($id)) {
            return false;
        }
        $sql = "SELECT Name, INN, KPP, Address, region_id, qu_of_complaints FROM distributor WHERE distributor.id = ? ORDER BY NULL";
        $info = $this->db->GetRow($sql,array((int)$id));
        return $info;
    }

    function get_region_of_distributor($region_id) {
        if (empty($region_id)) {
            return false;
        }
        $sql = "SELECT name, name_rp FROM region WHERE region.id = ? ORDER BY NULL";
        $region = $this->db->GetRow($sql, array((int)$region_id));
        return $region;
    }

    function get_contracts_of_distributor($id, $offset=0, $rectopg=10) { //оптимизировать!!!!
        if (empty($id)) {
            return false;
        }
        $sql = "SELECT id_contract FROM link_contracts_with_distributors WHERE id_distributor = ? ORDER BY NULL";
        $contr = $this->db->GetAll($sql, array((int)$id));
        $list = array();
        foreach ($contr as $ids) {
            $list[] = $ids['id_contract'];
        }

        $array=implode($list, ",");
        if (strlen($array)>0) {
            $sql = "SELECT SQL_CALC_FOUND_ROWS customer_inn, customer_kpp, execution_type, contract_date, contract_end_date, customer_name, id, contract_price FROM contract WHERE id IN (".$array.") ORDER BY NULL ";
        } else {
            $sql = "SELECT SQL_CALC_FOUND_ROWS customer_inn, customer_kpp, execution_type, id, contract_date, contract_end_date, customer_name, contract_price FROM contract WHERE id IN ('') ORDER BY NULL ";
        }
        $sql .= "LIMIT $offset, $rectopg";
        $contracts = $this->db->GetAll($sql, array());
        $contracts['count'] = $this->db->GetOne("SELECT FOUND_ROWS()");
        return $contracts;
    }

    function get_contract_info($id) {
        if (empty($id)) {
            return false;
        }
        $sql = "SELECT id_in_source, execution_type, customer_name, customer_inn, customer_kpp, contract_date, contract_end_date, contract_price FROM contract WHERE id = ? ORDER BY NULL";
        $info = $this->db->GetRow($sql,array((int)$id));
        return $info;
    }

    function get_goods($contract_id) {
        if (empty($contract_id)) {
            return false;
        }
        $sql = "SELECT SQL_CALC_FOUND_ROWS name, price, count FROM goods WHERE ctr_id_in_source = ? ORDER BY NULL";
        $info = $this->db->GetAll($sql,array((int)$contract_id));
        $info['count'] = $this->db->GetOne("SELECT FOUND_ROWS()");
        return $info;
    }

    function get_complaints_of_distributor($id) {
        if (empty($id)) {
            return false;
        }
        $sql = "SELECT SQL_CALC_FOUND_ROWS id, Subject_of_Contract, Date_of_Auction_Result, Sum, Date_of_execution FROM complaint WHERE id_of_Supplier = ? ORDER BY NULL";
        $list = $this->db->GetAll($sql, array((int)$id));
        $list['count'] = $this->db->GetOne('SELECT FOUND_ROWS()');
        return $list;
    }

    function get_complaint_info_by_distributor_id($id) {
        if (empty($id)) {
            return false;
        }
        $sql = "SELECT Register_Number, Date_of_Inclusion, Subject_of_Contract, Date_of_Auction_Result, Sum, Date_of_execution, Org_which_included, link, Cause_of_Inclusion FROM complaint WHERE id = ? ORDER BY NULL";
        $info = $this->db->GetRow($sql, array((int)$id));
        return $info;
    }
}