<?php

class multitender_model_my_comments extends multitender_model {

    function __construct() {
        parent::__construct();
        if (is_null($this->db_person = & $this->conf['dbs']['person'])) {
            trigger_error('Where DB person?');
            exit;
        }
    }

    function select_comments($offset=0, $rectopg=10, $user_id, $filter=NULL,$flag=1) {
        $sql = "SELECT SQL_CALC_FOUND_ROWS comments.id, comments.personal_flag, comments.text, comments.date_time, comments.tender_id FROM comments WHERE comments.user_id = ? ";
        if (empty($filter)) {
            switch ($flag)
            {
                case 1:
                    break;
                case 2:
                    $sql .= "AND comments.personal_flag=0 ";
                    break;
                case 3:
                    $sql .= "AND comments.personal_flag=1 ";
                    break;
                default:
                    break;
            }
        }
        if (!empty($filter)) {
            $names = explode(' ', $filter);
            $sql .= "AND (MATCH(text) AGAINST('";
            foreach ($names as $n) {
                $sql .=" +".$n."* ";
            }
           
            $sql .= "' IN BOOLEAN MODE))";
        }
        $sql .= " ORDER BY comments.id DESC LIMIT $offset, $rectopg";
        $comments = $this->db_person->GetAll($sql, array((int)$user_id));
        $comments['total'] = $this->db_person->GetOne("SELECT FOUND_ROWS()");
        return $comments;
    }

    function update_edited($array) {
        $sql = "UPDATE comments SET text = ? WHERE id = ? AND user_id = ?";
        $comment = $this->db_person->GetRow($sql, $array);
    }

    function rem_comment($array) {
        $sql = "DELETE FROM comments WHERE id = ? AND user_id = ? AND personal_flag = 1";
        $this->db_person->Execute($sql, $array);
    }
}