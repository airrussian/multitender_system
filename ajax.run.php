<?php
/**
 * Запуск произвольного экшена по GET без Joomla
 * подключите этот файл в корне файле /ajax.php
 *
 * $_GET:
 * uid  - user id, если есть
 * uck  - md5 for check, нужна, если есть uid
 * ...  - прочие проивольные параметры
 */

// FIXME проверка на отправленные заголовки
@header('Content-Type: text/html; charset=utf-8');

//sleep(1);

require_once dirname(__FILE__) . "/main.php";
$conf = & $GLOBALS['tenders']['conf'];


$uid = null;

if (!empty($_GET['uid'])) {
    $uid = (int)$_GET['uid'];
    if (empty($uid)) {
        trigger_error('Wrong uid');
        exit;
    }
    if ( tc_ajax_solt($uid) !== $_GET['uck']) {
        trigger_error('Wrong uck');
        exit;
    }
}

if ( isset($_COOKIE[TC_USER_ID]) ) {
    // FIXME: undefine var
    if (preg_match("#(\d+)z(\S+)#", $_COOKIE[TC_USER_ID], $tcuserid)) {
        $uid = (int)$tcuserid[1];
        if (tc_ajax_solt($uid) !== $tcuserid[2]) {
            setcookie(TC_USER_ID, FALSE, time()-3600, '/');
            trigger_error('Wrong cookie');
            exit;
        }
    } else {
        setcookie(TC_USER_ID, FALSE, time()-3600, '/');
        trigger_error('Wrong cookie');
        exit;
    }
}

// FIXME возможно уже устарело
if ( ! $uid ) {
    $GLOBALS['tenders']['conf']['full']  = false;
} else {
    $GLOBALS['tenders']['conf']['full']  = true;
}

// FIXME Доработать, объеденить каким-либо с joomla

if ( $uid ) {
    $GLOBALS['tenders']['conf']['user']['joomla_id'] = null;
    $GLOBALS['tenders']['conf']['user']['id']        = $uid;
    $GLOBALS['tenders']['conf']['user']['name']      = '';
    $GLOBALS['tenders']['conf']['user']['right']     = 1;
    $GLOBALS['tenders']['conf']['user']['type']      = 'user';
    $GLOBALS['tenders']['conf']['user']['last_visit']= null;
}
else {
    $GLOBALS['tenders']['conf']['user']['joomla_id'] = null;
    $GLOBALS['tenders']['conf']['user']['id']        = 0;
    $GLOBALS['tenders']['conf']['user']['name']      = 'anonym';
    $GLOBALS['tenders']['conf']['user']['right']     = 0;
    $GLOBALS['tenders']['conf']['user']['type']      = 'anonym';
    $GLOBALS['tenders']['conf']['user']['last_visit']= null;
    tc_try_set_bots();
}

// FIXME существенно переработать
tenders_run_action("users");

$action = @$_GET['action'] ? $_GET['action'] : 'view';
$REQUEST = $_SERVER['REQUEST_URI'];

if(preg_match("#detail/(\d+)/?#", $REQUEST, $m)) {
    $action = 'detail';
    $_GET['id'] = $m[1];
}

$ans = tenders_run_action($action);
if (!isset($_GET['ajax'])) {
    if (empty($_SERVER['HTTP_REFERER'])) {
        header("Location: " . $conf['pref']['link_base_http']);
    } else {
        header("Location: " . $_SERVER['HTTP_REFERER']);
    }
} else {
    echo $ans;
}
