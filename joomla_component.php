<?php
/**
 * Joomla! 1.5 component Tenders
 *
 * @version $Id: tenders.php 2009-10-07 00:21:56 svn $
 * @author Paul
 * @package Joomla
 * @subpackage Tenders
 * @license GNU/GPL
 *
 *
 *
 * This component file was created using the Joomla Component Creator by Not Web Design
 * http://www.notwebdesign.com/joomla_component_creator/
 *
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

require_once dirname(__FILE__) . DS. 'joomla.php';

if ( isset($_GET['action']) ) {
    $action = $_GET['action'];
}

// /tenders/detail/111
if(preg_match("#detail/(\d+)/?#", $_SERVER['REQUEST_URI'], $m)) {
    $action = 'detail';
    $_GET['id'] = $m[1];
}

// в файле лежат ЧПУ для каждого региона
include 'regions.php';
if (preg_match('~\/tenders\/(.*)~i', $_SERVER['REQUEST_URI'], $r)) {
    if ($raction = array_search($r[1], $regions_chpu)) {
        $action = 'region';
        DEFINE('REGION_CHPU_ID', $raction);
    }
}

if ( empty($action) && preg_match('#^/\w+/(\w+)#i', $_SERVER['REQUEST_URI'], $m)) {
    $action = $m[1];
}

if ( empty($action) ) {
    $action = 'view';
}

echo tenders_run_action($action);

$params   = &$GLOBALS['mainframe']->getParams();
$document = &JFactory::getDocument();

$params->set('page_description',  'Система поиска госзаказов, госзакупок Multitender.ru' );

$document->setDescription( $params->get('page_description' ) );

if( ! empty( $GLOBALS['tenders']['conf']['page_title'] ) ) {

    $params->set('page_title',  $params->get( 'page_title' ) . ' - ' . $GLOBALS['tenders']['conf']['page_title'] );
    //$params->set('page_description',  $params->get( 'page_description' ) . '. ' . htmlspecialchars($GLOBALS['tenders']['conf']['page_title']) );
    $document->setTitle( $params->get( 'page_title' ) );
    //$document->setDescription( $params->get('page_description' ) );
}

if (!empty ($GLOBALS['tenders']['conf']['keywords'])) {
    $params->set('page_keywords', $GLOBALS['tenders']['conf']['keywords']);
    $document->setMetaData('keywords', $params->get('page_keywords'));

}

if (!empty ($GLOBALS['tenders']['conf']['description'])) {
    $params->set('page_description', $GLOBALS['tenders']['conf']['description']);
    $document->setDescription($params->get('page_description'));

}

if( ! empty( $GLOBALS['tenders']['conf']['page']['description'] ) ) {
    $document->setDescription( $GLOBALS['tenders']['conf']['page']['description'] );
}

if( ! empty( $GLOBALS['tenders']['conf']['page']['title'] ) ) {
    $document->setTitle( $GLOBALS['tenders']['conf']['page']['title'] );
}

if( ! empty( $GLOBALS['tenders']['conf']['page_joomla_links'] ) ) {
    $new = array();
    foreach ($GLOBALS['tenders']['conf']['page_joomla_links'] as $v) {
        $new['links'][] = $v;
    }
    $document->setHeadData($new);
}
