<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * DRAFT
 */
class My_Memcache extends Memcache {
    private $MyConnection;
    private $MyTime;
    private $MyTimeUp;
    private $MyTimeDown;
    private $MyRandKoof;

    /**
     *
     * @param int $time
     * @param int $rand_multi in percent
     */
    public function SetRand($time, $rand_multi) {
        $time       = (int) $time;
        $rand_multi = (int) $rand_multi;
        if ($time < 5) {
            $time = 5;
        }
        $this->MyTime = $time;
        $this->MyRandKoof = $rand_multi;
        return true;
    }

    private function my_mt_rand($val){
        return mt_rand($this->MyTimeDown,$this->MyTimeUp);
    }

    public function connect($key, $val) {
        $this->MyConnection = parent::connect($key, $val);
        return $this->MyConnection;
    }

    /**
     * Plus replace
     * @param string $key
     * @param mixed $val
     * @return boolen
     */
    public function set($key, $val, $egnore, $egnore5) {
        if (empty($this->MyConnection)) {
            return false;
        }
        if( parent::replace($key, $val, 0, $this->my_mt_rand($val)) ) {
            return true;
        } else {
            return parent::set($key, $val, 0, $this->my_mt_rand($val));
        }
    }

    /**
     *
     * @param string $key
     * @return mixed
     */
    public function get($key) {
        if (empty($this->MyConnection)) {
            return false;
        }
        return parent::get($key);
    }

}
