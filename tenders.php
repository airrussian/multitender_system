<?php
/**
 * Joomla! 1.5 component Tenders
 *
 * @version $Id: tenders.php 2009-10-07 00:21:56 svn $
 * @author Paul
 * @package Joomla
 * @subpackage Tenders
 * @license GNU/GPL
 *
 *
 *
 * This component file was created using the Joomla Component Creator by Not Web Design
 * http://www.notwebdesign.com/joomla_component_creator/
 *
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

header('Content-Type: text/html; charset=utf-8');

// add tenders

require_once dirname(__FILE__) . "/main.php";

$conf = & $GLOBALS['tenders']['conf'];

$action = @$_GET['action'] ? $_GET['action'] : 'view';

//$anonymous = array('view');
//$registrat = array('view', 'detail');

//echo "<!-- ";
//print_r($_GET);
//print_r($_SERVER);
//echo "-->";


// /tenders/detail/111
$REQUEST = $_SERVER['REQUEST_URI'];

if(preg_match("#detail/(\d+)/?#", $REQUEST, $m)) {
    $action = 'detail';
    $_GET['id'] = $m[1];
}

$user =& JFactory::getUser();

if ( $user->get('guest') ) {
    $GLOBALS['tenders']['conf']['full']  = false;
} else {
    $GLOBALS['tenders']['conf']['full']  = true;
}


if ( ! $user->get('guest') ) {
    $GLOBALS['tenders']['conf']['user']['id']       = $user->get('id');
    $GLOBALS['tenders']['conf']['user']['name']     = $user->get('name');
    $GLOBALS['tenders']['conf']['user']['username'] = $user->get('username');
    $GLOBALS['tenders']['conf']['user']['email']    = $user->get('email');
    $GLOBALS['tenders']['conf']['user']['right'] = 1;
    $GLOBALS['tenders']['conf']['user']['type'] = 'user';
}
else {
    $GLOBALS['tenders']['conf']['user']['id']   = 0;
    $GLOBALS['tenders']['conf']['user']['name'] = 'anonym';
    $GLOBALS['tenders']['conf']['user']['right'] = 0;
    $GLOBALS['tenders']['conf']['user']['type'] = 'anonym';

    tc_try_set_bots();
}

tenders_run_action('users');
echo tenders_run_action($action);

$params   = &$GLOBALS['mainframe']->getParams();
$document = &JFactory::getDocument();

$params->set('page_description',  'Система поиска госзаказов, госзакупок' );

$document->setDescription( $params->get('page_description' ) );

if( ! empty( $GLOBALS['tenders']['conf']['page_title'] ) ) {

    $params->set( 'page_title',  $params->get( 'page_title' ) . ' - ' . $GLOBALS['tenders']['conf']['page_title'] );
    $params->set( 'page_description',  $params->get( 'page_description' ) . ', ' . $GLOBALS['tenders']['conf']['page_title'] );
    //	$params->set('page_title', $GLOBALS['tenders']['conf']['page_title']);

    $document->setTitle( $params->get( 'page_title' ) );
    $document->setDescription( $params->get('page_description' ) );
}

if( ! empty( $GLOBALS['tenders']['conf']['page']['description'] ) ) {
    $document->setDescription( $GLOBALS['tenders']['conf']['page']['description'] );
}

if( ! empty( $GLOBALS['tenders']['conf']['page']['title'] ) ) {
    $document->setTitle( $GLOBALS['tenders']['conf']['page']['title'] );
}

