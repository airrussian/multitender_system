<?php
/**
 * Joomla! 1.5 component Tenders
 *
 * @version $Id: tenders.php 2009-10-07 00:21:56 svn $
 * @author Paul
 * @package Joomla
 * @subpackage Tenders
 * @license GNU/GPL
 *
 *
 *
 * This component file was created using the Joomla Component Creator by Not Web Design
 * http://www.notwebdesign.com/joomla_component_creator/
 *
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

require_once dirname(__FILE__) . DS. 'joomla.php';

// redirect from index.php to /
if ($_SERVER['REQUEST_URI'] == '/index.php') {
    header("HTTP/1.1 301 Moved Permanently");
    header("Location:http://" . $_SERVER['HTTP_HOST'] . '/');
    exit;
}

$action = @$_GET['action'] ? $_GET['action'] : 'view';

//echo tenders_run_action($action);
