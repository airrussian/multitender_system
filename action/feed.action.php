<?php


class multitender_action_feed extends multitender_action {

    function run() {

        // обрезание строк
        $short_default = 160;
        $short_max     = 5555;
        $short_etc     = '…';
        $short_link    = '';
        mb_internal_encoding('UTF-8');

        if (isset($_GET['short'])) {
            $short_max = (int) $_GET['short'];
            if ($short_max < 15 || $short_max > 900) {
                $short_max = $short_default;
                $short_link = "&short";
            } else {
                $short_link = "&short=$short_max";
            }
            $short_max -= mb_strlen($short_etc);
        }

        header("Content-Type: application/xml; charset=utf-8");

        $this->conf['pref']['ppp'] = 15;

        if (isset($_GET['ppp'])) {
            $ppp = (int)$_GET['ppp'];
            if ($ppp >= 10 && $ppp <= 100) {
                $this->conf['pref']['ppp'] = $ppp;
            } else {
                $this->conf['pref']['ppp'] = 30;
            }
        }

        $tpl = $this->new_tpl();

        $s_obj = $this->new_model('search');
        $s_obj->clear_search_feed();

        $search = $s_obj->search;

        $s_obj->kostyl_type = 'feed';

        $r = $s_obj->query();

        $items = $r['items'];
        $total = $r['total'];

        foreach($items as &$item) {
            // FIXME: url to config
            $item['link'] = $this->conf['pref']['link_base_http_wo'] . $this->conf['pref']['link_detail'] . $item['id'];

            if (isset($_GET['short'])) {
                if (mb_strlen($item['name']) > $short_max) {
                    $item['name'] = preg_replace('/\s+?(\S+)?$/u', '', mb_substr($item['name'], 0, $short_max+1));
                    $item['name'] .= $short_etc;
                }
                // FIXME перенести это в лоадер при вставке тендеров
                // в нижний регистр, если больше 3 слов заглавными буквами
                if (preg_match_all('/[А-ЯЁA-Z]{3,}/u', $item['name'], $m) && (count($m[0]) >= 3) ) {
                    $item['name'] = mb_strtolower($item['name']);
                }
            }

            $date =  $this->db->UnixTimeStamp( $item['date_add'] );
            $item['date'] = gmdate('r', $date);

            if( $item['date_end'] ) {
                // 2009-11-10
                list($y, $m, $d) = explode('-', $item['date_end']);
                $date = gmmktime (0, 0, 0, $m, $d, $y);
                if( time() > $date ) {
                    $item['date_end_exit'] = true;
                }
            }
            /*
            if( !empty( $search['main'] ) ) {
                $search['main'] = trim($search['main']);
                $highlight = preg_replace("/\s+/u", "|", $search['main']);
                //FIXME: tag u - kostyl
                $item["name"] = preg_replace("/$highlight/xiu", '<b>\\0</b>', $item["name"]);
                $item["name"] = htmlspecialchars($item['name']);
            }
            */
        }

        if(empty($items[0]['date'])) {
            $tpl->assign('date', gmdate('r'));
        }
        else {
            $tpl->assign('date', $items[0]['date']);
        }
        $tpl->assign('gosexpress', isset($_GET['gosexpress'])?true:false);

        $tpl->assign('items', $items);

        $tpl->assign('name', @$_GET['name']);

        $tpl->assign('total', $total);

        $tpl->assign('search', $search);

        $hidden = $s_obj->to_hidden();

        $tpl->assign('hidden',  $hidden);

        $info_main = $s_obj->info_main();
        if (!$info_main) {
            $info_main = 'Все тендеры';
        }
        $tpl->assign('info_main', $info_main);

        $self = str_replace('&', '&amp;', $s_obj->to_url() . $short_link);
        $self = $self ? "?$self" : '';
        $self = $this->conf['pref']['link_base_rss'] . $self;
        
        $tpl->assign('self',  $self);

        //      $tpl->assign('form',  $tpl->fetch('form.tpl'));
        return $tpl->fetch('feed.tpl');

    } //function run

}
