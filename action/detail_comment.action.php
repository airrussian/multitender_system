<?php

class multitender_action_detail_comment extends multitender_action {

    function run() {

        $page = @ $_GET['page'] ? $_GET['page'] : 1;
        $edit = @ $_GET['edit'] ? $_GET['edit'] : NULL;
        $key = @ $_GET['key'] ? $_GET['key'] : NULL;
        $user = $this->conf['user'];
        $user_id = isset($user['id']) ? $user['id'] : null;
        $item_id = isset($_GET['id']) ? (int) $_GET['id'] : 20300;
        $del_key = @ $_GET['del_key'] ? $_GET['del_key'] : NULL;
        if ($key) {
            $edit_text = @ $_POST['message' . $key] ? $_POST['message' . $key] : NULL;
            $edit_text = trim($edit_text);
            if ($edit_text != '') {
                $edit_text = substr($edit_text, 0, 1999);
                $edit_text = nl2br($edit_text);
            }
        }

        $row2 = null;
        $comment_text = isset($_POST['message']) ? $_POST['message'] : NULL;
        $comment_public = isset($_POST['public']) ? $_POST['public'] : 0;
        if ($comment_public == 0) {
            $comment_public = 2;
        }
        $comment_public = $comment_public - 1;
        $edit_key = isset($_GET['edit_key']) ? $_GET['edit_key'] : NULL;
        $edit_flag = isset($_POST['edit_flag']) ? $_POST['edit_flag'] : NULL;

        $comment_text = trim($comment_text);

        $cm = $this->new_model('detail_comment');

        if ($del_key && $user_id) {
            $cm->del_rec($del_key, $user_id);
            header("Location: /tenders/detail/" . $item_id . '?page=' . $page);
        }

        if ($key && $edit) {
            if ($edit_text != '' && $edit) {
                $cm->update_edited(array($edit_text, (int) $edit, (int) $user_id));
                header("Location: /tenders/detail/" . $item_id . '?page=' . $page);
            }
        }

        if ($comment_text != '') {
            $comment_text = substr($comment_text, 0, 1999);
            $comment_text = nl2br($comment_text);
        }

        if (!$edit_key && !$edit_flag) {
            if ($comment_text != '' && $user_id) {

                $array[0] = (int) $user_id;
                $array[1] = (int) $comment_public;
                $array[2] = $comment_text;
                $array[3] = (int) $item_id;
                if ($comment_public != -1) {
                    $cm->add_comment($array);
                    header("Location: /tenders/detail/" . $item_id . "?page=" . $page);
                }
            }
        } elseif (!$edit_key && $edit_flag) {
            if ($comment_text != '' && $user_id) {
                $cm->update_edited(array($comment_text, (int) $edit_flag));
                header("Location: /tenders/detail/" . $item_id . '?page=' . $page);
            }
        } elseif ($edit_key && !$edit_flag) {
            $row2 = $cm->select_edited($edit_key);
            $row2['id'] = $edit_key;
        }

        $edit_flag = NULL;

        $rectopg = 10;
        $offset = $rectopg * ($page - 1);

        $comments3 = $cm->select_comments(array((int) $item_id, (int) $user_id), $offset, $rectopg);

        $total = $comments3['total'];
        unset($comments3['total']);

        foreach ($comments3 as &$item123) {
            if ((((strtotime(date('Y-m-d H:i:s'))) < (strtotime($item123['date_time']) + 30 * 60)) || ($item123['personal_flag'] == 1)) && $item123['user_id'] == $user_id) {
                $item123['edit_flag'] = 1;
            } else {
                $item123['edit_flag'] = 0;
            }
            $temp = explode(" ", $item123['text']);
            foreach ($temp as &$a) {
                $a = substr($a, 0, 100);
            }
            $item123['text'] = implode($temp, " ");
            $item123['date_time'] = date("d.m.Y H:i", strtotime($item123['date_time']));
            $item123['text'] = htmlspecialchars($item123['text'], ENT_QUOTES);
            $item123['text'] = str_replace('&lt;br/&gt;', '<br />', $item123['text']);
            $item123['text'] = str_replace('&lt;br&gt;', '<br />', $item123['text']);
            $item123['text'] = str_replace('&lt;br /&gt;', '<br />', $item123['text']);
            $item123['edittext'] = str_replace('<br />', "", $item123['text']);
        }
        reset($comments3);

        $scrol = $this->new_action("scrol");
        $link = $item_id;
        $scrol->addelement = '';
        $scrol->link = "/tenders/detail/" . $link . '?' . substr($scrol->link, 1);
        $scrol->total = $total;
        $scrol->onpage = $rectopg;        
        
        $tpl = $this->new_tpl();
        $tpl->assign('page', $page);
        $tpl->assign('edit_key', $edit_key);
        if ($row2) {
            if ($row2['user_id'] == $user_id) {
                $tpl->assign('edited', $row2);
            }
        }
        $tpl->assign('user', $user);
        $tpl->assign('item_id', $item_id);
        $tpl->assign('comments', $comments3);
        $tpl->assign('scrol', $scrol->run());
        return $tpl->fetch('detail_comment.tpl');
    }

}