<?php

class multitender_action_grapher extends multitender_action {

    private $tpl;
    private $type;
    function  __construct($type = 'column') {
        parent::__construct();
    }

    /**
     *
     * @param array $data
     * @param string $title
     * @param string $Xname
     * @param int $width
     * @param int $hight
     * @return string 
     */
    function draw_column($data, $title = 'Заголовок', $Xname = 'Ось Х', $width=800, $hight=400) {
        $this->tpl = $this->new_tpl();
        $this->tpl->assign('width', $width);
        $this->tpl->assign('height', $hight);
        $this->tpl->assign('title', $title);
        $this->tpl->assign('Xname', $Xname);
        $columns = array();
        $values = array();
        foreach ($data as $ttl => $vals) { // для каждого объекта
            $tempvalues = array();
            //for ($i=$start;$i<=$cnt;$i++) {
            foreach ($vals as $key => $val) {
                    $tempvalues[] = $val;
                    $columns[] = $key;

            }
            $values[] = "['$ttl', " . implode(', ', $tempvalues) . ']';
        }
        $values = '[' . implode(', ', $values) . ']';
        $columns = '[\'' . implode('\', \'', array_unique($columns)) . '\']';
        $this->tpl->assign('divid', 'vis'.mt_rand(1, 1000));
        $this->tpl->assign('values', $values);
        $this->tpl->assign('columns', $columns);
        return $this->tpl->fetch('graph_column.tpl');
    }

    function draw_area($data, $title = 'Заголовок', $Xname = 'Ось Х', $width=800, $hight=400) {
        $this->tpl = $this->new_tpl();
        $this->tpl->assign('width', $width);
        $this->tpl->assign('height', $hight);
        $this->tpl->assign('title', $title);
        $this->tpl->assign('Xname', $Xname);

        $values = array();
        foreach ($data as $ttl =>$data2){
            $line = $ttl;
            foreach ($data2 as $vals) {
                $values[] = '[\''. $this->num_to_month($vals[0]) . '\', ' . round($vals[1]) . ']';
            }
        }
        $values = '[' . implode(', ', $values) . ']';

        $this->tpl->assign('divid', 'vis'.mt_rand(1, 1000));
        $this->tpl->assign('values', $values);
        $this->tpl->assign('line_name', $line);
        return $this->tpl->fetch('graph_area.tpl');

    }

    function num_to_month($date) {
        $months = array('Нулябрь', 'Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь');
        $date = explode('-', $date);
        if (isset($months[$date[1]])) {
            $result = $months[$date[1]] . ' ' . $date[0];
        } else {
            $result = '';
        }
        return $result;
    }

}
