<?php

class multitender_action_user_tenders extends multitender_action {

    public $rectopg=10;
    public $total;

    function list_tender($user_id, $link, $task) {
        
        $vt = $this->new_model("billing");
        $tpl = $this->new_tpl();

        $items = array();

        $link_base    = $this->conf['pref']['link_base'];
        $link_detail  = $this->conf['pref']['link_detail'];
        
        $scrol = $this->new_action("scrol");
        $scrol->link = '/tenders/history_tenders/?page=';
        $this->total = $vt->get_counttenders($user_id, $task);
        $scrol->total = $this->total;
        $scrol->onpage = $this->conf['db_conf']['person']['history_tender'];
        $tpl->assign('scrol', $scrol->run());

        $offset = ($scrol->page - 1) * $this->rectopg;

        $lt = $vt->get_tenders($user_id, $offset, $this->rectopg, $task);

        $data_common = $this->new_model('data_common');
        $data_common = $data_common->get_data();
        if ($lt) {
            foreach ($lt as $p) {
                $ids[] = $p['item_id'];
            }

            $ft = $this->new_model("favorite_tenders");
            // Все заголовки для
            $items=$ft->get_title($ids);
            // Избранные среди них
            $ten_to_fav=$ft->tenders_to_favorite($user_id, $ids);
            foreach($items as &$item) {
                if ($ten_to_fav) {
                    $item['fav']=in_array($item['id'], $ten_to_fav);
                } else {
                    $item['fav']=false;
                }
                foreach ($data_common['region'] as $region) {
                    if ($region['id'] == $item['region_id']) {
                        $item['region'] = $region['name'];
                        break;
                    }
                }
                foreach ($data_common['type'] as $type) {
                    if ($type['id'] == $item['type_id']) {
                        $item['type'] = $type['name'];
                        break;
                    }
                }
                if (!is_numeric($item['price'])) {
                    $item['price'] = '&ndash;';
                } 
                foreach ($lt as $p) {
                    if ($p['item_id']==$item['id']) {
                        $item['buy'] = date2str($p['date_purches'],"&nbsp;");
                        if ($item['buy']=='-') {
                            $item['buy']='не&nbsp;приобретен';
                        }
                    }
                }
            }
        }

        $bl = $this->new_model("billing");
        $acc = $bl->getAccess($user_id);
        if (strtotime($acc['todate']) > time()) {
            $tpl->assign("WhenBuyShow", false);
        } else {
            $tpl->assign("WhenBuyShow", true);
        }

        $tpl->assign("items", $items);
        $tpl->assign("link_base", $link_base);
        $tpl->assign("link_detail", $link_detail);
        $tpl->assign("link", $link_base.$link);
        $tpl->assign("user_id", $user_id);
        return $tpl->fetch("user_tenders_list.tpl");
    }
    
    function show($task) {

        $user_id = $this->conf['user']['id'];
        if (!$user_id) {
            $tpl = $this->new_tpl();
            return $tpl->fetch("msg_auth.tpl");
        }

        $link_base = $this->conf['pref']['link_base'];
        $link = "action=user_tenders&task=$task";

        $list_tenders = $this->list_tender($user_id, $link, $task);

        $tpl = $this->new_tpl();
        $tpl->assign("link", $link_base.$link);
        $tpl->assign("user_id", $user_id);
        $tpl->assign("list_tenders", $list_tenders);

        switch ($task) {
            case 'last':
                $tpl->assign("title", "Просмотренные тендеры");
                $tpl->assign("empty", "Вы ещё не просматривали тендеры");
                break;
            case 'purc':
                $tpl->assign("title", "Приобретенные тендеры");
                $tpl->assign("empty", "Вы ещё не купили не одного тендера");
                break;
        }

        return $tpl->fetch("user_tenders.tpl");
    }

    function run() {

        $this->rectopg = $this->conf['db_conf']['person']['last_tenders_rectopg'];
        
        $this->fav_ten=$this->new_model('favorite_tenders');
        /* Получаем задачу для action из GET */
        $task = @ $_GET['task'] ? $_GET['task'] : 'last';
        return $this->show($task);
    }

}