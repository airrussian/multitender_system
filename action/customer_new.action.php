<?php
/**
 * Новые заказчики
*/
class multitender_action_customer_new extends multitender_action {
    function run() {
        $items     = $this->db->GetArray('SELECT * FROM customer ORDER BY datetime DESC LIMIT 50');
        $form_data = multitender_model_data_common::singleton()->get_data();

        foreach ($items as &$item) {
            if (isset ($form_data['region'][$item['region_id']])) {
                $item['region_name'] = $form_data['region'][$item['region_id']]['name'];
            }
        }

        $tpl = $this->new_tpl();
        $tpl->assign('items', $items );

        return $tpl->fetch('customer_new.tpl');
    } //function run
}
