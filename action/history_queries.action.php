<?php

class multitender_action_history_queries extends multitender_action {

    function run() {
        $this->conf['page_title'] = 'История';

        $user_id = $GLOBALS['tenders']['conf']['user']['id'];
        if (!$user_id) {
            $tpl = $this->new_tpl();
            return $tpl->fetch("msg_auth.tpl");
        }
        $rectopg = $this->conf['db_conf']['person']['history_search'];
        $lq = $this->new_action("last_queries");
        $user_id = $this->conf['user']['id'];
        $items = $lq->list_query($user_id, "action=history", $rectopg);
        return $items;
    }

}
