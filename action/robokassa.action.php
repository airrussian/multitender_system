<?php

class multitender_action_robokassa extends multitender_action {

    public function button($out_summ, $inv_id, $inv_desc) {

        $mrh_link  = $this->conf['robokasse']['mrh_link'];
        $mrh_login = $this->conf['robokasse']['mrh_login'];
        $mrh_pass  = $this->conf['robokasse']['mrh_pass1'];

        //$inv_desc  = mb_convert_encoding($inv_desc, "CP1251", "UTF-8");
        $crc       = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass");
        $in_curr   = $this->conf['robokasse']['in_curr'];
        $email     = ''; //$this->conf['robokasse']['email'];
        $culture   = $this->conf['robokasse']['culture'];

        $bl  = $this->new_model("billing");
        $payment = $bl->get_payment($inv_id);

        if ($payment['paid'] == 0) {
            $but = "<form action=".$mrh_link." method=POST style='padding: 0; margin: 0;'>";
            $but.= "<input type=hidden name='MrchLogin' value='$mrh_login'>";
            $but.= "<input type=hidden name='OutSum' value='$out_summ'>";
            $but.= "<input type=hidden name='InvId' value='$inv_id'>";
            $but.= "<input type=hidden name='Desc' value='$inv_desc'>";
            $but.= "<input type=hidden name='SignatureValue' value='$crc'>";
            $but.= "<input type=hidden name='IncCurrLabel' value='$in_curr'>";
            $but.= "<input type=hidden name='Email' value='$email'>";
            $but.= "<input type=hidden name='Culture' value='$culture'>";
            $but.= "<center><button class='pay' type='submit'></button></center></form>";
            return $but;
        } else {
            $but = "<center><button class='pay' type='submit' disabled></button></center>";
            return $but;
        }
    }

    function result() {
        $out_summ  = $_REQUEST["OutSum"];
        $inv_id    = $_REQUEST["InvId"];
        $crc       = $_REQUEST["SignatureValue"];

        $mrh_link  = $this->conf['robokasse']['mrh_link'];
        $mrh_login = $this->conf['robokasse']['mrh_login'];
        $mrh_pass  = $this->conf['robokasse']['mrh_pass2'];

        $crc = strtoupper($crc);
        $my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass"));

        if ($my_crc != $crc) {
            return "bad sign\n";
        }

        $bl = $this->new_model("billing");
        if (!$pay = $bl->get_payment($inv_id)) {
            return "error payments!\n";
        }

        if ( $out_summ < $pay['summa']*0.9 ) {
            return "nedostatochnaia summa!";
        } else {
            if ($bl->paid($pay['id'])) {
                return "OK$inv_id\n";
            } else {
                return "error transaction!";
            }
        }
    }

    function fail() {
        $out_summ  = $_REQUEST["OutSum"];
        $inv_id    = $_REQUEST["InvId"];
        $crc       = $_REQUEST["SignatureValue"];

        $mrh_link  = $this->conf['robokasse']['mrh_link'];
        $mrh_login = $this->conf['robokasse']['mrh_login'];
        $mrh_pass  = $this->conf['robokasse']['mrh_pass1'];

        $crc = strtoupper($crc);
        $my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass"));

        if ($my_crc != $crc) {
            return "bad sign\n";           
        }

        return "Вы отказались от оплаты?";
    }


    function run() {

        $task = @ $_GET['task'] ? $_GET['task'] : 'result';

        switch ($task) {
            case "fail":
                return $this->fail();
                break;
            case "result":
            default:
                return $this->result();
                break;
        }
    }

}
