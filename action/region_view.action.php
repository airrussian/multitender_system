<?php

class multitender_action_region_view extends multitender_action {
    private $query = '';
    private $error = 0;
    private $res = NULL;
    private $link_patter = NULL;
    private $table = NULL;
    private $after_patter = NULL;

    private function  construct($table) {
        switch ($table) {
            case 'distributor':
                $this->query = 'SELECT COUNT(*) as count, geoip_region.name, geoip_region.const as id FROM geoip_region, distributor WHERE distributor.region_id = geoip_region.const GROUP BY distributor.region_id';
                $this->link_patter = '/tenders/distributor_list/?region_id=?id?';
                $this->after_patter = ' <span class="sp4">?count?</span><br/>';
                $this->res = $this->db->GetAll($this->query);
                break;
            case 'region':
                //$this->query = 'SELECT COUNT(*) as count, geoip_region.name, geoip_region.const as id FROM geoip_region, item WHERE item.region_id = geoip_region.const AND item.date_end > NOW() GROUP BY item.region_id ORDER BY geoip_region.name';
                $sql = "SELECT name, geoip_region.const as id FROM geoip_region";
                $rs = $this->db->GetAll($sql);
                $sql = "SELECT COUNT(id) as count, region_id FROM item WHERE date_end > NOW() GROUP BY region_id";
                $rcnt = $this->db->GetAll($sql);

                foreach($rs as &$v) {
                    foreach ($rcnt as $cnt) {
                        if ($cnt['region_id'] == $v['id']) {
                            $v['count'] = $cnt['count'];
                        }
                    }
                }
                $this->res = $rs;
                $this->link_patter = '/tenders/?chpu?';
                $this->after_patter = ' <span class="sp4">?count?</span>&nbsp;<a href="/tenders/analytics/?region_id=?id?"><img alt="" src="/templates/mt/img/stat.gif"></a><br/>';
                break;
            case 'customer':
                $this->query = 'SELECT COUNT(*) as count, geoip_region.name, geoip_region.const as id FROM geoip_region, customer WHERE customer.region_id = geoip_region.const GROUP BY customer.region_id';
                $this->link_patter = '/tenders/by_customer/?region_id=?id?';
                $this->after_patter = ' <span class="sp4">?count?</span><br/>';
                $this->res = $this->db->GetAll($this->query);
                break;
            default:
                echo 'Ahtung!';
                $this->error = 1;
        }
        $this->table = $table;
    }

    public function run($tb) {
        $this->construct($tb);
        $other = array();
        $republic = array();

        foreach ($this->res as $element) {
            if ($element['id'] == 0) {
                continue;
            }
            if (!(strpos($element['name'], 'Республика') === false)) {
                $republic[] = array('name' => trim(str_replace('Республика', '', $element['name'])), 'id' => $element['id'], 'count'=>$element['count']);
            } else {
                $other[] = $element;
            }
        }


        foreach($other as $element) {
            $temp[] = $element['name'];
        }

        array_multisort($temp, SORT_ASC, $other);

        $temp = array();
        
        foreach($republic as $element) {
            $temp[] = $element['name'];
        }

        array_multisort($temp, SORT_ASC, $republic);

        return $this->generatehtml($other, $republic);

    }

    private function generatehtml($first, $second) {
        $html = '<div class="o1"><div class="stolb1"><p class="pp1">A</p><p>';
        $kolvo = count($first)/3;
        $stolb=1;
        $i=0;
        $currentchar = 'А';
        $fixmeflag = false;
        foreach ($first as $element) {
            if ($currentchar != substr($element['name'],0,2) && $i>=$kolvo) {
                $stolb++;
                $currentchar = substr($element['name'],0,2);
                $html .= '</div><div class="stolb' . $stolb . '"><p class="pp1">' . $currentchar . '</p>';
                $i=0;
            }

            if (substr($element['name'],0,2) == 'С' && $fixmeflag == false) {
                $stolb++;
                $currentchar = substr($element['name'],0,2);
                $html .= '</div><div class="stolb' . $stolb . '"><p class="pp1">' . $currentchar . '</p>';
                $i=0;
                $fixmeflag = true;
            }

            if ($currentchar != substr($element['name'],0,2) && $i != 0) {
                $currentchar = substr($element['name'],0,2);
                $html .= '</p><p class="pp1">' . $currentchar . '</p><p>';
            }
            $link = $this->link_patter;
            if ($this->table == 'region') {
                include $this->conf['basedir'] . '/regions.php';

                $link = str_replace('?chpu?', $regions_chpu[$element['id']], $link);
            } else {
                $link = str_replace('?id?', $element['id'], $link);
            }

            $after = str_replace('?id?', $element['id'], $this->after_patter);
            $after = str_replace('?count?', $element['count'], $after);

            $html .= '<a href="' . $link . '">' . $element['name'] . '</a>';
            $html .= $after;

            $i++;
        }
        $html .= '</div>';

        $html .= '<div class="o1"><span class="sp5">Республики</span></div><div class="stolb1"><p class="pp1">A</p><p>';
        $kolvo = count($second)/3;
        $stolb=1;
        $i=0;
        $fixmeflag = false;
        $currentchar = 'А';
        foreach ($second as $element) {
            if (substr($element['name'],0,2) == 'К' && $fixmeflag == false) {
                $stolb++;
                $currentchar = substr($element['name'],0,2);
                $html .= '</div><div class="stolb' . $stolb . '"><p class="pp1">' . $currentchar . '</p>';
                $i=0;
                $fixmeflag = true;
            }

            if ($currentchar != substr($element['name'],0,2) && $i>=$kolvo && substr($element['name'],0,2) != 'С') {
                $stolb++;
                $currentchar = substr($element['name'],0,2);
                $html .= '</div><div class="stolb' . $stolb . '"><p class="pp1">' . $currentchar . '</p>';
                $i=0;
            }

            

            if ($currentchar != substr($element['name'],0,2) && $i != 0) {
                $currentchar = substr($element['name'],0,2);
                $html .= '</p><p class="pp1">' . $currentchar . '</p><p>';
            }
            $link = $this->link_patter;
            if ($this->table == 'region') {
                include $this->conf['basedir'] . '/regions.php';

                $link = str_replace('?chpu?', $regions_chpu[$element['id']], $link);
            } else {
                $link = str_replace('?id?', $element['id'], $link);
            }

            $after = str_replace('?id?', $element['id'], $this->after_patter);
            $after = str_replace('?count?', $element['count'], $after);

            $html .= '<a href="' . $link . '">' . $element['name'] . '</a>';
            $html .= $after;

            $i++;
        }
        $html .= '</div></div><br class="fix"/>';

        return $html;
    }
}