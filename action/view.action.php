<?php
/**
 * action view
 * @author Paul
 */
class multitender_action_view extends multitender_action {
    
    function ajax() {
                
        mb_internal_encoding('utf-8');
                
        $s_obj = new multitender_model_search();
        $search = $s_obj->search;
                
        if (isset($_GET['count_rubrics'])) {
            $k=$s_obj->query_count_rubric();            
            return json_encode($k);
        }
        

        $r = $s_obj->query();
        $items = $r['items'];
        $total = $r['total'];
        //$search = $s_obj->search;
        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;

        $link = $this->conf['pref']['link_base'] . $s_obj->to_url();
        $link = str_replace('&', '&amp;', $link);

        $items = $s_obj->items_prepare($items);

        foreach($items as &$item) {
            $item['link'] = $this->conf['pref']['link_detail'] . $item['id'];
            if ( preg_match('/user/i', $this->conf['user']['type']) ) {
                $item['link'] .= '?cache=user';
            }
            $tenders[]=$item['id'];
        }
        
        foreach ($items as $k1 => $item) {
            foreach ($item as $k2 => $v) {
                if (preg_match("#^\d+$#i", $k2)) {
                    unset($items[$k1][$k2]);
                }                
            }
        }
        
        $scrol = new multitender_action_scrol();
        $scrol->run();
        
        $total_record = Ceil ($total / $scrol->onpage);
        
        $return = '{"page":'.$page.',"total":'.$total_record.', records":'.$total;               
        
        $return.= ',"rows":' . json_encode($items) . '}';
        
        return $return;
         
    }
    

    function run() {
        
        if (isset($_GET['ajax'])) {
            return $this->ajax();
        }
        
        $user = $GLOBALS['tenders']['conf']['user'];
        $user_id = $user['id'];
        // FIXME установить utf-8 по умолчнанию, все остальные - указывают мануально
        mb_internal_encoding('utf-8');
        $tpl = $this->new_tpl();

        //print_r($items);

        $s_obj = new multitender_model_search();
        $search = $s_obj->search;
        //var_dump($search);

        $r = $s_obj->query();
        $items = $r['items'];
        $total = $r['total'];
        //$search = $s_obj->search;
        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;

//        if ($page > ceil($total / $this->conf['pref']['ppp']) && $page != 1) {
//            $header = "Location:".$this->conf['pref']['link_base'] . $s_obj->to_url() . '&page=' . ceil($total / $this->conf['pref']['ppp']);
//            header($header);
//        }
        $link = $this->conf['pref']['link_base'] . $s_obj->to_url();
        $link = str_replace('&', '&amp;', $link);

        $items = $s_obj->items_prepare($items);

        foreach($items as &$item) {
            $item['link'] = $this->conf['pref']['link_detail'] . $item['id'];
            if ( preg_match('/user/i', $this->conf['user']['type']) ) {
                $item['link'] .= '?cache=user';
            }
            $tenders[]=$item['id'];
        }

        // FIX ME: Сдается, что это фрагмент так и неиспользуется...
        if ($user_id) {
            // ПРОВЕРКА ДЛЯ КАЖДОГО ТЕНДЕРА ИЗ СПИСКА ЕСТЬ ЛИ ОН В ИЗБРАННОМ
            $fav_ten = new multitender_model_favorite_tenders();
            if (!empty($tenders)) {
                $tenders_to_favorite=$fav_ten->tenders_to_favorite($user_id, $tenders);
            }
            foreach($items as &$item) {
                if ($tenders_to_favorite) {
                    $item['fav']=in_array($item['id'],$tenders_to_favorite);
                }
            }
        }
        
        $tpl->assign('items', $items);

        $tpl->assign('name', @$_GET['name']);

        $scrol = new multitender_action_scrol();
        $scrol->link  = "$link&page=";
        $scrol->onpage_link  = "$link&onpage=";
        $scrol->total = $total;
        if ($scrol->total > 5000) {
            $scrol->total = 5000;
        }
        $tpl->assign( 'scrol',  $scrol->run() );

        $tpl->assign('base_url', $this->conf['pref']['link_base']);

        if (!empty( $search ) ) {
            $lq = new multitender_model_query_users();
            $sid=$lq->user_add_find($user_id, $s_obj, $total);
            
            var_dump($sid);

            $tpl->assign('id', $sid);

            $fq = $this->new_action('favorite_queries');
            $favq=$fq->get_status($sid, true);
            $tpl->assign("fav_query", $favq[$sid]);

            $en = $this->new_action('email_newsletter');
            $mail = $en->get_status($sid);
            $tpl->assign("email_new", $mail);

            // Похожие
            /*
            if (!empty($s_obj->search['main'])) {
                $related = new multitender_action_related();
                $tpl->assign('related_view', true);
                $tpl->assign('related_rubric', $related->get_rubric($items));
                $tpl->assign('related_search', $related->get_search($s_obj));
            }*/
        }

        $tpl->assign('user_id', $user_id);
        $tpl->assign('user', $user);

        $tpl->assign('total', $total);

        $mtime = number_format($s_obj->mtime / 1000, 2, ',', '`');
        if ($mtime === '0,00') {
            $mtime = '0,01';
        }
        $tpl->assign('mtime', $mtime);

        $tpl->assign('search', $search);

//      $tpl->assign('link_ext', 'main.php?action=search&' . $s_obj->to_url());

        $tpl->assign('link_empty', 'main.php?action=view&');

        $tpl->assign('link_save', 'main.php?action=search_save&');

        //$hidden = $s_obj->to_hidden(); // говорят, устаревший кусок

        //$tpl->assign('hidden',  $hidden);

        $tpl->assign('form_info',  $s_obj->to_info() );

        //      $tpl->assign('form',  $tpl->fetch('form.tpl'));
        $ya = array();
        $ya['page'] = $page;
        $ya['search'] = $link;
        $tpl->assign('ya', $ya);
        
        $tpl->assign('list',  $tpl->fetch('list.tpl'));

        $form = new multitender_action_searchform();
        $form->total = $total;

        $form->s_obj = & $s_obj; // TRUE

        $tpl->assign('form',  $form->run());

        $feed = $s_obj->to_url();
        $feed = $feed ? "?$feed" : '' ;
        $feed = $this->conf['pref']['link_base_rss'] . $feed;

        $tpl->assign('link_feed',  $feed);
        $tpl->assign('link_feed_email',  'http://feed2email.ru?url=' . urlencode($feed) );
        $tpl->assign('link_feed_encode',  urlencode($feed));

        $tpl->assign('baners', tenders_run_action('banerrota'));

        return $tpl->fetch('index2.tpl');

    } //function run

}
