<?php

class multitender_action_subscribe_customer extends multitender_action {
    
    private $user;
    private $task;
    
    public function __construct() {
        parent::__construct();        
        $this->task = isset($_REQUEST['task']) ? $_REQUEST['task'] : 'index';
        $this->user = $this->conf['user'];
        if (!$this->user['id']) { exit("No access"); }
        
        $this->customer_inn = isset($_REQUEST['customer_inn']) ? $_REQUEST['customer_inn'] : false;        
    }
    
    public function run() {
        $taskName = $this->task;        
        return $this->$taskName();
    }
    
    public function index() {
        $customer = $this->new_model('subscribe_customer');        
        $list = $customer->GetList($this->user['id']);
        $tpl = $this->new_tpl();
        $tpl->assign('list', $list);        
        return $tpl->fetch('subscribe_customer/index.tpl');
    }
    
    public function add() {        
        if (!$this->customer_inn) { return false; }
        $customer = $this->new_model('subscribe_customer');         
        $customer->add($this->user['id'], $this->customer_inn);        
        header("Location: {$_SERVER['HTTP_REFERER']}");
        exit();
    }
    
    public function del() {
        if (!$this->customer_inn) { return false; }
        $customer = $this->new_model('subscribe_customer'); 
        $customer->del($this->user['id'], $this->customer_inn);                       
        header("Location: {$_SERVER['HTTP_REFERER']}");
        exit();
    }
    
    public function status() {
        $status = isset($_REQUEST['status']) ? $_REQUEST['status'] : false;
        $customer = $this->new_model('subscribe_customer'); 
        if ($status == 'off') {
            $customer->status($this->user['id'], 0);
        } else {
            $customer->status($this->user['id'], 1);
        }
        header("Location: {$_SERVER['HTTP_REFERER']}");
        exit();
    }
    
}