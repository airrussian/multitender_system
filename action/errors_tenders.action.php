<?php

class multitender_action_errors_tenders extends multitender_action {

    function  __construct() {
        parent::__construct();
    }

    function new_error() {
        $user_id = $this->conf['user']['id'];
        $item_id = (int) isset($_POST['item_id']) ? $_POST['item_id'] : 0;
        $comment = isset($_POST['comment']) ? $_POST['comment'] : false;

        $to     = $this->conf['email_error_to'];
        $from   = $this->conf['email_error_from'];

        if (!$user_id || !$item_id) { return false; }

        $subject = 'Ошибка в тендере';

        $message = "<h1>Ошибка в тендере №$item_id</h1>";
        $message.= "Пользователь ID$user_id сообщает: <br /> ";
        $message.= "<pre>$comment</pre> <br />";
        $message.= "<hr />";

        $sm = $this->new_model("send_moderator");
        if ($sm->bad_item($user_id, $item_id, $comment)) {
            $email = $this->new_model("email");
            $email->send($subject, $message, $to, $from);
            return "Спасибо, ваше замечание отправлено.";
        } else {
            return false;
        }
    }

    function show() {

        $rights = $this->conf['user']['right'];
        if ($rights<=1) {
            $tpl = $this->new_tpl();
            $tpl->display("msg_right.tpl");
            exit;
        }

        $this->conf['page_title'] = 'Ошибки на details';

        $tpl = $this->new_tpl();

        $link_base = $this->conf['pref']['link_base'];
        $link_detail = $this->conf['pref']['link_detail'];
        $link_act = "action=errors_tenders";
        $page = @ $_GET['page'] ? $_GET['page'] : 1;
        $total = 0;
        $rectopg = 25;

        $tpl->assign('link_base', $link_base);
        $tpl->assign('link_detail', $link_detail);

        $this->db_per = & $this->conf['dbs']['person'];
        $this->db_ten = & $this->conf['dbs']['tenders'];

        $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM errors_tenders ORDER BY timestamp DESC";
        $rs = $this->db_per->SelectLimit($sql, $rectopg, ($page-1)*$rectopg);

        if ( ($errors = $rs->GetAll()) ) {
            $total = $this->db_per->GetOne("SELECT FOUND_ROWS()");
            $tpl->assign('total', $total);

            $items_ids = array();
            $users_ids = array();

            foreach ($errors as $error) {
                $item_ids[] = $error['item_id'];
                $user_ids[] = $error['user_id'];
            }

            $sql = "SELECT * FROM item WHERE id IN (".implode("," ,$item_ids).")";
            $items = $this->db_ten->GetAll($sql);

            $sql = "SELECT * FROM users WHERE Id IN (".implode(",", $user_ids).")";
            $users = $this->db_per->GetAll($sql);

            foreach ($errors as &$error) {
                foreach ($items as $item) {
                    if ($item['id']==$error['item_id']) {
                        $error['item'] = $item;
                    }
                }

                foreach ($users as $user) {
                    if ($user['Id']==$error['user_id']) {
                        $error['user'] = $user;
                    }
                }
            }

            $tpl->assign("errors", $errors);

            $scrol = new multitender_action_scrol();
            $scrol->link = $link_base.$link_act.$scrol->link;
            $scrol->total = Ceil($total/$rectopg);
            $tpl->assign('scrol', $scrol->run());

        }

        return $tpl->fetch("errors_tenders.tpl");
    }


    function run() {
        $task = isset($_GET['task']) ? $_GET['task'] : 'show';
        switch ($task) {
            case 'send': return $this->new_error(); break;
            case 'show': default: return $this->show(); 
        }
    }

}