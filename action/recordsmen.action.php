<?php

class multitender_action_recordsmen extends multitender_action {
    private $model;

    function  __construct() {
        parent::__construct();
        $rights = $this->conf['user']['right'];
        if ($rights<=1) {
    	    $tpl = $this->new_tpl();
    	    $tpl->display("msg_right.tpl");
    	    exit;
        }
    }

    function run() {
        $this->conf['page_title'] = 'Рекордсмены';
        
        $tpl = $this->new_tpl();

        $tpl->caching = true;
        $tpl->cache_lifetime = 60*60;

        if (!$tpl->is_cached('recordsmen.tpl')) {
            $this->model = $this->new_model('recordsmen');
            $topcomments = $this->linkusers($this->model->getCommentsRecord());
            $tpl->assign('topcomments', $topcomments);//!!!!!!!!!

            $last_search_week = $this->linkusers($this->model->getLastSearch(date('Y-m-d', time()-3600*24*7)));
            $tpl->assign('last_search_week', $last_search_week);//!!!!!!!1

            $user_tenders_week = $this->linkusers($this->model->getUserTenders(date('Y-m-d', time()-3600*24*7)));
            $tpl->assign('user_tenders_week', $user_tenders_week);//!!!!!!!1

            $last_search_month = $this->linkusers($this->model->getLastSearch(date('Y-m-d', time()-3600*24*30)));
            $tpl->assign('last_search_month', $last_search_month);//!!!!!!!1

            $user_tenders_month = $this->linkusers($this->model->getUserTenders(date('Y-m-d', time()-3600*24*30)));
            $tpl->assign('user_tenders_month', $user_tenders_month);//!!!!!!!1

            $last_search_all = $this->linkusers($this->model->getLastSearch(NULL));
            $tpl->assign('last_search_all', $last_search_all);//!!!!!!!1

            $user_tenders_all = $this->linkusers($this->model->getUserTenders(NULL));
            $tpl->assign('user_tenders_all', $user_tenders_all);//!!!!!!!1

            $email_newsletter = $this->linkusers($this->model->getEmailNews());
            $tpl->assign('email_newsletter', $email_newsletter);//!!!!!!!

            $favorite_tenders = $this->linkusers($this->model->GetFavoriteTenders());
            $tpl->assign('favorite_tenders', $favorite_tenders);//!!!!!!!

            $favorite_queries = $this->linkusers($this->model->GetFavoriteQueries());
            $tpl->assign('favorite_queries', $favorite_queries);//!!!!!!!

            $payments0 = $this->linkusers($this->model->GetPayments0());
            $tpl->assign('payments0', $payments0);//!!!!!!!

            $payments1 = $this->linkusers($this->model->GetPayments1());
            $tpl->assign('payments1', $payments1);//!!!!!!!

            $paymentsSumma = $this->linkusers($this->model->GetPaymentsSumma());
            $tpl->assign('paymentsSumma', $paymentsSumma);//!!!!!!!
            
            $user_ips = $this->model->GetUserIps();
            $tpl->assign('user_ips', $user_ips);

            $user_agents = $this->model->GetUserAgents();
            $tpl->assign('user_agents', $user_agents);
        }

        return $tpl->fetch('recordsmen.tpl');
    }

    public function linkusers($array) {
        $ids = array();
        foreach ($array as $comment) {
            $ids[] = $comment['user_id'];
        }
        $users = $this->model->getUsersByIds($ids);

        foreach ($users as $user) {
            foreach ($array as &$com) {
                if ($user['id']==$com['user_id']) {
                    $com['username'] = $user['name'];
                }
            }
        }
        return $array;
    }
}