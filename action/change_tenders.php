<?php

class multitender_action_change_tenders extends multitender_action {

    private $fav_ten;

    public $rectopg2=30;
    public $total;

    function  __construct() {
        $this->fav_ten = $this->new_model('favorite_tenders');
        parent::__construct();
    }

    function show() {
        $this->conf['page_title'] = 'Избранные тендеры';

        $user_id = $this->conf['user']['id'];

        $tpl = $this->new_tpl();

        $scrol=$this->new_action("scrol");
        
        $link = '/tenders/';
        $scrol->link="/tenders/favorite_tenders/?task=show".$scrol->link;

        $this->total = $this->fav_ten->get_count_fav_tenders($user_id);

        //выводит все подряд. скроллер не пашет
        if ($this->total) {
            $scrol->total=$this->total;
            $scrol->onpage=$this->rectopg2;
            $tpl->assign('scrol', $scrol->run());
            $offset=($scrol->page-1) * $this->rectopg2;
            $ids = $this->fav_ten->get_fav_tenders($user_id, $offset, $this->rectopg2);
            if ($ids) {
                $items=$this->fav_ten->get_title($ids);
            } else {
                $items=0;
            }
        } else {
            $items=0;
        }


        $tpl->assign('link', $link);
        $tpl->assign('items', $items);

        if ($user_id) {
            $tpl->assign('reguser', true);
        } else {
            $tpl->assign('reguser', false);
        }

        return $tpl->fetch('favorite_tenders.tpl');
    }

    function add($status=true) {
        if (empty($_GET['id'])) { return false; }
        $item_id = $_GET['id'];
        $user_id = $this->conf['user']['id'];        
        $this->fav_ten->add_fav_tender($user_id, $item_id);       
        if ($status) {
            return $this->get_status();
        }
    }

    function del($status=true) {
        if (empty($_GET['id'])) { return false; }
        $item_id = $_GET['id'];
        $user_id = $this->conf['user']['id'];
        $this->fav_ten->del_fav_tender($user_id, $item_id);
        if ($status) {
            return $this->get_status();
        }
    }

    function del_and_update() {
        $this->del(false);
        if (isset($_SERVER['HTTP_REFERER'])) {
            if (preg_match("#page=(\d+)#i", $_SERVER['HTTP_REFERER'], $page)) {
                $_GET['page'] = $page[1];
            }
        }
        return $this->list_show();
    }

    function get_status() {
        if (empty($_GET['id'])) {
            return false;
        }
        $item_id = $_GET['id'];
        $user_id = $this->conf['user']['id'];        
        $showtext = (int) isset($_GET['showtext']) ? $_GET['showtext'] : 0;

        $html='<a id="dft" href="/ajax.php?action=favorite_tenders&task={$task}&item_id={$id}" title="{$title}" class="{$class}" onclick="javascript: favorite_tender($(this), \'{$task}\', \'{$id}\'); return false;">{$name}&nbsp;</a>';

        $assign['id'] = $item_id;
        if ($this->fav_ten->tender_to_favorite($user_id, $item_id)) {
            $assign['task']  = 'del';
            $assign['class'] = 'favorite-tender-del';
            $assign['title'] = 'Убрать из избранных тендеров';
            $assign['name']  = 'Убрать из избранного';
        } else {
            $assign['task']  = 'add';
            $assign['class'] = 'favorite-tender-add';
            $assign['title'] = 'Добавить в избранные тендеры';
            $assign['name']  = 'Добавить в избранное';
        }

        foreach ($assign as $k => $t) {
            $html = str_replace("{\$$k}", $t, $html);
        }

        return $html;
    }

    function run() {
	$this->rectopg = $this->conf['db_conf']['person']['fav_tenders_rectopg'];
        /* Получаем задачу для action из GET */
        $task = @ $_GET['task'] ? $_GET['task'] : 'show';
        /* Определяем задачу для этого action */
        switch ($task) {
            case 'del_and_update': echo $this->del_and_update();
                break;
            case 'show': echo $this->show();
                break;
            case 'update' : echo $this->list_show();
                break;
            case 'add':  echo $this->add();
                break;
            case 'del':  echo $this->del();
                break;
            case 'status': echo $this->get_status();
                break;
        }
    }

}