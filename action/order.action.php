<?php

class multitender_action_order extends multitender_action {

    public function send($title, $html) {        
        $subject = "Запрос на " . $title;
        $email = $this->new_model('email');
        $email->send($subject, $html, 'airrussian@mail.ru', 'subscribe@multitender.ru');
        $email->send($subject, $html, 'order@sbaspect.ru', 'subscribe@multitender.ru');
    }
    
    public function getParam($order) {
        
        switch ($order) {
            case 'tenspr': 
                $result = array('title' => 'Тендероное сопровождение', 'send_title' => 'тендероное сопровождение', 'classh3' => $order);
                break;
            case 'tenkrd': 
                $result = array('title' => 'Тендерный кредит/заем', 'send_title' => 'тендероный кредит/займ', 'classh3' => $order);
                break;
            case 'bankgrt': 
                $result = array('title' => 'Банковская гарантия', 'send_title' => 'банковскую гарантию', 'classh3' => $order);
                break;
            case 'cons': 
                $result = array('title' => 'Консультация по закупкам', 'send_title' => 'консультацию по закупкам', 'classh3' => $order);
                break;
            case 'cons_t':
                $result = array('title' => 'Консультация по участию в закупках', 'send_title' => 'консультацию по участию в закупках', 'classh3' => 'cons');
                break;            
            case 'help':
                $result = array('title' => 'Помощь участникам закупок', 'send_title' => 'на помощь по участию в закуках', 'classh3' => 'party');
                break;            
        }
        
        return $result;
    }

    public function run() {

        error_reporting(E_ALL);
               
        $order = isset($_REQUEST['order']) ? $_REQUEST['order'] : false;

        if (!file_exists($this->conf['smarty']['template_dir'] . '/order/' . $order)) {
            exit("error no order");
        }
        
        $param = $this->getParam($order);

        if (isset($_REQUEST['mtorder'])) {
            if (file_exists($this->conf['smarty']['template_dir'] . '/order/' . $order . '/validata.php')) {
                require_once $this->conf['smarty']['template_dir'] . '/order/' . $order . '/validata.php';
            } else {
                require_once $this->conf['smarty']['template_dir'] . '/order/validata.php';
            }
            $error = validata($_REQUEST);
            if (empty($error)) {
                $send = $this->new_tpl();
                $send->assign('param', $param);
                $send->assign('data', $_REQUEST);
                $send_body = $send->fetch("order/$order/$order.send.tpl");
                $this->send($param['send_title'], $send_body);
                $view = $this->new_tpl();
                $result = $view->fetch("order/thank.tpl");
            } else {
                $view = $this->new_tpl();
                $view->assign('error', $error);
                $result = $view->fetch("order/$order/$order.view.tpl");
            }
        } else {
            $view = $this->new_tpl();
            $view->assign('data', $_REQUEST);
            $result = $view->fetch("order/$order/$order.view.tpl");
        }
        
        if (isset($_REQUEST['ajax'])) {
            
            $ajax = $this->new_tpl();
            $ajax = $ajax->fetch('order/ajax.tpl');
            
            $result = array(
                'param' => $param, 
                'html'  => $result,   
                'tmpl'  => $ajax
            );            
            
            $result = json_encode($result);
        } else {
            $tpl = $this->new_tpl();
            $tpl->assign('param', $param);
            $tpl->assign('html', $result);
            $result = $tpl->fetch('order/page.tpl');
        }

        return $result;
    }

}
