<?php

class multitender_action_all_search extends multitender_action {
    
    function run() {

        $count_on_page = $this->conf['pref']['ppp'];

        $tpl = $this->new_tpl();

        $user = @$_GET['user'] ? $_GET['user'] : 'all';
        $find = @$_GET['find'] ? $_GET['find'] : 'all';

        $page = @$_GET['page'] ? $_GET['page'] : 1;

        $qu = $this->new_model("query_users");
        $sh = $this->new_model("search");
        $aq = $qu->get_all_query(($page-1)*$count_on_page, $count_on_page, $user, $find);

        if ($aq) {
            $sr = $this->new_model("list_queries");
            foreach ($aq as $q) {
                $s_ids[] = $q['search_id'];
                if ($q['user_id']) {
                    $u_ids[] = $q['user_id'];
                }
            }
            $total = $aq['total'];
            unset($aq['total']);
        } else {
            $total = 0;
        }

        $link_base = $this->conf['pref']['link_base'];
        $link_act  = "action=all_search";
        $link_filt = $link_base.$link_act."&amp;user=$user&amp;find=$find";

        $scrol = $this->new_action('scrol');
        $scrol->link  = $link_filt."&amp;page=";
        $scrol->total = ceil($total / $this->conf['pref']['ppp']);
        $tpl->assign( 'scrol',  $scrol->run() );

        $tpl->assign("total", $total);

        $tpl->assign("link_base", $link_base);
        $tpl->assign("link_act",  $link_act);
        $tpl->assign("user", $user);
        $tpl->assign("find", $find);

        if ($total) {
            $searches = $sr->getList($s_ids);
            $users = $qu->get_user_ids($u_ids);

            foreach ($aq as &$q) {
                foreach ($searches as $s) {
                    if ($q['search_id']==$s['id']) {
                        $q['search'] = $s;
                    }
                }

                // FIXME, Woronenko
                if (!empty($users)) {
                    foreach ($users as $us) {
                        if ($q['user_id']==$us['Id']) {
                            $q['user'] = $us;
                        }
                    }
                }

                $s_obj = $this->new_model('search', unserialize($q['search']['search']));
                $s_obj->SetSearch( unserialize($q['search']['search']) );
                $q['search_txt'] = $s_obj->to_info();
                $q['link']       = $s_obj->to_url();
                if (!$q['search']['main']) {
                    $q['search']['main'] = '[-ИСКАТЬ-]';
                }

                $timez = 0 * 60 * 60;
                $q['date_add'] = abs($this->db->UnixTimeStamp($q['date']." ".$q['time']) - (time() - $timez));
                if ( abs($q['date_add']) > 60*60 ) {
                    $q['date_add'] = round($q['date_add']/(60*60)) . '&nbsp;h';
                }
                elseif ( abs($q['date_add']) > 60) {
                    $q['date_add'] = round($q['date_add']/60) . '&nbsp;m';
                }
                else {
                    $q['date_add'] = $q['date_add'] . '&nbsp;s';
                }

            }
            $tpl->assign("items", $aq);
        }

        return $tpl->fetch("all_search.tpl");
    }
    
}
