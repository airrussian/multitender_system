<?php
// FIXME id !== joomla_id

class multitender_action_users extends multitender_action {
    function run() {

        $task = @ $_GET['task'] ? $_GET['task'] : '';
        if ($task=='alertlogin') {
    	    $user_id = @ $_GET['user_id'] ? $_GET['user_id'] : FALSE;
    	    setcookie(TC_ALERT_LOGIN, $user_id, 0, '/');
            header('Location:http://'.$_SERVER['HTTP_HOST'].'/');
            exit;
        }

        $this->id            = & $this->conf['user']['id'];
        $this->joomla_id     = & $this->conf['user']['joomla_id'];
        $this->name          = & $this->conf['user']['name'];
        $this->right         = & $this->conf['user']['right'];
        $this->type          = & $this->conf['user']['type'];
        $this->agent         = & $this->conf['user']['agent'];
        $this->payment_type  = & $this->conf['user']['payment_type'];

        $users = new multitender_model_users();

        $this->id = $users->get_user_id();

        //
        // вычисляем последний визит
        //
        
        $date_last_visit_db = $users->last_visit; // последний визит из базы
        $users->set_last_visit();
        $users->set_user_agent();
        $users->set_user_ip();
        if ( empty($_COOKIE[MT_LAST_VISIT]) ) {
            $date_last_visit = $date_last_visit_db;
            setcookie(MT_LAST_VISIT, $date_last_visit_db, time()+24*3600, '/');
        } else {
            $date_last_visit = $_COOKIE[MT_LAST_VISIT];
            $date_last_visit = strtotime($date_last_visit);
            if ( ($date_last_visit < (time() - 60*24*60*60)) || ($date_last_visit > (time() + 24*60*60)) ) {
                $date_last_visit = time();
            }
            $date_last_visit = date('Ymd', $date_last_visit);
        }

        // подменяем значение из куки
        $users->last_visit = $date_last_visit;

        if ( $this->right > 100 ) {
            if (isset($_COOKIE[TC_ALERT_LOGIN])) {
                if (!$this->id = (int) $_COOKIE[TC_ALERT_LOGIN]) {
                    trigger_error('Error in cookie '.TC_ALERT_LOGIN);
                    exit;
                }

            }
        }

        if ( $this->id ) {
            $cookie = $this->id."z".tc_ajax_solt($this->id);
            if ( !isset($_COOKIE[TC_USER_ID]) || $cookie !== $_COOKIE[TC_USER_ID] ) {
                setcookie(TC_USER_ID, $cookie, 0, '/');
            }
            if ( $this->type == 'user' ) {
                $bl  = $this->new_model('billing');
                $acc = $bl->getAccess($this->id);
                if ($acc['tenders']) {
                    $this->type         = 'user_payment';
                    $this->payment_type = 'limited';
                }
                if (strtotime($acc['todate']) > time()) {
                    $this->type         = 'user_payment';
                    $this->payment_type = 'unlimited';
                }
            }
        } else {
            if (!empty($_REQUEST[TC_USER_ID])) {
                setcookie(TC_USER_ID, "", time()-3600, "/");
            }
        }
    }
}
