<?php
class multitender_action_kostyl extends multitender_action {

    function run() {
        $tpl = $this->new_tpl();

        $page = @$_GET['page'] ? $_GET['page'] : 1;

        $type = @$_GET['type'] ? $_GET['type'] : 'search';

        $who = @ $_GET['who'];
        if ( $who === 'bots' ) {
            $where_who = "AND user_id = 555";
        } else {
            $who = 'notbots';
            $where_who = 'AND (NOT user_id <=> 555)';
        }

        $rs = $this->db->SelectLimit("
SELECT SQL_CALC_FOUND_ROWS * FROM kostyl
WHERE type = " . $this->db->Quote( $type ) . " $where_who
ORDER BY id DESC",
            $this->conf['pref']['ppp'], ($page - 1) * $this->conf['pref']['ppp']);

        $items = $rs->GetArray();

        $total = $this->db->GetArray("SELECT FOUND_ROWS()");
        $total = $total[0][0];

        $scrol = $this->new_action( 'scrol' );
        $scrol->link  = "?action=kostyl&amp;type=$type&amp;who=$who&amp;page=";
        $scrol->total = ceil($total / $this->conf['pref']['ppp']);
        $tpl->assign( 'scrol',  $scrol->run() );

        $tpl->assign( 'total',  $total );

        foreach( $items as &$v ) {
            $v['search'] = unserialize($v['search']);
            
            if( $page < 3 ) {
            //fixme time is moscow
                $v['date_add'] = ($this->db->UnixTimeStamp($v['date_add']) - (time() - 4*60*60 ) );
                if ( abs($v['date_add']) > 60*60 ) {
                    $v['date_add'] = round($v['date_add']/(60*60)) . '&nbsp;h';
                }
                elseif ( abs($v['date_add']) > 60) {
                    $v['date_add'] = round($v['date_add']/60) . '&nbsp;m';
                }
                else {
                    $v['date_add'] = $v['date_add'] . '&nbsp;s';
                }
            }

            //FIXME constract run!
            $s_obj = $this->new_model('search', $v['search'] );
            $s_obj->SetSearch( $v['search'] );
            $v['search_txt'] = $s_obj->to_info();
        }

        $tpl->assign('items',  $items);
        return $tpl->fetch('kostyl.tpl');
    } //function run

}

