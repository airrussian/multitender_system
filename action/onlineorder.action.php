<?php

class multitender_action_onlineorder extends multitender_action {

    private $outcaptcha = false;
    
    private $price = array(
        '0' => '',
        '1' => '8500',
        '2' => '8500',
        '3' => '1000',
        '4' => '',
        '5' => '',
        '6' => '8500',
        '7' => '3000',
        '8' => '3000',
    );
    private $titleSend = array(
        '.*' => 'тендерное обслуживание',
        'consaltplaytorg' => 'консультирование',
        'createaucorder' => 'подготовку аукционной заявки',
        'monitoring' => 'интеллектуальный мониторинг',
        'analiticsoncustomer' => 'аналитику по заказчику или конкуренту',
        'createaucorder' => 'подготовку аукционной заявки',
        'createconcursorder' => 'подготовку конкурсной заявки',
        'createkotirovokorder' => 'подготовку котировочной заявки',
        'expretiza' => 'экспертизу заявки участника торгов',
        'expretizadoc' => 'экспертизу документации заказчика',
        'rasyasneniedoc' => 'подготовку запроса на разъяснение',
        'analizproject' => 'анализ проекта контракта',
        'totalsoprovojdenieauc' => 'полное сопровождение электронных аукционов',
        'soprovojdenieauc' => 'сопровождение на аукционе',
        'jalobyetp' => 'подготовку жалобы',
        'tender-sopr' => 'тендерное сопровождение'
    );

    function get_title_send() {
        $url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : false;
        foreach ($this->titleSend as $preg => $val) {
            if (preg_match("#$preg#si", $url)) {
                $result = $val;
            }
        }
        return $result;
    }

    function validata($data) {
        if (empty($data['form']['email'])) {
            $this->errorMsg = 'Введите электронный почтовый адрес';
            return false;
        }
        if (!preg_match("#\S+?@\S+?\.\S{2,}#si", $data['form']['email'])) {
            $this->errorMsg = 'Введите корректный электронный почтовый адрес';
            return false;
        }
        if (empty($data['form']['name'])) {
            $this->errorMsg = 'Введите название организации';
            return false;
        }
        if (empty($data['form']['contact'])) {
            $this->errorMsg = 'Введите контактное лицо';
            return false;
        }
        if (empty($data['form']['telephone'])) {
            $this->errorMsg = 'Введите телефон';
            return false;
        }

        if (isset($data['recaptcha_challenge_field'])) {
            $this->outcaptcha = true;
            require_once dirname(__FILE__) . '/../libs_custom/recaptchalib.php';
            $privatekey = "код с странички которая появляется после добавления сайта(код называется, не перепутайте с предыдущим!! Private Key: длинный код)";
            $resp = recaptcha_check_answer('6LeTu-ESAAAAAF4QBs5W4GaGe51S8smNUClQPrGT', $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
            //$resp = recaptcha_check_answer('6Lea7uESAAAAABMvotwDclg0Z-wo6A2yXcAmLxIF', $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);            
            if (!$resp->is_valid) {
                $this->errorMsg = "Введен не верный код";
                return false;
            } 
        }

        return true;
    }

    function send($data) {
        $sendto = 'sbpost <sbpost@sbaspect.ru>';
        $sendfrom = 'multitender.online <support@multitender.ru>';

        $subject = "(Multitender.ru) Онлайн-заявка на " . $this->get_title_send();

        $tpl = $this->new_tpl();
        $tpl->assign('data', $data);
        $tpl->assign('title', $this->get_title_send());
        $message = $tpl->fetch("onlineorder_send.tpl");

        $email = new multitender_model_email();

        $email->send($subject, $message, 'sbpost <sbpost@sbaspect.ru>', $sendfrom);
        $email->send($subject, $message, 'airrussian <airrussian@mail.ru>', $sendfrom);
    }

    function spam($data) {
        if ($this->outcaptcha) return false;        
        if (preg_match("#http|www|com|net|ru|link#si", $data['form']['other']))
            return true;             
        if ($this->conf['user']['id'] <> $data['user_id'])
            return true;       
        if ($this->conf['user']['id'])
            return false;
        if (!preg_match("#mail|gmail|yandex|inbox|rambler|hotmail#si", $data['form']['email']))
            return true;
        return false;
    }
    
    function recaptcha() {
        require_once dirname(__FILE__) . '/../libs_custom/recaptchalib.php';
        $publickey = "6LeTu-ESAAAAAMFCzrbVd_x0Kr9-OjUJB7HwkUmX";
        //$publickey = "6Lea7uESAAAAAOoJdLvDdraCrL2wNR1NPCaKZD2U";        
        $html = recaptcha_get_html($publickey);       
        return $html;
    }

    function run() {
        if ($_REQUEST['task']=='captcha') {
            return $this->recaptcha();
        }
        
        $tpl = $this->new_tpl();
        $tpl->assign('link_base', $this->conf['pref']['link_base_http']);
        $tpl->assign('link_detail', $this->conf['pref']['link_detail']);

        $user_id = $this->conf['user']['id'];
        if (!isset($_POST['form'])) {
            if ($user_id) {
                $data = $this->new_model('firm')->load_prepared($user_id);
                $data['email'] = $this->conf['user']['email'];
                $data['other'] = "";
                if ($data['INN']) {
                    $data['other'].= "ИНН: {$data['INN']} \n";
                }
                if ($data['KPP']) {
                    $data['other'].= "KPP: {$data['KPP']} \n";
                }
                if ($data['domicile']) {
                    $data['other'].= "Юр.адрес: {$data['domicile_index']} {$data['domicile_region']} {$data['domicile_town']} {$data['domicile']}";
                }
            }
        } else {
            $data = $_POST;
            
            if ($this->validata($data)) {
                if (!$this->spam($data)) {
                    $this->send($data);
                    if ($_GET['tmpl'] == 'ajax') {
                        return "OK";                        
                    }                    
                    return "<div id='online-order'><div id='dfContactForm' style='font-size: 20px; padding: 30px; text-align: center'>Спасибо заявка отправлена.</form></div>";                    
                } else {
                    if ($_GET['tmpl'] == 'ajax') return "CAPTCHA";
                    $tpl->assign('captcha', $this->recaptcha());                        
                }
            } else {
                if ($this->outcaptcha) { $tpl->assign('captcha', $this->recaptcha()); }                
                $tpl->assign('errorMsg', $this->errorMsg);
                if ($_GET['tmpl'] == 'ajax') {
                    return $this->errorMsg;
                }
            }
        }

        if (isset($_GET['item_id'])) {
            $item_id = $_GET['item_id'];
            $item = $this->new_model('onlineorder')->get_item($item_id);
            if ($item['type_id'] && !preg_match("#consaltplaytorg#si", $_SERVER['REQUEST_URI'])) {
                $price = !empty($this->price[$item['type_id']]) ? $this->price[$item['type_id']] : $this->price[0];
                $tpl->assign('price', $price);
            }
        }

        $tpl->assign('data', isset($data) ? $data : false);
        $tpl->assign('item', isset($item) ? $item : false);        
        $tpl->assign('user_id', $user_id);        
        
        return $tpl->fetch("onlineorder.tpl");
    }

}
