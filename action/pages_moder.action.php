<?php
class multitender_action_pages_moder extends multitender_action {

    function  __construct() {
        parent::__construct();
        $rights = $this->conf['user']['right'];
        if ($rights<=1) {
    	    $tpl = $this->new_tpl();
    	    $tpl->display("msg_right.tpl");
    	    exit;
        }
    }

    function pages_show() {
        $tpl = $this->new_tpl();
        $mi = $this->new_model("moder_interface");

        $page = @ $_GET['page'] ? $_GET['page'] : 1;
        $rectopg = 25;
        $link_base = $this->conf['pref']['link_base'];
        $link = "action=pages_moder";

        $pages = $mi->get_list_pages($rectopg*($page-1), $rectopg);
        $total = $pages['total'];
        unset($pages['total']);

        $scrol = $this->new_action("scrol");
        $scrol->link = $link_base.$link.$scrol->link;
        $scrol->total = Ceil($total/$rectopg);
        $tpl->assign('scrol', $scrol->run());

        $tpl->assign("items", $pages);

        $mu = $this->new_model("users");
        $moders = $mu->get_list_moder();

        $tpl->assign("moders", $moders);

        return $tpl->fetch('pages_moder.tpl');
    }

    function page_add() {
        $user_id = $GLOBALS['tenders']['conf']['user']['id'];

        $url_page = @ $_POST['url_page'] ? $_POST['url_page'] : '';
        $mi = $this->new_model("moder_interface");
        $mi->add_pages($url_page, $user_id);
        return $this->pages_show();
    }

    function page_del() {
        $id_page = $_POST['id_page'];
        $mi = $this->new_model("moder_interface");
        $mi->del_pages($id_page);
        return $id_page;
    }

    function change_moderator() {
        $page_id  = @ $_POST['page_id'] ? $_POST['page_id'] : 0;
        $moder_id = @ $_POST['moder_id'] ? $_POST['moder_id'] : 0;
        if (!$moder_id || !page_id) {
            return false;
            exit();
        }

        $mi = $this->new_model("moder_interface");
        return $mi->change_moderator($page_id, $moder_id);
    }

    function run() {
        $user_id = $GLOBALS['tenders']['conf']['user']['id'];
        $GLOBALS['tenders']['conf']['page_joomla_links'][] = '<link rel="stylesheet" href="/templates/mt/css/moder.css" type="text/css"';

        if (!$user_id) {
            $tpl = $this->new_tpl();
            return $tpl->fetch("msg_auth.tpl");
            exit;
        }

        $task = isset($_GET['task']) ? $_GET['task'] : 'show';

        if (($task != 'show') && ($this->conf['user']['right'] <= 100 )) {
            $tpl = $this->new_tpl();
    	    return $tpl->display("msg_right.tpl");
        }

        switch ($task) {
            case 'pg_add':
                return $this->page_add();
                break;
            case 'pg_del':
                return $this->page_del();
                break;
            case 'change_moderator':
                return $this->change_moderator();
                break;
            case 'confirm':
                return $this->no_confirm_item();
                break;
            case 'show':
            default:
                return $this->pages_show();
                break;
        }
    }
}
