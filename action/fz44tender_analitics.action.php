<?php

class multitender_action_fz44tender_analitics extends multitender_action {
    
    private $url = "http://win.multitender.ru/index.php?controller=tender&action=analitic&tmpl=json";
    
    public function run() {        
        
        $purchaseNumber = isset($_REQUEST['purchaseNumber']) ? $_REQUEST['purchaseNumber'] : false;        
        if ($purchaseNumber) {
            $content = file_get_contents($this->url . "&purchaseNumber=" . $purchaseNumber);        
            $result = json_decode($content, TRUE);            
        }
        
        foreach ($result['applications'] as &$application) {
            $application['per_reduction'] = $application['reduction'] / $application['wins'];
            $avg_pre_reduction += $application['per_reduction'];            
        }
        
        $avg_pre_reduction = $avg_pre_reduction / count($result['applications']);
        
        $tpl = $this->new_tpl();        
        $tpl->assign('avg_pre_reduction', $avg_pre_reduction);
        $tpl->assign('tender', reset($result['tenders']));
        $tpl->assign('applications', $result['applications']);
        
        return $tpl->fetch('fz44tender_analitics/default.html.tpl');        
    }
        
    
}