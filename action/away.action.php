<?php
class multitender_action_away extends multitender_action {
    function run() {
        if (!isset($_GET['go']) || !isset($_GET['site_id'])) {
            return FALSE;
        }
        $am = $this->new_model('away');
        $am->click((int) $_GET['site_id']);
        $go = preg_match('~go=.*~', $_SERVER['REQUEST_URI'], $match);
        header("Location:" . str_replace('go=', '', $match[0]));
        exit;


    }
}