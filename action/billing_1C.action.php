<?php
class multitender_action_billing_1C extends multitender_action {
    function run() {
        exit("die");
        $this->db = & $this->conf['dbs']['person'];

        $from  = $this->db->BindTimeStamp(time() - 3*30*24*3600 );
        $where = "paid = 1 AND type_payment = 'cashless' AND datetime_adopted > '$from'";

        $firms = $this->db->GetArray('SELECT DISTINCT firms.* FROM firms, payments ' .
                "WHERE firms.user_id = payments.user_id AND $where ORDER BY user_id");
        $payments = $this->db->GetArray("SELECT * FROM payments WHERE $where ORDER BY id");

        $tpl = $this->new_tpl();
        $tpl->assign('from', $from);
        $tpl->assign('count', count($payments));

        $firm_obj = $this->new_model('firm');
        
        foreach ($firms as &$firm) {
            $firm = $firm_obj->prepare_firm($firm);
        }

        $tpl->assign('firms', $firms);
        $tpl->assign('payments', $payments);
        $return = $tpl->fetch('billing_1C.tpl');
        //header ('Content-Type: text/xml');
        echo $return;
        exit;
    }
}
