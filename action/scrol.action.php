<?php

class multitender_action_scrol extends multitender_action {
    public $link = '&page=';  //...page=
    public $total; // сколько элементов всего
    public $length=5; // длина туда-сюда
    public $page = 1; // текущая
    public $addelement = '';
    private $onpage_vals = array(10, 25, 50, 100, 200, 500);
    public $onpage = 50;
    public $onpage_link = NULL;

    function  __construct() {
        parent::__construct();
        $this->onpage = $this->conf['pref']['ppp'];
    }

    public function run() {
        $tpl = $this->new_tpl();
        
        // Номер текущей страницы
        $page = (isset ($_GET['page']) && is_numeric($_GET['page'])) ? (int)$_GET['page'] : $this->page;
        // Количество строк на страницу, если установлен onpage_link
        if (!is_null($this->onpage_link)) {
            if (isset($_GET['onpage']) && in_array($_GET['onpage'], $this->onpage_vals)) {
                setcookie('onpage', $_GET['onpage'], 0);
                $_COOKIE['onpage'] = $_GET['onpage'];
            }
            $this->onpage = (isset($_COOKIE['onpage']) && in_array($_COOKIE['onpage'], $this->onpage_vals)) ? $_COOKIE['onpage'] : $this->onpage;
            $onpage_html = '';
            foreach ($this->onpage_vals as $val) {
                if ($this->onpage == $val) {
                    $onpage_html .= '<b>' . $val . '</b> ';
                } else {
                    $onpage_html .= '<a href="' . $this->onpage_link . $val . '" rel="nofollow">' . $val . '</a> ';
                }
            }
            $tpl->assign('onpage_html', $onpage_html);
        }

        // Количетсво страниц
        $pages = ceil($this->total / $this->onpage);
        if ($pages < 2)
            return '';
        $tpl->assign('link', $this->link);
        $tpl->assign('pages', $pages);
        $tpl->assign('current', $page);
        $tpl->assign('total', $this->total);
        $tpl->assign('id', 'paginator' . mt_rand(1, 1000));

        $noscript_html = '';
        for ($i=1;$i<=$pages;$i++) {
            if ($i != $page) {
                $noscript_html .= '<a href="' . $this->link . $i . '">' . $i . '</a> ';
            } else {
                $noscript_html .= "<b>$i</b> ";
            }
        }
        $tpl->assign('noscript_html', $noscript_html);

        $this->page = $page;
        return $tpl->fetch('scrol.tpl');

    }
    
    function run2() {
        $tpl = $this->new_tpl();

        $this->page = (int) @$_GET['page'];

	if ( $this->page <= 0 ) {
	    $this->page = 1;
	}

        if ( $this->page > $this->total ) {
            $this->page = $this->total;
        }

        $scrol = array();

        if ( $this->page > 1 ) {
            $start = $this->page - $this->length;
            $start = $start > 1 ? $start : 1;

            if ( ( $this->page - $this->length ) >= 2 ) {
                $scrol[] = array('name' => '<', 'link' => $this->link . $start . $this->addelement);
                $start++;
            }

            for ( $i = $start; $i < $this->page; $i++ ) {
                $scrol[] = array('name' => $i, 'link' => $this->link . $i . $this->addelement);
                
            }
        }

        $scrol[] = array('name' => $this->page, 'link' => null);

        if ( $this->page < $this->total ) {
            $end = $this->page + $this->length;
            $end = $end <= $this->total ? $end : $this->total;

            for ( $i = ($this->page + 1); $i < $end; $i++ ) {
                $scrol[] = array('name' => $i, 'link' => $this->link . $i . $this->addelement);
            }

            if ( $this->total > $end ) {
                $scrol[] = array('name' => '>', 'link' => $this->link . $end . $this->addelement);
            }
            else {
                $scrol[] = array('name' => $end, 'link' => $this->link . $end . $this->addelement );
            }

        }

        if ($this->total == 1) {
            $scrol = array();
            $tpl->assign('scrol', $scrol);

            return $tpl->fetch('scrol.tpl');
        }

        $tpl->assign('scrol', $scrol);

        return $tpl->fetch('scrol.tpl');
    } //function run
}
