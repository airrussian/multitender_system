<?php
class multitender_action_bigtenders extends multitender_action {

    function run() {
        $tpl = $this->new_tpl();
        $page = @$_GET['page'] ? $_GET['page'] : 1;

        $rs = $this->db->SelectLimit("
SELECT SQL_CALC_FOUND_ROWS
   item.id, item.num, item.name, item.date_conf, item.date_publication, item.date_end, item.price,
   item.date_add,
   site.name as site_name,
   region.name as region_name,
   type.name as type_name
   FROM `item`, `site`, `region`, `type`
   WHERE item.site_id = site.id AND item.region_id = region.id AND item.type_id = type.id
   AND date_end > NOW()
   ORDER BY price DESC",
            $this->conf['pref']['ppp'], ($page - 1) * $this->conf['pref']['ppp']);

        $items = $rs->GetArray();

        $total = $this->db->GetArray("SELECT FOUND_ROWS()");
        $total = $total[0][0];

        $page = @$_GET['page'];
        $page = $page ? $page : 1;

        $link = "?action=bigtenders&amp;page=";

        foreach($items as &$item) {
            $item['link'] = $this->conf['pref']['link_detail'] . $item['id'];

            if( !empty( $search['main'] ) ) {
                $search['main'] = trim($search['main']);
                $highlight = preg_replace("/\s+/u", "|", preg_quote( $search['main'], '/') );
                //FIXME следить за входными параметрами!!
                //FIXME: tag u - kostyl
                $item["name"] = preg_replace("/$highlight/xiu", '<font color=#cc0000>\\0</font>', $item["name"]);
            }
        }

        $tpl->assign('items', $items);

        $tpl->assign('name', @$_GET['name']);

        $scrol = $this->new_action( 'scrol' );
        $scrol->link  = $link;
        $scrol->total = ceil($total / $this->conf['pref']['ppp']);
        $tpl->assign( 'scrol',  $scrol->run() );

        $tpl->assign('total', $total);

        $tpl->assign('link_empty', 'main.php?action=view&');

        $tpl->assign('link_save', 'main.php?action=search_save&');

        $tpl->assign('list',  $tpl->fetch('list.tpl'));

        return $tpl->fetch('bigtenders.tpl');
    } //function run

}

