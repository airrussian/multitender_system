<?php

class multitender_action_email_newsletter extends multitender_action {
    
    function get_status($search_id) {
        // Вернет ссылку HTML состояния запроса $search_id
        $user_id = $this->conf['user']['id'];
        if (!$user_id || !$search_id) { return false; }

        $mod_en = $this->new_model("email_newsletter");
        $id = $mod_en->check_newsletter($user_id, $search_id);
        if ($id) {
            $text   = "запрос в e-mail подписке";
            $title  = "перейти в настройки e-mail рассылки";
            $onclick= "";
            $href   = "/tenders/email_newsletter";
            $class  = "search-email-set";
        } else {
            $text   = "добавить в e-mail подписку";
            $title  = "нажмите для добавления текущего запроса в e-mail рассылку";
            $onclick= "onclick=\"javascript: email_newsletter($(this), 'add', $search_id); return false;\"";
            $href   = "/ajax.php?action=email_newsletter&task=add&sid=$search_id";
            $class  = "search-email-add";
        }
        return "<a href=\"$href\" title=\"$title\" $onclick class=\"$class\">$text</a>";
    }

    function add($user_id, $search_id) {
        // Добавляет запрос для пользователя в подписку
        $nl=$this->new_model("email_newsletter");
        $nl=$nl->add_newsletter($user_id, $search_id);
        return $this->get_status($search_id);
    }

    function del($user_id, $search_id) {
        // Удаляет запрос из подписки пользователя
        $nl=$this->new_model("email_newsletter");
        $nl->del_newsletter($user_id, $search_id);
        return $this->get_list($user_id);
    }

    function get_list($user_id) {

        $tpl = $this->new_tpl();

        $rectopg = $this->conf['db_conf']['person']['email_newsletter_rectopg'];
        $link_base = $this->conf['pref']['link_base'];       

        $page = @ $_GET['page'] ? $_GET['page'] : 1;

        $nl=$this->new_model("email_newsletter");

        $subscribe=$nl->get_list_newsletter($user_id, ($page-1)*$rectopg, $rectopg);
        $total = $subscribe['total'];
        unset($subscribe['total']);

        $scroll = new multitender_action_scrol();
        $scroll->onpage = $rectopg;       
        $scroll->total = $total;
        $scroll->link = "/tenders/email_newsletter?page=";

        $search_ids = array();
        foreach ($subscribe as $row) {
            $search_ids[] = $row['search_id'];
        }

        $lq=$this->new_model("list_queries");
        $searches=$lq->getList($search_ids);

        $qModel = $this->new_model('search');

        $list = array();
        if ($searches) {
            foreach( $searches as $search ) {
                $qModel->SetSearch( unserialize($search['search']) );
                $list[$search['id']] = array(
                    'txt'       =>  $qModel->to_info(),
                    'url'       =>  $qModel->to_url(),
                    'main'      =>  $search['main'],
                    'total'     =>  $search['total'],
                );
            }
        }

        $subscribe_temp = array();
        foreach ($subscribe as $row) {
            if (!empty($list[$row['search_id']])) {
                $subscribe_temp[] = array_merge($row, $list[$row['search_id']]);
            }
        }


        $tpl->assign("link", $this->conf['pref']['link_base']);
        $tpl->assign("subscribe", $subscribe_temp);
        $tpl->assign("total", $total);
        $tpl->assign("status", $nl->get_status($user_id));

        $tpl->assign("scroll", $scroll->run());
        return $tpl->fetch("email_newsletter_list.tpl");
    }

    function show($user_id) {
        $this->conf['page_title'] = 'Настройки E-mail рассылки';

        $tpl = $this->new_tpl();
        $tpl->assign('list_newsletter', $this->get_list($user_id));
        $nl=$this->new_model("email_newsletter");
        $tpl->assign("time", $nl->get_time_newsletter($user_id));
        return $tpl->fetch("email_newsletter_form.tpl");
    }

    function set_time($user_id) {
        $set_time = isset($_GET['set_time']) ? $_GET['set_time'] : 0;
        $en=$this->new_model("email_newsletter");
        $search_ids=$en->set_time_newsletter($user_id, $set_time);
    }

    function change_freq($user_id, $search_id) {
        $freq = (int) isset($_GET['value']) ? $_GET['value'] : false;
        if (!$freq) { return false; }
        if ($limit>7) { $limit = 7; }

        $nl=$this->new_model("email_newsletter");
        return (string) $nl->change_freq($freq, $user_id, $search_id);
    }

    function change_limit($user_id, $search_id) {
        $limit = (int) isset($_GET['value']) ? $_GET['value'] : false;
        if (!$limit) { return false; }
        if ($limit<10) { $limit = 10; }
        if ($limit>50) { $limit = 50; }

        $nl=$this->new_model("email_newsletter");
        return (string) $nl->change_limit($limit, $user_id, $search_id);
    }

    function change_status($user_id) {
        $nl=$this->new_model("email_newsletter");
        $status = $nl->get_status($user_id);
        return $nl->change_status($user_id, 1-$status);
    }

    function run() {
        $user_id = $this->conf['user']['id'];

        if (!$user_id) {
            $tpl = $this->new_tpl();
            return $tpl->fetch("msg_auth.tpl");
        }

        $search_id=isset($_GET['sid']) ? $_GET['sid'] : false;
        /* Свитч на действия данного action */
        $task = isset($_GET['task']) ? $_GET['task'] : 'show';
        /* Определяем задачу для этого action */
        switch ($task) {
            case 'add': return $this->add($user_id, $search_id);
            case 'del': return $this->del($user_id, $search_id);               
            case 'list': return $this->get_list($user_id);
            case 'set_time' : return $this->set_time($user_id, $search_id);
            case 'status': return $this->change_status($user_id);
            case 'freq': return $this->change_freq($user_id, $search_id);
            case 'limit': return $this->change_limit($user_id, $search_id);;
            case 'show': default: return $this->show($user_id);               
        }
    }

}

