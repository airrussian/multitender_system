<?php

class multitender_action_send_moderator extends multitender_action {

    function run() {
        $user_id = $GLOBALS['tenders']['conf']['user']['id'];
        $item_id = (int) isset($_POST['item_id']) ? $_POST['item_id'] : 0;
        $comment = isset($_POST['comment']) ? $_POST['comment'] : false;

        $to     = $this->conf['email_error_to'];
        $from   = $this->conf['email_error_from'];

        if (!$user_id || !$item_id) { return false; }

        $subject = 'Ошибка в тендере';

        $message = "<h1>Ошибка в тендере №$item_id</h1>";
        $message.= "Пользователь ID$user_id сообщает: <br /> ";
        $message.= "<pre>$comment</pre> <br />";
        $message.= "<hr />";

        $sm = $this->new_model("send_moderator");
        if ($sm->bad_item($user_id, $item_id, $comment)) {
            $email = $this->new_model("email");
            $email->send($subject, $message, $to, $from);
            return "<ans>OK</ans>";
        } else {
            return false;
        }
    }

}
