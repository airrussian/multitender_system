<?php
/*
 * Страница со списком регионов с заказчиками
*/

class multitender_action_by_customer extends multitender_action {

    function run() {

        $region_id = @ $_GET['region_id'] ? $_GET['region_id'] : NULL;
        $filter = @ $_REQUEST['filter'] ? $_REQUEST['filter'] : NULL;

        $filter = preg_replace('#[^a-zа-яё0-9/_]#iu', ' ', $filter);
        $filter = trim(preg_replace('/\s+/', ' ', $filter));
        $filter = trim($filter);

        $names = explode(' ', $filter);
        $filter = '';
        foreach ($names as $v) {
            if (mb_strlen($v,'UTF-8') >= 3) {
                $filter .= $v . ' ';
            }
        }
        $filter = trim($filter);

        $cm = $this->new_model('customer');

        $link_base = $this->conf['pref']['link_base'];
        $link_act  = "action=by_customer";
        $rectopg = 25;
        $page = @ $_GET['page'] ? $_GET['page'] : 1;

        if ($region_id) {
            $region = $cm->get_date_region($region_id);
            $tpl = $this->new_tpl();
            $offset = $rectopg*($page-1);

            $customers = $cm->get_customers_by($region_id, $offset, $rectopg, $filter);
            $total = $customers['total'];
            unset($customers['total']);

            $ids = array();
            foreach ($customers as &$customer) {
                if ($filter) {
                    $names = explode(' ', $filter);
                    foreach ($names as $v) {
                        $customer["name"] = preg_replace("/$v/iu", '<font color=#cc0000>\\0</font>', $customer["name"]);
                    }
                }
                $ids[] = $customer['id'];
            }

            if ($items = $cm->get_items_by_ids($ids)) {
                foreach ($customers as &$customer) {
                    foreach ($items as $item) {
                        if ($item['customer_id']==$customer['id']) {
                            $customer['count_items'] = $item['count'];
                        }
                    }
                }

            }

            $scrol = $this->new_action("scrol");
            $link = "&region_id=".$region_id;
            if ($filter) {
                $fr = "&filter=$filter";
            } else {
                $fr = "";
            }
            $scrol->link = $link_base.$link_act.$link.$fr.$scrol->link;
            $scrol->total = $total;
            $tpl->assign('scrol', $scrol->run());
            $tpl->assign('filter', $filter);
            $tpl->assign('total', $total);
            $tpl->assign('link_base', $link_base);
            $tpl->assign('link_act', $link_act);
            $tpl->assign('region', $region);
            $tpl->assign('customers', $customers);

            return $tpl->fetch("by_customer_reg.tpl");

        } else {
            $tpl = $this->new_tpl();
            $tpl->caching = false;
            $tpl->cache_lifetime = mt_rand(20, 45) * 59;

            if ($tpl->is_cached('by_customer.tpl')) {
                return $tpl->fetch('by_customer.tpl');
            }

            $data = multitender_model_data_common::singleton()->get_data();

            $cust = $this->db->GetAll("SELECT COUNT(*) as count, region.id as region_id FROM region, customer WHERE customer.region_id = region.id GROUP BY customer.region_id");
            foreach ($cust as $c) { $arr[$c['region_id']] = $c['count']; }

            foreach ($data['region'] as $region) {
                $region['count'] = isset($arr[$region['id']]) ? (int)$arr[$region['id']] : 0;

                $region['link'] = 'by_customer/?region_id='.$region['id'];

                $contry_id = Ceil($region['id'] / 100);

                if ($region['id'] % 100==0) {
                    $contry[$contry_id]['info'] = $region;
                    continue;
                }

                if ($region['count']==0) { continue; }

                $firstletter = mb_substr($region['name'], 0, 1, 'utf-8');
                $group = 0;
                if (preg_match("#республика#sui", $region['name'])) {
                    if (preg_match("#республика ([A-я]{1})#sui", $region['name'], $fl)) {
                        $firstletter = $fl[1];
                    }
                    if ($contry_id==1) {
                        $group=1;
                        $region['name'] = trim(preg_replace("#республика#siu", "", $region['name']));
                    }
                }

                $contry[$contry_id][$group][$firstletter][] = $region;
            }

            ksort($contry);

            for ($i=1; $i<=count($contry); $i++) {
                foreach ($contry[$i] as $key => $group) {
                    $s = 0;
                    foreach ($group as $letter) {
                        $s += count($letter);
                    }
                    ksort($group);
                    $contry[$i][$key] = $group;
                    $contry[$i][$key]['heightcolumn'] = Ceil($s / 3)-1;
                }
            }

            $list = $this->new_action('region_view');
            $list = $list->run('customer');
            $tpl->assign('contries', $contry);
            return $tpl->fetch('by_customer.tpl');
        }
    }

}

