<?php

class multitender_action_cabinet extends multitender_action {

    function run() {
        $this->conf['page_title'] = 'Личный кабинет';

        $user_id = $this->conf['user']['id'];

        $tpl = $this->new_tpl();

        if (!$user_id) {
            return $tpl->fetch("msg_auth.tpl");
        }

        $bl = $this->new_model("billing");
        $user = $bl->getAccess($user_id);

        if ($user['todate'] > date('Y-m-d')) {
            $user['dateoffinish'] = date2rus($user['todate'], '&nbsp;');
            $tpl->assign('todate', Ceil( (strtotime($user['todate']) - time()) / (60*60*24) ));
            $tpl->assign('dateoffinish', $user['dateoffinish']);
        }
        if ($user['tenders'] > 0) {
            $tpl->assign('tenders', $user['tenders']);
        }

        $tpl->assign('messages', 0);//здесь должна быть инфа о сообщениях

        $firm = $this->new_model('firm')->load_prepared($user_id);

        $link = $this->conf['pref']['link_base'];
        $tpl->assign("baselink", $link);
        $tpl->assign("firm", $firm);
        return $tpl->fetch("cabinet.tpl");
    }

}