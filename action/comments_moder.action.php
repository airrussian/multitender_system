<?php

class multitender_action_comments_moder extends multitender_action {

    function run() {
        $user = $GLOBALS['tenders']['conf']['user'];
        $page = @ $_GET['page'] ? $_GET['page'] : 1;
        $del = @ $_GET['del'] ? $_GET['del'] : NULL;
        $filter = @ $_REQUEST['filter'] ? $_REQUEST['filter'] : NULL;
        
        $rights = $this->conf['user']['right'];

        if ($rights<=1) {
            $tpl2 = $this->new_tpl();
            return $tpl2->fetch('msg_right.tpl');
        }

        $filter = preg_replace('#[^a-zа-яё0-9/_]#iu', ' ', $filter);
        $filter = trim(preg_replace('/\s+/', ' ', $filter));
        $filter = trim($filter);

        $names = explode(' ', $filter);
        $filter = '';
        foreach ($names as $v) {
            if (mb_strlen($v,'UTF-8') >= 3) {
                $filter .= $v . ' ';
            }
        }
        $filter = trim($filter);

        $cm = $this->new_model('comments_moder');

        if ($rights>100) {
            $cm->del_rec($del);
        }
        $rectopg = 50;
        $offset = $rectopg*($page-1);
        
        $comments = $cm->select_comments($offset,$rectopg,$filter);
        $total = $comments['total'];
        unset($comments['total']);

        foreach ($comments as &$item123) {
            $temp=explode(" ",$item123['text']);
            foreach ($temp as &$a) {
                $a = substr($a, 0, 100);
            }
            $item123['text'] = implode($temp, " ");
            $item123['date_time'] = date("d.m.Y H:i:s",strtotime($item123['date_time']));
            $item123['text'] = htmlspecialchars($item123['text'], ENT_QUOTES);
            $item123['text'] = str_replace('&lt;br/&gt;', '<br/>', $item123['text']);
            $item123['text'] = str_replace('&lt;br&gt;', '<br>', $item123['text']);
            $item123['text'] = str_replace('&lt;br /&gt;', '<br>', $item123['text']);
            if ($filter) {
                $names = explode(' ', $filter);
                foreach ($names as $v) {
                    $item123["name"] = preg_replace("/$v/iu", '<font color=#cc0000>\\0</font>', @ $item123["name"]);
                    $item123["tender_id"] = preg_replace("/$v/iu", '<font color=#cc0000>\\0</font>', @ $item123["tender_id"]);
                    $item123["text"] = preg_replace("/$v/iu", '<font color=#cc0000>\\0</font>', @ $item123["text"]);
                }
            }
        }
        
        $scrol = $this->new_action("scrol");
        $scrol->addelement = '#comments';
        $scrol->link = "/tenders/comments_moder" . '?' . substr($scrol->link, 1);
        $scrol->total = Ceil($total/$rectopg);
        
        $tpl = $this->new_tpl();
        
        $tpl->assign('scrol', $scrol->run());
        $tpl->assign('comments', $comments);
        
        return $tpl->fetch('comments_moder.tpl');
    }
}