<?php

class multitender_action_ezp_kontur extends multitender_action {
    
    private $pathSaveXML = "/home/tenders/www/beta.multitender.ru/tmp/ezp";
    private $addressSendXML = "http://clientinfo.kontur.ru/orderrecipient";
    
    private function generationXML($data) {
        $tpl = $this->new_tpl();
        
        $fio = $data['organization']['personal']['fio'];
        preg_match("#(.*?)\s(.*?)\s(.*)#", $fio, $FIO);
        $data['organization']['personal']['FirstName'] = $FIO[1];
        $data['organization']['personal']['LastName'] = $FIO[2];       
        $data['Created'] = date("Y-m-d\TH:i:s.UP");
        
        
        $tpl->assign("data", $data);
        return $tpl->fetch("ezp_kontur_xml.tpl");
    }
    
    private function validateData($data) {
        return true; 
    }
    
    private function generationFileName($data) {
        $counter = file_get_contents($this->pathSaveXML . "/counter.txt");
        if (!$counter) { $counter = 1; }
        $result = "multitender.ru-" . $data['organization']['INN'];
        if ($data['organization']['KPP']) {
            $result.= "-" . $data['organization']['KPP'];
        }
        $result.="-$counter";
        $result.=".xml";
        $counter++;
        file_put_contents($this->pathSaveXML . "/counter.txt", $counter);
        return $result;
    }

    private function SendXMLtoKontur($filename, $xml) {
        file_put_contents($this->pathSaveXML . '/' . $filename, $xml);
        $this->addressSendXML = "http://94.73.211.6/orderrecipient";
        $upload = $this->pathSaveXML . '/' . $filename;
        $postdata = array( 'orderXml' => "@".$upload );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->addressSendXML);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_exec($ch); 
        curl_close($ch);       
    }
       
    function run() {
        $tpl = $this->new_tpl();
        
        if (isset($_POST['OrderSubmit'])) {
            $data = $_POST;
            if ($this->validateData($data)) {
                $xml = $this->generationXML($data);
                $filename = $this->generationFileName($data);
                $this->SendXMLtoKontur($filename, $xml);
            }            
        } else {
            $user = $this->conf['user'];
            $data['organization']['email'] = $user['email'];
            if ($user['id']>0) {
                $firmModel = $this->new_model('firm');
                $firm = $firmModel->load($user['id']);
                if ($firm) {
                    $data['organization']['name'] = $firm['name'];
                    $data['organization']['personal']['fio'] = $firm['contact'];
                    $data['organization']['telephone'] = $firm['telephone'];
                    $data['organization']['INN'] = $firm['INN'];
                    if (isset($firm['KPP'])) { $data['organization']['KPP'] = $firm['KPP']; }
                }
            }
        }
        $tpl->assign("data", $data);            
        return $tpl->fetch("ezp_kontur.tpl");
    }
    
}