<?php

class multitender_action_banerRedirect extends multitender_action {
    
    function run() {
        $link = $_GET['link']?$_GET['link']:false;

        $model = $this->new_model('banerRedirect');

        if ($link) {
            $user = $GLOBALS['tenders']['conf']['user'];
            if ($user['id']>=100 || $user['id']==0) {
                $link = $model->plusOne($link);
            } else {
                $link = $model->getLink($link);
            }
        } else {
            header('location:/');
        }

        if ($link) {
            header('location:' . $link);
        } else {
            header('location:/');
        }
    }
    
}
