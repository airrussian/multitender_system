<?php

class multitender_action_card_user extends multitender_action {

    function  __construct() {
        parent::__construct();
        $rights = $this->conf['user']['right'];
        if ($rights<=50) {
    	    $tpl = $this->new_tpl();
    	    $tpl->display("msg_right.tpl");
    	    exit;
        }
    }

    function fullcard_show($user_id) {
        $tpl=$this->new_tpl();

        $card_user = new multitender_model_card_user();

        if ( ($us = $card_user->get_user($user_id)) ) {
            $user = array();
            $user['joomla'] = $card_user->get_user_joomla($user_id);
            $user['access'] = $card_user->get_access($user_id);
            if (strtotime($user['access']['todate']) > time()) {
                $user['access']['count_days'] = Ceil( (strtotime($user['access']['todate']) - time()) / (60*60*24) );
            }
            $user['favorite'] = $card_user->get_favorites($user_id);
            $user['system']['user']['name'] = $us['name'];
            $user['system']['user']['rights'] = $us['rights'];
            $user['system']['search']['count'] = $card_user->get_count_search($user_id);
            $user['system']['subscribe']['count'] = $card_user->get_email_subscribe_count($user_id);
            $user['system']['subscribe']['last_send']  = $card_user->get_email_subscribe_last_send($user_id);
            $user['system']['subscribe']['list'] = $card_user->get_email_subscribe_list($user_id);
            $user['payments'] = $card_user->get_count_payments($user_id);
            $user['payments']['buy'] = $card_user->get_count_payments($user_id, false);
            $user['tenders'] = $card_user->get_tenders($user_id);

            $user['users'] = $us;

            $last_query = $card_user->get_last_query($user_id);

            $tpl->assign('last_query', $last_query);

            $link_detail = $this->conf['pref']['link_detail'];

            $fav_tender = $this->new_model("favorite_tenders");
            $ids = $fav_tender->get_fav_tenders($user_id, 0, 5);
            $tpl->assign('fav_ten_count', count($ids));
            $ft = $fav_tender->get_title($ids);


            $short_max = 150;
            foreach ($ft as &$f) {
                if (mb_strlen($f['name']) > $short_max) {
                    $f['name'] = mb_substr($f['name'], 0, $short_max)."...";
                }
            }
            $tpl->assign('link_detail', $link_detail);
            $tpl->assign('fav_tender', $ft);

            $billing = $this->new_model('billing');
            $rates = $billing->get_rates();
            $kind_rates = $billing->get_kind_rates();
            $payments = $billing->get_payments($user_id, 0, 5);

            // FIXME опять копипаста
            if ($payments) {
                foreach ($payments as &$pay) {
                    foreach ($kind_rates as $kr) {
                        if ($pay['kind_rates_id']==$kr['id']) {
                            $pay['kind_rates'] = $kr['name'];
                        }
                    }
                    if ($pay['type_payment']=="electron") {
                        $rb = $this->new_action("robokassa");
                        $pay['robokasse'] = $rb->button($pay['summa'], $pay['id'], "Оплата услуг " . $this->conf['pref']['site_name']);
                        $pay['type_payment_rus'] = "Электронный";
                    } else {
                        $pay['type_payment_rus'] = "Безналичный";
                    }
                    $pay['datetime']=date2str($pay['datetime']);
                    $pay['datetime_adopted']=date2str($pay['datetime_adopted']);
                }
            } else {
                $payments = null;
            }
            $tpl->assign('payments', $payments);
            $tpl->assign('link_base', $this->conf['pref']['link_base']);
            $tpl->assign('firm', $this->firmcard_show($user_id, false));

        } else {
            $user = false;
        }

        $tpl->assign('user_id', $user_id);
        $tpl->assign('user', $user);

        return $tpl->fetch("card_user.tpl");
    }

    function firmcard_show($user_id, $full_link=true) {
        $tpl=$this->new_tpl();

        $firm   = $this->new_model('firm')->load_prepared($user_id);
        $joomla = $this->new_model('card_user')->get_user_joomla($user_id);

        $tpl->assign('joomla',    $joomla);
        $tpl->assign('firm',      $firm);
        $tpl->assign('user_id',   $user_id);
        $tpl->assign('full_link', $full_link);
        $tpl->assign('link_base', $this->conf['pref']['link_base']);

        return $tpl->fetch('card_firm.tpl');
    }

    function setaccess($user_id) {
        $accessModel = new multitender_model_billing();
        if ($_GET['p']=='overworld') {
            $accessModel->updateAccess($user_id, '2012-12-21');
        } else {
            $access = $accessModel->getAccess($user_id);
            if ($access) {
                $todate = date("Y-m-d", strtotime($access['todate']) + 7*24*60*60);
            } else {
                $todate = date("Y-m-d", time() + 7*24*60*60);
            }
            $accessModel->updateAccess($user_id, $todate);
        }
        header('Location:'.$_SERVER['HTTP_REFERER']);
        exit;
    }

    function create($user_id) {
        $users = new multitender_model_users();
        $users->id = NULL;
        $users->joomla_id = $user_id;
        $user_id = $users->get_user_id();
        header('Location:'.$this->conf['pref']['link_base'].'action=card_user&task=fullcard_show&user_id='.$user_id);
        exit;
    }

    function run() {
        $user_id = @ $_GET['user_id'] ? $_GET['user_id'] : 0;
        if (!$user_id) {
            return false;
        }
        $task = @ $_GET['task'] ? $_GET['task'] : 'firmcard';
        switch ($task) {
            case 'fullcard':
                return $this->fullcard_show($user_id);
                break;
            case 'firmcard':
                return $this->firmcard_show($user_id);
                break;
            case 'setaccess':
                return $this->setaccess($user_id);
                break;
            case 'add':
                return $this->create($user_id);
                break;
            default:
                return $this->fullcard_show($user_id);
                break;
        }
    }

}