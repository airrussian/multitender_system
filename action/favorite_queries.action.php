<?php

class multitender_action_favorite_queries extends multitender_action {
 
    function show() {
        $this->conf['page_title'] = 'Избранные запросы';

        $tpl = $this->new_tpl();

        $user_id = $this->conf['user']['id'];

        $link = $this->conf['pref']['link_base'];

        $fq      = new multitender_model_favorite_queries();
        $queries = new multitender_model_list_queries();

        $tpl = $this->new_tpl();

        $scrol = new multitender_action_scrol();
        
        $this->total = $fq->get_favorities_total($user_id);

        $scrol->link="/tenders/favorite_queries/?task=show".$scrol->link;

        $scrol->onpage = 30;
        $scrol->total = $this->total;

        $tpl->assign('scrol', $scrol->run());

        $offset=($scrol->page-1) * $scrol->onpage;

        if ($this->total) {
            $ids = $fq->get_favorities($user_id,$offset,$scrol->onpage);

            // 2010-08-24 02:53:27
            // $date = strtotime($this->conf['user']['lastvisitDate']);

            //$this->conf['user']['last_visit'];
            $last_visit = (int)$this->conf['user']['last_visit'];

            if ($last_visit >= date('Ymd')) {
                $last_visit = date('Ymd', time() - (24*60*60));
            }

            $last_visit = date('Y-m-d', strtotime($last_visit));
            $three      = date('Y-m-d', time() - 7*(24*60*60));

            $tpl->assign('date_new',   $last_visit);
            $tpl->assign('date_three', $three);

            $total_three = $fq->get_total_by_date($ids, $three);
            $total_new   = $fq->get_total_by_date($ids, $last_visit);

            $total_stat = $fq->get_stat($ids,  date('Y-m-d', time() - 7*(24*60*60)));

            $items = $queries->getList($ids);
            if ($items) {
                $s_obj = new multitender_model_search();
                foreach( $items as &$v ) {
                    $v['search'] = unserialize($v['search']);
                    $s_obj->SetSearch( $v['search'] );
                    $v['search_txt'] = $s_obj->to_info();
                    $v['search_url'] = $s_obj->to_url();

                    $v['name'] = trim(@$v['search']['main']);
                    if ($v['search_txt']) {
                        $v['name'] .= " ({$v['search_txt']})";
                    }
                    if ( ! $v['name'] ) {
                        $v['name'] = "[ЗАПРОС]";
                    }

                    if ( isset($total_three[$v['id']]) ) {
                        $v['total_three'] = $v['total'] - $total_three[$v['id']];
                    } else {
                        $v['total_three'] = '';
                    }

                    if ( isset($total_new[$v['id']]) ) {
                        $v['total_new'] = $v['total'] - $total_new[$v['id']];
                    } else {
                        $v['total_new'] = '';
                    }

                    if ( isset($total_stat[$v['id']]) ) {
                        $v['stat'] = $total_stat[$v['id']];
                    }
                }
                $tpl->assign('items', $items);
            }
        }
        
        $tpl->assign('link', $link);

        return $tpl->fetch('favorite_queries.tpl');
    }

     function del_and_update() {
        $this->del();
        if (isset($_SERVER['HTTP_REFERER'])) {
            if (preg_match("#page=(\d+)#i", $_SERVER['HTTP_REFERER'], $page)) {
                $_GET['page'] = $page[1];
            }
        }
        return $this->list_show();
    }

    function add() {
        if (empty($_GET['sid'])) { return false; }
        $search_id = $_GET['sid'];
        $user_id = $this->conf['user']['id'];
        
        $fq = new multitender_model_favorite_queries();
        $fq->add_favorite($user_id, $search_id);

        $status = $this->get_status($search_id);
        return $status[$search_id];
    }

    function del() {
        if (empty($_GET['sid'])) { return false; }
        $search_id = $_GET['sid'];
        $user_id = $this->conf['user']['id'];

        $fq = new multitender_model_favorite_queries();
        $fq->del_favorite($user_id, $search_id);

        $status = $this->get_status($search_id);       
        return $status[$search_id];
    }

    function get_status($qids) {
        $user_id = $this->conf['user']['id'];
        if (!is_array($qids)) { $qids = array($qids); }

        $modq = new multitender_model_favorite_queries();
        $favq = $modq->get_favorite_byid($user_id, $qids);

        $return = array();
        foreach ($qids as $qid) {
            $href   = "/ajax.php?action=favorite_queries&task=";
            $title  = "добавить в избранные запросы";
            $status = "no";
            if (!empty($favq) && in_array($qid, $favq)) {
                $title  = "удалить из избранных запросов";
                $task   = "del";
                $status = "in";
            } else {
                $task   = "add";
            }
            $href .= $task."&sid=". $qid;
            $return[$qid] = "<a rel='nofollow' href='$href' title='$title' class='favorite-query $status' onclick=\"javascript: favorite_query($(this), '$task', '$qid'); return false;\"><span>$title</span>&nbsp;</a>";
        }

        return $return;
    }

    function run() {

        /* Получаем задачу для action из GET */
        $task = @ $_GET['task'] ? $_GET['task'] : 'show';
        /* Определяем задачу для этого action */
        switch ($task) {
            case 'del_and_update': echo $this->del_and_update();
                break;
            case 'show': echo $this->show();
                break;
            case 'update' : echo $this->list_show();
                break;
            case 'add': return $this->add();
                break;
            case 'del': return $this->del();
                break;
        }
    }

}