<?php

class multitender_action_banerrota extends multitender_action {
    function run() {
        $cm = $this->new_model('banerrota');
        $tpl = $this->new_tpl();
        $user = $GLOBALS['tenders']['conf']['user'];

        $geoip_m = new multitender_model_geoip();

        $region_id = null;
        if (isset($_COOKIE['tc_region'])) {
            $region_id = (int) $_COOKIE['tc_region'];
        }

        if ( is_null($region_id) ) {
            $region_id = $geoip_m->get_region();
            $region_id = (int) $region_id; // кэшируем 0
            setcookie('tc_region', $region_id, 0, '/');
        }

        $otvet = $cm->get_rand_baner_horiz($region_id);
        $root = $this->is_root();
        if ($otvet && $root == true) {
            $tpl->assign('flag',$otvet['type']);

            if($otvet['type'] == 1) {
                if (isset($otvet[0]['text'])) {
                    $tpl->assign('direct1',$otvet[0]['text']);
                } else {
                    $ext = explode('.', $otvet[0]['pathway']);
                    $ext = $ext[count($ext)-1];
                    if ($ext == 'swf') {
                        $tag1 = 1;
                    } else {
                        $tag1 = 0;
                    }
                    $tpl->assign('tag1', $tag1);
                    $tpl->assign('baner1', $otvet[0]['pathway']);
                    $tpl->assign('link1', $otvet[0]['link']);
                    if ($user['id']>=100 || $user['id']==0) {
                        $cm->updateShows($otvet[0]['link']);
                    }
                }
                if (isset($otvet[1]['text'])) {
                    $tpl->assign('direct2',$otvet[1]['text']);
                } else {
                    $ext = explode('.', $otvet[1]['pathway']);
                    $ext = $ext[count($ext)-1];
                    if ($ext == 'swf') {
                        $tag2 = 1;
                    } else {
                        $tag2 = 0;
                    }
                    $tpl->assign('tag2', $tag2);
                    $tpl->assign('baner2', $otvet[1]['pathway']);
                    $tpl->assign('link2', $otvet[1]['link']);
                    if ($user['id']>=100 || $user['id']==0) {
                        $cm->updateShows($otvet[1]['link']);
                    }
                }
            } else {
                if (isset($otvet[0]['text'])) {
                    $tpl->assign('direct',$otvet[0]['text']);
                } else {
                    $ext = explode('.', $otvet[0]['pathway']);
                    $ext = $ext[count($ext)-1];
                    if ($ext == 'swf') {
                        $tag = 1;
                    } else {
                        $tag = 0;
                    }
                    $tpl->assign('tag', $tag);
                    $tpl->assign('baner', $otvet[0]['pathway']);
                    $tpl->assign('link', $otvet[0]['link']);
                    if ($user['id']>=100 || $user['id']==0) {
                        $cm->updateShows($otvet[0]['link']);
                    }
                }
            }
            return $tpl->fetch("banerrota.tpl");
        } else {
            return false;
        }
    }

    function is_root() {
        return true;
        if ($_SERVER['REQUEST_URI'] == '/') {
            return true;
        } else {
            if (!empty($this->conf['user']['payment_type'])) {
                return false;
            } else {
                return true;
            }
        }
    }
}