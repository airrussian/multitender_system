<?php

class multitender_action_quality_analyzer extends multitender_action {

    function  __construct() {
        parent::__construct();
        $rights = $this->conf['user']['right'];
        if ($rights<=1) {
    	    $tpl = $this->new_tpl();
    	    $tpl->display("msg_right.tpl");
    	    exit;
        }
    }

    function run() {
        $this->conf['page_title'] = 'Анализатор качества поиска';

        $coldatecount = 7;

        $qa = new multitender_model_quality_analyzer();

        $tpl = $this->new_tpl();

        $link_base = $this->conf['pref']['link_base'];

        $period = @ $_GET['period'] ? $_GET['period'] : 'day';

        $page = @ $_GET['page'] ? $_GET['page'] : 1;
        $tpl->assign("page", $page);


        $filter = @ $_REQUEST['filter'] ? $_REQUEST['filter'] : '';

        $filter = preg_replace('#[^a-zа-яё0-9/_]#iu', ' ', $filter);
        $filter = trim(preg_replace('/\s+/', ' ', $filter));
        $filter = trim($filter);

        $names = explode(' ', $filter);
        $filter = '';
        foreach ($names as $v) {
            if (mb_strlen($v,'UTF-8') >= 3) {
                $filter .= $v . ' ';
            }
        }
        $filter = trim($filter);

        $tpl->assign("filter", $filter);

        switch ($period) {
            case 'day'   : $long_period = 24*60*60;
                break;
            case 'week'  : $long_period = 7*24*60*60;
                break;
            case 'month' : $long_period = 30*24*60*60;
                break;
        }

        $tpl->assign("period", $period);

        $type = @ $_GET['type'] ? $_GET['type'] : 'all';
        $tpl->assign("type", $type);

        $date_curr = @ strtotime($_GET['godate']) ? strtotime($_GET['godate']) : time();
        $tpl->assign("date_curr", date('Y-m-d', $date_curr));
        $date_prev = $date_curr-$long_period;
        $tpl->assign("date_prev", date('Y-m-d', $date_prev));
        $date_next = $date_curr+$long_period;
        if ($date_next > time()) {
            $tpl->assign("date_next", NULL);
        } else {
            $tpl->assign("date_next", date('Y-m-d', $date_next));
        }

        $rectopg = 50;
        $mrks = $qa->get_search($filter, $type, ($page-1)*$rectopg, $rectopg);

        if ($mrks) {
            $total = $mrks['total'];
            $tpl->assign("total", $total);
            unset($mrks['total']);


            $link = $link_base."action=quality_analyzer&godate=".date('Y-m-d', $date_curr)."&period=".$period."&type=".$type;
            if ($filter) {
                $link.="&filter=".$filter;
            }

            $scrol = new multitender_action_scrol();
            
            $scrol->link = $link.$scrol->link;
            $scrol->total = Ceil($total/$rectopg);
            $tpl->assign('scrol', $scrol->run());

            $dates = array();
            for ($i=0; $i<=$coldatecount; $i++) {
                $dates[$i] = $this->db->DBDate($date_curr-($coldatecount-$i)*$long_period);
            }

            $i=0;
            foreach ($mrks as $m) {
                $markers[$i] = array(
                        'id' => $m['search_id'],
                        'name' => $m['main'],
                        'search' => $m['search'],
                        'info' => array(),
                );
                foreach ($dates as $date) {
                    $markers[$i]['info'][] = array(
                            'date' => preg_replace("#['\"]#", "", $date),
                            'total_items' => 0,
                            'new_items' => 0,
                    );
                }
                $i++;
            }

            $ids = array();
            foreach ($markers as $marker) {
                $ids[] = $marker['id'];  // Список индентификаторов
            }

            $s_obj = new multitender_model_search();

            foreach ($markers as &$marker) {
                $s_arr = unserialize($marker['search']);
                $s_obj->SetSearch( $s_arr );
                if ($filter) {
                    $names = explode(' ', $filter);
                    foreach ($names as $v) {
                        $marker['name'] = preg_replace("/$v/iu", '<font color=#cc0000>\\0</font>', $marker['name']);
                    }
                }
                $marker['search'] = $s_obj->to_info();
                $marker['search_link'] = "action=view";

                // FIX ME повторяемость с last_queries
                $marker['search_link'] .= "&search[main]=";
                if (!empty($s_arr['main'])) {$marker['search_link'].=$s_arr['main'];}
                if (!empty($s_arr['okr'])) {
                    foreach ($s_arr['okr'] as $o => $val) {
                        $marker['search_link'].="&amp;search[okr][$o]=on";
                    }
                }
                if (!empty($s_arr['reg'])) {
                    foreach ($s_arr['reg'] as $r => $val) {
                        $marker['search_link'].="&amp;search[reg][$r]=on";
                    }
                }
                if (!empty($s_arr['price_min'])) {
                    $marker['search_link'].="&amp;search[price_min]=".$s_arr['price_min'];
                }
                if (!empty($s_arr['price_max'])) {
                    $marker['search_link'].="&amp;search[price_max]=".$s_arr['price_max'];
                }
                if (!empty($s_arr['date_min'])) {
                    $marker['search_link'].="&amp;search[date_min]=".$s_arr['date_min'];
                }
                if (!empty($s_arr['date_max'])) {
                    $marker['search_link'].="&amp;search[date_max]=".$s_arr['date_max'];
                }
            }

            if ($searches = $qa->get_update_searcher($ids, $dates)) {
                foreach ($markers as &$marker) {
                    $prev_total_items = 0;
                    foreach ($searches as $search) {
                        if ($search['search_id']==$marker['id']) {
                            foreach ($marker['info'] as &$info) {
                                if (preg_match("#".$search['date']."#siu", $info['date'])) {
                                    $info['new_items']   = $search['total_items'] - $prev_total_items;
                                    //$info['new_items']   = $search['new_items'];
                                    $info['total_items'] = $search['total_items'];
                                    $prev_total_items = $search['total_items'];
                                }
                            }
                        }
                    }
                }
            }

            $tpl->assign('markers', $markers);
        }

        $tpl->assign('base_link', $link_base);
        return $tpl->fetch("quality_analyzer.tpl");

    }

}
