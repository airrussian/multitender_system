<?php

class multitender_action_yandex_widget extends multitender_action {

    function run() {

        $memcache = new Memcache;
        $memcache->connect('emo-russia.ru', 11611, 15);

        $uri = $_SERVER['REQUEST_URI'];
        preg_match("#action=yandex_widget#sui", $uri, $keys);

        $key = $keys[0];

        $cache = false;

        if (strlen($uri) > 249) {
            $key = md5($uri);
        }

        if ( ($result = $memcache->get($key)) && $cache ) {
            echo ("<!-- cache -->");
            return $result;
        }
        echo ("<!-- nocache -->");

        // обрезание строк
        $short_default = 160;
        $short_max     = 5555;
        $short_etc     = '…';
        mb_internal_encoding('UTF-8');

        if (isset($_GET['short'])) {
            $short_max = (int) $_GET['short'];
            if ($short_max < 15) {
                $short_max = $short_default;
            }
            $short_max -= mb_strlen($short_etc);
        }

        $this->conf['pref']['ppp'] = 50;
        $rectopg = 5;

        $tpl = $this->new_tpl();

        $s_obj = $this->new_model('search');
        $s_obj->clear_search_feed();

        $search = $s_obj->search;

        $s_obj->kostyl_type = 'feed';

        $r = $s_obj->query();

        $total = $r['total'];

        $pagecount = Ceil($this->conf['pref']['ppp']/$rectopg);
        $page = 1;
        $i=0;

        foreach($r['items'] as $item) {
            $item['link'] = $this->conf['pref']['link_base_http_wo'] . $this->conf['pref']['link_detail'] . $item['id'];

            if (isset($_GET['short'])) {
                if (mb_strlen($item['name']) > $short_max) {
                    $item['name'] = preg_replace('/\s+?(\S+)?$/u', '', mb_substr($item['name'], 0, $short_max+1));
                    $item['name'].= $short_etc;
                }
            }

            $date =  $this->db->UnixTimeStamp( $item['date_add'] );
            $item['date'] = gmdate('r', $date);

            if( $item['date_end'] ) {
                // 2009-11-10
                list($y, $m, $d) = explode('-', $item['date_end']);
                $date = gmmktime (0, 0, 0, $m, $d, $y);
                if( time() > $date ) {
                    $item['date_end_exit'] = true;
                }
            }
            $i++;
            $pages[$page][$i]=$item;
            if ($i == $rectopg) {
                $page++;
                $i=0;
            }
        }

        $tpl->assign("pagecount", $pagecount);

        $tpl->assign('pages', $pages);

        $tpl->assign('name', @$_GET['name']);

        $tpl->assign('total', $total);

        $tpl->assign('search', $search);

        $hidden = $s_obj->to_hidden();

        $tpl->assign('hidden',  $hidden);

        $form_info = $s_obj->to_info();
        if ($form_info) {
            $form_info = ", $form_info";
        }
        $tpl->assign('form_info', $form_info);

        $yw_content = $tpl->fetch('yandex_widget.tpl');

        // anit deadlock, psevdo connection pull
        define('YW_DB_LOCK', 'YW_DB_LOCK');
        $i = 0;
        while (!$memcache->add(YW_DB_LOCK, YW_DB_LOCK, false, 10)) {
            sleep(1);
            $i++;
            if ($i > 33) {
                exit('Sorry, sever is overloaded');
            }
        }

        @$memcache->set($key, $yw_content, 0, 2.3*60*60);
        $memcache->delete(YW_DB_LOCK, 0);

        return $yw_content;

    }
}