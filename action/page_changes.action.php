<?php
class multitender_action_page_changes extends multitender_action {

    function  __construct() {
        parent::__construct();
        $rights = $this->conf['user']['right'];
        if ($rights<=50) {
    	    $tpl = $this->new_tpl();
    	    $tpl->display("msg_right.tpl");
    	    exit;
    	}
    }

    function run() {
        $GLOBALS['tenders']['conf']['page_joomla_links'][] = '<link rel="stylesheet" href="/templates/mt/css/moder.css" type="text/css"';

        $page_id = @ $_GET['page_id'] ? $_GET['page_id'] : '';
        $user_id = $GLOBALS['tenders']['conf']['user']['id'];
        $item_id = @ $_GET['item_id'] ? $_GET['item_id'] : 0;

        $link_d = $this->conf['pref']['link_detail'];

        $task = @ $_GET['task'] ? $_GET['task'] : '';

        if (!$page_id || !$user_id) {
            return false;
        }

        $mi = $this->new_model("moder_interface");
        $tpl = $this->new_tpl();

        $info = "";
        if (!empty($_POST)) {
            $status="error";
            $ni['link_source'] = $_POST['defsource'];
            if (!empty($_POST['link_source'])) {
                $ni['link_source'] = $_POST['link_source'];
            }

            $ni['id'] = $_REQUEST['page_id'];

            if (empty($_POST['customer'])) {
                $ni['customer'] = NULL;
            } else {
                $ni['customer'] = $_POST['customer'];
            }

            if (empty($_POST['summa'])) {
                $ni['summa'] = NULL;
            } else {
                $ni['summa'] = $_POST['summa'];
            }
            if (empty($_POST['end'])) {
                $ni['end'] = NULL;
            } else {
                $ni['end'] = $_POST['end'];
            }
            if (empty($_POST['begin'])) {
                $ni['begin'] = NULL;
            } else {
                $ni['begin'] = $_POST['begin'];
            }
            if (empty($_POST['region'])) {
                $info = "выберите регион";
            } else {
                $ni['region'] = $_POST['region'];
            }
            if (empty($_POST['type'])) {
                $info = "выберите тип";
            } else {
                $ni['type'] = $_POST['type'];
            }
            if (empty($_POST['name'])) {
                $info = "не указано 'название' тендера";
            } else {
                $ni['name'] = $_POST['name'];
            }

            if (!$info) {
                if ($task=='edit') {
                    if ($item_id) {
                        $mi->edit_item($ni, $item_id);
                        $info="Данные тендера изменены <a href='$link_d.$item_id' target='_blank'>$link_d.$item_id</a>";
                        $status=false;
                    } else {
                        $info="Ошибка данных";
                        $status="edit";
                        $ni['id'] = $item_id;
                    }
                } else {
                    $item_id = $mi->add_item($ni, $user_id);
                    if ( $item_id < 0 ) {
                        $info="Успешно добавлен новый тендер: <a href='$link_d.$item_id' target='_blank'>$link_d.$item_id</a>";
                        $status=false;
                    } else {
                        $link = $link.$item_id;
                        $info="Тендер уже есть в системе: <a href='$link_d.$item_id' target='_blank'>$link_d.$item_id</a>";
                    }
                    if ( !$item_id) {
                        $info="Не удается осуществить вставку";
                    }
                }
            } else {
                if ($task=='edit') {
                    $status="edit";
                    $ni['id'] = $item_id;
                }
            }
            $tpl->assign("ni", $ni);
        } else {
            if ($task=='edit') {
                $status="edit";
                $item = $mi->get_items(array($item_id));
                $ni['id']           = $item[0]['id'];
                $ni['name']         = $item[0]['name'];
                $ni['type']         = $item[0]['type_id'];
                $ni['region']       = $item[0]['region_id'];
                if ($item[0]['date_publication']) {
                    list($y, $m, $d) = explode('-', $item[0]['date_publication']);
                    $ni['begin']        = $d.".".$m.".".$y;    
                } else {
                    $ni['begin']        = date("j.m.Y");
                }
                if ($item[0]['date_end']) {
                    list($y, $m, $d) = explode('-', $item[0]['date_end']);
                    $ni['end']      = $d.".".$m.".".$y;
                } else {
                    $ni['end']      = '';
                }
                $ni['summa']        = $item[0]['price'];
                $ni['link_source']  = $item[0]['detail_link'];
                $tpl->assign("ni", $ni);
            } else {
                $status=false;
            }
        }

        $link_base = $this->conf['pref']['link_base'];
        $detail_link = $this->conf['pref']['link_detail'];

        $pg = $mi->get_page($page_id);

        // Обрезка текста до $cl строки
	$n = 0; $cl = 15; $p = 0;
        $pg['text_change'] = preg_replace('#\n#si', '<br>', $pg['text_change']);
        if ($pg['text_change']) {
            do { $p = mb_strpos($pg['text_change'], "<br>", $p+1, 'UTF-8');  $n++; } while ($p && $n<$cl);
            $pg['text_change'] = mb_substr($pg['text_change'], 0, @ $p ? $p : mb_strlen($pg['text_change'], 'UTF-8'), 'UTF-8');
            if ($p > mb_strlen($pg['text_change'], 'UTF-8')-1) {
        	$pg['text_change'].="...";
            }
        }
        
        $tpl->assign("pg", $pg);

        $tpl->assign("status", $status);

        $tpl->assign("link_base", $link_base);
        $tpl->assign("detail_link", $detail_link);

        $ts = $mi->get_types();
        $tpl->assign("ts", $ts);

        $rg = $mi->get_regions();
        $tpl->assign("rg", $rg);

        $last_item = $mi->get_last_item($page_id);

        $tpl->assign("li", $last_item);

        $tpl->assign("info", $info);

        $page = @ $_GET['page'] ? $_GET['page'] : 1;
        $rectopg = 25;

        $link = "action=page_changes&page_id=$page_id";

        if ($it_pg = $mi->get_item_form_page($page_id, $rectopg*($page-1), $rectopg)) {

            $total = $it_pg['total'];
            unset($it_pg['total']);

            $scrol = $this->new_action("scrol");
            $scrol->link = $link_base.$link.$scrol->link;
            $scrol->total = Ceil($total/$rectopg);
            $tpl->assign('scrol', $scrol->run());

            foreach ($it_pg as $i) {
                $item_ids[] = $i['item_id'];
            }

            $items = $mi->get_items($item_ids);

            foreach ($it_pg as &$i) {
                foreach ($items as &$it) {
                    if ($it['id']==$i['item_id']) {
                        foreach ($ts as $t) {
                            if ($t['id']==$it['type_id']) {
                                $it['type_name']=$t['name'];
                            }
                        }

                        foreach ($rg as $r) {
                            if ($r['id']==$it['region_id']) {
                                $it['region_name']=$r['sname'];
                            }
                        }

                        $i['items'] = $it;
                    }
                }
            }

            $tpl->assign("items", $it_pg);

        }

        return $tpl->fetch("page_changes.tpl");
    }
}
