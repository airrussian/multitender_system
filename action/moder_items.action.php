<?php
class multitender_action_moder_items extends multitender_action {

    private $rights;

    function  __construct() {
        parent::__construct();
        $this->rights = $this->conf['user']['right'];
        if ($this->rights<=1) {
    	    $tpl = $this->new_tpl();
    	    $tpl->display("msg_right.tpl");
    	    exit;
        }
    }

    function item_confirm() {

        if ($this->rights<=100) { exit; }

        $item_id = $_GET['item_id'];

        $mi = $this->new_model("moder_interface");
        if ($mi->item_confirm($item_id, 1)) {
            return "OK";
        } else {
            return "FALSE";
        }
    }

    function item_cancel() {

        if ($this->rights<=100) { exit; }

        $item_id = $_GET['item_id'];

        $mi = $this->new_model("moder_interface");
        if ($mi->item_confirm($item_id, -1)) {
            return "OK";
        } else {
            return "FALSE";
        }
    }

    function list_confirm_show() {
        $tpl = $this->new_tpl();
        $mi = $this->new_model("moder_interface");
        $us = $this->new_model("users");

        $filter = @ $_GET['filter'] ? $_GET['filter'] : 'all';

        $tpl->assign("filter", $filter);

        $page = @ $_GET['page'] ? $_GET['page'] : 1;
        $rectopg = 10;
        $link_base = $this->conf['pref']['link_base'];
        $link_detail = $this->conf['pref']['link_detail'];
        $tpl->assign("link_detail", $link_detail);
        $link = "action=moder_items";

        if ($this->rights>100) {
            $moder_id = 0;
        } else {
            $moder_id = $this->conf['user']['id'];
        }

        $items = $mi->get_no_confirm_item($filter, $rectopg*($page-1), $rectopg, $moder_id);

        $tpl->assign("rights", $this->rights);
        
        $total = $items['total'];
        unset($items['total']);

        $scrol = $this->new_action("scrol");
        $scrol->link = $link_base.$link."&filter=".$filter.$scrol->link;
        $scrol->total = Ceil($total/$rectopg);

        $tpl->assign("scrol", $scrol->run());
        
        if ($items) {
            foreach ($items as $it) {
                $items_ids[] = $it['id'];
            }

            $moder_item = $mi->get_moder_items($items_ids);
            $types = $mi->get_types();
            $users = $us->get_list_moder();

            foreach ($items as &$it) {
                foreach ($moder_item as &$m) {
                    if ($it['id']==$m['item_id']) {
                        foreach ($users as $u) {
                            if ($m['user_id']==$u['Id']) {
                                $m['user_name'] = $u['name'];
                            }
                        }
                        $it['moder'] = $m;
                    }
                }
                foreach ($types as $t) {
                    if ($it['type_id']==$t['id']) {
                        $it['type_name'] = $t['name'];
                    }
                }
                $it['date_add'] = date('j/m H:i T',(strtotime($it['date_add'])));
            }
        }
        $tpl->assign("items", $items);
        return $tpl->fetch('pages_item_confirm.tpl');
    }

    function run() {
        $this->conf['page_title'] = 'Список добавленных тендеров';

        $user_id = $GLOBALS['tenders']['conf']['user']['id'];

        if (!$user_id) {
            $tpl = $this->new_tpl();
            return $tpl->fetch("msg_auth.tpl");
            exit;
        }

        $task = @ $_GET['task'] ? $_GET['task'] : 'show';

        switch ($task) {
            case 'confirm':
                return $this->item_confirm();
                break;
            case 'cancel':
                return $this->item_cancel();
                break;
            case 'show':
            default:
                return $this->list_confirm_show();
                break;
        }
    }
}
