<?php
/**
 * КОСТЫЛЬ для встраиваемого виджета
 */
class multitender_action_sidebar_geoip_frame extends multitender_action {
    function run() {
        $action = new multitender_action_sidebar_geoip();
        $html = $action->run();
        // надо поставить target, обязательно
        $html = str_replace('<a ', '<a target="_parent" ', $html);
        $css = '<style type="text/css">.module_geoip a{color:#004477;text-decoration:underline;font-family:"Lucida Grande","Lucida Sans Unicode",Helvetica,Arial,sans-serif;font-size:13px;margin:0;padding:0} </style>';
        return $css . $html;
    } //function run
}
