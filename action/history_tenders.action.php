<?php

class multitender_action_history_tenders extends multitender_action {
    function run() {
        $this->conf['page_title'] = 'История';

        $user_id = $GLOBALS['tenders']['conf']['user']['id'];
        if (!$user_id) {
            $tpl = $this->new_tpl();
            return $tpl->fetch("msg_auth.tpl");
        }

        $lt = $this->new_action("user_tenders");
        $user_id = $this->conf['user']['id'];
        $link_base = $this->conf['pref']['link_base'];
        return $lt->list_tender($user_id, "action=history", 'last');
    }

}
