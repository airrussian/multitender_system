<?php

class multitender_action_sidebar_info extends multitender_action {
    function run() {
        $tpl = $this->new_tpl();
        if (preg_match('/payment/i', $this->conf['user']['type'])) {
            $tpl->assign("payment", true);
        }
        return $tpl->fetch("sidebar_info.tpl");
    }

}
