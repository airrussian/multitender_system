<?php

class multitender_action_related extends multitender_action {

    // сортирует двух мерный массив $array по полю $on в порядке $order
    private function array_sort($array, $on, $order=SORT_ASC) {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }

    // Возвращает корневую рубрику
    private function get_parent_rubric($rubs, $r) {
        if ($r['parent']==0) return $r=array('name'=>$r['name'], 'id'=>$r['id']);

        foreach ($rubs as $rr) {
            if ($rr['id']==$r['parent']) {
                return $this->get_parent_rubric($rubs, $rr);
            }
        }
    }

    // Возвращает список $maxout рубрик наиболее входящих в $items
    public function get_rubric($items, $maxout=4) {
        $data_common = $this->new_model('data_common');
        $data_common = $data_common->get_data();
        $rubs = $data_common['rubric'];

        $rubrics = array();
        foreach ($rubs as $r) {
            $count = 0;
            foreach ($items as $item) {
                if ($item['rubric_id']==$r['id']) {
                    $count++;
                }
            }
            $pr = $this->get_parent_rubric($rubs, $r);
            $ins = true;
            foreach ($rubrics as &$rub) {
                if ($pr['id']==$rub['id']) {
                    $rub['count'] += $count;
                    $ins = false;
                }
            }
            if ($ins) {
                $rubrics[] = array('id'=>$pr['id'], 'name'=> $pr['name'], 'count' => $count);
            }
        }

        foreach ($rubrics as $key => $rubric) {
            if ($rubric['count']==0) {
                unset($rubrics[$key]);
            }
            if (preg_match("#Другое#si", $rubric['name'])) {
                unset($rubrics[$key]);
            }
        }

        $rubrics = $this->array_sort($rubrics, 'count', SORT_DESC);

        $rubrics = array_slice($rubrics, 0, $maxout);

        $tpl = $this->new_tpl();
        $tpl->assign("link_base", $this->conf['pref']['link_base']);
        $tpl->assign("rubrics", $rubrics);
        return $tpl->fetch("related_rubrics.tpl");
    }

    // Возвращает похожие запросы
    // Похожие выбираются LIKE %search[main]%
    public function get_search(& $obj, $maxout=4) {
        if (!$main = @$obj->search['main']) {
            return false;
        }
        $db = $this->conf['dbs']['person'];
        $sql = "SELECT main, count(*) as count FROM search WHERE main LIKE ? AND main NOT LIKE ? GROUP BY main ORDER BY count DESC LIMIT 0,$maxout";
        if (!$arr = $db->GetAll($sql, array("%$main%", $main))) {
            return false;
        }
        foreach ($arr as &$row) {
            $row['view'] = preg_replace("/$main/sui", "<b>\\0</b>", $row['main']);
        }
        $tpl = $this->new_tpl();
        $tpl->assign("main", $main);
        $tpl->assign("link_base", $this->conf['pref']['link_base']);
        $tpl->assign("searchers", $arr);
        return $tpl->fetch("related_search.tpl");
    }

}