<?php

class multitender_action_sidebar_total extends multitender_action {

    const MEMCACHEFLAG = 'MTSIDEBARCACHE';
    var $mc = NULL;
    function __construct() {
        parent::__construct();
        $this->mc = $GLOBALS['memcached_res'];
    }

    function run() {
        if (isset($_GET['tocache'])) {
            $this->createCache();
            exit;
        }
        $tpl=$this->new_tpl();
        $tpl->caching = false;
        if (!$info = $this->mc->get(self::MEMCACHEFLAG)) {
            $info = $this->createCache();
        }
        $tpl->assign("link", $this->conf['pref']['link_base']);
        $tpl->assign("info", $info);
        //$sb = $this->new_model("sidebar_total");
        //$tpl->assign('regname',$sb->getRegName());
        return $tpl->fetch("sidebar_total.tpl");
    }
    
    public function createCache() {
        $sb = $this->new_model("sidebar_total");
        $info = $sb->get_info();
        $this->mc->set(self::MEMCACHEFLAG, $info, false, 86400);
        return $info;
    }

}
