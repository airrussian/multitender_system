<?php

class multitender_action_subscribe extends multitender_action {

    public function status() {

        $view = $this->new_tpl();
        $view->caching = false;
        $view->cache_lifetime = 23 * 60 * 60;
        if ($view->is_cached('subscribe.tpl')) {
            return $view->fetch('subscribe.tpl');
        }

        $date = isset($_REQUEST['date']) ? $_REQUEST['date'] : date("Y-m-d", time() - 24 * 60 * 60);

        $rubric = multitender_model_data_common::singleton()->get_raw('rubric');

        $subscribe = $this->new_model('subscribe');
        $usersearches = $subscribe->GetUserSearches($date);

        $subdata = array();
        $subdata['date'] = $date;
        $subdata['datetime'] = date("r", strtotime($date));
        $subdata['rubrics'][0] = array('name' => 'не указана категория', 'count' => 0);
        $subdata['users']['send'] = 0;
        $subdata['count'] = count($usersearches);
        $subdata['users']['new'] = $subscribe->GetNewUsers($date);
        $subdata['new'] = $subscribe->GetNewSearch($date);
        $subdata['users']['count'] = $subscribe->GetCountUserActive();

        $nowuser = 0;

        foreach ($usersearches as $row) {
            if ($row['user_id'] > $nowuser) {
                $nowuser = $row['user_id'];
                $subdata['users']['send'] ++;
            }
            $row['search'] = unserialize($row['search']);
            if (!empty($row['search']['rubric'])) {
                foreach ($row['search']['rubric'] as $rubric_id => $r) {
                    $subdata['rubrics'][$rubric_id]['count'] ++;
                    $subdata['rubrics'][$rubric_id]['name'] = $rubric[$rubric_id]['name'];
                }
            } else {
                $subdata['rubrics'][0]['count'] ++;
            }
        }



        $view->assign('subscribe', $subdata);
        return $view->fetch("subscribe.tpl");
    }

    /**
     * Крайне идиотский функционал для детеила, по просьбе Романа от 26-03-2015
     */
    public function freenewsletter() {                
        
        $user_id = isset($this->conf['user']['id']) ? $this->conf['user']['id'] : false;
        
        if (isset($_REQUEST['get']) && ($_REQUEST['get'] == 'count') && $user_id) {
            $sm = new multitender_model_subscribe();
            $sids = array(1448516, 1448517, 1448518, 1448519, 1448520); // prod
            $sids = array(1153246, 1153247, 1153248, 1153249, 1153250); // beta
            $v = $sm->GetFreeSub($sids);
            header("Content-type: text/xml; charset=utf-8");
            echo '<?xml version="1.0" encoding="UTF-8"?>';
            echo '<freesubscribers count="'.count($v).'">';
            foreach ($v as $u) {
                echo '<user>'. $u['name'] . '</user>';
            }            
            echo '</freesubscribers>';
            exit();
        }

        $s_obj = new multitender_model_search();
        $lq = new multitender_model_query_users();
        $nl = new multitender_model_email_newsletter();

        $freesearch = array(
            1 => array(
                'name' => 'Кровля',
                'search' => array(
                    'main' => 'ремонт кровли OR ремонт крыши OR кровл OR кровель OR крыш -(уборка OR очистка OR налед OR сосул OR проект OR осмотр OR трапы OR слив)',
                    'param_no_customer' => 'on'
                ),
                'status' => false
            ),
            2 => array(
                'name' => 'Ограждения',
                'search' => array(
                    'main' => 'огражден OR забор OR периметр -(испытан OR аттестация OR нагруз OR проект)',
                    'param_no_customer' => 'on'
                ),
                'status' => false
            ),
            3 => array(
                'name' => 'Капитальный ремонт',
                'search' => array(
                    'main' => 'капитальн OR реконструкц OR переустройств OR строител -(проектно OR ПСД OR текущ)',
                    'param_no_customer' => 'on'
                ),
                'status' => false
            ),
            4 => array(
                'name' => 'Текущий ремонт',
                'search' => array(
                    'main' => 'текущ OR отделк OR отделочн OR квартир OR подъезд OR общего -(капитальн OR реконструкц OR переустройств OR строител)',
                    'param_no_customer' => 'on'
                ),
                'status' => false
            ),
            5 => array(
                'name' => 'Благоустройство',
                'search' => array(
                    'main' => 'благоустройств OR озеленен OR окраск OR газон OR посадк OR бордюр OR ландшафтн OR тропиночн OR посев',
                    'param_no_customer' => 'on'
                ),
                'status' => false
            ),
        );

        $query = isset($_REQUEST['query']) ? (int) $_REQUEST['query'] : false;

        if (isset($freesearch[$query]['search'])) {        
            if ($user_id) {
                $s_obj->SetSearch($freesearch[$query]['search']);
                $sid = $lq->user_add_find($user_id, $s_obj, 0);

                $opt = isset($_REQUEST['opt']) ? $_REQUEST['opt'] : 'add';
                if ($opt == 'add') {
                    $nl->add_newsletter($user_id, $sid);
                } else {
                    $nl->del_newsletter($user_id, $sid);
                }
                if (isset($_SERVER['HTTP_REFERER']) && $user_id) {                
                    header("Location:" . $_SERVER['HTTP_REFERER']);
                } 
            } else {
                header("Location: http://{$_SERVER['HTTP_HOST']}/component/user/?task=register&freenewsletter");
            }
            exit();
        } else {
            $tpl = $this->new_tpl();
            if ($user_id) {
                foreach ($freesearch as &$s) {
                    $s_obj->SetSearch($s['search']);
                    $sid = $lq->user_add_find($user_id, $s_obj, 0);
                    $s['status'] = $nl->check_newsletter($user_id, $sid);
                }
            }
            $tpl->assign('freesearch', $freesearch);
            return $tpl->fetch('subscribe/freenewsletter.tpl');
        }
    }        

    public function run() {
        $task = isset($_REQUEST['task']) ? $_REQUEST['task'] : false;
        if (!method_exists($this, $task)) {
            exit('not task');
        }

        return $this->$task();
    }

}
