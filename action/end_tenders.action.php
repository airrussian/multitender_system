<?php

class multitender_action_end_tenders extends multitender_action {

    function show_list() {
        $view = $this->new_tpl();

        $search = $this->new_model("search");

        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;

        if (isset($_GET['onpage'])) { $onpage = (int)$_GET['onpage']; }
        elseif (isset($_COOKIE['onpage'])) { $onpage = (int)$_COOKIE['onpage']; }
        else { $onpage = $this->conf['pref']['ppp']; }
        $tenders = $search->get_item_report(($page-1)*$onpage, $onpage);

        $total = $tenders['total'];
        $items = $tenders['items'];

        foreach($items as &$item) {
            $item['link'] = $this->conf['pref']['link_detail'] . $item['id'];
            if ( preg_match('/user/i', $this->conf['user']['type']) ) {
                $item['link'] .= '?cache=user';
            }
        }

        $tpl = $this->new_tpl();
        $tpl->assign("items", $items);
        $list = $tpl->fetch("list.tpl");

        $tpl = $this->new_tpl();
        $form = $tpl->fetch("searchform.tpl");

        $link = $this->conf['pref']['link_base_http_wo'] . "/tenders/end_tenders/";
        $scrol = new multitender_action_scrol();
        $scrol->link  = "$link?page=";
        $scrol->onpage_link  = "$link?onpage=";
        $scrol->total = $total;
        if ($scrol->total > 5000) {
            $scrol->total = 5000;
        }
        $view->assign( 'scrol',  $scrol->run() );

        $view->assign("list", $list);
        $view->assign("total", $total);
        //$view->assign("form", $form);
        $view->assign("link_feed", $this->conf['pref']['link_base_http_wo'] . "/tenders/end_tenders/?task=feed");
        $this->conf['page_title'] = "Итоги торгов";
        return $view->fetch("index2.tpl");
    }

    function feed() {
        header("Content-Type: application/xml; charset=utf-8");
        $search = $this->new_model("search");
        $tenders = $search->get_item_report(0, $this->conf['pref']['ppp']);
        $total = $tenders['total'];
        $items = $tenders['items'];
        foreach($items as &$item) {
            $item['link'] = $this->conf['pref']['link_base_http_wo'] . $this->conf['pref']['link_detail'] . $item['id'];
        }
        $tpl = $this->new_tpl();
        $tpl->assign("items", $items);
        echo $tpl->fetch("feed.tpl");
        exit;
    }


    function run() {
        $task = isset($_GET['task']) ? $_GET['task'] : false;
        switch ($task) {
            case 'feed': return $this->feed();
            default: return $this->show_list();
        }
    }

}
