<?php
class multitender_action_feedback extends multitender_action {

    function run() {
        $key = @ $_POST['key'] ? $_POST['key'] : NULL;
        $result = @ $_GET['result'] ? $_GET['result'] : NULL;
        $answer['who_are_you'] = @ $_POST['who_are_you'] ? $_POST['who_are_you'] : NULL;
        $answer['why_dont_use'] = @ $_POST['why_dont_use'] ? $_POST['why_dont_use'] : NULL;
        $answer['no_function_answer'] = @ $_POST['no_function_answer'] ? $_POST['no_function_answer'] : NULL;
        $answer['no_comfort_answer'] = @ $_POST['no_comfort_answer'] ? $_POST['no_comfort_answer'] : NULL;
        $answer['other_answere'] = @ $_POST['other_answer'] ? $_POST['other_answer'] : NULL;
        $answer['other_services'] = @ $_POST['other_services'] ? $_POST['other_services'] : NULL;
        $answer['interest'] = @ $_POST['interest'] ? $_POST['interest'] : NULL;
        $answer['other_region'] = @ $_POST['other_region'] ? $_POST['other_region'] : NULL;
        $answer['other_serv_answer'] = @ $_POST['other_serv_answer'] ? $_POST['other_serv_answer'] : NULL;
        $flag=0;

        if (!$result || $result!='showresult') {
        foreach ($answer as $item) {
            if ($item != NULL) {
                $flag++;//если заполнено хотя бы одно поле
            }
        }

        if ($flag > 1) {
            $flag = true;
        } else {
            $flag = false;
        }


        $link_base = $this->conf['pref']['link_base'];
        $link_act  = "feedback";

        $tpl = $this->new_tpl();
        $tpl->caching = false;

        if (!$key)
        {
            $link = substr($link_base,0,-1) . $link_act;

            $tpl->assign("action_link", $link);

            return $tpl->fetch('feedback.tpl');
        } elseif ($key) {
            if ($flag) {
                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $insertarray[0]=$_SERVER['HTTP_CLIENT_IP'];
                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $insertarray[0]=$_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $insertarray[0]=$_SERVER['REMOTE_ADDR'];
                }

                $insertarray[1]=$answer['who_are_you'];
                $insertarray[2]=$answer['why_dont_use'];
                $insertarray[3]=$answer['other_services'];
                $insertarray[4]=$answer['interest'];
                $insertarray[5]=$answer['other_region'];
                switch ($insertarray[2]) {
                    case 'no_function':
                        $insertarray[6]=$answer['no_function_answer'];
                        break;
                    case 'no_comfort':
                        $insertarray[6]=$answer['no_comfort_answer'];
                        break;
                    case 'other':
                        $insertarray[6]=$answer['other_answere'];
                        break;
                    default:
                        $insertarray[6]='';
                }
                $insertarray[7]= @ $insertarray[3] == 'yes_now' ? $answer['other_serv_answer'] : '';
                $cm = $this->new_model('feedback');

                $cm->add_record($insertarray);
            }

            return $tpl->fetch('feedback_action.tpl');
        }
        } else {
                $cm = $this->new_model('feedback');

                $result = $cm->get_result();

                $tpl = $this->new_tpl();
                $tpl->caching = false;

                foreach ($result as &$item) {
                    switch ($item['wha_are_you']) {
                        case 'private':
                            $item['wha_are_you'] = 'Частное лицо';
                            break;
                        case 'ip':
                            $item['wha_are_you'] = 'Индивидуальный предприниматель';
                            break;
                        case 'staff':
                            $item['wha_are_you'] = 'Сотрудник фирмы';
                            break;
                        case 'cheef':
                            $item['wha_are_you'] = 'Руководитель фирмы';
                            break;
                        default:
                            ;
                    }
                    switch ($item['why_dont_use']) {
                        case 'no_needless':
                            $item['why_dont_use'] = 'Нет необходимости';
                            break;
                        case 'no_function':
                            $item['why_dont_use'] = @ $item['why_dont_use_text'] ? $item['why_dont_use_text'] : 'Нет необходимых функций';
                        case 'no_comfort':
                            $item['why_dont_use'] = @ $item['why_dont_use_text'] ? $item['why_dont_use_text'] : 'Неудобен';
                            break;
                        case 'other':
                            $item['why_dont_use'] = @ $item['why_dont_use_text'] ? $item['why_dont_use_text'] : 'Другое';
                            break;
                        default:
                            ;
                    }

                    switch ($item['other_services']) {
                        case 'yes_in_past':
                            $item['other_services'] = 'Да, приходилось';
                            break;
                        case 'yes_now':
                            $item['other_services'] = @ $item['other_services_text'] ? $item['other_services_text'] : 'Да, пользуюсь сейчас';
                        case 'no':
                            $item['other_services'] = @ $item['other_services_text'] ? $item['other_services_text'] : 'Нет';
                            break;
                        default:
                            ;
                    }

                    switch ($item['interest']) {
                        case 'gos_zacup':
                            $item['interest'] = 'Государственные закупки';
                            break;
                        case 'com_zacup':
                            $item['interest'] = 'Коммерческие тендеры';
                            break;
                        case 'both':
                            $item['interest'] = 'оба типа';
                            break;
                        case 'no_interest':
                            $item['interest'] = 'закупки мне не интересны';
                            break;
                        default:
                            ;
                    }

                    switch ($item['other_region']) {
                        case 'yes':
                            $item['other_region'] = 'Да';
                            break;
                        case 'yes_with_UIC':
                            $item['other_region'] = 'да, и не только других регионов, но и стран СНГ';
                            break;
                        case 'no':
                            $item['other_region'] = 'нет';
                            break;
                        default:
                            ;
                    }
                }

                $tpl->assign('result', $result);
                return $tpl->fetch('feedback_result.tpl');
        }
    }

}