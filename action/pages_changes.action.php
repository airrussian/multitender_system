<?php
class multitender_action_pages_changes extends multitender_action {

    function  __construct() {
        parent::__construct();
        $rights = $this->conf['user']['right'];
        if ($rights<=1) {
    	    $tpl = $this->new_tpl();
    	    $tpl->display("msg_right.tpl");
    	    exit;
        }
    }

    function run() {
        $this->conf['page_title'] = 'Отслеживаемые страницы';
        $GLOBALS['tenders']['conf']['page_joomla_links'][] = '<link rel="stylesheet" href="/templates/mt/css/moder.css" type="text/css"';

        $tpl = $this->new_tpl();

        $user_id = @ $_POST['user_id'] ? $_POST['user_id'] : 0;
        $mu = $this->new_model("users");
        $moders = $mu->get_list_moder();
        $tpl->assign("moders", $moders);

        $mi = $this->new_model("moder_interface");

        if (!empty($_POST['no_change'])) {
            $page_id = $_POST['page_id'];
            $mi->nochange_pages($page_id);
        }

        $page = @ $_GET['page'] ? $_GET['page'] : 1;
        $rectopg = 10;
        $link_base = $this->conf['pref']['link_base'];
        $link = "action=pages_changes";

        $tpl->assign("user_id", $user_id);
        $tpl->assign('rights', $this->conf['user']['right']);

        $pages = $mi->get_list_pages_change($user_id, $rectopg*($page-1), $rectopg);
        $total = $pages['total'];
        unset($pages['total']);

        $scrol = $this->new_action("scrol");
        $scrol->link = $link_base.$link.$scrol->link;
        $scrol->total = Ceil($total/$rectopg);

        $tpl->assign('scrol', $scrol->run());

        if ($pages) {
            foreach ($pages as &$pg) {
                // Обрезка текста до $cl строки
                $n = 0;
                $cl = 5;
                $p = 0;
                $pg['cont']['chgs'] = preg_replace('#\n#si', '<br>', $pg['cont']['chgs']);
                if ($pg['cont']['chgs']) {
                    do {
                        $p = mb_strpos($pg['cont']['chgs'], "<br>", $p+1, 'UTF-8');
                        $n++;
                    } while ($p && $n<$cl);
                    $pg['cont']['chgs'] = mb_substr($pg['cont']['chgs'], 0, @ $p ? $p : mb_strlen($pg['cont']['chgs'], 'UTF-8'), 'UTF-8');
                    if ($p >  mb_strlen($pg['cont']['chgs']-1 )) {
                        $pg['cont']['chgs'].="...";
                    }
                }
                $dt = explode(' ', $pg['last_datetime_change']);
                $pg['last_datetime_change'] = date('d.m.Y', strtotime($dt[0])) . ' ' . $dt[1];
            }
        }

        $tpl->assign("items", $pages);
        return $tpl->fetch('pages_changes.tpl');
    }
}
