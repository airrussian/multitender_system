<?php
/**
 * Модуль предложений в зависимости от IP
 * TODO: сменить кэш на уровень выше
 * 
 * Changelog:
 *    избавление от _SESSION, хранить в куке? tc_region
 */
class multitender_action_sidebar_geoip extends multitender_action {
    function run() {
        
        $sidebar_geoip = isset($_GET['tpl']) ? $_GET['tpl'] : 'sidebar_geoip';
        
        $return = '';

        $tpl     = $this->new_tpl();

        $tpl->caching = TRUE;
        $tpl->cache_lifetime = 60*30;
        $geoip_m = new multitender_model_geoip();
        
        $region_id = null;
        if (isset($_COOKIE['tc_region'])) {
            $region_id = (int) $_COOKIE['tc_region'];
        }

        if ( is_null($region_id) ) {
            $return .= "<!-- (sidebar_geoip:get_region) -->";
            $region_id = $geoip_m->get_region();
            $region_id = (int) $region_id; // кэшируем 0
            setcookie('tc_region', $region_id, 0, '/');
        }

        if (!$tpl->is_cached("$sidebar_geoip.tpl", "$sidebar_geoip|$region_id")) {
            $return .= "<!-- ($sidebar_geoip:db) -->";
            $tpl->assign('region_id', $region_id);
            if ($region_id) {
                // FIXME to config!
                $tpl->assign('feed_link',   $this->conf['pref']['link_base_rss'] . "?search%5Breg%5D%5B{$region_id}%5D");
                $tpl->assign('search_link', '/tenders/' . "?search%5Breg%5D%5B{$region_id}%5D");
                $tpl->assign('region_id', $region_id);
            } else {
                $tpl->assign('feed_link',   $this->conf['pref']['link_base_rss']);
                $tpl->assign('search_link', '/tenders/');
            }
            $tpl->assign( 'items',  $geoip_m->get_last_items($region_id)  );
            $tpl->assign( 'region', $geoip_m->get_region_info($region_id) );
            include $this->conf['basedir'] . '/regions.php';
            $tpl->assign('region_link', $regions_chpu[$region_id]);
        } else {
            $return .= "<!-- ($sidebar_geoip:cache) -->";
        }

        return $return . $tpl->fetch("$sidebar_geoip.tpl", "$sidebar_geoip|$region_id");
    } //function run
}
