<?php
/*
<?php //DO NOT EDIT, IF YOU DON'T UNDERSTAND
require_once 'system/main.php';
$_GET['tags_type']='quer';
$_GET['tags_size']=15;
echo tenders_run_action('tag_cloud'); ?>
*/

class multitender_action_tag_cloud extends multitender_action {

    function run() {

        $link = $this->conf['pref']['link_base'];

        $tags['type']  = @ $_GET['tags_type'] ? $_GET['tags_type'] : 'type';
        $tags['count'] = @ $_GET['tags_size'] ? $_GET['tags_size'] : 10;

        $tpl = $this->new_tpl();
        $tpl->caching = true;
        $tpl->cache_lifetime = 6*60*60;//mt_rand(0.5*$this->conf['cache_lifetime'], 2*$this->conf['cache_lifetime']);

        if (!$tpl->is_cached('tag_cloud.tpl', "tag_cloud|".$tags['type'])) {
            echo "<!-- (tag_cloud:db) -->";
            $tc = $this->new_model("tag_cloud");

            switch ($tags['type']) {
                case 'regs':
                    $tagw = $tc->get_weight_regs($tags['count']);
                $tpl->assign('noindex', 1);
                    break;
                case 'quer':
                    $tagw = $tc->get_weight_quers($tags['count']);
                    break;
                case 'type':
                default:
                    $tagw = $tc->get_weight_type($tags['count']);
                    break;
            }



            $max = $tagw[ceil(count($tagw)/10)-1]['weight'];
            $min = $tagw[count($tagw)-1]['weight'];

            foreach ($tagw as &$t) {
                if ($t['weight'] > $max) {
                    $t['weight'] = 1;
                } else {
                    if ($max-$min != 0) {
                        $t['weight'] = ($t['weight'] - $min) / ($max - $min);
                    } else {
                        $t['weight'] = 0;
                    }
                }
                $t['px']=round(10*(1+$t['weight']));
            }

            shuffle($tagw);
            $tpl->assign("link", $link);
            $tpl->assign("type", $tags['type']);
            $tpl->assign("tags", $tagw);
        } else {
            echo "<!-- (tag_cloud:cache) -->";
        }

        return $tpl->fetch("tag_cloud.tpl", "tag_cloud|".$tags['type']);

    }
}
