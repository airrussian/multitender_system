<?php
class multitender_action_region extends multitender_action_sidebar_last {
    function run() {
        $region_id = (defined('REGION_CHPU_ID')) ? REGION_CHPU_ID : FALSE;
        if (!$region_id) {
            $region_id = @(int)$_GET['region_id'];
        }
        if (!$region_id) {
            $region_id = @(int)$_COOKIE['tc_region'];
        }
        if (!$region_id) {
            return 'Запрашиваемого вами региона не существует';
        }
        $_REQUEST['search'] = array('reg'=>array($region_id=>'on'));
        $tpl = $this->new_tpl();
        $tpl->caching = TRUE;
        $tpl->cache_lifetime = 60 * 60 * 24 * 15; 
        
        $sf = $this->new_action('searchform');
        
        $tpl->assign('searchform',$sf->run());

        $data_common = $this->new_model('data_common');
        $data_common = $data_common->get_data();
        foreach ($data_common['region'] as $region) {
            if ($region['id'] == $region_id) {
                $tpl->assign('reg_name', $region['name']);
                $tpl->assign('reg_name_r', $region['name_rp']);
                $region_name = $region['name'];
            }
        }

        if (!$tpl->is_cached('region.tpl', $region_id)) {
            $tpl->assign('reg_id', $region_id);
            $rm = $this->new_model('region');
            $tpl->assign('reg_text', $rm->getRegionText($region_id));
            $last_items = $rm->getLastItems($region_id);
            $tpl->assign('reg_last_items', $last_items);
            $top_price = $rm->getTopPrice($region_id);
            $tpl->assign('reg_top_price', $top_price);
            $grapher = $this->new_action('grapher');
            $data = $rm->getAvgPrice($region_id);
            $graph1 = $grapher->draw_area(array('т.руб.' => $data), 'Средняя цена контракта', '', 372, 300);
            $data = $rm->getTotalItems($region_id);
            $graph2 = $grapher->draw_area(array('шт.'=>$data), 'Количество новых тендеров', '', 372, 300);
            $tpl->assign('graph2', $graph2);
            $tpl->assign('graph1', $graph1);
            
        }
        
        $this->conf['page_title'] = "$region_name. Госзакупки, госзаказ и коммерческие тендеры региона";
        $this->conf['description'] = "$region_name. Закупки, статистика, заказчики и поставщики региона. Обзор экономики";
        $this->conf['keywords'] = "$region_name, тендеры, госзакупки, госзаказ, коммерческие тендеры, заказчики, поставщики, статистика, экономика";

        /*Костиков костыль*/
        if (($region_name=='Архангельская область') || ($region_name=='Астраханская область') || ($region_name=='Белгородская область') || ($region_name=='Владимирская область')) {
            $this->conf['description'] = ' ';
            $this->conf['page']['description'] = ' ';
            $this->conf['keywords'] = ' ';
            $this->conf['page_title'] = $region_name;
        }

        return $tpl->fetch('region.tpl', $region_id);
        
    }
}
