<?php
/*
 * Последние тендеры
 */
class multitender_action_sidebar_last extends multitender_action {
    function run() {
        $tpl = $this->new_tpl();
        $geoip_m = new multitender_model_geoip();
        $geoip_m->number = 10;
        $items = $geoip_m->get_last_items(null);
        
        $data_common = multitender_model_data_common::singleton()->get_data();
        
        foreach ($items as &$item) {
            if ($item['region_id']) {
                $item['region'] = $data_common['region'][$item['region_id']]['sname'];
            } 
            if ($item['type_id']) {
                $item['type'] = $data_common['type'][$item['type_id']]['name'];
            }
        }
        
        $tpl->assign( 'items', $items );
        return $tpl->fetch('sidebar_last.tpl');
    } //function run
}
