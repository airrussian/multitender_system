<?php

class multitender_action_detail44 extends multitender_action {

    public function index() {

        if (!isset($_REQUEST['purchaseNumber'])) {
            header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
            exit();
        }
        $purchaseNumber = $_REQUEST['purchaseNumber'];

        try {
            $url = "http://win.multitender.ru/index.php?controller=tender&action=detail&tmpl=json&purchaseNumber=";
            $detail = file_get_contents($url . $purchaseNumber);            
            if ($detail != "") {
                $item = json_decode($detail, true);
            } else {
                $item = array();
            }
        } catch (Exception $ex) {
            
        }


        if (empty($item)) {
            header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
            exit;
        }
        
        $status_day = Ceil((strtotime($item['tender']['endDate']) - time()) / (60 * 60 * 24));
        
        //var_dump($item['tender']);

        $subs = $this->new_action('subscribe');
        $freesubscribe = $subs->freenewsletter();        
        
        $tpl = $this->new_tpl();
        $tpl->assign('tender', $item['tender']);
        $tpl->assign('status', $status_day);
        $tpl->assign('freesubscribe', $freesubscribe);
        return $tpl->fetch('detail44/index.html.tpl');
    }

}
