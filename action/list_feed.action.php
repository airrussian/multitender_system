<?php
class multitender_action_list_feed extends multitender_action {

    function run() {
        $tpl = $this->new_tpl();
        $rs = $this->db->SelectLimit("
SELECT id,name FROM region
ORDER BY id ASC"
);
        $items = $rs->GetArray();

        $total = $this->db->GetArray("SELECT FOUND_ROWS()");
        $total = $total[0][0];

        $scrol = $this->new_action( 'scrol' );
        $scrol->link  = "?action=kostyl&amp;type=$type&amp;page=";
        $scrol->total = ceil($total / $this->conf['pref']['ppp']);
        $tpl->assign( 'scrol',  $scrol->run() );

        $tpl->assign( 'total',  $total );

        foreach( $items as &$i ) {
            $i['link'] = $this->conf['pref']['link_base_rss'] . '?search%5Breg%5D%5B' . $i['id'] . "%5D";
        }

        $tpl->assign('items',  $items);
        return $tpl->fetch('list_feed.tpl');
    } //function run

}

