<?php

class multitender_action_cabinet_baner extends multitender_action {
    function run() {
        $user = $GLOBALS['tenders']['conf']['user'];
        
        $rights = $this->conf['user']['right'];

        if (($rights<=1) || (($rights<=100) && (isset($_REQUEST['do'])))) {
            $tpl2 = $this->new_tpl();
            return $tpl2->fetch('msg_right.tpl');
        }

        $cm = $this->new_model('cabinet_baner');

        $do = @$_REQUEST['do'];

        switch ($do) {
            case 'remove':
                $cm->rem_record($_GET['id']);
                header('location: /tenders/cabinet_baner');
                break;
            case 'add':
                if (!isset($_POST['freq']) || !isset($_POST['pathway']) || !isset($_POST['region']) || !isset($_POST['type']) || !isset($_POST['link'])) {
                    header('location: /tenders/cabinet_baner');
                    break;
                }
                if (!isset($_POST['onlymain'])) {
                    $_POST['onlymain'] = 0;
                }
                if (!isset($_POST['published'])) {
                    $_POST['published'] = 0;
                }
                $cm->add($_POST['pathway'], $_POST['region'], $_POST['type'], $_POST['onlymain'], $_POST['link'],$_POST['published'], $_POST['freq']);
                header('location: /tenders/cabinet_baner');
                break;
            case 'edit':
                if (!isset($_POST['freq']) || !isset($_POST['id']) || !isset($_POST['region']) || !isset($_POST['type']) || !isset($_POST['link'])) {
                    header('location: /tenders/cabinet_baner');
                    break;
                }
                if (!isset($_POST['onlymain'])) {
                    $_POST['onlymain'] = 0;
                }
                if (!isset($_POST['published'])) {
                    $_POST['published'] = 0;
                }
                $cm->edit($_POST['id'], $_POST['region'], $_POST['type'], $_POST['onlymain'], $_POST['link'],$_POST['published'], $_POST['freq']);
                header('location: /tenders/cabinet_baner');
                break;
            case 'direct':
                $cm->direct($_POST['468x120'], $_POST['950x120'], $_POST['195x500'], $_POST['195x235']);
                header('location: /tenders/cabinet_baner');
                break;
            case 'directfreq':
                $cm->directfreq($_POST['hleft'], $_POST['hright'], $_POST['hwide'], $_POST['vtop'],$_POST['vbot'], $_POST['vwide']);
                header('location: /tenders/cabinet_baner');
                break;
            default:
                break;
        }

        $tpl = $this->new_tpl();

        $records = $cm->show_all();

        $regions = $cm->get_regions();

        $direct = $cm->getdirect();

        foreach ($direct as $element) {
            $tpl->assign('direct' . $element['id'], $element['text']);
        }

        foreach ($regions as $item) {
            $temp[] = $item['name'];
        }

        array_multisort($temp, SORT_ASC, $regions);

        foreach ($records as &$item) {
            if ($item['region'] == 0) {
                $item['regionname'] = 'Для всех регионов';
            } else {
                foreach ($regions as $reg) {
                    if ($reg['const']==$item['region']) {
                        $item['regionname'] = $reg['name'];
                        break;
                    }
                }
            }
        }

        $tpl->assign('regions', $regions);

        $tpl->assign('list', $records);
        $tpl->assign('dfreq',$cm->getdirectfreq());

        return $tpl->fetch('cabinet_baner.tpl');
    }

}