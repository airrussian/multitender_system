<?php

class multitender_action_joomla_checkregform extends multitender_action {

    function run() {

        $joomla_users = "ten_users";

        if (empty($_GET['field']) || empty($_GET['value'])) {
            return "&nbsp;";
        }

        if (($_GET['field']<>'username') && ($_GET['field']<>'email')) {
            return "&nbsp;";
        }

        if (empty($this->db_joomla)) {
            $this->db_joomla = ADONewConnection($this->conf['db_conf']['joomla']['dsn']);
            $this->db_joomla->Execute('set names utf8');
        }
        if ($this->db_joomla->GetOne("SELECT ".$_GET['field']." FROM $joomla_users WHERE ".$_GET['field']."=?", array($_GET['value']))) {
            if ($_GET['field']=='username') {
                return "<span style='color: red' error=1>Имя пользователя занято</span>";
            }
            if ($_GET['field']=='email') {
                return "<span style='color: red' error=1>Пользователей с таким email уже зарегистрирован</span>";
            }
        } else {
            return "<span style='color: green' error=0>Свободно</span>";
        }
    }
}
