<?php

class multitender_action_cabinet_moderator extends multitender_action {

    function  __construct() {
        parent::__construct();
        $rights = $this->conf['user']['right'];
        if ($rights<=1) {
    	    $tpl = $this->new_tpl();
    	    $tpl->display("msg_right.tpl");
    	    exit;
        }
    }

    function run() {
        $this->conf['page_title'] = 'Кабинет модератора';
        $GLOBALS['tenders']['conf']['page_joomla_links'][] = '<link rel="stylesheet" href="/templates/mt/css/moder.css" type="text/css"';
        $tpl = $this->new_tpl();
        $tpl->assign('link_base', $this->conf['pref']['link_base']);
        $tpl->assign('right', $this->conf['user']['right']);
        return $tpl->fetch("cabinet_moderator.tpl");
    }

}