<?php

class multitender_action_cabinet_baner_stat extends multitender_action {
    function run() {
        $user = $GLOBALS['tenders']['conf']['user'];
        
        $rights = $this->conf['user']['right'];

        if ($rights<=1) {
            $tpl2 = $this->new_tpl();
            return $tpl2->fetch('msg_right.tpl');
        }

        $id = $_GET['id']?$_GET['id']:false;

        $model = $this->new_model('cabinet_baner_stat');
        $list = $model->GetStat($id);
        $string = '<table class="view">';
        foreach ($list as $elem) {
            $string .= '<tr><td width="300px"><b>' . date('j.m.Y', $elem['date']*604800+345600) . '-' . date('j.m.Y', ($elem['date']+1)*604800+259200) . '</b></td><td>Кликов: ' . $elem['cnt_click'] . '</td><td>Показов: ' . $elem['cnt_shows'] . '</td><td>CTR: ' . round($elem['cnt_click']/$elem['cnt_shows']*100) . '%</td></tr>';
        }
        $string .= '</table>';

        $tpl = $this->new_tpl();
        $tpl->assign('list',$string);

        return $tpl->fetch('cabinet_baner_stat.tpl');
    }

}