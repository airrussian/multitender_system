<?php

class multitender_action_gosexpress extends multitender_action {

    private $data = false;
    private $page = false;  // [bg, cg, gt, tc]
    private $messageError = false;

    private $sendto         = "sbpost@sbaspect.ru";
    private $backaddress    = false;

    private $send = false;

    private $body = "";
    private $head = "";
    private $theme = "";

    private function validate() {
        $this->messageError = false;

        $this->data = $_POST;

        $this->data['user_id'] = $this->conf['user']['id'];
        $this->data['page']    = "gosexpress - " . $this->page;

        $telephone = $this->data['contact']['telephone']['plus'] . $this->data['contact']['telephone']['code'] . $this->data['contact']['telephone']['numb'];
        
        if (!$telephone) {
            $this->messageError = "Укажите номер телефона";
            return false;
        }        
        
        if (empty($this->data['contact']['personal'])) {
            $this->messageError = "Введите ваше имя";
            return false;
        } 
        
        if (empty($this->data['contact']['email'])) {
            $this->messageError = "Введите email";
            return false;
        }
        
        if (empty($this->data['contact']['personal'])) {
            $this->messageError = "Укажите контактное лицо";
            return false;            
        }

        $this->backaddress = $this->data['contact']['email'];

        $this->head = $this->GetHeader();
        switch ($this->page) {
            case "bg":
                $this->theme = "Новая онлайн-заявка на Банковскую гарантию с сайта http://multitender.ru/ \n";
                break;
            case "cg":
                $this->theme = "Новая онлайн-заявка на Кредит на исполнение государственного контракта с сайта http://multitender.ru/ \n";
                break;
            case "gt":
                $this->theme = "Новая онлайн-заявка на Поручительство с сайта http://multitender.ru/ \n";               
                break;
            case "tc":
                $this->theme = "Новая заявка на Тендерный кредит с сайта http://multitender.ru/ \n";
                break;
        }
       
        $tpl=$this->new_tpl();
        $tpl->assign("data", $this->data);
        $this->body = $tpl->fetch("gosexpress_mail_{$this->page}.tpl");

        return true;
    }

    private function show() {

        if (!preg_match("#bg|cg|gt|tc#", $this->page)) {
            return false;
        }

        $tpl=$this->new_tpl();
        $tpl->assign("send", $this->send);
        $tpl->assign("messageError", $this->messageError);
        $tpl->assign("data", $this->data);
        $tpl->assign("link_base", $this->conf['pref']['link_base']);
        return $tpl->fetch("gosexpress_form_{$this->page}.tpl");

    }

    private function GetHeader() {
        $header  = "MIME-Version: 1.0"."\n";
        $header .= "Content-Type: text/html; charset=UTF-8 \n";
        $header .= "Date: ".date("Y-m-d (H:i:s)",time())."\n";
        $header .= "From: GosExpress.online <GosExpress.online@gos-express.ru>\n";
        $header .= "X-Mailer: PHP/".phpversion()."\n";
        $header .= "Reply-To: ".$this->backaddress."\n\n";
        return $header;
    }

    private function send() {
        
        mail($this->sendto,                     $this->theme, $this->body, $this->head);
        mail($this->backaddress,                $this->theme, $this->body, $this->head);

        /*$onlineorder = $this->new_model("onlineorder");
        $onlineorder->gozexpress_addrequest($this->data);*/

        $this->send = true;
    }

    public function run() {
        $this->page = isset($_GET['page']) ? $_GET['page'] : false;

        if (!$this->page) {
            return false;
        }

        if (!empty($_POST)) {
            if ($this->validate()) {
                $this->send();
            }
        }
        
        return $this->show();
    }


}