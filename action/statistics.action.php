<?php
/**
 * Модуль статистики
 * http://wiki.consalt-it.ru/tenders:statistics
 *
 */
class multitender_action_statistics extends multitender_action {
    
    function right_check() {
        $rights = $this->conf['user']['right'];
        if ($rights<=1) {
    	    $tpl = $this->new_tpl();
    	    $tpl->display("msg_right.tpl");
    	    exit;
        }                
    }

    function run() {
                
        $tpl = $this->new_tpl();

        $site_id = @ (int)$_GET['id'];

        if(!$site_id) {
            $items = $this->get_statistics();
            $total = $items['total']; unset($items['total']);
            $tpl->assign('total', $total);
            $tpl->assign('items', $items);
            if (isset($_GET['ajax'])) {
                return $tpl->fetch('statistics_all.xml.tpl');
            } else {           
                $this->right_check();
                return $tpl->fetch('statistics_all.tpl');
            }
        }
        else {
            $this->right_check();
            $site = $this->db->GetArray("SELECT * FROM site WHERE id = $site_id");
            if(empty($site) && ($site_id !== 1001)) {
                return null;
            }

            $items = $this->get_statistics_site($site_id);
            $tpl->assign('items', $items);
            

            $item1 = @array_shift($items);
            $tpl->assign('date', $item1['date']);

            return $tpl->fetch('statistics_site.tpl');
        }

    } //function run


    function get_statistics_site($id) {
        $id = (int) $id;

        $where = "site_id = $id AND";        

        $regions = $this->db->GetArray("SELECT * FROM region");
        $regions = $this->array_make($regions);

        $fetch = $this->db->GetArray("
            SELECT item.region_id, COUNT(*) as count, CAST(date_add AS DATE) as date
            FROM item
            WHERE
                $where
                date_add > " . $this->db->DBDate(time() - 7*24*60*60) . "
            GROUP BY region_id, CAST(date_add AS DATE)
            ORDER BY region_id, date
        ");

        foreach($fetch as $f) {
            $dates[$f['date']] = null;
        }

        foreach($fetch as $f) {
            $return[$f['region_id']] = array(
                'date'       => $dates,
                'region_id'  => $f['region_id'],
            );
        }

        foreach($fetch as $f) {
            $return[$f['region_id']]['date'][$f['date']] = $f['count'];           
        }

        foreach ($return as $k=>&$r) {
            if(isset($regions[$k]['name'])) {
                $r['name'] = $regions[$k]['name'];
            }
        }

        //FIXME Итого

        return $return;
    }


    function get_statistics() {
        $return = array();

        $fetch = $this->db->GetArray("SELECT * FROM site ORDER BY priority DESC, disable ASC, error_counter DESC");

        foreach( $fetch as $f ) {
            $return[$f['id']] = $f;
        }

        $week  = $this->get_some_per(time() - 7*24*60*60);
        $day   = $this->get_some_per(time() -   24*60*60);
        $day3  = $this->get_some_per(time() - 3*24*60*60);
        $hour3 = $this->get_some_per(time() -    3*60*60);
        $all   = $this->get_some_per();

        $total = array( 'count_week'=>0, 'count_day'=>0, 'count_day3'=>0, 'count_hour3'=>0, 'count_all'=>0 );

        foreach( $return as &$r ) {
            $r['count_week']  = @$week[$r['id']];
            $r['count_day']   = @$day[$r['id']];
            $r['count_day3']  = @$day3[$r['id']];
            $r['count_hour3'] = @$hour3[$r['id']];
            $r['count_all']   = @$all[$r['id']];

            $total['count_week']  += @$week[$r['id']];
            $total['count_day']   += @$day[$r['id']];
            $total['count_day3']  += @$day3[$r['id']];
            $total['count_hour3'] += @$hour3[$r['id']];
            $total['count_all']   += @$all[$r['id']];
        }
       
        $regions = $this->db->GetArray("SELECT * FROM region");
        $regions = $this->array_make($regions);

        foreach( $return as &$r ) {
            $r['region_name'] = @$regions[$r['region_id']]['name'];
        }

        $return['total'] = $total;

        return $return;
    }


    function get_some_per($time = null) {
        $where = '';
        if( ! is_null($time) ) {
            $where = 'WHERE item.date_add > ' . $this->db->DBDate($time);
        }

        $fetch = $this->db->GetArray("
            SELECT COUNT(*) as count, site_id as id FROM item
            $where
            GROUP BY site_id
            ");

        $fetch = $this->array_make($fetch);

        foreach ($fetch as $f) {
            $return[$f['id']]  = $f['count'];
        }

        return $return;
    }


    function array_make(array $array, $key = 'id') {
        $return = array();
        foreach( $array as $v ) {
            $return[$v['id']] = $v;
        }
        return $return;
    }

}

