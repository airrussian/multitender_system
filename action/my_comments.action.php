<?php

class multitender_action_my_comments extends multitender_action {

    function run() {
        $user = $GLOBALS['tenders']['conf']['user'];
        $user_id = @ $user['id'] ? $user['id'] : null;
        $page = @ $_GET['page'] ? $_GET['page'] : 1;
        $key = @ $_GET['key'] ? $_GET['key'] : NULL;
        $edit = @ $_GET['edit'] ? $_GET['edit'] : NULL;
        $rem = @ $_GET['rem'] ? $_GET['rem'] : NULL;
        $filter = @ $_REQUEST['filter'] ? $_REQUEST['filter'] : NULL;
        $filter = preg_replace('#[^a-zа-яё0-9/_]#iu', ' ', $filter);
        $filter = trim(preg_replace('/\s+/', ' ', $filter));
        $filter = trim($filter);

        $flag = @ $_GET['flag'] ? $_GET['flag'] : 1;

        $names = explode(' ', $filter);
        $filter = '';
        foreach ($names as $v) {
            if (mb_strlen($v,'UTF-8') >= 3) {
                $filter .= $v . ' ';
            }
        }
        $filter = trim($filter);

        $rights = $this->conf['user']['right'];

        if ($rights<=0 || !$user_id) {
            $tpl2 = $this->new_tpl();
            return $tpl2->fetch('msg_right.tpl');
        }

        if ($key) {
            $edit_text = @ $_POST['message' . $key] ? $_POST['message' . $key] : NULL;
            $edit_text = trim($edit_text);
            if ($edit_text != '') {
                $edit_text = substr($edit_text, 0, 1999);
                $edit_text = nl2br($edit_text);
            }
        }

        $cm = $this->new_model('my_comments');

        if ($key && $edit) {
            if ($edit_text != '' && $edit) {
                $cm->update_edited(array($edit_text, (int)$edit, (int)$user_id));
                header("Location: /tenders/my_comments/" . '?page=' . $page);
            }
        }

        if ($rem) {
            $cm->rem_comment(array((int)$rem, (int)$user_id));
            header("Location: /tenders/my_comments/" . '?page=' . $page);
        }

        $rectopg = 10;
        $offset = $rectopg*($page-1);
        $comments = $cm->select_comments($offset, $rectopg, $user_id, $filter, $flag);

        $total = $comments['total'];
        unset($comments['total']);

        $scrol = $this->new_action("scrol");
        $scrol->addelement = '';
        $scrol->link = "/tenders/my_comments/?" . $scrol->link;
        $scrol->total = $total;
        $scrol->onpage= $rectopg;

        foreach ($comments as &$items) {
            if ((((strtotime(date('Y-m-d H:i:s'))) < (strtotime($items['date_time'])+30*60))  || ($items['personal_flag']==1))) {
                $items['edit_flag'] = 1;
            } else {
                $items['edit_flag'] = 0;
            }
            $temp=explode(" ",$items['text']);
            foreach ($temp as &$a) {
                $a = substr($a, 0, 100);
            }
            $items['text'] = implode($temp, " ");
            $items['text'] = htmlspecialchars($items['text'], ENT_QUOTES);
            $items['text'] = str_replace('&lt;br/&gt;', '<br />', $items['text']);
            $items['text'] = str_replace('&lt;br&gt;', '<br />', $items['text']);
            $items['text'] = str_replace('&lt;br /&gt;', '<br />', $items['text']);
            $items['edittext'] = str_replace('<br />', "", $items['text']);
            $items['date_time'] = date("d.m.Y H:i",strtotime($items['date_time']));

            if ($filter) {
                    $names = explode(' ', $filter);
                    foreach ($names as $v) {
                        $items["text"] = ltrim(preg_replace("/^[\.,\?\-_!()]/", '', ltrim($items["text"]),1));
                        $items["text"] = preg_replace("/$v/iu", '<font color=#cc0000>\\0</font>', $items["text"]);
                    }
            }

            if ($items['personal_flag'] == 1) {
                $items['remlink'] = '/tenders/my_comments?rem=' . $items['id'];
            }

            if ($items['edit_flag'] == 1) {
                $items['editlink'] = '/tenders/my_comments?edit=' . $items['id'];
            }
        }

        
        $tpl = $this->new_tpl();

        if ($filter) {
            $tpl->assign("flagfilter", 1);
        } else {
            $tpl->assign("flagfilter", 0);
        }

        $tpl->assign('filter',$filter);
        $tpl->assign("flag", $flag);
        $tpl->assign("scrol", $scrol->run());
        $tpl->assign("comments", $comments);

        $tpl->assign("page", $page);

        return $tpl->fetch('my_comments.tpl');

    }
}