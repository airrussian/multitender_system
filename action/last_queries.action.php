<?php

class multitender_action_last_queries extends multitender_action {

    function list_query($user_id, $link_act, $rectopg) {
        
	$link_base = $this->conf['pref']['link_base'];
        $pg = @ $_GET['page'] ? $_GET['page'] : 1;

        $qu = $this->new_model('query_users');

        $items = $qu->get_last_query($user_id, ($pg-1)*$rectopg, $rectopg);
        $total = $items['total'];
        unset($items['total']);

        $q_ids = array();
        if (!empty($items)) {
            foreach ( $items as &$v ) {
                $q_ids[] = $v['id'];
            }
        }

        $tpl= $this->new_tpl();
        $tpl->assign("reguser", $user_id);
	$tpl->assign('link', $link_base);

        $scrol = $this->new_action("scrol");
        $scrol->link = '/tenders/history_queries/?page=';
        $scrol->total = $total;
        $scrol->onpage = $rectopg;
        $tpl->assign('scrol', $scrol->run());

        if ($user_id) {
            $fq=$this->new_action('favorite_queries');
            $favq=$fq->get_status($q_ids);
        }
        
        if ($items) {
            foreach( $items as &$v ) {
                $v['search'] = unserialize($v['search']);
                //FIXME constract run!
                $s_obj = $this->new_model('search', $v['search'] );
                $s_obj->SetSearch( $v['search'] );
                $v['search_txt'] = $s_obj->to_info();
                $v['favorite'] = $favq[$v['id']];
                $v['link_query'] = $s_obj->to_url();
            }
        }
        $tpl->assign('items', $items);
        return $tpl->fetch('last_queries_list.tpl');
    }

    function show() {
        $user_id = $this->conf['user']['id'];
	$link    = $this->conf['pref']['link_base'];
        $rectopg = $this->conf['db_conf']['person']['last_tenders_rectopg'];
        $items   = $this->list_query($user_id, "action=last_queries", $rectopg);

        $tpl = $this->new_tpl();
        $tpl->assign('list', $items);
        return $tpl->fetch('last_queries.tpl');
    }

    function run() {
        /* Получаем задачу для action из GET */
        $task = @ $_GET['task'] ? $_GET['task'] : 'show';
        /* Определяем задачу для этого action */
        switch ($task) {
            case 'list_query': return $this->list_query();
                break;
            case 'show': return $this->show();
                break;
        }
    }
}

