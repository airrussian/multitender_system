<?php

class multitender_action_by_analit extends multitender_action {
    
    function run() {
        
        $tpl = $this->new_tpl();
        $tpl->assign('regions', tenders_run_action('by_region'));
        $tpl->assign('customer', tenders_run_action('by_customer'));
        return $tpl->fetch('by_analit.tpl');
        
    }
    
}