<?php
/**
 * Base class for ACTIONs
 * ИДЕЯ, базовый метод, где по умолчанию все запрещено (безопасность, доступ)
 */
abstract class multitender_action {
    /**
     * @var ADOConnection
     */
    public $db;
    public $conf;
    /**
     * @var Memcache
     */
    public $memcache_obj;

    function  __construct() {
        $this->db           = &$GLOBALS['tenders']['db'];
        $this->conf         = &$GLOBALS['tenders']['conf'];
        $this->memcache_obj = &$GLOBALS['memcached_res'];
    }

    /**
     * new object Smarty with config
     * @return Smarty
     */
    function new_tpl() {
        $tpl = new Smarty();

        $tpl->template_dir    = $this->conf['smarty']['template_dir'];
        $tpl->compile_dir     = $this->conf['smarty']['compile_dir'];
        $tpl->cache_dir       = $this->conf['smarty']['cache_dir'];
        //$tpl->config_dir    = dirname(__FILE__) . '/web/www.domain.com/smarty/configs';
        $tpl->plugins_dir[]   = $this->conf['smarty']['plugins_dir'];
        $tpl->error_reporting = $this->conf['smarty']['error_reporting'];

        $tpl->assign('conf',  $this->conf );

        return $tpl;
    }

    function new_model($name) {
        return tenders_new_model($name);
    }

    function new_action($action) {
        return tenders_new_action($action);
    }
    
    function run() {        
        $task = isset($_REQUEST['task']) ? $_REQUEST['task'] : 'index';
        if (method_exists($this, $task)) {
            return $this->$task();
        }               
    }

}
