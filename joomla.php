<?php
/**
 * Joomla! 1.5 component Tenders
 *
 * @version $Id: tenders.php 2009-10-07 00:21:56 svn $
 * @author Paul
 * @package Joomla
 * @subpackage Tenders
 * @license GNU/GPL
 *
 *
 *
 * This component file was created using the Joomla Component Creator by Not Web Design
 * http://www.notwebdesign.com/joomla_component_creator/
 *
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

header('Content-Type: text/html; charset=utf-8');

// add tenders

require_once dirname(__FILE__) . "/main.php";

$conf = & $GLOBALS['tenders']['conf'];

$user =& JFactory::getUser();

// FIXME возможно уже устарело
if ( $user->get('guest') ) {
    $GLOBALS['tenders']['conf']['full']  = false;
} else {
    $GLOBALS['tenders']['conf']['full']  = true;
}

if ( ! $user->get('guest') ) {
    $GLOBALS['tenders']['conf']['user']['joomla_id']     = $user->get('id');
    $GLOBALS['tenders']['conf']['user']['id']            = null;
    $GLOBALS['tenders']['conf']['user']['name']          = $user->get('username');
    $GLOBALS['tenders']['conf']['user']['right']         = 1;
    $GLOBALS['tenders']['conf']['user']['email']         = $user->get('email');
    $GLOBALS['tenders']['conf']['user']['type']          = 'user';
    $GLOBALS['tenders']['conf']['user']['payment_type']  = null;
    $GLOBALS['tenders']['conf']['user']['lastvisitDate'] = $user->get('lastvisitDate'); // 2010-08-24 02:53:27
    $GLOBALS['tenders']['conf']['user']['last_visit']    = null;
} else {
    $GLOBALS['tenders']['conf']['user']['joomla_id']     = null;
    $GLOBALS['tenders']['conf']['user']['id']            = 0;
    $GLOBALS['tenders']['conf']['user']['name']          = 'anonym';
    $GLOBALS['tenders']['conf']['user']['right']         = 0;
    $GLOBALS['tenders']['conf']['user']['type']          = 'anonym';
    $GLOBALS['tenders']['conf']['user']['payment_type']  = null;
    $GLOBALS['tenders']['conf']['user']['last_visit']    = null;
    tc_try_set_bots();
}

// FIXME существенно переработать
tenders_run_action('users');
