<?php

$debug_base = false;

if (isset($_GET['debug'])) {
    $debug_base = true;
}

$conf['basedir'] = dirname(__FILE__);

$conf['dir']['action'] = dirname(__FILE__) . '/action';
$conf['dir']['model']  = dirname(__FILE__) . '/models';
$conf['dir']['view']   = dirname(__FILE__);
$conf['dir']['templ']  = dirname(__FILE__);
$conf['dir']['helper'] = dirname(__FILE__);

$conf['libdir'] = '/home/tenders/www/libs';

$conf['db_conf']['tenders']['dsn']  = 'EMPTY';
$conf['db_conf']['tenders']['debug'] = $debug_base;

// *** Подключение к krasnoyarsk.tenders
$conf['db_conf']['tenders_write']['dsn']  = 'EMPTY';
$conf['db_conf']['tenders_write']['debug'] = $debug_base;

// *** Подключение ко второй базе ***
$conf['db_conf']['person']['dsn']  = 'EMPTY';
$conf['db_conf']['person']['debug'] = $debug_base;

// *** Joomla ***
$conf['db_conf']['joomla']['dsn']  = 'mysql://beta:mtbeta@localhost/beta_joomla?new';
$conf['db_conf']['joomla']['debug'] = $debug_base;

// *** Sphinx ***
$conf['db_conf']['sphinx']['dsn']  = 'mysql://sphinx@127.0.0.1:9306';
$conf['db_conf']['sphinx']['debug'] = $debug_base;

// FOR COMPITABLE
$conf['db']['tenders']['host']  = 'beta.multitender.ru';
$conf['db']['tenders']['user']  = 'beta';
$conf['db']['tenders']['pass']  = 'mtbeta';
$conf['db']['tenders']['db']    = 'beta_tenders';
$conf['db']['tenders']['debug'] = $debug_base;

$conf['pref']['ppp'] = 50;

$conf['pref']['link_base']    = "index.php?";
$conf['pref']['link_detail']  = 'index.php?action=detail&id=';

$conf['pref']['link_base_http']        = "http://beta.multitender.ru/";
$conf['pref']['link_base_http_wo']     = "http://beta.multitender.ru";
$conf['pref']['link_base_http_rss']    = "http://feed.multitender.ru/rss/?";
$conf['pref']['link_base_http_rss_wo'] = "http://feed.multitender.ru/rss/";
$conf['pref']['link_base_http_feed']   = "http://feed.tender-consalt.ru/feed.php?";

$conf['pref']['link_base_rss']         = "http://feed.multitender.ru/rss/";

$conf['pref']['site_name'] = 'Multitender.ru';

$conf['pref']['generate_url'] = 'tenders_generate_url_default';

// Последние тендеры (не добавлять как просмотренный если он есть среди последних N)
$conf['db_conf']['person']['last_tenders_replays'] = 15; // FIXME УСТАРЕВШАЯ ПЕРЕМЕННАЯ?
// Последние запросы (не добавлять как последний если он есть среди последний N)
$conf['db_conf']['person']['last_searchs_replays'] = 15;
// Количество выводимых просмотренных тендеров на странице
$conf['db_conf']['person']['last_tenders_rectopg'] = 50;
// Количество выводимых избранных тендеров на странице
$conf['db_conf']['person']['fav_tenders_rectopg'] = 50;
$conf['db_conf']['person']['billing_exhibit_count']=100;
$conf['db_conf']['person']['email_newsletter_rectopg'] = 20;
// Длины для списков на странице История
$conf['db_conf']['person']['history_search'] = 15;
$conf['db_conf']['person']['history_tender'] = 10;

// почта для отправки онлайн заявок
$conf['db_conf']['person']['consultion_email_to']   = 'info@multitender.ru';
$conf['db_conf']['person']['consultion_email_from'] = 'support@multitender.ru';

$conf['onlineorder'] = array (
    'ten'   =>  array(
        'subject'   =>  'Онлайн заявка по тендерам',
        'tpl'       =>  'onlineorder_tenders.tpl',
        'form'      =>  'onlineorder_form.tpl',
        'sendto'    =>  'Alexey <airrussian@mail.ru>',
        'sendfrom'  =>  'support <support@multitender.ru>',
    ),
    'acp'   =>  array(
        'subject'   =>  'Заявка на получение ЭЦП',
        'tpl'       =>  'onlineorder_acp.tpl',
        'form'      =>  'onlineorder_form.tpl',
        'sendto'    =>  'Garant <psv@garant.ru>',
        'sendfrom'  =>  'support <support@multitender.ru>',
    ),
    'goz'   =>  array(
        'subject'   =>  'Заявка на обеспечение госконтракта',
        'tpl'       =>  'onlineorder_goz.tpl',
        'type'      =>  array(
            'credit'    =>  array(
                'title' =>  'тендерный кредит',
                'url'   =>  '/media/credit.doc',
            ),
            'garant'    =>  array(               
                'title' =>  'банковская гарантия',
                'url'   =>  '/media/garant.doc',
            ),
            'surety'    =>  array(
                'title' =>  'поручительство',
                'url'   =>  '/media/surety.doc',
            ),
        ),
        'form'      =>  'onlineorder_form_goz.tpl',
        'sendto'    =>  'ob <ob@multitender.ru>',
        'sendfrom'  =>  'support <support@multitender.ru>',
    )
);

$conf['emails']['onlineorder']['from']      = 'support <support@multitender.ru>';
$conf['emails']['onlineorder']['to_tender'] = 'Alexey <airrussian@mail.ru>';
$conf['emails']['onlineorder']['to_acp']    = 'Vladimir <vladimir@multitender.ru>';
$conf['onlineorder']['preg_acp_page_url']   = "#uslugi#si";

$conf['email_error_to']   = 'support@multitender.ru';
$conf['email_error_from'] = 'support@multitender.ru';

$conf['cache_lifetime'] = 60*60;
$conf['robokasse']['mrh_link']  = "https://merchant.roboxchange.com/Index.aspx";
$conf['robokasse']['mrh_login'] = "Multitender";
$conf['robokasse']['mrh_pass1'] = "123qwe123_pass1";
$conf['robokasse']['mrh_pass2'] = "123qwe123_pass2";
$conf['robokasse']['in_curr'] = "PCR";
$conf['robokasse']['email'] = "hz@mail.ru";
$conf['robokasse']['culture'] = "ru";

$conf['tender']['subscribe']['email'] = 'support@multitender.ru';
$conf['tender']['subscribe']['name']  = 'Multitender.ru';

$conf['smarty']['template_dir']    = dirname(__FILE__) . '/templates';
$conf['smarty']['compile_dir']     = dirname(__FILE__) . '/templates_c';
$conf['smarty']['cache_dir']       = dirname(__FILE__) . '/templates_cache';
$conf['smarty']['plugins_dir']     = dirname(__FILE__) . '/smarty_plugins';
$conf['smarty']['error_reporting'] = false;

// see #187
$conf['search']['min_word_len']    = 2;

define('TC_USER_ID',      'tc_user_id');
define('TC_ALERT_LOGIN',  'TC_ALERT_LOGIN');
define('AJAX_SOLT',       'AJAX_SOLT_SECRET_WORD_TENDER');
define('MT_LAST_VISIT',   'mt_last_visit');
