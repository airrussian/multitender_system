<?php

/***
 * Base class for MODELs
 */
abstract class multitender_model {
    /**
     * @var ADOConnection
     */
    public $db;
    public $conf;
    /**
     * @var Memcache
     */
    public $memcache_obj;

    function  __construct() {
        $this->db   = &$GLOBALS['tenders']['db'];
        $this->conf = &$GLOBALS['tenders']['conf'];
        $this->memcache_obj = &$GLOBALS['memcached_res'];
    }

    function new_model($name) {
        return tenders_new_model($name);
    }

}
