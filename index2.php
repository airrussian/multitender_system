<?php
// пример запуска

header('Content-Type: text/html; charset=utf-8');

// add tenders
require_once dirname(__FILE__) . "/main.php";

$GLOBALS['tenders']['conf']['pref']['link_base']    = "index.php2?";
$GLOBALS['tenders']['conf']['pref']['link_detail']  = 'index2.php?action=detail&id=';

$action = @$_GET['action'] ? $_GET['action'] : 'view';

//$enable = array('view', 'detail');

$enable = array('view');

if( in_array($action, $enable)) {
    echo tenders_run_action($action);
} else {
    echo "Доступ закрыт. Зарегестрируйтесь";
}

