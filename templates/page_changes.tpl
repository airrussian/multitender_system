<div class="pathway">
    <span style="background: transparent url(/templates/mt/img/sl.gif) no-repeat scroll left center; padding-left: 20px;">
        <a href="/tenders/cabinet_moderator">Кабинет модератора</a>
    </span>
    <span style="background: transparent url(/templates/mt/img/sl.gif) no-repeat scroll left center; padding-left: 20px;">
        <a href="/tenders/?action=pages_changes">Отслеживаемые страницы</a>
    </span>
</div>

<h1 id="add">Добавление новых тендеров</h1>

<div class="rc-white-head"></div>
<div class="rc-white-cont">

    <h2>Список послeдний добавлений для <a href="{$pg.page_url}" target="_blank">{$pg.page_url}</a>:</h2>

    {if $pg.text_change}
    <div><b>На странице были обнаружены изменения в тексте:</b></div>
    <div style="padding: 5px; background-color: #EEE; color: black; font-style: italic; font-size: 12pt; min-height: 100px; max-height: 200px; overflow: auto;">{$pg.text_change}</div>
    <br />
    {/if}
    <div style="width: 100%; text-align: center">
        <form action="{$link_base}action=pages_changes" method="post" id="add-new-tenders-form-no-change">
            <input type="hidden" value="{$pg.id}" name="page_id" />
            <button type="submit" value="измений больше нет" name="no_change">измений больше нет</button>
        </form>
    </div>
    <br />

    <div class="form_change">
        <form method="post" action="{$link_base}action=page_changes&page_id={$pg.id}{if $status=='edit'}&task=edit&item_id={$ni.id}{/if}">
            <table>
                <tr>
                    <td><label for="name">Название(*)</label></td>
                    <td><input id="name" name="name" type="text" value="{if $status}{$ni.name}{/if}" style="width: 100%" /></td>
                </tr>
                <tr>
                    <td><label for="type">Тип закупки</label></td>
                    <td><select id="type" name="type" type="text" style="width: 200px">
                            <option value="NULL">не указан</option>
                            {foreach from=$ts item=item}
                            <option value="{$item.id}" {if $status}{if $item.id==$ni.type}selected{/if}{/if}>{$item.name}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label for="region">Регион(*)</label></td>
                    <td><select id="region" name="region" type="text" style="width: 200px">
                            <option value="">---выберите регион---</option>
                            {foreach from=$rg item=item}
                            <option value="{$item.id}" {if $item.id==$li.region_id}selected{/if}>{$item.sname}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label for="begin">Дата публикации</label></td>
                    <td><input id="date_begin" name="begin" type="text" value="{if $status}{$ni.begin}{/if}" /></td>
                </tr>
                <tr>
                    <td><label for="end">Последний срок</label></td>
                    <td><input id="date_end" name="end" type="text" value="{if $status}{$ni.end}{/if}" /></td>
                </tr>
                <tr>
                    <td><label for="summa">Сумма, руб.</label></td>
                    <td><input id="summa" name="summa" type="text" class="num_only" style="width: 100%" value="{if $status}{$ni.summa}{/if}" /></td>
                </tr>
                <tr>
                    <td><label for="source">Ссылка на Detail</label></td>
                    <td><input id="source" name="link_source" type="text" value="{if $status}{$ni.link_source}{/if}" style="width: 100%" /></td>
                </tr>
                <tr>
                    <td><label for="customer">Заказчик</label></td>
                    <td><input id="customer" name="customer" type="text" value="{if $status}{$ni.customer|escape}{else}{$li.customer|escape}{/if}" style="width: 100%" /></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><button type="submit">{if $status=='edit'}Записать изменения{else}Добавить новый тендер{/if}</button></td>
                </tr>
            </table>
            <input type="hidden" name="defsource" value="{$pg.page_url}" />
            <input type="hidden" name="page_id" value="{$pg.id}" />
        </form>
    </div>

    <div class="info">{$info}</div>

    {if $items}

    <table class="view tbl_sort">
        <thead>
            <tr>
                <th class="{literal}{sorter: false}{/literal}" style="width:32px"></th>
                <th class="title">Название</th>
                <th class="type">Тип</th>
                <th class="type">Регион</th>
                <th class="date  {literal}{sorter: 'mtShortDate'}{/literal}">Публикация </th>
                <th class="lastd {literal}{sorter: 'mtShortDate'}{/literal}">Последний срок</th>
                <th class="price">Цена, тыс. руб.</th>
            </tr>
        </thead>
        <tbody>

            {foreach from=$items item=item}
            <tr>
                <td>
                    <a href="index.php?action=page_changes&page_id={$pg.id}&task=edit&item_id={$item.items.id}">
                        <img width="32" height="32" alt="редактировать" title="редактировать" src="/i/images/b_edit.png"/>
                    </a>
                </td>
                <td align="left"><a href="{$detail_link}{$item.items.id}">{$item.items.name}</a></td>
                <td>{$item.items.type_name}</td>
                <td>{$item.items.region_name}</td>
                <td>{$item.items.date_publication}</td>
                <td>{$item.items.date_end|tc_date}</td>
                <td>{$item.items.price|tc_price}</td>
            </tr>
            {/foreach}
            
        </tbody>
    </table>
    {$scrol}
    {else}
    <h4>С источника <a href="{$pg.page_url}" target="_blank">{$pg.page_url}</a> тендеров ещё не добавлялось</h4>
    {/if}

</div>
<div class="rc-white-foot"></div>