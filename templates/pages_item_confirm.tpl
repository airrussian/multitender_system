{literal}
<style>
    .cancel {
        background-image: url("/i/images/delete.png");
    }

    .edit {
        background-image: url(/i/images/b_edit.png);
    }
</style>

{/literal}

<h1>Список добавленных тендеров</h1>

<div class="rc-white-head"></div>
<div class="rc-white-cont">

    <ul class="filter-item">
        <li>{if $filter=="all"}<b>все</b>{else}<a href="index.php?action=moder_items&filter=all">все</a>{/if}</li>
        <li>{if $filter=="con"}<b>подвержденные</b>{else}<a href="index.php?action=moder_items&filter=con">подвержденные</a>{/if}</li>
        <li>{if $filter=="can"}<b>удаленые</b>{else}<a href="index.php?action=moder_items&filter=can">удаленые</a>{/if}</li>
        <li>{if $filter=="ncn"}<b>неподвержденные</b>{else}<a href="index.php?action=moder_items&filter=ncn">неподвержденные</a>{/if}</li>
    </ul>
    <br />
    {if $items}
    <table class="view tbl_sort">
        <thead>
            <tr>
                <th class="title">Название</th>
                <th class="date">Когда?</th>
                <th class="type">Тип</th>
                <th class="date  {literal}{sorter: 'mtShortDate'}{/literal}">Публикация </th>
                <th class="lastd {literal}{sorter: 'mtShortDate'}{/literal}">Последний срок</th>
                <th class="price">Цена, тыс. руб.</th>
                <th class="title">Источник</th>
                {if $rights>100}
                <th class="title">Модератор</th>
                {/if}
                {if $filter=="all"}
                <th class="title">Статус</th>
                {/if}
                {if $rights>100}
                <th class="title"></th>
                <th class="title"></th>
                {/if}
            </tr>
        </thead>
        <tbody>
            {foreach from=$items item=item}
            <tr>
                <td align="left"><a href="{$link_detail}{$item.id}">{$item.name}</a></td>
                <td>{$item.date_add}</td>
                <td>{$item.type_name}</td>
                <td>{$item.date_publication}</td>
                <td>{$item.date_end|tc_date}</td>
                <td>{$item.price|tc_price}</td>
                <td><a href="{$item.detail_link}" target="_blank">{$item.detail_link}</a></td>
                {if $rights>100}
                <td>{$item.moder.user_name}</td>
                {/if}
                {if $filter=="all"}
                <td>
                    {if $item.status==-1}удален{/if}
                    {if $item.status==0}неподвержденные{/if}
                    {if $item.status==1}подвержденные{/if}
                </td>
                {/if}
                {if $rights>100}
                <td>
                    {if $item.status<=0}<a href="/ajax.php?action=moder_items&task=confirm&item_id={$item.id}" title="потвердить" class="button-confirm">&nbsp;</a>{/if}
                </td>
                <td>
                    {if $item.status>=0}<a href="/ajax.php?action=moder_items&task=cancel&item_id={$item.id}" title="удалить" class="button-cancel">&nbsp;</a>{/if}
                </td>
                {/if}
            </tr>
            {/foreach}
        </tbody>
    </table>
    {$scrol}
    {else}
    <b>не найдено тендеров</b>
    {/if}

</div>
<div class="rc-white-foot"></div>