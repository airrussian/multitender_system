<h1>Кабинет модератора</h1>
<h2>Редактирование комментариев</h2>
<br/>
<form action="/tenders/comments_moder" method="POST">
    <label for="filter">Фильтр по тексту сообщения, имени автора или номеру тендера: </label><input id="filter" name="filter" value="{$filter}" />
    <button type="submit">Фильтр</button>
</form>
{if $comments}
{$scrol}
<table class="searchers" style="width:800px">
    <tr class="uunc">
        <td><b>Имя пользователя</b></td>
        <td style="width:500px"><b>Текст комментария</b></td>
        <td><b>Номер тендера в системе</b></td>
        <td><b>Тип комментария</b></td>
        <td><b>Дата добавления</b></td>
        <td><b>Удалить</b></td>
    </tr>
    {foreach from=$comments item=item}
    <tr class="{cycle values='unc,uunc'}">
        <td><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$item.user_id}">{$item.name}</a></td>
        <td>{$item.text}</td>
        <td><a href="/tenders/detail/{$item.tender_id}#comments">{$item.tender_id}</a></td>
        <td>{if $item.personal_flag}Заметка{else}Комментарий{/if}</td>
        <td>{$item.date_time}</td>
        <td><a href="/tenders/comments_moder?del={$item.id}">Удалить</a></td>
    </tr>
    {/foreach}
</table>
{$scrol}
{else}
<br/>
Комментариев нет!
{/if}