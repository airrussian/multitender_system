<div id='youRegion'>Ваш регион:<br /><a href="/tenders/{$region_link}" style='font-weight: bold; font-size:14px;'>{$region.name}</a></div>
<div id='lastTendersRegion'>
    <h3>Тендеры региона:</h3>
    {foreach from=$items item=item}
    <p><a href="{$item.link}" rel="nofollow">{$item.name|truncate}</a>{if $item.price}<br />{$item.price|mt_price} руб.{/if}</p>
    {/foreach}
</div>
<div id='allStatRegion'><a href="/tenders/analytics/?region_id={$region_id}" style='color: gray'>Вся статистика региона</a></div>

{*<div class="w6">
    <img class="w6i" src="/templates/mt/img/rss.png" alt="" /> <span class="w6r">RSS потоки</span><br />
    <a href="{$feed_link}" rel="nofollow" target="_blank">Читать RSS поток</a><br />
    <a href="http://lenta.yandex.ru/settings.xml?name=feed&amp;url={$feed_link|escape:'url'}" rel="nofollow" target="_blank">Читать в Яндекс.Ленте</a><br />
    <a href="http://fusion.google.com/add?feedurl={$feed_link|escape:url}" rel="nofollow" target="_blank">Добавить в Google Reader</a><br /><br />

    <img class="w6ii" src="/templates/mt/img/she.png" alt="" /> <span class="w6r">Виджеты</span><br />
    <a href="http://fusion.google.com/add?source=atgs&amp;feedurl={$feed_link|escape:url}" rel="nofollow" target="_blank">Виджет для Google</a><br />
    <a href="http://www.yandex.ru?add={$region.ya_widget_id}" target="_blank" rel="nofollow">Виджет для Яндекса</a><br />
    <a href="http://www.yandex.ru/catalog/?text=multitender" rel="nofollow" target="_blank">Каталог виджетов</a><br /><br />

    <table class="w6t" border="0" cellspacing="0" cellpadding="0">
        <tr>
        <td align="right"><strong>10000</strong></td>
        <td>Все тендеры</td>
        </tr>
        <tr>
        <td align="right"><strong>234</strong></td>
        <td>Актуальные тендеры</td>
        </tr>
        <tr>
        <td align="right"><strong>1234</strong></td>
        <td>Заказчики</td>
        </tr>
        <tr>
            <td class="w6t1" align="right"><img src="templates/mt/img/gr.gif" alt="стат." /></td>
            <td class="w6t1"><a href="/tenders/analytics/?region_id={$region_id}">Вся статистика региона</a></td>
        </tr>
    </table>

</div>
*}