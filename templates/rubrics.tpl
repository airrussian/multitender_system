<h1>Классификатор</h1>

<div class="mt_boxwhite">
    <div id="rubrics" class='tabs'>
        <ul>
            <li><a href="#rubRUS"><span>России</span></a></li>
            <li><a href="#rubSNG"><span>СНГ</span></a></li>
        </ul>

        <div id="rubRUS" class="rub-acc">{$rubRUS}</div>
        <div id="rubSNG" class="rub-acc">{$rubSNG}</div>
    </div>
</div>