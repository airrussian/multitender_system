<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
    <channel>
        <title>Статистика загрузки тендеров на multitender.ru</title>
        <description>Статистика загрузки тендеров на multitender.ru</description>
{foreach from=$items item=site}{if !$site.disable}
        <item>
            <title>{$site.name}</title>
            <link>http://multitender.ru/tenders/?search[site][{$site.id}]</link>
            <description>
                Всего загружена:{$site.count_all|default:"&mdash;"}&lt;br/&gt;
                За последние сутки: {$site.count_day|default:"&mdash;"}&lt;br/&gt;
            </description>             
        </item>
{/if}{/foreach}
    </channel>
</rss>
