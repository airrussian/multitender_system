<img class="y1" src="/templates/mt/img/sl.gif" alt="" /><div class="y2"><a class="c2" href="{$backlink}">Вернуться к списку поставщиков</a></div>
<h1 class="y4i">Информация по поставщику</h1><br/>

<div class="mt_boxwhite">
<h3>{$mas.Name}</h3>
<hr/>
<div id="statistics">
    <dl>
        <dt><b>ИНН/КПП</b></dt>
        <dd>{$mas.INN}/{$mas.KPP}</dd>
    </dl>
    <dl>
        <dt><b>Адрес</b></dt>
        <dd>{$mas.Address}</dd>
    </dl>
    <dl>
        <dt><b>Регион</b></dt>
        <dd>{$mas.region_name}</dd>
    </dl>
    <dl>
        <dt><b>Количество жалоб</b></dt>
        <dd>{$mas.qu_of_complaints}</dd>
    </dl>
</div>
{$graph}
{if $contracts}
<hr/>
<br/>
<h4>Контракты поставщика</h4>
<small>Найдено контрактов: {$count}</small><br/>
<table class="view tbl_sort contracts">
    <thead>
    <tr>
        <th align="left"><b>Название организации-заказчика</b></th>
        <th align="left"><b>ИНН\КПП</b></th>
        <th align="left" class="{literal}{sorter: 'mtShortDate'}{/literal}"><b>Дата заключения контракта</b></th>
        <th align="left" class="{literal}{sorter: 'mtShortDate'}{/literal}"><b>Дата окончания контракта</b></th>
        <th align="left"><b>Тыс. руб.</b></th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$contracts item=item}
    <tr onclick="go('{$item.link}');">
        <td align="left"><a href="{$item.link}" title="Подробная информация по контракту">{$item.customer_name}</a></td>
        <td align="left">{$item.customer_inn}\{$item.customer_kpp}</td>
        <td align="left">{$item.contract_date}</td>
        <td align="left">{$item.contract_end_date}</td>
        <td align="left">{$item.contract_price}</td>
    </tr>
    {/foreach}
    </tbody>
</table>
{$scrol}
{/if}
{if $complaints}
<hr/>
<br/>
<h4>Жалобы на поставщика</h4>
<small>Найдено записей: {$complaints_count}</small><br/>
<table class="view tbl_sort complaints">
    <thead>
    <tr class="title">
        <th align="left"><b>Предмет контракта</b></th>
        <th align="left" class="{literal}{sorter: 'mtShortDate'}{/literal}"><b>Дата проведения конкурса</b></th>
        <th align="left" class="{literal}{sorter: 'mtShortDate'}{/literal}"><b>Срок исполнения контракта</b></th>
        <th align="left"><b>Тыс. руб.</b></th>
    </tr>
    </thead>
    <tbody>
    {$complaints}
    </tbody>
</table>
{/if}
<br/>
<small>Данные получены из открытых источников: <a href="http://reestrgk.roskazna.ru/">Росказна</a> и <a href="http://rnp.fas.gov.ru/">Реестр недобросовестных поставщиков</a></small>
</div>