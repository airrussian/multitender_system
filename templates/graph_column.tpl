{literal}
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load('visualization', '1', {packages: ['corechart']});
    </script>
    <script type="text/javascript">
      function drawVisualization() {

        var data = new google.visualization.DataTable();{/literal}
        var raw_data = {$values};

        var years = {$columns};
{literal}
        data.addColumn('string', 'Year');
        for (var i = 0; i  < raw_data.length; ++i) {
          data.addColumn('number', raw_data[i][0]);
        }

        data.addRows(years.length);

        for (var j = 0; j < years.length; ++j) {
          data.setValue(j, 0, years[j].toString());
        }
        for (var i = 0; i  < raw_data.length; ++i) {
          for (var j = 1; j  < raw_data[i].length; ++j) {
            data.setValue(j-1, i+1, raw_data[i][j]);
          }
        }

        // Create and draw the visualization.
        new google.visualization.ColumnChart(document.getElementById('{/literal}{$divid}{literal}')).
            draw(data,
                 {title:"{/literal}{$title}{literal}",
                  width:{/literal}{$width}{literal}, height:{/literal}{$height}{literal},
                  hAxis: {title: "{/literal}{$Xname}{literal}"}}
            );
      }


      google.setOnLoadCallback(drawVisualization);
    </script>
{/literal}

<div id="{$divid}" style="width: {$width}px; height: {$height}px;"></div>