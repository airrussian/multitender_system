<img alt="" src="templates/mt/img/sl.gif" class="y1">
<div class="y2"><a href="/tenders/by_customer" class="c2">Госзаказчики по регионам России</a></div>
<h1 class="y4i">Заказчики {$region.name_rp} <span class="c7">&ndash; {$total}</span></h1>
<div class="q1">
    		<div class="wt"></div>
	        <div class="wo">
            <div class="wo1">
<div class="o1">
	<div>Найдите госзаказчика по названию</div>
    <div class="s1">
        <form action="{$link_base}{$link_act}&region_id={$region.id}" method="post">
            <input id="filter" name="filter" value="{$filter}" class="in in12"><button type="submit" class="iskat"></button></form></div>
	<table cellspacing="0" cellpadding="0" class="view">
            <thead>
                    <tr><th>Названия</th>
                        <th>Тендеры</th></tr>
        </thead>
            <tbody>
{foreach from=$customers item=item}
  <tr>
    <td><a href="{$link_base}search[customer][{$item.id}]" title="Все тендеры заказчика: «{$item.name|escape}»">{$item.name}</a></td>
    <td class="t52">{$item.count_items}</td>
  </tr>
{/foreach}
	</tbody></table>
</div>

<div class="y5 fl">
{$scrol}
</div>
			</div>
            <div class="wb"></div>
        </div>
</div>
