{include file="statistics_menu.tpl"}
{if $site}
<h1>Сайт {$site.url}</h2>
{/if}
<h1>Статистика по регионам</h1>

<div class="rc-white-head"></div>
<div class="rc-white-cont">

    <table width="100%">
        <tr bgcolor="{cycle values="#eeeeee,#dddddd"}">
            <td>&nbsp;</td>
            {foreach from=$date item=count key=date}
            <td style="border-left: 1px solid #555">{$date}</td>
            {/foreach}
        </tr>

        {foreach from=$items item=region key=region_id}
        <tr bgcolor="{cycle values="#ffffff,#cccccc"}">
            <td><b>{$region.name}</b></td>
            {foreach from=$region.date item=total}
            <td align="right" style="border-left: 1px solid #555">{$total|default:"&mdash;"}</td>
            {/foreach}
        </tr>
        {/foreach}

    </table>

</div>
<div class="rc-white-foot"></div>