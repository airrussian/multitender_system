<img alt="" src="templates/mt/img/sl.gif" class="y1">
<div class="y2"><a href="/tenders/cabinet" class="c2">Личный кабинет</a></div>
<h1 class="y4i">Пополнение баланса</h1>

<div class="q1">
    		<div class="wt"></div>
	        <div class="wo">
            <div class="wo1">
<div class="l1">
	<span class="l5">Шаг 1</span><br>
	<span class="l6">Выбор способа оплаты</span>
</div>
<div class="l4"></div>
<div class="l1">
	<span class="l2">Шаг 2</span><br>
	<span class="l3">Выбор тарифа</span>
</div>
<div class="l4"></div>
<div class="l1">
	<span class="l5">Шаг 3</span><br>
	<span class="l6">Оплата счёта</span>
</div>

<div class="l9">Выберите один из тарифных планов Мультитендера.</div>

<div id="ul">
    <div id="navigation2"><ul class="select-block"><li class="active"><a href="#"> Безлимитный</a></li><li><a href="" onclick="$('#r').show();$('#ul').hide();return false;">Розничный</a></li></ul></div>

<div class="l10">
	<p>Тариф «Безлимитный». Доступ к полной информации ко всем тендерам в оплаченный период.</p>
	<p>Выберите период подписки:</p>
        <form action="/tenders/billing/" method="get">
            <input type="hidden" name="step" value="3" />
            <input type="hidden" name="kind_rates_id" value="1" />
            <input type="hidden" name="type_payment" value="{$type_payment}" />
<table cellspacing="0" cellpadding="0" class="t12">
<tbody>
    {foreach from=$rates item=rate}
    {if $rate.kind_rates_id == 1}
    {if $rate.id==1} {* Удалить этот IF когда нужны все тарифы *}
<tr>

    <td width="200"><input type="radio" name="rate" value="{$rate.id}" id="urate{$rate.id}"{if $rate.id == 1} checked="checked"{/if} /> <label for="urate{$rate.id}">{$rate.name}</label></td>
    <td width="100"><strong>{$rate.summa} руб.</strong></td>
</tr>
    {/if}
    {/if}
    {/foreach}
<tr>
    <td style="border-bottom: 1px solid rgb(255, 255, 255); text-align: center;" colspan="2"><button type="submit" class="accept"></button></</td>
</tr>
</tbody></table></form>
</div>
</div>
<div id="r" style="display: none;">
<div id="navigation2"><ul class="select-block"><li><a href="#" onclick="$('#r').hide();$('#ul').show();return false;"> Безлимитный</a></li><li class="active"><a href="#">Розничный</a></li></ul></div>
<div class="l10">
	<p>Тариф «Розничный». Доступ к полной информации некоторого количества тендеров без временного лимита.</p>
	<p>Выберите количество тендеров:</p>
        <form action="/tenders/billing/" method="get">
            <input type="hidden" name="step" value="3" />
            <input type="hidden" name="kind_rates_id" value="2" />
            <input type="hidden" name="type_payment" value="{$type_payment}" />
<table cellspacing="0" cellpadding="0" class="t12">
<tbody>
{if $type_payment == 'cashless'}
<tr>
    <td width="200"><input type="radio" name="count" value="10" id="rate10" checked="checked"> <label for="rate10">Доступ к 10 тендерам</label></td>
    <td width="100"><strong>900 руб.</strong></td>
</tr>
{* Временно не удаленны
<tr>
    <td width="200"><input type="radio" name="count" value="20" id="rate20"> <label for="rate20">Доступ к 20 тендерам</label></td>
    <td width="100"><strong>1800 руб.</strong></td>
</tr>
<tr>
    <td width="200"><input type="radio" name="count" value="30" id="rate30"> <label for="rate30">Доступ к 30 тендерам</label></td>
    <td width="100"><strong>2700 руб.</strong></td>
</tr>
<tr>
    <td width="200"><input type="radio" name="count" value="40" id="rate40"> <label for="rate40">Доступ к 40 тендерам</label></td>
    <td width="100"><strong>3200 руб.</strong></td>
</tr>
*}
{elseif $type_payment == 'electron'}{* розничный *}

<tr>
    <td width="200"><input type="radio" name="count" value="1" id="rate1" checked="checked"> <label for="rate1">Доступ к 1 тендеру</label></td>
    <td width="100"><strong>90 руб.</strong></td>
</tr>
{* Временно не удаленны
<tr>
    <td width="200"><input type="radio" name="count" value="2" id="rate2"> <label for="rate1">Доступ к 2 тендерам</label></td>
    <td width="100"><strong>180 руб.</strong></td>
</tr>
<tr>
    <td width="200"><input type="radio" name="count" value="3" id="rate3"> <label for="rate3">Доступ к 3 тендерам</label></td>
    <td width="100"><strong>270 руб.</strong></td>
</tr>
<tr>
    <td width="200"><input type="radio" name="count" value="4" id="rate4"> <label for="rate4">Доступ к 4 тендерам</label></td>
    <td width="100"><strong>320 руб.</strong></td>
</tr>
<tr>
    <td width="200"><input type="radio" name="count" value="5" id="rate5"> <label for="rate5">Доступ к 5 тендерам</label></td>
    <td width="100"><strong>450 руб.</strong></td>
</tr>
<tr>
    <td width="200"><input type="radio" name="count" value="10" id="rate10"> <label for="rate10">Доступ к 10 тендерам</label></td>
    <td width="100"><strong>900 руб.</strong></td>
</tr>
<tr>
    <td width="200"><input type="radio" name="count" value="20" id="rate20"> <label for="rate20">Доступ к 20 тендерам</label></td>
    <td width="100"><strong>1800 руб.</strong></td>
</tr>
<tr>
    <td width="200"><input type="radio" name="count" value="30" id="rate30"> <label for="rate30">Доступ к 30 тендерам</label></td>
    <td width="100"><strong>2700 руб.</strong></td>
</tr>
<tr>
    <td width="200"><input type="radio" name="count" value="40" id="rate40"> <label for="rate40">Доступ к 40 тендерам</label></td>
    <td width="100"><strong>3200 руб.</strong></td>
</tr>
*}
{/if}
<tr>
    <td style="border-bottom: 1px solid rgb(255, 255, 255); text-align: center;" colspan="2"><button type="submit" class="accept"></button></</td>
</tr>
</tbody></table></form>
</div>
</div>
<div class="l11">
<a href="/help/kak-vibrat-tarif-i-oplatit">Как выбрать тариф и оплатить подписку?</a><br>
<a href="/tarify">Тарифы</a>
</div>



			</div>
            <div class="wb"></div>
        </div>
</div>