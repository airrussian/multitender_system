{literal}
<style>
    .contry { font-size: 11pt; padding-top: 5px; border-top: 3px solid #DDD; }
    #contry-1 { border: 0 none; }
    .contry-name { display: block; font-size: 14pt; padding: 0 0 10px 0; }
    .group { float: left; width: 300px; }
    .group ul { list-style: none; margin-bottom: 10px; }
    .group ul p { margin-bottom: 5px; font-weight: bold; }
    .group ul li span { padding: 0 2px; font-size: 10pt; color: #666; }
    .contry #resp { font-size: 14px; font-weight: bold; }
</style>
{/literal}

<h2>Заказчики по регионам</h2>

<div class="mt_boxwhite">

<a href="/tenders/customer_new">Новые заказчики</a>

{counter name='idcontry' start=0 print=false}
{foreach from=$contries item=contry}

<div class="contry" id="contry-{counter name='idcontry'}">
    <a href="/tenders/{$contry.info.link}" class="contry-name">{$contry.info.name}</a>
    {foreach from=$contry key=gkey item=group}
    {if $gkey!=='info'}
    {if $gkey==1}<p id="resp">Респулики</p>{/if}
    <div class="group">
        {counter start=0 name='rowwwww' print=false}
        {foreach from=$group key=lkey item=letter}
        {if $lkey!=='heightcolumn'}
        <ul>
            <p>{$lkey}</p>
            {foreach from=$letter item=r}           
            <li>
                <a href="/tenders/{$r.link}">{$r.name}</a>
                <span title="актуальных на текущий момент">{$r.count}</span>
                <a href="/tenders/analytics/?region_id={$r.id}"></a>
            </li>
            {counter print=false assign=row name='rowwwww'}
            {/foreach}
        </ul>
        {if $row>$group.heightcolumn}{counter start=0 name='rowwwww'}</div><div class="group">{/if}
        {/if}
        {/foreach}
    </div>
    <br clear="all" />
    {/if}
    {/foreach}
</div>

{/foreach}
</div>