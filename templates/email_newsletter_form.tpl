<div class="pathway">
    <span style="padding-left: 20px; background: transparent url(/templates/mt/img/sl.gif) no-repeat left center;">
        <a href="/tenders/cabinet">Личный кабинет</a>
    </span>
</div>

<h1 class="email">e-mail рассылка</h1>

<div class="mt_boxwhite">
    <p>Не нужно каждый день заходить на Мультитендер.ру, чтобы проверить, появились ли новые тендеры по интересующей вас тематике. Вы можете получать новые тендеры по электронной почте.</p>
    <p>Перед вами запросы, уведомления по которым вы будете получать.</p>

    <a href="/help-video#mail">Как настроить рассылку?</a>

    {$list_newsletter}

</div>