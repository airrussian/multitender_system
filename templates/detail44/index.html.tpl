{literal}
    <style>
        .detail-header {
            background: #f6f4ea url('/templates/mt/img/corner.png') no-repeat 100% 0;
            margin: 0 0 15px 0;
            min-height: 50px;
            padding: 20px 10px 10px 10px;
        }

        .detail-number {
            text-align: right;                        
            padding: 0 0 0 20px;            
            margin: 5px 30px 0 0;
            color: #666;
        }
        .detail-number span {
            line-height: 14px; color: #999;
        }

        .detail-number .purchaseNumber {
            line-height: 16px;
            background: transparent url('/templates/mt/img/note.gif') no-repeat left center;
            padding: 2px 0 2px 16px;
        }

        .detail-container {
            width: 680px;
            float: left;
        }
        .detail-right {
            float: right;
            width: 250px;
        }
        #detail strong {
            font-weight: 700;
            font-size: 15px;
            color: #CBBA6E;
        }

        dl dt { float: left; width: 150px; font-weight: bold; }
        dl dd { margin-left: 160px; }

        #detail h5 { margin: 0; }

        #detail .customer { background: #F9F9F9; padding: 5px; margin: 5px 0; }        
        #detail .customer dl { padding: 0 0 0 5px; }

        #detail .tender-desc .type {   }
        #detail .tender-desc .status {  }        
        
        #detail .mt_order {
            border: 1px solid #61c290;
            color: #61c290;
            padding: 2px 10px;
            font-weight: bold;
        }
        
        .Guarantee span {
            width: 110px;
            float: left;
            display: block;
        }
        .lot {
            border-left: 1px solid #EEE;            
            margin-bottom: 15px;
        }
        
        .lot h5 {
            background: #EEE;
            padding: 10px;
        }

    </style>
{/literal}


<div id="detail">
    <div class="mt_boxwhite mt_boxwhitenew">
        <div class="detail-container">
            <div class="detail-header">
                <h1>{$tender.name|escape}</h1>
                <div class="detail-number">
                    <span class="purchaseNumber">&numero; {$tender.purchaseNumber|escape}</span><span> от {$tender.pubDate|tc_date}</span>
                    <a href="http://zakupki.gov.ru/epz/order/notice/ok44/view/common-info.html?regNumber={$tender.purchaseNumber|escape}">первоисточник &rtrif;</a>
                </div>

                <div class="detail-menu">
                </div>
            </div>

            <div class="tender-desc">
                <dl class="type" style="float: left; width: 350px; margin: 0;">
                    <dt>Форма торгов:</dt>
                    <dd>{$tender.type.name|escape}</dd>
                </dl>
                <dl class="status" style="margin: 0 0 0 360px;">
                    <dt style="width: 45px;">Статус:</dt>
                    <dd style="margin: 0 0 0 50px">{if $status > 0}Подача заявок (<strong>{$status} дней</strong> до окончания){else}Прием заявок окончен {$tender.endDate|tc_date}{/if}</dd>
                </dl>                
                <br clear="all" />
                <dl>
                    <dt>Регион:</dt>
                    <dd>{$tender.region.name|escape}</dd>
                </dl>
                <dl>
                    <dt>Официальный сайт:</dt>
                    <dd>{$tender.site.name|escape}</dd>
                </dl>
            </div>

            <div class="lots">
                {foreach from=$tender.lots item=lot}
                    <div class="lot">
                        <h5>Лот &numero; {$lot.lotNumber}: {$lot.lotObjectInfo}</h5>
                        <dl>
                            <dt>Цена:</dt>
                            <dd><strong>{$lot.price/100|mt_price}</strong> руб.</dd>
                        </dl>                        
                        <div class="customers">
                            {foreach from=$lot.customers item=customer}                                
                                <div class="customer">
                                    <h5>Заказчик:</h5>
                                    <dl>
                                        {if $customer.name}
                                            <dt>Название:</dt>
                                            <dd>{$customer.name|escape}</dd>
                                        {/if}

                                        {if $customer.telephone}
                                            <dt>Телефон:</dt>
                                            <dd>{$customer.telephone|escape}</dd>
                                        {/if}

                                        {if $customer.fax}
                                            <dt>Факс:</dt>                                        
                                            <dd>{$customer.fax|escape}</dd>
                                        {/if}

                                        {if $customer.email}
                                            <dt>Email:</dt>
                                            <dd>{$customer.email|escape}</dd>
                                        {/if}

                                        {if $customer.contactPerson}
                                            <dt>Контактное лицо:</dt>
                                            <dd>{$customer.contactPerson|escape}</dd>
                                        {/if}

                                        {if $customer.postAddress}
                                            <dt>Почтовый адрес:</dt>
                                            <dd>{$customer.postAddress}, <br /> <a href="http://maps.yandex.ru/?text={$customer.postAddress|escape}">на карте &raquo;</a></dd>
                                            {/if}

                                        {if $customer.factAddress}
                                            <dt>Фактический адрес:</dt>
                                            <dd>{$customer.factAddress}, <br /> <a href="http://maps.yandex.ru/?text={$customer.factAddress|escape}">на карте &raquo;</a></dd>
                                            {/if}
                                    </dl>                                    
                                    <div class="Guarantee">
                                        <dl>
                                            <dt>Обеспечение заявки:</dt>
                                            <dd>
                                                <span><strong>{$customer.applicationGuarantee/100|mt_price}</strong> руб.</span>
                                                <a class="mt_order linkorder" href="/ajax.php?action=order&order=tenkrd&tender[name]={$tender.name|escape}&ajax">помочь в оформлении</a>
                                                <br clear="all" />
                                            </dd>

                                            <dt>Обеспечение контракта:</dt>
                                            <dd>
                                                <span><strong>{$customer.contractGuarantee/100|mt_price}</strong> руб.</span>
                                                <a class="mt_order linkorder" href="/ajax.php?action=order&order=bankgrt&tender[name]={$tender.name|escape}&ajax">помочь в оформлении</a>
                                                <br clear="all" />
                                            </dd>
                                        </dl>                                        
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                        {if $lot.apps}
                            {if $lot.apps.0.rating==1}
                                <div class="winner">
                                    <h5>Победитель</h5>
                                    <dl>
                                        <dt>Название:</dt>
                                        <dd><a href="http://www.filetender.ru/info_new.php?inn={$lot.apps.0.org.inn}&num={$tender.purchaseNumber|escape}">{$lot.apps.0.org.name|escape}</a></dd>

                                        <dt>Предложеная цена:</dt>
                                        <dd>{if $lot.apps.0.apps_price}{$lot.apps.0.apps_price|escape}{else}Не указано{/if}</dd>

                                        <dt>Дата подачи заявки:</dt>
                                        <dd>{$lot.apps.0.appDate|tc_date}</dd>
                                    </dl>                                    
                                </div>
                            {/if}
                            <div class="applications">
                                <h5>Участники:</h5>                                
                                {foreach from=$lot.apps item=app}
                                    {if $app.rating<>1}
                                        <dl>
                                            <dt>Название:</dt>
                                            <dd>{$app.org.name|escape}</dd>

                                            <dt>Предложеная цена:</dt>
                                            <dd>{if $app.apps_price}{$app.apps_price|escape}{else}Не указано{/if}</dd>

                                            <dt>Дата подачи заявки:</dt>
                                            <dd>{$app.appDate|tc_date}</dd>

                                            <dt>Рейтинг:</dt>
                                            <dd>{if $app.rating==1}Победитель{else}{$app.rating}{/if}</dd>
                                        </dl>
                                    {/if}
                                {/foreach}
                            </div>
                        {/if}
                    </div>
                {/foreach}
            </div>
        </div>


        <div class="detail-right">
            <div id="training" class="right-block">
                <h3>Обучение закупкам</h3>
                <div id="blockselected">
                    <a href="/webinar">Вебинары по 44-ФЗ и 223-ФЗ</a>

                </div>
            </div>
            
            {$freesubscribe}
            
        </div>

        <br clear="all" />

    </div>
</div>