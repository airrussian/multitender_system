<div class="pathway">
    <span style="padding-left: 20px; background: transparent url(/templates/mt/img/sl.gif) no-repeat left center;">
        <a class="c2" href="/tenders/cabinet">Личный кабинет</a>
    </span>
</div>

<h1 style="padding-left: 40px; background: transparent url('/templates/mt/img/vash_acc.gif') no-repeat left center;">Редактирование реквизитов организации</h1>

<div class="mt_boxgreen">
    <div id="frm-firm">
        <div class="txt">Для оформления договора и выставления счета заполните реквизиты вашей организации. <div class="warning">Внимание! Все поля (кроме КПП) обязательны к заполнению!</div>Без них мы не сможем корректно оформить бухгалтерские документы, необходимые при оплате по безналичному расчету.</div>
        {if $err}<p>Заполните все поля отмечанные <span class="star">*</span></p>{/if}
        <form method="POST" name="firm" id="firm" action="{$action}">

            <div class="frm-firm-input frm-firm-name">
                <label>Название организации:</label>
                <input name="name" type="text" value="{$arr.name|escape}" maxlength="100" />
                <span>Пример: ООО «Иванов Гроуп» или ИП Иванов И.И.</span>
            </div>

            <div class="frm-firm-input frm-firm-inn">
                <label>ИНН:</label>
                <input name="INN" type="text" value="{$arr.INN}" maxlength="15" class="int" />
                <span>Пример: 8401000000</span>
            </div>

            <div class="frm-firm-input frm-firm-kpp">
                <label>КПП <i>(необязательно)</i>:</label>
                <input name="KPP" type="text" value="{$arr.KPP}" maxlength="14" class="int" />
                <span>Пример: 997500000</span>
            </div>

            <div class="frm-firm-input frm-firm-phone">
                <label>Контактный телефон:</label>
                <input name="telephone" type="text" value="{$arr.telephone|escape}" maxlength="40" />
                <span>Пример: +7 (495) 000-00-01 <font color="red">Указывайте телефон с кодом города</font></span>
            </div>

            <div class="frm-firm-input frm-firm-contact">
                <label>Контактное лицо:</label>
                <input name="contact" type="text" value="{$arr.contact|escape}" maxlength="255" />
                <span>Пример: Иванов Иван Иванович, менеджер</span>
            </div>

            <div class="frm-firm-domicile">
                <h3>Юридический адрес</h3>
                <div class="txt">Пожалуйста, не пишите здесь почтовый адрес с пометкой &laquo;для корреспонденции&raquo;, для этого есть поле &laquo;Почтовый адрес&raquo;</div>
                <fieldset>
                    <div class="frm-firm-input frm-firm-index">
                        <label>Индекс:</label>
                        <input class="index" name="domicile_index" maxlength="6" value="{$arr.domicile_index|escape}" type="text" />
                        <span>Пример: 660043</span>
                    </div>
                    <div class="frm-firm-input frm-firm-region">
                        <label>Регион, страна:</label>
                        <input class="region" name="domicile_region" maxlength="255" value="{$arr.domicile_region|escape}" type="text" />
                        <span>Пример: Красноярский край, Россия</span>
                    </div>
                    <div class="frm-firm-input frm-firm-town">
                        <label>Населённый пункт:</label>
                        <input class="town" name="domicile_town" maxlength="255" value="{$arr.domicile_town|escape}" type="text"/>
                        <span>Пример: г. Красноярск</span>
                    </div>
                    <div class="frm-firm-input frm-firm-other">
                        <label>Улица, дом, номер офиса и пр.:</label>
                        <input class="other" name="domicile" value="{$arr.domicile|escape}" type="text"/>
                        <span>Пример: ул. Пушкинская, д. 268, а/я 4577</span>
                    </div>
                </fieldset>
            </div>
            <div class="frm-firm-address">
                <h3>Почтовый адрес</h3>
                <div class="txt">По этому адресу мы будем оправлять счета-фактуры<br />
                    <input id="addom" type="checkbox" name="addom" {if !($arr.address || $arr.address_index || $arr.address_region || $arr.address_town)}checked="checked"{/if} />
                           <label for="addom">Почтовый адрес совпадает с юридическим</label>
                </div>
                <fieldset>
                    <div class="frm-firm-input frm-firm-index">
                        <label>Индекс:</label>
                        <input class="index" name="address_index" maxlength="6" value="{$arr.address_index|escape}" type="text" />
                        <span>Пример: 660043</span>
                    </div>
                    <div class="frm-firm-input frm-firm-region">
                        <label>Регион, страна:</label>
                        <input class="region" name="address_region" maxlength="255" value="{$arr.address_region|escape}" type="text" />
                        <span>Пример: Красноярский край, Россия</span>
                    </div>
                    <div class="frm-firm-input frm-firm-town">
                        <label>Населённый пункт:</label>
                        <input class="town" name="address_town" maxlength="255" value="{$arr.address_town|escape}" type="text" />
                        <span>Пример: г. Красноярск</span>
                    </div>
                    <div class="frm-firm-input frm-firm-other">
                        <label>Улица, дом, номер офиса и пр.:</label>
                        <input class="other" name="address" value="{$arr.address|escape}" type="text" />
                        <span>Пример: ул. Пушкинская, д. 268, а/я 4577</span>
                    </div>
                </fieldset>
            </div>
            <div align="center">
            <input type="submit" class="button save_all" name="submit" id="save" value="Сохранить изменения" />
            </div>
        </form>
    </div>
</div>