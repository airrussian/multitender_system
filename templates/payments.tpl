<img class="y1i" src="/templates/mt/img/sl.gif" alt="" /><div class="y2i"><a class="c2" href="/tenders/cabinet">Личный кабинет</a></div>
<h1 class="y4i">Мои платежи</h1><br/>

<div class="rc-white-head"></div>
<div class="rc-white-cont">
<div class="u1"><p>В Моих платежах собраны выписанные вами счета и статус их оплаты. Чтобы распечатать счет для безналичной оплаты, нажмите кнопку «Распечатать» в крайней правой колонке.</p>
<p>Если при выставлении счета вы выбрали платёж через сервис Робокасса (Яндекс.Деньги, Webmoney, SMS, платежные терминалы, бакновские карты и т.д.) — у таких платежей имеется кнопка «Оплатить» ведущая на сайт Робокассы. При удачной оплате статус платежа меняется на «Оплачен» и вы получаете полный доступ.</p>
<p style="font-size:12px; font-style:italic">При оплате SMS сообщением взимается комиссия в размере 100%, лимит платежа — один тендер в день.</p>
</div>
<div class="u2">
Для зачисления платежа в день оплаты счёта, отправьте платежное поручение с отметкой банка (или с отметкой из банк–клиента) нам по <a href="mailto:{$email}">электронной почте</a> или по факсу +7 (391) 265-17-63.

</div>
{if $pays || $paid}
<table class="view bills" cellspacing="0" cellpadding="0">
<thead>
  <tr>
    <th>№&nbsp;Счёта</th>
    <th>Тариф</th>
    <th>Содержание подписки</th>
    <th>Сумма</th>

    <th>Способ оплаты</th>
    <th>Дата&nbsp;выставления счёта</th>
    <th>Дата оплаты счёта</th>
    <th>Состояние</th>
    <th></th>
  </tr>
</thead>
<tbody>
{if $pays}
    <tr>
        <td colspan="9"><div class="u4">Счета, ожидающие оплаты</div></td>
    </tr>
{foreach from=$pays item=pay}
  <tr>
    <td>{$pay.id}</td>
    <td>{$pay.kind_rates}</td>
    <td>{if $pay.kind_rates_id==1}{$pay.count*30}&nbsp;дн.{else}{$pay.count}&nbsp;тен.{/if}</td>
    <td><strong>{$pay.summa}&nbsp;руб.</strong></td>
    <td>{$pay.type_payment_rus}</td>

    <td>{$pay.datetime}</td>
    <td>{if $pay.datetime_adopted !== '-'}{$pay.datetime_adopted}{else}&mdash;{/if}</td>
    <td>
        {if $pay.paid == 0}
            <span class="c4">не оплачен</span>
        {/if}
    </td>
    <td class="view-bull {if $pay.NOW}now{/if}">
            {if $pay.type_payment=="cashless"}
        	    {if $pay.paid != -1}
                        <button class="print" onclick="location.href='/ajax.php?action=billing&task=payment_view&payment_id={$pay.id}'"></button>
                    {else}
                        <button class="print"></button>
                    {/if}
            {else}
                {$pay.robokasse}
            {/if}</td>
  </tr>
{/foreach}
{/if}
{if $paid}
<tr>
    <td colspan="9"><div class="u4">Оплаченые счета</div></td>
</tr>
{foreach from=$paid item=pay}
  <tr>
    <td>{$pay.id}</td>
    <td>{$pay.kind_rates}</td>
    <td>{if $pay.kind_rates_id==1}{$pay.count*30}&nbsp;дн.{else}{$pay.count}&nbsp;тен.{/if}</td>
    <td><strong>{$pay.summa}&nbsp;руб.</strong></td>
    <td>{$pay.type_payment_rus}</td>

    <td>{$pay.datetime}</td>
    <td>{if $pay.datetime_adopted !== '-'}{$pay.datetime_adopted}{else}&mdash;{/if}</td>
    <td>{if $pay.paid > 0}
            <span class="c5">оплачен</span>
        {/if}
    </td>
    <td class="view-bull {if $pay.NOW}now{/if}">
            {if $pay.type_payment=="cashless"}
        	    {if $pay.paid != -1}
                        <button class="print" onclick="location.href='/ajax.php?action=billing&task=payment_view&payment_id={$pay.id}'"></button>
                    {else}
                        <button class="print"></button>
                    {/if}
            {/if}</td>
  </tr>
{/foreach}
{/if}
</tbody>
</table>
{else}
<br class="fix"/>
<br/>Вы еще не выписывали счетов.
{/if}
{$scrol}
</div>
<div class="rc-white-foot"></div>
{$print}