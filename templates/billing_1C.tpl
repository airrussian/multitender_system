<?xml version="1.0" encoding="utf-8" ?>
<!-- Оплаченные начиная с {$from}, {$count} платежей -->
<tenders_personal>
    <firms_list>
{foreach from=$firms item=firm}
        <firm>
            <user_id>{$firm.user_id}</user_id>
            <name>{$firm.name|escape}</name>
            <INN>{$firm.INN|escape}</INN>
            <KPP>{$firm.KPP|escape}</KPP>
            <domicile>{$firm.domicile_full|escape}</domicile>
            <telephone>{$firm.telephone|escape}</telephone>
            <contact>{$firm.contact|escape}</contact>
        </firm>
{/foreach}
    </firms_list>
    <payments_list>
{foreach from=$payments item=payment}
        <payment>
            <id>{$payment.id}</id>
            <user_id>{$payment.user_id}</user_id>
            <datetime>{$payment.datetime}{* время выставления счета *}</datetime>
            <summa>{$payment.summa}</summa>
            <datetime_adopted>{$payment.datetime_adopted}{* время оплаты *}</datetime_adopted>
        </payment>
{/foreach}
    </payments_list>
</tenders_personal>