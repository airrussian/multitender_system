<div style="height: 80px; overflow: hidden;">
    <img src="http://multitender.ru/templates/mt/img/logo_mt.jpg" alt="" style="float: left; display: block; margin-right: 10px;" />
    <div style="color: #999; font-size: 16px; margin:0; padding:5px 0 0 0;">Онлайн-заявка на</div>
    <h1 style="font-size: 22px; font-weight: normal; color: #444; padding: 0; margin: 0;">{$title}</h1>
</div>
<div style="padding: 5px; border:1px solid #CCC; background: #FFF none; box-shadow: 1px 1px 10px 0px #CCC">
    <table style="font-size: 12px; color: #444">
        <col width="140" /><col />
        {if $data.item.id}
            <tr>   
                <td valign="top" style="font-weight: bold; text-align: right; padding: 5px;">Заявка по тендеру:</td>
                <td valign="top" style="font-weight: normal; text-align: left; padding: 5px;"><a href="http://multitender.ru/tenders/detail/{$data.item.id}">{$data.item.name}</a></td>
            </tr>
            <tr>
                <td valign="top" style="font-weight: bold; text-align: right; padding: 5px;">ID тендера:</td>
                <td valing="top" style="font-weight: normal; text-align: left; padding: 5px;">{$data.item.id}</td>
            </tr>
        {/if}
        <tr>
            <td valign="top" style="font-weight: bold; text-align: right; padding: 5px;">Контактный e-mail:</td>
            <td valign="top" style="font-weight: normal; text-align: left; padding: 5px;"><a href="mailto:{$data.form.email}">{$data.form.email}</a></td>
        </tr>
        <tr>
            <td valign="top" style="font-weight: bold; text-align: right; padding: 5px;">Наименование организации:</td>
            <td valign="top" style="font-weight: normal; text-align: left; padding: 5px;">{$data.form.name}</td>
        </tr>
        <tr>
            <td valign="top" style="font-weight: bold; text-align: right; padding: 5px;">Контактное лицо:</td>
            <td valign="top" style="font-weight: normal; text-align: left; padding: 5px;">{$data.form.contact}</td>
        </tr>
        <tr>
            <td valign="top" style="font-weight: bold; text-align: right; padding: 5px;">Контакный телефон:</td>
            <td valign="top" style="font-weight: normal; text-align: left; padding: 5px;">{$data.form.telephone}</td>
        </tr>
        <tr>
            <td valign="top" style="font-weight: bold; text-align: right; padding: 5px;">Доп. информация:</td>
            <td valign="top" style="font-weight: normal; text-align: left; padding: 5px;">{$data.form.other}</td>
        </tr>
    </table>

</div>