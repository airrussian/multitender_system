<h1>Заявка на покупку сертификата ЭЦП</h1>

<div class='mt_boxwhite'>
<form action="" method="POST" id="ezpkontur">

    <div class="b-orderForm_block">
        <div class="b-form_field">
            <label class="b-orderForm_field_label b-orderForm_field_label__top g-inlineBlock" for="input2">Ф.И.О:</label>
            <div class="g-inlineBlock">
                <input class="b-form_textInput b-form_textInput__l" id="input2" maxlength="50" name="organization[personal][fio]" size="29" type="text" value="{$data.organization.personal.fio}">
                <span class="b-form_fieldError"></span>
                <p class="b-form_comment">Например: Сомов Александр Леонидович</p>
            </div>
        </div>

        <div class="b-form_field">
            <label class="b-orderForm_field_label b-orderForm_field_label__top g-inlineBlock" for="input6">Телефон:</label>
            <div class="g-inlineBlock">
                <input class="b-form_textInput" id="input6" maxlength="50" name="organization[telephone]" size="29" type="text" value="{$data.organization.telephone}">
                <span class="b-form_fieldError"></span>
                <p class="b-form_comment">Например: (343) 258-54-25 или 89058753478</p>
            </div>
        </div>
        <div class="b-form_field">
            <label class="b-orderForm_field_label b-orderForm_field_label__top g-inlineBlock" for="input5">Эл.почта:</label>
            <div class="g-inlineBlock"><input class="b-form_textInput" id="input5" maxlength="50" name="organization[email]" size="29" type="text" value="{$data.organization.email}">
                <span class="b-form_fieldError"></span>
            </div>
        </div>
    </div>

    <div id="select-holdertype">                
        <div class="b-orderForm_block">
            <p class="b-orderForm_field_label g-inlineBlock">Кто получает сертификат:</p>
            <ul class="b-orderForm_tabs g-safeContext g-inlineBlock">
                <li class="b-orderForm_tabs_item g-inlineBlock rc5">
                    <span class="dashed js-holder-type-tab"><label for="IsOrganizationHolder_Id">Юр.лицо</label></span>
                    <input class="b-orderForm_tabs_item_hiddenRadio" name="HolderType" type="radio" value="IsOrganizationHolder" checked="checked" />
                </li>
                <li class="b-orderForm_tabs_item g-inlineBlock rc5">
                    <span class="dashed js-holder-type-tab"><label for="IsIndividualEntrepreneur_Id">ИП</label></span>
                    <input class="b-orderForm_tabs_item_hiddenRadio" name="HolderType" type="radio" value="IsIndividualEntrepreneur" />
                </li>
                <li class="b-orderForm_tabs_item g-inlineBlock rc5">
                    <span class="dashed js-holder-type-tab"><label for="IsBusinessmanHolder_Id">Физ.лицо</label></span>
                    <input class="b-orderForm_tabs_item_hiddenRadio" name="HolderType" type="radio" value="IsBusinessmanHolder" />
                </li>
            </ul>        
        </div>

        <div class="b-orderForm_requisites" id="IsOrganizationHolder">
            <div id="IsOrganizationHolder_Container" class="b-orderForm_block holder-form-container">
                <div class="b-form_field">
                    <p class="b-orderForm_field_label g-inlineBlock"><label for="OrganizationTitle">Название организации</label>:</p>
                    <div class="g-inlineBlock">
                        <input class="b-form_textInput b-form_textInput__l" id="OrganizationTitle" maxlength="400" name="organization[name]" type="text" value="{$data.organization.name|escape:'html'}" />

                    </div>
                </div>
                <div class="b-form_field g-safeContext">
                    <p class="b-orderForm_field_label g-inlineBlock"><label for="Inn">ИНН</label><span class="labelsDeltimiter">-</span><label for="Kpp" class="kppFieldLabel">КПП</label>:</p>
                    <div class="g-inlineBlock">
                        <input class="b-form_textInput b-form_textInput__s" id="Inn" maxlength="12" name="organization[INN]" type="text" value="{$data.organization.INN}" />&nbsp;-&nbsp;<input class="b-form_textInput b-form_textInput__s" id="Kpp" maxlength="9" name="organization[KPP]" type="text" value="{$data.organization.KPP}" />
                    </div>
                </div>
            </div>
        </div>

        <div class="b-orderForm_requisites" id="IsIndividualEntrepreneur">
            <div id="IsOrganizationHolder_Container" class="b-orderForm_block holder-form-container">
                <div class="b-form_field">
                    <p class="b-orderForm_field_label g-inlineBlock"><label for="OrganizationTitle">Название организации</label>:</p>
                    <div class="g-inlineBlock">
                        <input class="b-form_textInput b-form_textInput__l" id="OrganizationTitle" maxlength="400" name="organization[name]" type="text" value="{$data.organization.name|escape:'html'}" />

                    </div>
                </div>
                <div class="b-form_field g-safeContext">
                    <p class="b-orderForm_field_label g-inlineBlock"><label for="Inn">ИНН</label>:</p>
                    <div class="g-inlineBlock">
                        <input class="b-form_textInput b-form_textInput__s" id="Inn" maxlength="12" name="organization[INN]" type="text" value="{$data.organization.INN}" />
                    </div>
                </div>
            </div>
        </div>

        <div class="b-orderForm_requisites" id="IsBusinessmanHolder">
            <div id="IsBusinessmanHolder_Container" class="b-orderForm_block holder-form-container">
                <div class="b-form_field">
                    <p class="b-orderForm_field_label g-inlineBlock">Паспортные данные</p>
                    <div class="g-inlineBlock b-orderForm_passportBlock">
                        <input class="b-form_textInput b-form_textInput__xs" id="passportId" maxlength="50" name="organization[pesonal][PassportSeriesAndNumber]" size="29" type="text" value="">

                        <p class="b-form_comment"><label for="PassportSeriesAndNumber">Серия и номер</label></p>
                    </div>
                    <div class="g-inlineBlock b-orderForm_passportBlock">
                        <input class="b-form_textInput b-form_textInput__m" id="issuePlaceId" maxlength="255" name="organization[pesonal][PassportIssuePlace]" size="29" type="text" value="">
                        <p class="b-form_comment"><label for="PassportIssuePlace">Кем выдан</label></p>

                    </div>
                    <div class="g-inlineBlock b-orderForm_passportBlock">
                        <input class="b-form_textInput b-form_textInput__xs" id="issueDateId" maxlength="10" name="organization[pesonal][PassportIssueDate]" size="29" type="text" value="">
                        <p class="b-form_comment"><label for="PassportIssueDate">Когда выдан</label></p>

                    </div>
                </div>
                <div class="b-form_field">
                    <p class="b-orderForm_field_label g-inlineBlock"><label for="InnOwner">ИНН</label>:</p>
                    <div class="g-inlineBlock">
                        <input class="b-form_textInput" id="InnOwner" innownerid="innId" maxlength="12" name="organization[INN]" size="29" type="text" value="{$data.organization.INN}">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input class="button" name="OrderSubmit" id="OrderSubmitId" type="submit" default="true" value="Отправить заказ">                

</form>
</div>

{literal}
    <script type="text/javascript" language="javascript">
        $(function () {
            $(".b-orderForm_requisites").addClass("hide");
            
            $('.js-holder-type-tab').click(function(){
            SelectTab($(this).parent());
            });
                
            SelectTab($('#IsOrganizationHolder_Id').parent());
                
                
            $("#ezpkontur").submit(function(){
                $(this).find(".hide input").attr("disabled", "disabled");
            });
        });
        
        function SelectTab(element) {
            $('.js-holder-type-tab').parent().removeClass('b-orderForm_tabs_item__selected');
            element.addClass('b-orderForm_tabs_item__selected');
            var id=element.find("input").val();
            $(".b-orderForm_requisites").addClass("hide");
            $("#"+id).removeClass("hide");
        }
    </script>

    <style>
        #ezpkontur .g-inlineBlock { display: -moz-inline-stack; display: inline-block; }
        #ezpkontur .b-orderForm_block { margin: 0 0 30px } 
        #ezpkontur .b-orderForm .b-form_field { padding: 0; margin: 0 0 10px; }   
        #ezpkontur .b-orderForm_field_label { width: 90px; padding: 0 15px 0 0; position: relative; top: -2px; vertical-align: middle; }
        #ezpkontur .b-orderForm_field_label__top { top: 3px; vertical-align: top; }
        #ezpkontur .b-form_textInput__l { width: 415px; }
        #ezpkontur .b-form_textInput { width: 222px; border-width: 1px; border-style: solid; border-color: #ABADB3 #DBDFE6 #E3E9EF #E2E3EA; padding: 2px; line-height: 1.231em; font-family: Arial; font-size: 1em; }
        #ezpkontur .b-form_comment { margin: 0; padding: 2px 0 0; font-size: 85%; color: #5F5F5F; }
        #ezpkontur .b-form_textInput__l { width: 415px; }
        #ezpkontur .b-form_textInput__s { width: 150px; }


        #ezpkontur .b-orderForm_tabs { margin: 0 0 0 -5px; }
        #ezpkontur .g-inlineBlock { display: -moz-inline-stack; display: inline-block; }
        #ezpkontur ul { margin: 0; padding: 0; list-style: none; }
        #ezpkontur .rc5 { -webkit-border-radius: 5px; border-radius:5px; -moz-border-radius: 5px; }
        #ezpkontur .b-orderForm_tabs_item { padding: 5px 5px 7px; margin: 0 5px 0 0; position: relative; }
        #ezpkontur .g-inlineBlock { display: -moz-inline-stack; display: inline-block; }
        #ezpkontur .dashed, a.dashed { color: #0F4DA2; text-decoration: none; border-bottom: dashed 1px #0F4DA2; cursor: pointer; }
        #ezpkontur .b-orderForm_tabs_item label { cursor: pointer; }
        #ezpkontur .b-orderForm_tabs_item_hiddenRadio { width: 1px; height: 1px; overflow: hidden; position: absolute; left: -100500px; }
        #ezpkontur .b-orderForm_tabs_item__selected { background: #ECF0F2; }
        #ezpkontur .b-orderForm_tabs_item__selected .dashed, #ezpkontur .b-orderForm_tabs_item__selected .dashed:hover { cursor: default; color: black !important; border-bottom: none; }
        #ezpkontur .b-orderForm_tabs_item__selected label { cursor: default; }
        #ezpkontur .hide { display: none; }
    </style>
{/literal}