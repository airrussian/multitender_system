<h1>Платежи пользователей</h1>

<div class="mt_boxwhite" id="moder_payments">

    <div class="mp-filter">
        <h2>Фильтр</h2>
        <form action="?action=moder_payments&task=filter" method="POST">
            <label for="filter_by_pay_id">Укажите номер платежа:</label><input id="filter_by_pay_id" name="pay_id" value="{$pay_id|escape}" type="text" />
            <label for="filter_by_inn">или укажите ИНН:</label><input id="filter_by_inn" name="inn" value="{$inn|escape}" type="text" />
            <input class="submit" type="submit" value="{$keysubmit}" name="submit" />
        </form>

        <p>{if $pays}Найдено: <b>{$payscount}</b> платяжей.{else}Счетов не найденно{/if}</p>
    </div>

    {if $pays}
    <table class="view payments tbl_sort">
        <thead>
            <tr>
                <th width="30px">#&nbsp;п.</th>
                <th>ID User</th>
                <th>Имя пользователя / Название фирмы / ИНН</th>
                <th>Вид тарифа</th>
                <th>Период</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Дата выставления</th>
                <th>Дата принятия</th>
                <th>Состояние</th>
            </tr>
        </thead>

        <tbody>
            {foreach from=$pays item=pay}
            <tr>
                <td align="right" class="payid" >{$pay.id}</td>
                <td align="center" class="userid firmcard" ><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$pay.user_id}">{$pay.user_id}</a></td>
                <td align="left">{if $right>100}{$pay.username} / {$pay.firms.name} / {$pay.firms.INN}{else}{$pay.username} / Не достаточно прав для просмотра информации{/if}</td>
                <td align="center">{$pay.kind_rates_id}</td>
                <td align="left">{if $pay.kind_rates_id==1}{$pay.count*30} дн.{else}-{/if}</td>
                <td align="left">{if $pay.kind_rates_id==2}{$pay.count} тен.{else}-{/if}</td>
                <td align="right">{$pay.summa}</td>
                <td align="center">{$pay.datetime}</td>
                <td align="center">{if $pay.datetime_adopted}{$pay.datetime_adopted}{else} - {/if}</td>
                <td align="center">
		{if $pay.paid > 0}Оплачен{else}
                    <a href="/ajax.php?action=moder_payments&task=acceptpayment&payment_id={$pay.id}&user_id={$pay.user_id}"
                       title="если счёт оплачен нужно нажать"
                       onclick="{literal}javascript: if (!confirm('Вы подтверждаете факт оплаты, счета c номером #'+$(this).parents('tr').find('td.payid').text()+'?')) { return false;}{/literal}">Оплатить</a>
                {/if}
                </td>
            </tr>
            {/foreach}
        </tbody>
    </table>

    {$scrol}

    {/if}

</div>