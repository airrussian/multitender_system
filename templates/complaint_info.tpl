<img class="y1" src="/templates/mt/img/sl.gif" alt="" /><div class="y2"><a class="c2" href="/tenders/distributor_list?id_distributor={$id_dist}">Вернуться к информации о поставщике</a></div>
<h1 class="y4i">Информация по жалобе</h1><br/>

<div class="mt_boxwhite">

<div id="statistics">
    <dl>
        <dt><b>Наименование поставщика</b></dt>
        <dd>{$distributor.Name}</dd>
    </dl>
    <dl>
        <dt><b>ИНН\КПП</b></dt>
        <dd>{$distributor.INN}{if $distributor.KPP}\{$distributor.KPP}{/if}</dd>
    </dl>
    <dl>
        <dt><b>Регион</b></dt>
        <dd>{$distributor.region_name}</dd>
    </dl>
    <dl>
        <dt><b>Адрес</b></dt>
        <dd>{$distributor.Address}</dd>
    </dl>
</div>
<hr/>
<br/>
<div id="statistics">
<br><a href="{$complaint.link}" target="_blank" title="Информация о жалобе на оффициальном источнике"> Ссылка на оффициальный источник </a>
    <dl>
        <dt><b>Номер реестровой записи</b></dt>
        <dd>{$complaint.Register_Number}</dd>
    </dl>
    {if $complaint.organisation_which_included}
    <dl>
        <dt><b>Уполномоченный орган, осуществивший включение сведений в реестр</b></dt>
        <dd>{$complaint.organisation_which_included}</dd>
    </dl>
    {/if}
    <dl>
        <dt><b>Дата включения сведений в реестр</b></dt>
        <dd>{$complaint.Date_of_Inclusion}</dd>
    </dl>
    <dl>
        <dt><b>Предмет контракта</b></dt>
        <dd>{$complaint.Subject_of_Contract}</dd>
    </dl>
    <dl>
        <dt><b>Сумма контракта</b></dt>
        <dd>{$complaint.Sum} тыс. руб.</dd>
    </dl>
    <dl>
        <dt><b>Дата проведения конкурса</b></dt>
        <dd>{$complaint.Date_of_Auction_Result}</dd>
    </dl>
    <dl>
        <dt><b>Срок исполнения контракта</b></dt>
        <dd>{$complaint.Date_of_execution}</dd>
    </dl>
</div>
<br/>
<small>Данные получены из открытых источников: <a href="http://reestrgk.roskazna.ru/">Росказна</a> и <a href="http://rnp.fas.gov.ru/">Реестр недобросовестных поставщиков</a></small>
</div>