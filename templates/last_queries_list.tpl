<div class="pathway">
    <span style="padding-left: 20px; background: transparent url(/templates/mt/img/sl.gif) no-repeat left center;">
        <a href="/tenders/cabinet">Личный кабинет</a>
    </span>
</div>

<div id="last_queries_list" class="systems-page">
    <h1>История последних запросов</h1>   
    <div class="mt_boxwhite ui-tabs">
    
        <ul class="ui-tabs-nav">
            <li><a href="/tenders/history_tenders"><span>Просмотренные тендеры</span></a></li>
            <li class="ui-state-active"><a href="#"><span>Поисковые запросы</span></a></li>
        </ul>
        {if $items}
        <table class="view tbl_sort">
            <thead>
                <tr>
                    <th>Наименование поискового запроса</th>
                    <th>Найденные тендеры</th>
                    <th class="{literal}{sorter: false}{/literal}"></th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$items item=item}
                <tr>
                    <td onclick="go('/tenders/?{$item.link_query}');"><a href="/tenders/?{$item.link_query}">{$item.search.main|escape}{if $item.search_txt}({$item.search_txt}){/if}</a></td>
                    <td onclick="go('/tenders/?{$item.link_query}');" class="t56" align="right">{$item.total}</td>
                    {if $reguser}<td align="center">{$item.favorite}</td>{/if}
                </tr>
                {/foreach}
            </tbody>
        </table>

        {$scrol}
        {else}
            <b>{$empty}</b>
        {/if}
    </div>
</div>