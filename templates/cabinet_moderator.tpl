<div>
    <h1>Кабинет модератора</h1>

    <div class="mt_boxwhite" id="cabinet_moderator">

        <ul style="font-size: 120%">           
            <li><span>Пользователи</span>
                <ul>
                    <li><a href="{$link_base}action=moder_payments">Платежи пользователей</a></li>
                    <li><a href="{$link_base}action=find_users">Поиск пользователей</a></li>
                    <ul>
                        <li><a href="{$link_base}action=card_user&task=fullcard_show&user_id=21">Yandex Bot</a>, <a href="{$link_base}action=card_user&task=fullcard_show&user_id=102">OLD id</a></li>
                        <li><a href="{$link_base}action=card_user&task=fullcard_show&user_id=22">Rambler</a></li>
                        <li><a href="{$link_base}action=card_user&task=fullcard_show&user_id=23">Google Bot</a>, <a href="{$link_base}action=card_user&task=fullcard_show&user_id=104">OLD id</a></li>
                        <li><a href="{$link_base}action=card_user&task=fullcard_show&user_id=24">Yahoo Bot</a>, <a href="{$link_base}action=card_user&task=fullcard_show&user_id=103">OLD id</a></li>
                        <li><a href="{$link_base}action=card_user&task=fullcard_show&user_id=25">MSN Bot</a>, <a href="{$link_base}action=card_user&task=fullcard_show&user_id=114">OLD id</a></li>
                    </ul>
                </ul>
            </li>            
            <li><span>Тендеры</span>
                <ul>
                    <li><a href="{$link_base}action=pages_changes">Отслеживаемые страницы</a></li>
                    <li><a href="{$link_base}action=moder_items">Список добавленных тендеров</a></li>
                    {if $right<>51}<li><a href="{$link_base}action=pages_moder">База страниц сайтов</a></li>{/if}
                    <li><a href="{$link_base}action=onlineorder&task=list">Онлайн заявки на консультации</a></li>
                </ul>
            </li>
            <li><span>Разное</span>
                <ul>
                    <li><a href="{$link_base}action=errors_tenders">Ошибки на тендерах</a></li>
                    <li><a href="{$link_base}action=load_seo_keys">SEO::Добавление ключевых фраз</a></li>
                    <li><a href="{$link_base}action=quality_analyzer">Анализатор качества поиска</a></li>
                    <li><a href="{$link_base}action=statistics">Статистика загрузчиков</a></li>
                    <li><a href="{$link_base}action=all_search">Все запросы (бывший Костыль™)</a></li>
                    <li><a href="{$link_base}action=graph">Графики</a></li>
                    <li><a href="/tenders/comments_moder">Комментарии к тендерам</a></li>
                    <li><a href="/tenders/recordsmen">Рекордсмены Мультитендера</a></li>
                    <li><a href="/tenders/cabinet_baner">Редактирование банеров</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>