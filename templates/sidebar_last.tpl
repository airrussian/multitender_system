<div>
{foreach from=$items item=item}
    <p style="color: #3c3c3c">
        <a href="{$item.link}">{$item.name}</a><br />
        {if $item.region}<span style="color: #999999">Регион:&nbsp;</span>{$item.region}&nbsp;&nbsp;&nbsp;{/if}
        {if $item.type}<span style="color: #999999">Тип тендера:&nbsp;</span>{$item.type}&nbsp;&nbsp;&nbsp;{/if}
        {if $item.price}<span style="color: #999999">Стоимость:&nbsp;</span>{$item.price|mt_price}&nbsp;руб.{/if}
    </p>
{/foreach}
</div>
