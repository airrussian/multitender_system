<h1 class="cabinettitle">Личный кабинет</h1><br/>

<div class='mt_boxwhite'>
    <div style='width: 290px; float: left; margin-top: 8px; margin-left: 10px;'>
        <div class="lk-account">
            <h2>Ваш аккаунт</h2>
        {if $firm.name}<p><strong>Организация</strong><br/><span style="font-size:14px">{$firm.name}</span></p>{/if}
    {if $firm.domicile_full}<p><strong>Адрес</strong><br /><span style="font-size:14px">{$firm.domicile_full}</span></p>{/if}
    <a href="/component/user/?task=edit">Редактировать учетные данные</a>
</div>
</div>
<div style='width: 290px; float: left; margin-top: 8px; margin-left: 20px;'>    
    <div id='lk-searcht-tenders'>
        <h2><a href="/tenders/?">Поиск тендеров</a></h2>
        <p>Не можете найти нужный госзаказ?<br />Воспользуйтесь поиском.<br /><a href="/help-video#poisk">Как искать тендеры?</a></p>
    </div>

    <div id='lk-email-subscribe'>
        <h2><a href="/tenders/email_newsletter">E-mail рассылка</a></h2>
        <p>Вы можете настроить рассылку для получения новой информации о тендерах на ваш е-мейл.<br /><a href="/help-video#mail">Как настроить рассылку?</a></p>
    </div>
    
    <div id='lk-email-subscribe'>
        <h2><a href='/tenders/subscribe_customer'>Подписка на новые тендеры заказчиков</a></h2>
        <p>Вы можете получать рассылку новых тендеров выбранных вами заказчиков.</p>
    </div>

    <div id='lk-comments'>
        <h2><a class="i21ii" href="/tenders/my_comments">Комментарии и заметки</a>   </h2>
    </div>
</div>
<div style='width: 290px; float: left; margin-top: 8px; margin-left: 20px;'>
    <div id='lk-favorite'>
        <h2><a href="/tenders/favorite_tenders">Избранное</a></h2>       
        <p><strong>Тендеры</strong><br />
            Добавьте любой тендер из нашей системы в избранное, и просматривать их в любое время в дальнейшем.</p>
        <p><strong>Поисковые запросы</strong><br />
            Если вы часто ищите одни и те же ключевые слова, с одинаковыми настройками расширенного поиска — удобно пользоваться заранее сохраненными избранными запросами.</p>
    </div>
    <div id='lk-history'>
        <h2><a href="/tenders/history_tenders">История</a></h2>
            <p>В истории сохраняются просмотренные вами тендеры и поисковые запросы.</p>
    </div>
</div>
    <br clear="all" />
</div>