<h1>Анализатор качества поиска</h1>

{if $type!='all'}<a href="{$base_link}action=quality_analyzer&period=day&godate={$date_curr}&type=all">все</a>{else}<b>все</b>{/if}
&nbsp;
{if $type!='email'}<a href="{$base_link}action=quality_analyzer&period=day&godate={$date_curr}&type=email">email</a>{else}<b>email</b>{/if}
&nbsp;
{if $type!='popul'}<a href="{$base_link}action=quality_analyzer&period=day&godate={$date_curr}&type=popul">популярные и маркеры</a>{else}<b>популярные и маркеры</b>{/if}

<br /><br />

<form action="{$base_link}action=quality_analyzer&period=day&godate={$date_curr}&type={$type}" method="POST">
    <input id="filter" name="filter" value="{$filter}" /><input type="submit" value="Фильтр" />
</form>
<br /><br />
{if $markers}
{if $period!='day'}<a href="{$base_link}action=quality_analyzer&period=day&page={$page}&godate={$date_curr}{if $filter}&filter={$filter}&type={$type}{/if}">по дням</a>{else}<b>по дням</b>{/if}
&nbsp;
{if $period!='week'}<a href="{$base_link}action=quality_analyzer&period=week&page={$page}&godate={$date_curr}{if $filter}&filter={$filter}&type={$type}{/if}">по неделям</a>{else}<b>по неделям</b>{/if}
&nbsp;
{if $period!='month'}<a href="{$base_link}action=quality_analyzer&period=month&page={$page}&godate={$date_curr}{if $filter}&filter={$filter}&type={$type}{/if}">по месяцам</a>{else}<b>по месяцам</b>{/if}

<br /><br />

{if $period=='day'}
<a href="{$base_link}action=quality_analyzer&godate={$date_prev}&period={$period}&page={$page}{if $filter}&filter={$filter}&type={$type}{/if}">&laquo; день</a>
<span>{$date_curr}</span>
{if $date_next}
<a href="{$base_link}action=quality_analyzer&godate={$date_next}&period={$period}&page={$page}{if $filter}&filter={$filter}&type={$type}{/if}">день &raquo;</a>
{else}
<span>день &raquo;</span>
{/if}
{/if}

{if $period=='week'}
<a href="{$base_link}action=quality_analyzer&godate={$date_prev}&period={$period}&page={$page}{if $filter}&filter={$filter}&type={$type}{/if}">&laquo; неделя</a>
<span>{$date_curr}</span>
{if $date_next}
<a href="{$base_link}action=quality_analyzer&godate={$date_next}&period={$period}&page={$page}{if $filter}&filter={$filter}&type={$type}{/if}">неделя &raquo;</a>
{else}
<span>неделя &raquo;</span>
{/if}
{/if}

{if $period=='month'}
<a href="{$base_link}action=quality_analyzer&godate={$date_prev}&period={$period}&page={$page}{if $filter}&filter={$filter}&type={$type}{/if}">&laquo; месяц</a>
<span>{$date_curr}</span>
{if $date_next}
<a href="{$base_link}action=quality_analyzer&godate={$date_next}&period={$period}&page={$page}{if $filter}&filter={$filter}&type={$type}{/if}">месяц &raquo;</a>
{else}
<span>месяц &raquo;</span>
{/if}
{/if}

<br / ><br />
Всего: {$total}
<br />
{if $filter}{if $total>10}уточните запрос{/if}{else}{$scrol}{/if}
<br /><br />
<table class="tbl-quality_analyzer" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th class="title" width="300px">Поиск</th>
            {foreach from=$markers[0].info item=item key=key}
            {if $key!=0}
            <th class="title" width="200px">{$item.date}</th>
            {/if}
            {/foreach}
        </tr>
    </thead>
    <tbody>
        {foreach from=$markers item=marker}
        <tr>
            <td><a href="{$base_link}{$marker.search_link}">{if $marker.name}<big>{$marker.name}</big><br />{/if}{if $marker.search}<small>{$marker.search}</small>{/if}</a></td>
            {foreach from=$marker.info item=item key=key}
            {if $key!=0}
            {if $item.total_items<=0 and $item.new_items<=0}
            <td align="center">&mdash;</td>
            {else} 
            <td>всего:&nbsp;{$item.total_items}<br>новых:&nbsp;{$item.new_items}</td>
            {/if} 
            {/if}
            {/foreach}
        </tr>
        {/foreach}
    </tbody>
</table>

{if $filter}{if $total>10}уточните запрос{/if}{else}{$scrol}{/if}
{else}
<b>К сожжалению, ничего не обнаружено</b>
{/if}