<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="templates/mt/js/plugins/excanvas.min.js"></script><![endif]-->
<script type="text/javascript" src="templates/mt/js/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="templates/mt/js/plugins/plugins.js"></script>
<link rel="stylesheet" type="text/css" href="templates/mt/css/jquery.jqplot.css" />
<!--<link rel="stylesheet" type="text/css" href="templates/mt/css/graph.css" />-->
{literal}
<style type="text/css">
ul.tabs {
    list-style: none;
    margin: 0;
    padding: 0;
    clear: both;
}

.tabs li {
    list-style: none;
    margin: 0;
    padding: 0;
    float: left;
    clear: right;
}

.tabs li a {
    font-size: 14px;
    margin: 0 10px;
    text-decoration: none;
    border: none;
    border-bottom: 1px dashed #01A2D7 !important;
}

.tabs li a:hover {
    border: none;
    border-bottom: 1px dashed #0069A6 !important;
}

#control_form {
    margin: 20px 0;
}

#control_form input {
    border: 1px solid #AAA;
}

.graph_area {
    width: 100%;
    height: 100%;
    /*width: 900px;
    height: 400px;
    margin: 20px auto;*/
    display: none;
}
#graph_box {
    width: 900px;
    height: 400px;
    margin: 20px auto;
}
.periods {
    line-height: 16px;
    text-decoration: none;
    border-bottom: 1px dashed #01A2D7 !important;
}
.periods:hover {
    border-bottom: 1px dashed #0069A6 !important;
}
#group {
    border: 1px solid #444;
}
#submit_form {
    margin: 0 0 0 10px;
    background: #DDD;
    padding: 5px;
    border-radius: 3px;
}
#submit_form:hover {
    background: #EEE;
}
#cmp_box {
    display: none;
}
#ajax_loader {
    display: none;
    z-index: 100;
    position: absolute;
    text-align: center;
    padding: 30px;
    height: 50px;
    width: 200px;
    top: 450px;
    left: 100px;
    border: 1px solid #666;
    background: #EEE;
    border-radius: 5px;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
    var active = 'money';
    var block_all = true;

    $('.graph_toogle').click(function(){
	if (block_all == false) {
	   $('.graph_area').hide();
	   var id = $(this).attr('id');
	   active = id.substr(0, id.length - 7);
	   $('#'+active+'_plot').show();
	   if (regen[active] === 0) {
	      ajax_request();
	   }
	}
    });
    
    $('#cmp_toogle').click(function() {
	var box = $('#cmp_box');
        if (box.css('display') == 'block') {
            $('#cmp').val('0');
            box.slideUp('fast');
        } else {
            $('#cmp').val('1');
            $('#from_cmp').val($('#from').val());
            $('#to_cmp').val($('#to').val());
            box.slideDown('slow');
        }
    });

    $('#last5day').click(function() {
        var date = new Date();
        var month = date.getMonth();
        var day = date.getDate();
        var year = date.getFullYear();
        var new_date = new Date(year, month, day - 4, 0, 0, 0, 0);
        $('#from').val(new_date.getDate() + '.' + (parseInt(new_date.getMonth(), 10)+1) + '.' + new_date.getFullYear());
        $('#to').val(date.getDate() + '.' + (parseInt(date.getMonth(), 10)+1) + '.' + date.getFullYear());
        $("#group [value='1']").attr('selected', 'selected');
        $('#from_cmp').val($('#from').val());
        $('#to_cmp').val($('#to').val());
    });
    $('#last_week').click(function() {
        var date = new Date();
        var month = date.getMonth();
        var day = date.getDate();
        var year = date.getFullYear();
        var day_of_week = date.getDay();
        var new_date = new Date(year, month, day - day_of_week - 6, 0, 0, 0, 0);
        date = new Date(year, month, day - day_of_week, 0, 0, 0, 0);
        $('#from').val(new_date.getDate() + '.' + (parseInt(new_date.getMonth(), 10)+1) + '.' + new_date.getFullYear());
        $('#to').val(date.getDate() + '.' + (parseInt(date.getMonth(), 10)+1) + '.' + date.getFullYear());
        $("#group [value='1']").attr('selected', 'selected');
        $('#from_cmp').val($('#from').val());
        $('#to_cmp').val($('#to').val());
    });
    $('#next_month').click(function() {
        var date = new Date();
        var month = date.getMonth();
        var year = date.getFullYear();
        var new_date = new Date(year, month+2, 0, 0, 0, 0, 0);
        date = new Date(year, month+1, 1, 0, 0, 0, 0, 0);
        $('#from').val(date.getDate() + '.' + (parseInt(date.getMonth(), 10)+1) + '.' + date.getFullYear());
        $('#to').val(new_date.getDate() + '.' + (parseInt(new_date.getMonth(), 10)+1) + '.' + new_date.getFullYear());
        $("#group [value='1']").attr('selected', 'selected');
        $('#from_cmp').val($('#from').val());
        $('#to_cmp').val($('#to').val());
    });
    $('#cur_month').click(function() {
        var date = new Date();
        var month = date.getMonth();
        var year = date.getFullYear();
        var new_date = new Date(year, month, 1, 0, 0, 0, 0);
        $('#from').val(new_date.getDate() + '.' + (parseInt(new_date.getMonth(), 10)+1) + '.' + new_date.getFullYear());
        $('#to').val(date.getDate() + '.' + (parseInt(date.getMonth(), 10)+1) + '.' + date.getFullYear());
        $("#group [value='1']").attr('selected', 'selected');
        $('#from_cmp').val($('#from').val());
        $('#to_cmp').val($('#to').val());
    });
    $('#last_month').click(function() {
        var date = new Date();
        var month = date.getMonth();
        var year = date.getFullYear();
        var new_date = new Date(year, month-1, 1, 0, 0, 0, 0);
        date = new Date(year, month, 0, 0, 0, 0, 0, 0);
        $('#from').val(new_date.getDate() + '.' + (parseInt(new_date.getMonth(), 10)+1) + '.' + new_date.getFullYear());
        $('#to').val(date.getDate() + '.' + (parseInt(date.getMonth(), 10)+1) + '.' + date.getFullYear());
        $("#group [value='1']").attr('selected', 'selected');
        $('#from_cmp').val($('#from').val());
        $('#to_cmp').val($('#to').val());
    });
    $('#all_period').click(function() {
        var date = new Date();
        $('#from').val('01.12.2010');
        $('#to').val(date.getDate() + '.' + (parseInt(date.getMonth(), 10)+1) + '.' + date.getFullYear());
        $("#group [value='3']").attr('selected', 'selected');
        $('#from_cmp').val($('#from').val());
        $('#to_cmp').val($('#to').val());
    });

    var timeDifference = function(begin, end) {
        var begin = $('#from').val().split('.', 3);
        begin = new Date(begin[2], begin[1]-1, begin[0]);
        var end = $('#to').val().split('.', 3);
        end = new Date(end[2], end[1]-1, end[0]);
        if (end < begin) {
            return 0;
        }
        return Math.floor((end - begin)/(60*60*24*1000));
    };
    
    $('#next_period').click(function() {
        var diff = timeDifference();
        $('#from_cmp').val($('#to_cmp').val());
        var end = $('#to_cmp').val().split('.', 3);
        end = new Date(end[2], end[1]-1, parseInt(end[0],10)+diff);
        $('#to_cmp').val(end.getDate() + '.' + (parseInt(end.getMonth(), 10)+1) + '.' + end.getFullYear());
    });
    $('#prev_period').click(function() {
        var diff = timeDifference();
        $('#to_cmp').val($('#from_cmp').val());
        var begin = $('#from_cmp').val().split('.', 3);
        begin = new Date(begin[2], begin[1]-1, parseInt(begin[0],10) - diff);
        $('#from_cmp').val(begin.getDate() + '.' + (parseInt(begin.getMonth(), 10)+1) + '.' + begin.getFullYear());
    });

    var height = Math.floor(($(window).height() - $('#ajax_loader').height())/2);
    var width = Math.floor(($(window).width() - $('#ajax_loader').width())/2);
    $('#ajax_loader').css('top', height+'px').css('left', width+'px').fadeIn('slow');

    var options = {
          title: { fontSize: '16px', fontFamily: 'Arial' },
          seriesDefaults: { rendererOptions: {barMargin: 15, barPadding: 5}, pointLabels: { show: true, hideZeros: true }},
          series: [{ color:"#c30707", renderer: $.jqplot.BarRenderer }, { color:"#3757f4", renderer: $.jqplot.BarRenderer }, { color:"#ef4d4d", renderer: $.jqplot.BarRenderer }, { color:"#8195fb", renderer: $.jqplot.BarRenderer }],
          axes: { xaxis: {renderer: $.jqplot.CategoryAxisRenderer}, yaxis: { min: 0 }},
          axesDefaults: { tickRenderer: $.jqplot.CanvasAxisTickRenderer, tickOptions: { formatString: '%d', fontFamily: 'Arial', fontSize: '11px' }},
          legend: {show: true, fontSize: '12px', renderer:$.jqplot.EnhancedLegendRenderer}};

    var opt = new Array();
    opt['payments'] = {}; opt['money'] = {}; opt['regs'] = {}; opt['new_tenders'] = {}; opt['new_moderator'] = {}; opt['payed_nonpayed'] = {}; opt['com_gos'] = {}; opt['ending'] = {};
    $.extend(true, opt['payments'], options, {title: {text: 'Оплатившие'}, legend: {labels:['Всего оплативших', 'Оплатившие впервые', 'Всего оплативших (период)', 'Оплатившие впервые (период)']}});
    $.extend(true, opt['money'], options, {title: {text: 'Деньги'}, legend: {labels:['Всего', 'Безналичные', 'Электронные', 'Всего (период)', 'Безналичные (период)', 'Электронные (период)']}, series: [
            { color:"#c30707", renderer: $.jqplot.BarRenderer },
            { color:"#3757f4", renderer: $.jqplot.BarRenderer },
            { color:"#dba707", renderer: $.jqplot.BarRenderer },
            { color:"#ef4d4d", renderer: $.jqplot.BarRenderer },
            { color:"#8195fb", renderer: $.jqplot.BarRenderer },
            { color:"#f4c127", renderer: $.jqplot.BarRenderer }]});
    $.extend(true, opt['regs'], options, {title: {text: 'Регистрации'}, legend: {labels:['Регистрации', 'Выставленные счета', 'Регистрации (период)', 'Выставленные счета (период)']}});
    $.extend(true, opt['new_tenders'], options, {title: {text: 'Новые тендеры'}, legend: {labels:['Всего', 'Государственные', 'Коммерческие', 'Всего (период)', 'Государственные (период)', 'Коммерческие (период)']}, series: [
            { color:"#c30707", renderer: $.jqplot.BarRenderer },
            { color:"#3757f4", renderer: $.jqplot.BarRenderer },
            { color:"#dba707", renderer: $.jqplot.BarRenderer },
            { color:"#ef4d4d", renderer: $.jqplot.BarRenderer },
            { color:"#8195fb", renderer: $.jqplot.BarRenderer },
            { color:"#f4c127", renderer: $.jqplot.BarRenderer }]});
    $.extend(true, opt['new_moderator'], options, {title: {text: 'Тендеры добавленные модератором'}, legend: {labels:['Всего', 'Всего (период)']}});
    $.extend(true, opt['payed_nonpayed'], options, {title: {text: 'Оплаченные/Неоплаченные'}, legend: {labels:['Оплаченные', 'Неоплаченные', 'Оплаченные (период)', 'Неоплаченные (период)']}});
    $.extend(true, opt['com_gos'], options, {title: {text: 'Государственные/Коммерческие'}, legend: {labels:['Государственные', 'Коммерческие']}});
    $.extend(true, opt['ending'], options, {title: {text: 'Заканчивающиеся'}, legend: {labels:['Заканчивающиеся', 'Заканчивающиеся (период)']}});

    var regen = new Array();
    regen['payments'] = 0;
    regen['money'] = 0;
    regen['regs'] = 0;
    regen['new_tenders'] = 0;
    regen['new_moderator'] = 0;
    regen['payed_nonpayed'] = 0;
    regen['com_gos'] = 0;
    regen['ending'] = 0;

    var plot = new Array();
    var plot_data = new Array();

    $.datepicker.regional['ru'] = {
                closeText: 'Закрыть',
                prevText: '&#x3c;Пред',
                nextText: 'След&#x3e;',
                currentText: 'Сегодня',
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
                dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                weekHeader: 'Не',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['ru']);
    $('#to').datepicker({
        inline: true,
        dateFormat: 'dd.mm.yy'
    });
    $('#from').datepicker({
        inline: true,
        dateFormat: 'dd.mm.yy'
    });

    $('#graph_box').resizable({delay:20});
    $('#graph_box').bind('resizestop', function(event, ui) {
    	for (var p in plot) {
	   delete plot[p];
	   plot[p] = $.jqplot(p+'_plot', plot_data[p], opt[p]);
	   plot[p].redraw();
	}
    });

    function ajax_request() {
        var from = $('#from').val();
        var to = $('#to').val();
        var group = $('#group').val();
	var cmp = $('#cmp').val()
	var from_cmp = $('#from_cmp').val();
	var to_cmp = $('#to_cmp').val();

	if (block_all == true) {
		return;
	}

        $.ajax({
            'async': true,
	    'cache': false,
	    'url': 'ajax.php',
            'dataType': 'json',
            'beforeSend': function() {
                // AjaxLoader animation
		$('#ajax_loader').show();
		$('#submit_form').attr('disabled', 'true');
		block_all = true;
	    },
	    'success': function(e) {
		$('#ajax_loader').hide();
		$('#submit_form').removeAttr('disabled');
		block_all = false;
		//console.log(e);
		if (plot[active] !== undefined) {
		   delete plot[active];
		}
		if (e == null) {
		   plot[active] = $.jqplot(active+'_plot', [[[1,0]]], opt[active]);
		   plot_data[active] = [[[1,0]]];
		} else {
		   plot[active] = $.jqplot(active+'_plot', e.plots, opt[active]);
		   plot_data[active] = e.plots;
		}
	   	regen[active] = 1;
		plot[active].redraw();
	    },
            'error': function(e) {
		$('#ajax_loader').hide();
		$('#submit_form').removeAttr('disabled');
		block_all = false;
            },
	    'data': {
		'from': from,
		'to': to,
		'group': group,
		'active': active,
		'cmp': cmp,
		'from_cmp': from_cmp,
		'to_cmp': to_cmp,
		'action': 'graph'
	    }
        });
    }
    
    function ajax_request_form(e) {
	for (var p in regen) {
	   regen[p] = 0;
	}
	ajax_request();
	return false;
    }

    $('#control_form').submit(ajax_request_form);
    $('#'+active+'_plot').show();
    block_all = false;
    ajax_request();
});
</script>
{/literal}
{$fetch}
<div id="ajax_loader"><img src="http://multitender.ru/templates/mt/img/ajax-loader2.gif" /><br /><br /><p>Подождите, идет загрузка</p></div>
<div class="mt_boxwhite">
<ul class="tabs">
    <li><a href="javascript:void(0)" class="graph_toogle" id="payments_toogle">Оплатившие</a></li>
    <li><a href="javascript:void(0)" class="graph_toogle" id="regs_toogle">Количество регистраций</a></li>
    <li><a href="javascript:void(0)" class="graph_toogle" id="money_toogle">Деньги</a></li>
    <li><a href="javascript:void(0)" class="graph_toogle" id="payed_nonpayed_toogle">Оплаченные/Неоплаченные</a></li>
    <li><a href="javascript:void(0)" class="graph_toogle" id="ending_toogle">Заканчивающиеся</a></li>
    <li><a href="javascript:void(0)" class="graph_toogle" id="new_tenders_toogle">Количество новых тендеров</a></li>
    <li><a href="javascript:void(0)" class="graph_toogle" id="new_moderator_toogle">Количество новых тендеров от модератора</a></li>
</ul>
<br clear="all" />
<form action="ajax.php" method="POST" id="control_form">
    <div style="margin: 0 0 20px 10px;">
        <label>Отобразить с</label>
        <input type="text" value="{$from}" name="from" id="from" />
        <label>по</label>
        <input type="text" value="{$to}" name="to" id="to" />
    </div>
    <div style="margin: 0 0 20px 10px;">
        <a class="periods" id="last5day" href="javascript:void(0);">Последние 5 дней</a>&nbsp;
        <a class="periods" id="last_week" href="javascript:void(0);">Прошлая неделя</a>&nbsp;
        <a class="periods" id="cur_month" href="javascript:void(0);">Текущий месяц</a>&nbsp;
        <a class="periods" id="next_month" href="javascript:void(0);">Следующий месяц</a>&nbsp;
        <a class="periods" id="last_month" href="javascript:void(0);">Прошлый месяц</a>&nbsp;
        <a class="periods" id="all_period" href="javascript:void(0);">Весь период</a>
    </div>
    <div style="margin: 0 0 20px 10px;">
        <label>Группировать по:</label>
        <select name="group" id="group">
            <option value="1">Дням</option>
            <option value="2">Неделям</option>
            <option value="3" selected>Месяцам</option>
        </select>
    </div>
    <a href="javascript:void(0)" class="periods" style="margin: 0 0 10px 10px" id="cmp_toogle">Сравнить с периодом</a>
    <div style="margin: 10px 0 0 10px;" id="cmp_box">
	<label>с</label>
        <input type="text" value="{$from_cmp}" name="from_cmp" id="from_cmp" />
        <label>по</label>
        <input type="text" value="{$to_cmp}" name="to_cmp" id="to_cmp" />
	<input type="hidden" value="0" id="cmp" name="cmp" />
	<a class="periods" id="prev_period" href="javascript:void(0);">На период назад</a>&nbsp;
	<a class="periods" id="next_period" href="javascript:void(0);">На период вперед</a>
    </div>
    <br clear="all" />
    <br clear="all" />
    <input type="submit" value="Сгенерировать" name="submit" id="submit_form" />
</form>
<div id="graph_box">
   <div id="money_plot" class="graph_area"></div>
   <div id="payments_plot" class="graph_area"></div>
   <div id="regs_plot" class="graph_area"></div>
   <div id="new_tenders_plot" class="graph_area"></div>
   <div id="new_moderator_plot" class="graph_area"></div>
   <div id="payed_nonpayed_plot" class="graph_area"></div>
   <div id="ending_plot" class="graph_area"></div>
</div>
</div>
