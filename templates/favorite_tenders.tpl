<div class="pathway">
    <span style="padding-left: 20px; background: transparent url(/templates/mt/img/sl.gif) no-repeat left center;">
        <a class="c2" href="/tenders/cabinet">Личный кабинет</a>
    </span>
</div>

<div id="favorite_tenders" class="systems-page">

    <h1 style="background: transparent url('/templates/mt/img/big_patric.gif') no-repeat left center">Избранное</h1>

    <div class="mt_boxwhite ui-tabs">

        <ul class="ui-tabs-nav">
            <li class="ui-state-active"><a href="#tabs1"><span>Избранные тендеры</span></a></li>
            <li><a href="/tenders/favorite_queries"><span>Избранные запросы</span></a></li>
        </ul>

        <div id="tabs1">
        {if $items}
        <table class="view fav_tenders tbl_sort" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>Название</th>
                    <th class="{literal}{sorter: 'mtShortDate'}{/literal}">Начало</th>
                    <th class="{literal}{sorter: 'mtShortDate'}{/literal}">Окончание</th>
                    {if $reguser}<th class="{literal}{sorter: false}{/literal}">Удалить</th>{/if}
                </tr>
            </thead>
            <tbody>
                {foreach from=$items item=item}
                <tr>
                    <td><a href="/tenders/detail/?id={$item.id}">{$item.name}</a></td>
                    <td>{$item.date_publication|tc_date}</td>
                    <td>{$item.date_end|tc_date_span}</td>
                    {if $reguser}<td align="center"><a class="delete" href="/ajax.php?action=favorite_tenders&task=del&id={$item.id}" title="удалить">&nbsp;</a></td>{/if}
                </tr>
                {/foreach}
            </tbody>
        </table>
        {$scrol}
        {else}
        <b>Нет добавленных</b>
        {/if}
        </div>
    </div>
</div>