<h1>Ошибки на details</h1>

<div class="mt_boxwhite">


    {if $errors}

    {$scrol}
    <b>Всего найдено: {$total} </b>
    <br />
    <table class="view tbl_sort">
        <thead>
            <tr>
                <th class="title" style="width: 40%">Тендер</th>
                <th class="title" style="width: 10%">Пользователь<br />Время (UTC)</th>
                <th class="title" style="width: 50%">Сообщение</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$errors item=error}
            <tr class="{cycle values='unc,uunc'} firmcard">
                <td align=left><a href="{$link_detail}{$error.item_id}">{$error.item.name}</a></td>
                <td align=left><a href="{$link_base}action=card_user&task=fullcard_show&user_id={$error.user_id}">{$error.user.name}</a><br />{$error.timestamp}</td>
                <td align=left>{$error.comment}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <br />
    {$scrol}
    {else}
    <b>Ошибок не обнаружено!</b>
    {/if}

</div>