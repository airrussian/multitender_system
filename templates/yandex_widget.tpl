<html>
    <head>
        {literal}
        <link href="{$conf.pref.link_base_http}i/yandex-widget.css" media="screen" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="http://img.yandex.net/webwidgets/1/WidgetApi.js"></script>
        <script type="text/javascript" src="{$conf.pref.link_base_http}i/yandex-widget0.js"></script>
        {/literal}
    </head>
    <body>
        <div class="yw-body">
            <div class="yw-list">
                {foreach from=$pages key=page item=items}
                <div class="yw-page" id="yw-page{$page}">
                    {foreach from=$items item=item}
                    <div class="yw-item {cycle values='even,odd'}">
                        <div class="yw-name"><a href="{$item.link|escape}">{$item.name}</a></div>
                        <div class="yw-desc">{$item.region_name|escape}, {$item.date_publication|escape}, {$item.price|tc_price} т.р., {$item.type_name|escape}</div>
                        <div class="yw-date">{$item.date}</div>
                    </div>
                    {/foreach}
                </div>
                {/foreach}
            </div>
            <div class="yw-navig">
                <div class="yw-pg-prev" onclick="page_go('prev');">пред.</div>
                <div class="yw-pg-nav"><span id="yw-pagenow">1</span> из <span id="yw-pagecount">{$pagecount}</span></div>
                <div class="yw-pg-next" onclick="page_go('next');">след.</div>
            </div>
        </div>
    </body>
</html>