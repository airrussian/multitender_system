<h1 class="y4i">Рекордсмены Мультитендера</h1><br/>

<div class="rc-white-head"></div>
<div class="rc-white-cont">
    <table style="float:right; margin-right:40px">
        <thead>
            <tr>
                <th colspan="2">Рекорд по поисковым запросам за неделю</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$last_search_week item=item2}
            <tr>
                <td><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$item2.user_id}">{$item2.username}</a></td>
                <td>{$item2.cnt}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <table style="float:right; margin-right:40px">
        <thead>
            <tr>
                <th colspan="2">Рекорд по поисковым запросам за месяц</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$last_search_month item=item2}
            <tr>
                <td><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$item2.user_id}">{$item2.username}</a></td>
                <td>{$item2.cnt}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <table style="float:right; margin-right:40px">
        <thead>
            <tr>
                <th colspan="2">Рекорд по поисковым запросам за весь период</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$last_search_all item=item2}
            <tr>
                <td><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$item2.user_id}">{$item2.username}</a></td>
                <td>{$item2.cnt}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <br class="fix"/>
    <br class="fix"/>
    <table style="float:right; margin-right:40px">
        <thead>
            <tr>
                <th colspan="2">Рекорд по купленным тендерам за неделю</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$user_tenders_week item=item2}
            <tr>
                <td><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$item2.user_id}">{$item2.username}</a></td>
                <td>{$item2.cnt}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <table style="float:right; margin-right:40px">
        <thead>
            <tr>
                <th colspan="2">Рекорд по купленным тендерам за месяц</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$user_tenders_month item=item2}
            <tr>
                <td><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$item2.user_id}">{$item2.username}</a></td>
                <td>{$item2.cnt}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <table style="float:right; margin-right:40px">
        <thead>
            <tr>
                <th colspan="2">Рекорд по купленным тендерам за весь период</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$user_tenders_all item=item2}
            <tr>
                <td><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$item2.user_id}">{$item2.username}</a></td>
                <td>{$item2.cnt}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <br class="fix"/>
    <br class="fix"/>
    <table style="float:right; margin-right:50px">
        <thead>
            <tr>
                <th colspan="2">Рекорд по комментариям</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$topcomments item=item}
            <tr>
                <td><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$item.user_id}">{$item.username}</a></td>
                <td>{$item.cnt}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <table style="float:right; margin-right:50px">
        <thead>
            <tr>
                <th colspan="2">Рекорд по email рассылкам</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$email_newsletter item=item2}
            <tr>
                <td><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$item2.user_id}">{$item2.username}</a></td>
                <td>{$item2.cnt}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <table style="float:right; margin-right:50px">
        <thead>
            <tr>
                <th colspan="2">Рекорд по избранным тендерам</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$favorite_tenders item=item2}
            <tr>
                <td><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$item2.user_id}">{$item2.username}</a></td>
                <td>{$item2.cnt}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <table style="float:right; margin-right:50px">
        <thead>
            <tr>
                <th colspan="2">Рекорд по избранным запросам</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$favorite_queries item=item2}
            <tr>
                <td><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$item2.user_id}">{$item2.username}</a></td>
                <td>{$item2.cnt}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <br class="fix"/>
    <br class="fix"/>
    <table style="float:right; margin-right:80px">
        <thead>
            <tr>
                <th colspan="2">Рекорд по выставленным счетам</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$payments0 item=item2}
            <tr>
                <td><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$item2.user_id}">{$item2.username}</a></td>
                <td>{$item2.cnt}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <table style="float:right; margin-right:80px">
        <thead>
            <tr>
                <th colspan="2">Рекорд по оплаченным счетам</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$payments1 item=item2}
            <tr>
                <td><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$item2.user_id}">{$item2.username}</a></td>
                <td>{$item2.cnt}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <table style="float:right; margin-right:80px">
        <thead>
            <tr>
                <th colspan="2">Рекорд по сумме оплаченных счетов</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$paymentsSumma item=item2}
            <tr>
                <td><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$item2.user_id}">{$item2.username}</a></td>
                <td>{$item2.sum}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <br class="fix"/>
    <br class="fix"/>
    <table style="float:right; margin-right:140px">
        <thead>
            <tr>
                <th>Рекорд по кол-ву используемых IP</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$user_ips item=item2}
            <tr>
                <td><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$item2.user_id}">{$item2.name}</a></td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <table style="float:right; margin-right:140px">
        <thead>
            <tr>
                <th>Рекорд по кол-ву используемых браузеров</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$user_agents item=item2}
            <tr>
                <td><a href="/tenders/?action=card_user&task=fullcard_show&user_id={$item2.user_id}">{$item2.name}</a></td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <br class="fix"/>
</div>
<div class="rc-white-foot"></div>