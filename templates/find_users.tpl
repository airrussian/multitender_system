<div id="find-users">
    <h1>Поиск пользователей</h1>

    <div class="mt_boxwhite">


        <form method="post" action="{$link_base}{$link_act}">
            <label>Строка поиска</label>
            <input name="search" value="{$search|escape}" type="text" />
            <button type="submit">Поиск</button>
        </form>

        {if $users}
        <b>Всего найдено: {$total} </b>
        <br />
        <table class="view users tbl_sort">
            <thead>
                <tr>
                    <th>ID Tenders</th>
                    <th>ID Joomla</th>
                    <th>Имя пользователя</th>
                    <th>Электронный адрес</th>
                    <th>Название фирмы</th>
                    <!-- <th>ИНН&nbsp;/&nbsp;КПП</th> -->
                    <th>Юр. адрес</th>
                    <th>Телефон</th>
                    <th>Контактное лицо</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$users item=user}
                <tr>
                    <td {if $user.sys_id}class="userid"{/if}>{if $user.sys_id}<a href="{$link_base}action=card_user&task=fullcard_show&user_id={$user.sys_id}">{$user.sys_id}</a>{else}<a href="{$link_base}action=card_user&task=add&user_id={$user.id}">создать</a>{/if}</td>
                    <td>{$user.id}</td>
                    <td>{$user.username}</td>
                    <td>{$user.email}</td>
                    {if $user.firm}
                    <td>{$user.firm.name}</td>
                    <!--<td>{$user.firm.INN}&nbsp;/&nbsp;{if $user.firm.KPP}{$user.firm.KPP}{else}не&nbsp;указан{/if}</td>-->
                    <td>{$user.firm.domicile_full}</td>
                    <td>{$user.firm.telephone}</td>
                    <td>{$user.firm.contact}</td>
                    {else}
                    <td colspan="5" align="center">&mdash;</td>
                    {/if}
                </tr>
                {/foreach}
            </tbody>
        </table>
        <br />
        {$scrol}
        {else}
        {if $message}<b>{$message}</b>{/if}
        {/if}
    </div>
</div>