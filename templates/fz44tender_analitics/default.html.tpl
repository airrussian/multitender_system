<div class="mt_boxwhite mt_boxwhitenew">
    <dl>
        <dt style="font-weight: bold">Закупка:</dt>
        <dd><a href="/tenders/detail44/?purchaseNumber={$tender.purchaseNumber}">{$tender.name}</a></dd>
        <dt style="font-weight: bold">Заказчик:</dt>
        <dd>{$tender.organizer.name}</dd>        
    </dl>
    
    <h1>Анализ похожих закупок данного заказчика</h1>
    
    <dl>
        <dt>Среднее количество участников:</dt>
        <dd>{$applications|@count}</dd>
        <dt>Среднее снижение на торгах:</dt>
        <dd>{$avg_pre_reduction|string_format:"%.2f"}%</dd>
    </dl>
    
    <table class="view searches tbl_sort">
        <caption style="font-size: 150%; padding: 10px 0 20px;">Участники и победители</caption>
        <thead>
            <tr>
                <th>Организация</th>
                <th>Участий (шт.)</th>
                <th class="{literal}{sorter: 'price'}{/literal} headerSortUp">Побед (шт.)</th>                
                <th>Средний % снижения</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$applications item=application}
            <tr>
                <td><a href="http://www.filetender.ru/info_new.php?inn={$application.org.inn}&num={$tender.purchaseNumber}" target="_blank">{$application.org.name}</a></td>
                <td align="center">{$application.part}</td>
                <td align="center">{$application.wins}</td>
                <td align="center">{$application.per_reduction|string_format:"%.2f"}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
</div>