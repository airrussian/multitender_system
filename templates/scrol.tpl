<link rel="stylesheet" type="text/css" href="/templates/mt/css/paginator.css" />
<script type="text/javascript" src="/templates/mt/js/paginator.js"></script>
<div class="paginator" id="{$id}"></div>
<div class="paginator_pages">{$pages} страниц</div>
{if $onpage_html}<div style="color: #808080; font-size: 0.8em; margin-top: -12px;">Выводить по {$onpage_html} тендеров на страницу</div>{/if}
<script type="text/javascript">
	paginator = new Paginator(
		"{$id}", // id контейнера, куда ляжет пагинатор
		{$pages}, // общее число страниц
		15, // число страниц, видимых одновременно
		{$current}, // номер текущей страницы
		"{$link}" // url страниц
	);
</script>
<noscript><div id="paninator_plain">Страницы: {$noscript_html}</div></noscript>