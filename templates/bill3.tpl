<img alt="" src="templates/mt/img/sl.gif" class="y1">
<div class="y2"><a href="/tenders/cabinet" class="c2">Личный кабинет</a></div>
<h1 class="y4i">Пополнение баланса</h1>
<div class="q1">
    		<div class="wt"></div>
	        <div class="wo">
            <div class="wo1">
<div class="l1">
	<span class="l7">Шаг 1</span><br>
	<span class="l8">Выбор способа оплаты</span>
</div>
<div class="l4"></div>
<div class="l1">
	<span class="l5">Шаг 2</span><br>
	<span class="l6">Выбор тарифа</span>
</div>
<div class="l4"></div>
<div class="l1">
	<span class="l2">Шаг 3</span><br>
	<span class="l3">Оплата счёта</span>
</div>


<div class="l10">
<form action="/tenders/billing/?task=exhibit" method="post">
    <input type="hidden" name="type_payment" value="{$type_payment}" />
    <input type="hidden" name="count_tender" value="{$count_tender}" />
    <input type="hidden" name="kind_rates_id" value="{$kind_rates_id}" />
    <input type="hidden" name="rate" value="{$rate}" />
<table cellspacing="0" cellpadding="5" class="t13">
<tbody>
<tr>
<td>Выбранный тариф:</td>
<td>{$tariff}</td>
</tr>
<tr>
<td>Содержание подписки:</td>
<td>{$count}</td>
</tr>
<tr>
<td>Сумма к оплате:</td>
<td>{$summ}</td>
</tr>
<tr>
<td>Способ оплаты:</td>
<td>{$kind_rate}</td>
</tr>
<tr>
    <td style="border-bottom: 1px solid rgb(245, 243, 232); text-align: center;" colspan="2"><button type="submit" class="accept"></button></td>
</tr>
</tbody></table></form>
</div>

<div class="l12">
<p>Перед вами счёт на пополнение баланса в Мультитендере. Если все данные верны, нажмите кнопку «Продолжить».</p>
{if $type_payment == electron}
<p>Электронный платеж вы будете производить на сайте нашего партнера, сервиса RoboKassa. Вы можете быть уверены в его надежности. Платеж будет получен нами оперативно и в полном объеме.</p>
{else}
<p>Для зачисления платежа в день оплаты счёта, отправьте платежное поручение с отметкой банка (или с отметкой из банк&ndash;клиента) нам по <a href="mailto:support@multitender.ru">электронной почте</a> или по факсу<br><strong>+7 (391) 265-17-63</strong>.</p>
{/if}
<p><a href="/tenders/billing/?task=exhibit">Перейти к остальным счетам</a></p>
<p></p>

</div>



			</div>
            <div class="wb"></div>
        </div>
</div>