<div class="username">
    <p>{$username},</p>
    <span>добро пожаловать</span>
</div>

<ul class="menu menu-user">
    <li class="cabinet"><a href="/tenders/cabinet">Личный кабинет</a></li>
    <li class="favorite"><a href="/tenders/favorite_tenders">Избранное</a></li>
    <li class="history"><a href="/tenders/history_tenders">История</a></li>
{if $rights>1}
    {if $alert_user}
    <li class="alert_user"><a title="Вы находитесь в просмотра чужого аккаунта {$alert_user.name}" href="/ajax.php?action=users&task=alertlogin">Alert View</a></li>
    {else}
    <li class="moderation"><a title="Кабинет модератора" href="/tenders/cabinet_moderator">Настройки</a></li>
    {/if}
{/if}
{if $message}
    <li><a href="#" class="messages" title="Вам новое сообщение" onclick="{literal}javascript: $('#dialog-message').dialog('open'); return false; {/literal}">&nbsp;</a></li>
{/if}
    <li class="logout"><a href="/index.php?option=com_user&task=logout">Выход</a></li>
</ul> 



{if $message}
    <div id="dialog-message" title="Сообщение" style="display: none;">
        <p class="validateTips">Здравствуйте.</p>
        <p>Вы используете устаревший браузер.</p>
        <p>Чтобы использовать все возможности сайта, мы рекомендуем загрузить и установить один из следующих браузеров</p>
        <div id="browser-select">
            <a style="background: transparent url(/templates/mt/img/chrome.gif) no-repeat scroll 50% 6px;" target="_blank" href="http://www.google.com/chrome/" class="fl_l">Google Chrome</a>
            <a style="background: transparent url(/templates/mt/img/firefox.gif) no-repeat scroll 50% 7px;" target="_blank" href="http://www.mozilla-europe.org/" class="fl_l">Mozilla Firefox</a>
            <a style="background: transparent url(/templates/mt/img/safari.gif) no-repeat scroll 50% 0px;" target="_blank" href="http://www.apple.com/safari/" class="fl_l">Safari</a>
            <a style="background: transparent url(/templates/mt/img/opera.gif) no-repeat scroll 50% 7px;" target="_blank" href="http://www.opera.com/" class="fl_l">Opera</a>
        </div>
    </div>
{/if}