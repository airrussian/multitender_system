<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Госзаказ :: {$info_main|escape}</title>
    <link>{$conf.pref.link_base_http}</link>
    <description>Система поиска по сайтам госзакупок. {if $info_main}Параметры поиска: {$info_main|escape}, найдено: {$total} тендеров.{/if}</description>
    <atom:link href="{$self}" rel="self" type="application/rss+xml" />
{foreach from=$items item=item}
    <item>
        <title>{$item.name|escape}</title>
        <link>{$item.link|escape}</link>
        <description>{$item.region_name|escape}, {$item.date_publication|tc_date}, {$item.price|tc_price} т.р., {$item.type_name|escape}</description>
        <category>{$item.type_name}</category>
        <pubDate>{$item.date}</pubDate>
        {if $gosexpress}<endDate>{$item.date_end|tc_date}</endDate>{/if}
        <guid>{$item.link|escape}</guid>
    </item>
{/foreach}
  </channel>
</rss>