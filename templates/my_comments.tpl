<img class="y1" src="/templates/mt/img/sl.gif" alt="" /><div class="y2"><a class="c2" href="/tenders/cabinet/">Личный кабинет</a></div>
<img class="y3" src="/templates/mt/img/comment.gif" alt="" /><h1 class="y4">Комментарии и заметки</h1>
<br class="fix"/>

<div class="mt_boxwhite">
    {if $flag==1}<div class="ezii">{else}<a class="ezi0" href="/tenders/my_comments">{/if}<strong>Всё</strong>{if $flag==1}</div>{else}</a>{/if}{if $flag==2}<div class="ezii">{else}<a class="ezi0" href="/tenders/my_comments/?flag=2">{/if}<strong>Комментарии</strong>{if $flag==2}</div>{else}</a>{/if}{if $flag==3}<div class="ezii">{else}<a class="ezi0" href="/tenders/my_comments/?flag=3">{/if}<strong>Заметки</strong>{if $flag==3}</div>{else}</a>{/if}

{*<form action="/tenders/my_comments" method="POST">
    <table style="width:100%"><tr><td><label for="filter">Фильтр по тексту комментария\заметки: </label><input id="filter" name="filter" value="{$filter}" style="border-color: #B2B2B2 #E5E5E5 #E5E5E5 #B2B2B2;border-style: solid;border-width: 1px;height: 28px;margin-right: 10px;margin-top: 1px;padding-left: 8px;"/><button type="submit" style="background: url('/templates/mt/img/send.png') no-repeat scroll center center transparent; height: 27px; width: 110px;"></button>
    <td/>
    <td align=right>Отобразить:
    <select name="flags" onchange="location.href = '/tenders/my_comments?flag=' + this.value" style="width : 220px" class="in">
        <option value="1" {if $flag==1}selected{/if}>комментарии и заметки</option>
        <option value="2" {if $flag==2}selected{/if}>только комментарии</option>
        <option value="3" {if $flag==3}selected{/if}>только заметки</option>
    </select>
    </td>
    </table>
</form>*}
    <br/>
{if $comments}
<table class="view my_comments tbl_sort">
    <thead>
    <tr>
        <th>Текст комментария</th>
        <th class="{literal}{sorter: false}{/literal}">Редактировать</th>
        <th>Номер тендера</th>
        <th class="{literal}{sorter: 'mtShortDate'}{/literal}">Дата добавления</th>
        <th class="{literal}{sorter: fasle}{/literal}">Удалить</th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$comments item=item}
    <!--{$i++}-->
    <tr>
        <td><div id="text{$i}">{$item.text}</div>
        {if $item.edit_flag}
        <div id="text_edit{$i}" style='display:none;'>
        <form action="{$item.editlink}&key={$i}&flag={$flag}&page={$page}" method="post" name="comment_edit_form{$i}">
            <textarea style="width:470px;font-size: 100%;border:1px solid #888888;padding: 0px;font-family: arial;" name="message{$i}" cols="79" rows="4" maxlength="2000" onKeyPress="return isNotMax(event,{$i})" onChange="return isNotMax(event, {$i})">{$item.edittext}</textarea>
            <br/>
            <div id="error_msg{$i}" style='display:none;'><b><font color="#FF0000">Сообщение не может привышать 2000 символов!</font></b></div>
            <br/><center>
                <button id="subm_butt{$i}" name="send" type="submit" style="background: url('/templates/mt/img/save.png') no-repeat scroll center center transparent;height: 32px;width: 145px;"></button>
                <button id="reset{$i}" name="reset" value="Отменить" onclick="closeedit({$i});return false" class="otmena"></button>
                </center>
        </form>
        </div>
        {/if}
        </td>
        <td><center>{if $item.edit_flag}<a href="/tenders/my_comments" onclick="openedit({$i});return false;"><img src="/templates/mt/img/pen.gif" alt="" /></a>{else}<img src="/templates/mt/img/ui-tab.png" title="Редактирование возможно только в течении 30 минут после добавления или изменения" />{/if}</center></td>

        <td><a href="/tenders/detail/{$item.tender_id}">{$item.tender_id}</a></td>
        <td>{$item.date_time}</td>
        <td>
        <center>{if $item.remlink}<a href="{$item.remlink}"><img src="/templates/mt/img/delete1.png" alt="" /></a>{else}<img src="/templates/mt/img/ui-tab.png" title="Удаление коментариев невозможно" />{/if}</center>
         </td>
    </tr>
    {/foreach}
    </tbody>
</table>
{$scrol}
{else}
<br/>
{if $flag == 1}
У вас нет ни комментариев, ни заметок.
{elseif $flag == 2}
У вас нет комментариев.
{else}
У вас нет заметок.
{/if}
{/if}
</div>