{if $subscribe}
<h3>Список запросов вашей подписки</h3>

<div>Вы подписаны на <b>{$total}</b> запросов.</div>
<div>Состояние подписки {if $status==1}<b>Включена</b>&nbsp;<a href="/ajax.php?action=email_newsletter&task=status">Выключить</a>{else}<b>Выключена</b>&nbsp;<a href="/ajax.php?action=email_newsletter&task=status">Включить</a> {/if} </div>

<table class="view email_subscribe tbl_sort {if !$status}email_subscribe_off{/if}" cellspacing="0" cellpadding="0">
    <thead>
        <tr>
            <th>Тема, формирующая рассылку</th>
            <th class="{literal}{sorter: false}{/literal}">Частота</th>
            <th class="{literal}{sorter: false}{/literal}">Кол-во тендеров</th>
            <th class="{literal}{sorter: false}{/literal}">Удалить</th>
        </tr>
    </thead>
    <tbody>
        {foreach from=$subscribe item=search}
        <tr>
            <td><div><a title="перейти на поиск по {$search.main|escape}{if $search.txt}({$search.txt}){/if}" href="{$link}{$search.url}">{$search.main|escape}{if $search.txt}({$search.txt}){/if}</a></div>
                <div style="font-size:10px; color: #aaa">{if $search.datetime}время последней отправки: {$search.datetime|date_format:"%e.%m.%Y %H:%M"}{else}Рассылка по запросу не производилась{/if}</div>
            </td>
            <td>                
                <select name="freq" class="subscribe" id="{$search.search_id}">
                    {*<option value="0" {if $search.freq==0}selected{/if}>Не отправлять</option>*}
                    <option value="1" {if $search.freq==1}selected{/if}>Раз в день</option>
                    <option value="3" {if $search.freq==3}selected{/if}>Каждые 3 дня</option>
                    <option value="7" {if $search.freq==7}selected{/if}>Каждую неделю</option>
                </select>
            </td>
            <td>
                <select name="limit" class="subscribe" id="{$search.search_id}">
                    <option value="10" {if $search.limit==10}selected{/if}>10</option>
                    <option value="25" {if $search.limit==25}selected{/if}>25</option>
                    <option value="50" {if $search.limit==50}selected{/if}>50</option>
                </select>
            </td>
            <td>
                <a href="/ajax.php?action=email_newsletter&task=del&sid={$search.search_id}" title="удалить запрос из подписки" class="delete"><span>удалить запрос из подписки</span></a>
            </td>
        </tr>
        {/foreach}
    </tbody>
</table>

{$scroll}

{else}
<h3>Вы не подписаны ни на один запрос</h3>
{/if}