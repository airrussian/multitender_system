<table class="searchres">
    <tr class="title">
        <td align="left"><b>В качестве кого Вы осуществляете поиск закупок?</b></td>
        <td align="left"><b>Почему Вы в данный момент не пользуетесь multitender.ru в полном объеме (не имеете оплаченного периода)?</b></td>
        <td align="left"><b>Пользовались ли Вы другими сервисами поиска закупок?</b></td>
        <td align="left"><b>Какой тип закупок представляет для Вас интерес?</b></td>
        <td align="left"><b>Интересны ли Вам закупки других регионов, а не только Вашего?</b></td>
    </tr>
{foreach from=$result item=item}
    <tr class="{cycle values='unc,uunc'}">
        <td align="left">{$item.wha_are_you}</td>
        <td align="left">{$item.why_dont_use}</td>
        <td align="left">{$item.other_services}</td>
        <td align="left">{$item.interest}</td>
        <td align="left">{$item.other_region}</td>
    </tr>
{/foreach}
</table>