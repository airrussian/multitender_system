{literal}
<style>
    .add_change {
        display: block;
        height: 25px;
        background: transparent url(/i/images/change_page.png) no-repeat scroll left center;
        padding-left: 25px;
        line-height: 25px;
        margin-top: 10px;
    }
</style>
{/literal}

<div class="pathway">
    <span style="background: transparent url(/templates/mt/img/sl.gif) no-repeat scroll left center; padding-left: 20px;">
        <a href="/tenders/cabinet_moderator">Кабинет модератора</a>
    </span>
</div>

<h1>Отслеживаемые страницы</h1>

<div class="rc-white-head"></div>
<div class="rc-white-cont">
    {if $rights>51}
    <form method="POST" action="index.php?action=pages_changes">
        <label>Модератором:</label>
        <select class="moderator" id="{$item.id}" onchange="this.parentNode.submit();" name="user_id">
            <option value="0">--- всех ---</option>
            {foreach from=$moders item=m}
            <option value="{$m.Id}" {if $m.Id==$user_id}selected{/if}>{$m.name}</option>
            {/foreach}
        </select>
    </form>
    {/if}

    {if $items}
    {$scrol}
    <table width=100%>
        <col width=20%>
        <col>
        <tr>
            <th align="left">На странице</th>
            <th align="left">Появился текст</th>
        </tr>
        {foreach from=$items item=item}
        <tr style="background-color: {cycle values='#FFF,#EEE'}">
            <td>
                <a href="{$item.page_url}">{$item.page_url}</a>
                <a class="add_change" href="index.php?action=page_changes&page_id={$item.id}">Добавить&nbsp;тендеры</a>
                {if $rights>51 && $user_id==0}
                {foreach from=$moders item=m}
                {if $m.Id==$item.user_id}Назначено на: <b>{$m.name}</b>{/if}
                {/foreach}
                {/if}
                <br>Последнее изменение: <b>{$item.last_datetime_change}</b>
            </td>
            <td>{if $item.page_change==1}{$item.cont.chgs}{else}<b><i>изменений нет</i></b>{/if}</td>
        </tr>
        {/foreach}
    </table>
    {$scrol}
    {else}
    <div>нет изменений</div>
    {/if}
</div>
<div class="rc-white-foot"></div>
