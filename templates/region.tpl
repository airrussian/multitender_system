<img class="y1" src="/templates/mt/img/sl.gif" alt=""/><div class="y2"><a class="c2" href="/tenders/by_region">Закупки в регионах России</a></div>
<h1>{$reg_name}</h1>
{if $reg_name!='Алтайский край' && $reg_name!='Амурская область' && $reg_name!='Архангельская область' && $reg_name!='Белгородская область' && $reg_name!='Владимирская область' && $reg_name!='Брянская область'}{* КОСТИКОВ КОСТЫЛЬ SEO TEST *}
{$searchform}
<br class="fix"/>
{/if}

<div class="mt_boxwhite">
    <div class="s6">
        <img src="/images/region/{$reg_id}.gif" alt="{$reg_text}" />
        <p style="margin-top:10px; font-weight:bold; margin-bottom:5px">Экономика региона</p>
        <p>{$reg_text}</p>
    </div>

    {if $reg_name!='Амурская область' && $reg_name!='Архангельская область' && $reg_name!='Владимирская область' && $reg_name!='Брянская область'}{* КОСТИКОВ КОСТЫЛЬ SEO TEST *}
    <div class="s7">
        <div class="u3i"><a href="/tenders/?search[reg][{$reg_id}]=on">Тендеры и госзакупки</a></div>

        <div class="u3i"><a href="/tenders/by_customer/?region_id={$reg_id}">Заказчики региона</a></div>
        <div class="u3i"><a href="/tenders/distributor_list/?region_id={$reg_id}">Поставщики региона</a></div>
        <div class="u3">Новые тендеры</div>
        <p class="fl">Динамика появления новых тендеров и госзаказов в регионе</p>
        {$graph2}
        <div class="u3">Средняя цена контракта</div>

        <p class="fl">Динамика изменения средней цены контракта по тендерам в регионе</p>
        {$graph1}
        <div class="u3"><img src="/templates/mt/img/gr.gif" align="" /> <a href="/tenders/analytics?region_id={$reg_id}">Вся статистика региона</a></div>
    </div>
    {/if}
    <br class="fix"/>
</div>
    
    {literal}
<!-- Яндекс.Директ --><div id="yandex_ad" style="margin-top: 10px"></div><script type="text/javascript">(function(w, d, n, s, t) {w[n] = w[n] || [];w[n].push(function() {Ya.Direct.insertInto(117786, "yandex_ad", {stat_id: 7,site_charset: "utf-8",ad_format: "direct",font_size: 1,type: "horizontal",limit: 3,title_font_size: 3,site_bg_color: "FFFFFF",header_bg_color: "288ED1",title_color: "01A2D7",url_color: "000000",text_color: "000000",hover_color: "EE2653",favicon: true});});t = d.documentElement.firstChild;s = d.createElement("script");s.type = "text/javascript";s.src = "http://an.yandex.ru/system/context.js";s.setAttribute("async", "true");t.insertBefore(s, t.firstChild);})(window, document, "yandex_context_callbacks");</script>    

{/literal}
    
{if $reg_name!='Амурская область' && $reg_name!='Архангельская область' && $reg_name!='Белгородская область' && $reg_name!='Владимирская область' && $reg_name!='Брянская область'}{* КОСТИКОВ КОСТЫЛЬ SEO TEST *}
<br />
<div class="mt_boxwhite">
    <div>Последние тендеры региона <a href="/tenders/?search[reg][{$reg_id}]=on">{$reg_name}</a></div>   
    <table class="view tbl_sort last_tenders" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th class="title">Название</th>
                <th class="date">Начало</th>
                <th class="lastd">Окончание</th>
                <th class="type">Тип&nbsp;тендера</th>
                <th class="price">Сумма</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$reg_last_items item=item}
            <tr>
                <td class="title"><a href="/tenders/detail/{$item.id}" title="{$item.name}">{$item.name}</a></td>
                <td class="date" align="center">{if $item.date}{$item.date}{else}<center>&ndash;</center>{/if}</td>
                <td class="lastd" align="center">{if $item.date_end}{$item.date_end}{else}<center>&ndash;</center>{/if}</td>
                <td class="type" align="left">{if $item.type}{$item.type}{else}<center>&ndash;</center>{/if}</td>
                <td class="price" align="right"><b>{if $item.price neq 0}{$item.price|mt_price}{else}<center>&ndash;</center>{/if}</b></td>
            </tr>
            {/foreach}
        </tbody>
    </table>
       
</div>
<br />
<div class="mt_boxwhite">
    <div>Самые дорогие закупки региона <a href="/tenders/?search[reg][{$reg_id}]=on">{$reg_name}</a></div>   
    <table class="view tbl_sort expen_tends" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th class="title">Название</th>
                <th class="date">Начало</th>
                <th class="lastd">Окончание</th>
                <th class="type">Тип&nbsp;тендера</th>
                <th class="price">Сумма</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$reg_top_price item=item}
            <tr>
                <td class="title"><a href="/tenders/detail/{$item.id}" title="{$item.name}">{$item.name}</a></td>
                <td class="date" align="center">{if $item.date}{$item.date}{else}<center>&ndash;</center>{/if}</td>
                <td class="lastd" align="center">{if $item.date_end}{$item.date_end}{else}<center>&ndash;</center>{/if}</td>
                <td class="type" align="left">{if $item.type}{$item.type}{else}<center>&ndash;</center>{/if}</td>
                <td class="price" align="right"><b>{if $item.price neq 0}{$item.price|mt_price}{else}<center>&ndash;</center>{/if}</b></td>
            </tr>
            {/foreach}
        </tbody>
    </table>
</div>
{/if}