<div id="card_user">
    {if $user}
    <h1>Карточка пользователя <b>ID{$user_id}</b>&nbsp;<a href="/ajax.php?action=users&task=alertlogin&user_id={$user_id}" title="Войти под пользователем ID{$user_id}">Вход</a></h1>

    <div class="mt_boxwhite">

    <table>
        <tr>
            <td valign="top">
                <h3 style="font-weight: bold">Общая информация</h3>
                <table><col width="240px" /><col />
                    <tr><td>Индентификатор (multitender)</td><td>{$user.users.Id}</td></tr>
                    <tr><td>Индентификатор (JoomlaID)</td><td>{$user.joomla.id}</td></tr>
                    <tr><td>Имя пользователя:</td>
                        <td>
                            {if $user.joomla.username}
                            {$user.joomla.username}
                            {else}
                            {if $user.system.user.name}{$user.system.user.name}{/if}
                            {/if}
                        </td></tr>
                    <tr><td>Электронный адрес:</td><td>{$user.joomla.email}</td></tr>
                    <tr><td>Дата регистрации:</td><td>{$user.joomla.registerDate}</td></tr>
                    <tr><td>Последний раз замечен:</td><td>{$user.joomla.lastvisitDate}</td></tr>
                </table>
                <h3 style="font-weight: bold">Дополнительная информация</h3>
                <table><col width="240px" /><col />
                    <tr><td>Права:</td><td>{$user.system.user.rights}</td></tr>
                    <tr><td>Время последний отправки:</td><td>{if $user.system.subscribe.last_send}{$user.system.subscribe.last_send}{else}Отправки не было{/if}</td></tr>
                    <tr><td>Подписан на запросов:</td><td>{$user.system.subscribe.count}</td></tr>
                </table>
                <h3 style="font-weight: bold">Запросы</h3>
                <table><col width="240px" /><col />
                    <tr><td>Всего искал:</td><td>{$user.system.search.count}</td></tr>
                    <tr><td>Запросов в избранном:</td><td>{$user.favorite.search}</td></tr>
                </table>
                <h3 style="font-weight: bold">Платежи</h3>
                <table><col width="240px" /><col />
                    <tr><td>Всего (кол-во, сумма): </td><td><b>{$user.payments.count}</b> / <b>{$user.payments.summa}</b></td></tr>
                    <tr><td>Оплаченные (кол-во, сумма): </td><td><b>{$user.payments.buy.count}</b> / <b>{$user.payments.buy.summa}</b></td></tr>
                </table>
            </td>
            <td valign="top">
                {$firm}
                <h3 style="font-weight: bold">Тендеры</h3>
                <table><col width="240px" /><col />
                    <tr><td>Доступ до:</td><td>{$user.access.todate|tc_date_span}{if $user.access.count_days>=0} (ещё <b>{$user.access.count_days} дней</b>){/if}{if $user.users.Id}<a href="{$link_base}action=card_user&task=setaccess&user_id={$user.users.Id}&p=overworld">до 21.12 .2012</a>&nbsp;<a href="{$link_base}action=card_user&task=setaccess&user_id={$user.users.Id}&p=week">на неделю</a>{/if}</td></tr>
                    <tr><td>Осталось штук:</td><td>{$user.access.tenders}</td></tr>
                    <tr><td>Всего за период:</td><td>{$user.access.count_puchased}</td></tr>
                    <tr><td>В избранном:</td><td>{$user.favorite.tenders}</td></tr>
                    <tr><td>Просмотренных:</td><td>{$user.tenders.count}</td></tr>
                    <tr><td>Купленных:</td><td>{$user.tenders.buy}</td></tr>
                </table>
            </td>
        </tr>
    </table>
    <h3 style="font-weight: bold">Последние платeжи</h3>
    {if $payments}
    <table>
        <tr>
            <th width="30px">№</th>
            <th>Тип оплаты</th>
            <th>Тариф</th>
            <th>Период</th>
            <th>Кол-во</th>
            <th>Сумма</th>
            <th>Дата выставления</th>
            <th>Дата оплаты</th>
            <th>Состояние</th>
        </tr>
        {foreach from=$payments item=pay}
        <tr class="{cycle values='unc,uunc'}" {if $pay.paid>0}style="color: green"{/if}{if $pay.paid<0}style="color: #BBB"{/if}>
            <td align="right">{$pay.id}</td>
            <td align="left">{$pay.type_payment_rus}</td>
            <td align="left">{$pay.kind_rates}</td>
            <td align="left">{if $pay.kind_rates_id==1}{$pay.count*30}&nbsp;дн.{else}&mdash;{/if}</td>
            <td align="left">{if $pay.kind_rates_id==2}{$pay.count}&nbsp;тен.{else}&mdash;{/if}</td>
            <td align="right">{$pay.summa}&nbsp;руб.</td>
            <td align="center">{$pay.datetime}</td>
            <td align="center">{if $pay.datetime_adopted !== '-'}{$pay.datetime_adopted}{else}&mdash;{/if}</td>
            <td align="center">
                {if $pay.paid > 0}
                <div>оплачен</div>
                {/if}
                {if $pay.paid == 0}
                <div>ожидание оплаты</div>
                {/if}
                {if $pay.paid < 0}
                <div>отменён</div>
                {/if}
            </td>
            <td align="center" class="view-bull">
                {if $pay.type_payment=="cashless"}
                {if $pay.paid != -1}
                <a href="/ajax.php?action=billing&task=payment_view&payment_id={$pay.id}">Распечатать</a>
                {else}
                Распечатать
                {/if}
                {/if}
            </td>

        </tr>
        {/foreach}
    </table>
    {else}
    <span>Выставлений не было.</span>
    {/if}

    <h3 style="font-weight: bold">Запросы EMAIL рассылки</h3>
    {if $user.system.subscribe.list}
    <table class="view">
        <thead>
            <tr>
                <th>Запрос</th>
                <th>Частота</th>
                <th>Количество</th>
                <th>Дата обновление</th>
                <th>Последний тендер / Дата публикации</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$user.system.subscribe.list item=item}
            <tr {if !$item.last_tender}style="background-color: #aaa"{/if} >
                <td>{$item.search.main}<br /><i>{$item.search.info}</i></td>
                <td>{$item.freq}</td>
                <td>{$item.limit}</td>
                <td>{$item.datetime}</td>
                <td>{if $item.last_tender}{$item.last_tender.name} от <b>{$item.last_tender.date_publication}</b>{else}<i>По текущему запросу тендеров не найдено</i>{/if}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    {/if}

    <h3 style="font-weight: bold">Последние запросы</h3>
    {if $last_query}
    <table class="view">
        <tr>
            <th>ID</th>
            <th>Запросы</th>
            <th>Дата</th>
            <th>Время</th>
            <th>Кол-во найденых</th>
        </tr>
        {foreach from=$last_query item=lq}
        <tr class="{cycle values='unc,uunc'}">
            <td align="right">{$lq.id}</td>
            <td align="left"><big>{$lq.search.main}</big><br /><small>{$lq.search.info}</small></td>
            <td align="left">{$lq.date}</td>
            <td align="left">{$lq.time}</td>
            <td align="right">{$lq.search.total}</td>
        </tr>
        {/foreach}
    </table>
    {else}
    ещё ничего не искал
    {/if}
    <h3 style="font-weight: bold">Избранные тендеры</h3>
    {if $fav_tender}
    <div>Показано: {$fav_ten_count} / {$user.favorite.tenders}</div>
    <table>
        <tr>
            <th>ID</th>
            <th>Название</th>
        </tr>
        {foreach from=$fav_tender item=ft}
        <tr class="{cycle values='unc,uunc'}">
            <td>{$ft.id}</td>
            <td><a href="{$link_detail}{$ft.id}">{$ft.name}</a></td>
        </tr>
        {/foreach}
    </table>
    {else}
    нет избранных тендеров
    {/if}
    <h3 style="font-weight: bold">Браузеры клиента</h3>
    <i>В порядке использования снизу вверх</i>
    <p>{$user.users.user_agent|nl2br}</p>
    <h3 style="font-weight: bold">IP-адреса клиента</h3>
    <i>В порядке использования снизу вверх</i>
    <p>{$user.users.ip|nl2br}</p>
    {else}
    <h2>Пользователь с номером <b>ID{$user_id}</b> не обнаружен в системе</h2>
    {/if}

    </div>
</div>
