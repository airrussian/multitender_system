<div class="pathway">
    <span style="padding-left: 20px; background: transparent url(/templates/mt/img/sl.gif) no-repeat left center;">
        <a href="/tenders/cabinet">Личный кабинет</a>
    </span>
</div>

<div id="user_tenders_list" class="systems-page">

    <h1 style="background: transparent url('/templates/mt/img/clock.gif') no-repeat left center">История просмотра тендеров</h1>

     <div class="mt_boxwhite ui-tabs">

        <ul class="ui-tabs-nav">
            <li class="ui-state-active"><a href="#tabs1"><span>Просмотренные тендеры</span></a></li>
            <li><a href="/tenders/history_queries"><span>Поисковые запросы</span></a></li>
        </ul>        
        {if $items}
        <table class="view tbl_sort">
            <thead>
                <tr>
                    <th>Название</th>
                    <th class="date {literal}{sorter: 'mtShortDate'}{/literal}">Начало</th>
                    <th class="date {literal}{sorter: 'mtShortDate'}{/literal}">Окончание</th>
                    <th>Тип&nbsp;тендера</th>
                    <th>Регион</th>
                    <th>Сумма</th>
                    {if $WhenBuyShow}<th>Когда приобретен</th>{/if}
                </tr>
            </thead>
            <tbody>
                {foreach from=$items item=item}
                <tr class="t51" onclick="go('{$link_detail}{$item.id}')">
                    <td><a href="{$link_detail}{$item.id}">{$item.name}</a></td>
                    <td align="center" class="t55">{$item.date_publication|tc_date}</td>
                    <td align="center" class="t55">{$item.date_end|tc_date_span}</td>
                    <td align="left" class="t55">{$item.type}</td>
                    <td align="left" class="t55">{$item.region}</td>
                    <td align="right" class="t56">{$item.price|mt_price}</td>
                    {if $WhenBuyShow}<td width=90px>{if $item.buy}{$item.buy}{/if}</td>{/if}
                </tr>
                {/foreach}
            </tbody>
        </table>

        {$scrol}
        {else}
        <b>{$empty}</b>
        {/if}
    </div>
</div>
