<div class="pathway">
    <span style="padding-left: 20px; background: transparent url(/templates/mt/img/sl.gif) no-repeat left center;">
        <a class="c2" href="/tenders/cabinet">Личный кабинет</a>
    </span>
</div>

<div id="favorite_queries" class="systems-page">

    <h1 style="background: transparent url('/templates/mt/img/big_patric.gif') no-repeat left center">Избранные запросы</h1>

    <div class="mt_boxwhite ui-tabs">

        <ul class="ui-tabs-nav">
            <li><a href="/tenders/favorite_tenders"><span>Избранные тендеры</span></a></li>
            <li class="ui-state-active"><a href="#"><span>Избранные запросы</span></a></li>
        </ul>

        {if $items}
        {assign var="empty" value='style="cursor: help;" title="Пока новый запрос, статиситка собирается..."'}
        <table class="view fav_queries tbl_sort" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>Наименование поискового запроса</th>
                    <th>Всего тендеров найдено</th>
                    <th>Новых за 7 дней</th>
                    <th>Новых с <br />{$date_new|tc_date}</th>
                    <th class="{literal}{sorter: false}{/literal}">Удалить</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$items item=item}
                <tr>
                    <td align=left><a href="{$link}{$item.search_url}">{$item.name|escape}</a></td>
                    <td align=right style="cursor: help;" title="дата обновления: {$item.total_update|tc_date}
                        // Статистика за неделю //
                        {foreach from=$item.stat item=total key=date}
                        {$date|tc_date} ~ {$total} //
                        {/foreach}
                        ">{$item.total}</td>
                    <td align=right {if ! $item.total_three}{$empty}{/if}>{$item.total_three|default:'&mdash;'}</td>
                    <td align=right {if ! $item.total_new}{$empty}{/if}>{$item.total_new|default:'&mdash;'}</td>
                    <td align=center><a class="delete" href="/ajax.php?action=favorite_queries&task=del&sid={$item.id}" title="удалить">&nbsp;</a></td>
                </tr>
                {/foreach}
            </tbody>
        </table>
        {$scrol}
        {else}
        <b>нет ни одного фильтра</b>
        {/if}
    </div>   
</div>