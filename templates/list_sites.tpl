<h1 class="y4i">Список площадок<span class="c7"> &ndash; {$total}</span></h1><br/>

<div class="mt_boxwhite">

<table class="view squares" width="100%">
    <thead>
        <tr>
            <th>Площадка</th>
            <th width="20%">Регион</th>
            <th width="100px">Кол-во</th>
        </tr>
    </thead>
    <tbody>
        {foreach from=$sites item=item}
        <tr>
            <td>{if $item.link}<a href="{$item.link}">{/if}{$item.name}{if $item.link}</a>{/if}</td>
            <td>{if $item.region_id}<a href="{$link}search[reg][{$item.region_id}]" title="Все госзакупки региона «{$item.region_name}»">{$item.region_name}</a>{else}Федеральная площадка{/if}</td>
            <td align="right">{$item.item_total}</td>
        </tr>
        {/foreach}
    </tbody>
</table>
{$scrol}
</div>