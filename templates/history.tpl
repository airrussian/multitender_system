<h1>История</h1>

<div class="history tenders" id="tenders">
    <h2>Просмотренные тендеры</h2>
    <div>{$view_tenders}</div>
</div>

<div class="history queries" id="queries">
    <h2>Последние запросы</h2>
    <div>{$last_searches}</div>
</div>

<div style="clear: both"></div>