<table id="sidebar_total" cellspacing="5">
    <tr>
        <td align="right"><b>{$info.tenders.count}</b></td>
        <td align="left">всего тендеров</td>
    </tr>
    <tr>
        <td align="right"><b>{$info.tenders.actual.count}</b></td>
        <td align="left">актуальных, на сумму </td>
    </tr>
    <tr>
        <td align="right"><b>{math equation="round(x)" x=$info.tenders.actual.summa}</b></td>
        <td align="left"> млрд. рублей</td>
    </tr>
    <tr>
        <td align="right"><b>{$info.users.count}</b></td>
        <td align="left">пользователей</td>
    </tr>
    <tr>
        <td align="right"><b>{$info.firms.count}</b></td>
        <td align="left">организаций</td>
    </tr>
    <tr>
        <td align="right"><b>{$info.customers.count}</b></td>
        <td align="left"><a href="tenders/by_customer">госзаказчиков</a></td>
    </tr>
    {*<tr>
        <td align="right"><b>{$info.distributors.count}</b></td>
        <td align="left"><a href="/tenders/distributor_list">поставщиков</a></td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    {*<tr>
        <td align="right"><img src="/i/icons/line_chart.png" alt="Статистика" /></td>
        <td align="left"><a href="/tenders/analytics" title="Статистика вашего региона">Статистика вашего региона</a></td>
    </tr>*}
</table>