{literal}
<style>
    .url_del{
        background: transparent url(/i/images/del.png) no-repeat scroll center center;
        width: 16px;
        height: 16px;
        cursor: pointer;
    }
</style>
{/literal}
<h1>База страниц сайтов</h1>

<div class="mt_boxwhite">
    <div>
        <form action="index.php?action=pages_moder&task=pg_add" method="post" >
            <label>Адрес страницы:</label>
            <input name="url_page" type="input" style="width: 400px; border: 1px solid gray" value="" />
            <input name="submit" type="submit" value="добавить" style="padding: 3px 10px;"/>
        </form>
    </div>

    {if $items}
    {$scrol}
    <table width="80%" class="view">
        <thead>
        <tr>
            <th>URL страниц</th>
            <th colspan="2">Модератор</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$items item=item}
        <tr class="{cycle values='unc,uunc'}">
            <td width="90%" class="url"><a href="{$item.page_url}" targer="_blank">{$item.page_url}</a></td>
            <td>
                <select class="moderator" id="{$item.id}">
                    {foreach from=$moders item=m}
                    <option value="{$m.Id}" {if $m.Id==$item.user_id}selected{/if}>{$m.name}</option>
                    {/foreach}
                </select>

            </td>
            <td width="20px"><div class="url_del" id="{$item.id}" title="удалить"></div></td>
        </tr>
        {/foreach}
        </tbody>
    </table>
    {$scrol}
    {else}
    <h3>нет страниц</h3>
    {/if}

    <div class="debug"></div>
</div>
