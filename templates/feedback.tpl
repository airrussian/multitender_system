<h1>Опрос</h1>
Приглашаем Вас принять участие в специальном опросе для тех, кто уже имеет опыт работы с системой поиска по закупкам Multitender.ru. Вы очень поможете нам совершенствовать сервис, ответив на вопросы ниже:
<form action="{$action_link}" id="anketa" name="anketa" method="post">
<input type="hidden" name="key" value="key">
<p>В качестве кого Вы осуществляете поиск закупок?</p>
<br/><input type="radio" name="who_are_you" value="private"> частное лицо
<br/><input type="radio" name="who_are_you" value="ip"> индивидуальный предприниматель
<br/><input type="radio" name="who_are_you" value="staff"> сотрудник фирмы
<br/><input type="radio" name="who_are_you" value="cheef"> руководитель фирмы

<br/><p>Почему Вы в данный момент не пользуетесь multitender.ru в полном объеме (не имеете оплаченного периода)?</p>
<br/><input type="radio" name="why_dont_use" value="no_needless"> нет необходимости
<br/><input type="radio" name="why_dont_use" value="no_function" onclick="document.getElementById('nofunction').style.display = 'block';document.getElementById('nocomfort').style.display = 'none';document.getElementById('other').style.display = 'none';"> нет нужных функций. Если да, то каких?
<div id='nofunction' style='display:none;'>
<br/><input type="text" name="no_function_answer" value="" maxlength="255">
</div>
<br/><input type="radio" name="why_dont_use" value="no_comfort" onclick="document.getElementById('nofunction').style.display = 'none';document.getElementById('nocomfort').style.display = 'block';document.getElementById('other').style.display = 'none';"> сервис неудобен. Если да, то почему?
<div id='nocomfort' style='display:none;'>
<br/><input type="text" name="no_comfort_answer" value="" maxlength="255">
</div>
<br/><input type="radio" name="why_dont_use" value="other" onclick="document.getElementById('nofunction').style.display = 'none';document.getElementById('nocomfort').style.display = 'none';document.getElementById('other').style.display = 'block';"> другое
<div id='other' style='display:none;'>
<br/><input type="text" name="other_answer" value="" maxlength="255">
</div>

<br/><p>Пользовались ли Вы другими сервисами поиска закупок?</p>
<br/><input type="radio" name="other_services" value="yes_in_past" onclick="document.getElementById('other_serv').style.display = 'none';"> да, ранее приходилось
<br/><input type="radio" name="other_services" value="yes_now"  onclick="document.getElementById('other_serv').style.display = 'block';"> да, пользуюсь сейчас
<div id='other_serv' style='display:none;'>
<br/><input type="text" name="other_serv_answer" value="" maxlength="255">
</div>
<br/><input type="radio" name="other_services" value="no" onclick="document.getElementById('other_serv').style.display = 'none';"> нет

<br/><p>Какой тип закупок представляет для Вас интерес?</p>
<br/><input type="radio" name="interest" value="gos_zacup"> государственные закупки
<br/><input type="radio" name="interest" value="com_zacup"> коммерческие тендеры
<br/><input type="radio" name="interest" value="both"> оба типа
<br/><input type="radio" name="interest" value="no_interest"> закупки мне не интересны

<br/><p>Интересны ли Вам закупки других регионов, а не только Вашего?</p>
<br/><input type="radio" name="other_region" value="yes"> да
<br/><input type="radio" name="other_region" value="yes_with_UIC"> да, и не только других регионов, но и стран СНГ
<br/><input type="radio" name="other_region" value="no"> нет

<br/><input type="submit" value="Отправить"><br/>
</form>
<br/>Спасибо, что помогаете нам стать лучше!