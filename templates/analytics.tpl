<div class="pathway">
    <a href="/tenders/by_region">Закупки в регионах России</a>
    <img alt="" src="/templates/mt/img/sp.gif" />
    <a href="/tenders/{$region_link}">{$regname.name}</a>
</div>

<h1>Статистика по тендерам {$regname.name_rp}</h1>

<div class="mt_boxwhite">
    <h4>Виды тендеров</h4>
    <div>{$graph_type_counts}</div>
    <h4>Участники тендера</h4>
    <div>{$graph_bidders_count}</div>
    <div>Максимальное количество участников (<b>{$max_bidders_count}</b>) было зарегистрировано в тендере «<a href="/tenders/detail/{$max_bidders_id}">{$max_bidders_name}</a>».</div>
    <div>{$graph_ddrop}</div>
    <div>{$graph_rubric_drops}</div>
    <div>Наибольшее падение составило <b>{$max_ddrop}%</b> от начальной максимальной цены в тендере <a href="/tenders/detail/{$max_ddrop_id}">«{$max_ddrop_name}»</a>.</div>
</div>

<div class="mt_boxwhite" style="margin: 5px 0;">
    <h4>Самые дорогие закупки региона <a href="{$region_link}">{$regname.name}</a></h4>
    <table class="view" style="margin: 5px 0;">
        <thead>
            <tr>
                <th>Название</th>
                <th class='price'>Сумма</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$top_price item=item}
            <tr onclick="'/tenders/detail/{$item.id}'">
                <td><a href="/tenders/detail/{$item.id}">{$item.name}</a></td>
                <td align="right" class='price'>{$item.price|mt_price}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <div style="margin-top: 5px;"><a href="/tenders/?search[reg][{$region_id}]"><strong>Все тендеры {$regname.name_rp}</strong></a></div>
</div>

<div class="mt_boxwhite" style="margin: 5px 0;">
    <h4>Крупнейшие заказчики <a href="{$region_link}">{$regname.name_rp}</a></h4>
    <table class="view" style="margin: 5px 0;">
        <thead>
            <tr>
                <th>Название</th>
                <th>Размещенные заказы</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$top_customers item=item}
            <tr onclick="go('/tenders/?search[customer][{$item.customer_id}]')">
                <td><a href="/tenders/?search[customer][{$item.customer_id}]">{$item.name}</a></td>
                <td>{$item.str}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <div style="margin-top: 5px;"><a href="/tenders/by_customer/?region_id={$region_id}"><strong>Все заказчики {$regname.name_rp}</strong></a></div>
</div>

<div class="mt_boxwhite" style="margin: 5px 0;">
    <h4>Крупнейшие поставщики <a href="{$region_link}">{$regname.name_rp}</a></h4>
    <table class="view" style="margin: 5px 0;">
        <thead>
            <tr>
                <th>Название</th>
                <th>Выполненные заказы</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$top_distributor item=item}
            <tr onclick="go('/tenders/distributor_list/?id_distributor={$item.dist_id}')">
                <td><a href="/tenders/distributor_list/?id_distributor={$item.dist_id}">{$item.Name}</a></td>
                <td>{$item.cnt} заказов на сумму более {$item.summa} миллионов рублей.</td>
            </tr>
            {/foreach}
        </tbody></table>
    <div style="margin-top: 5px;"><a href="/tenders/distributor_list/?region_id={$region_id}"><strong>Все поставщики {$regname.name_rp}</strong></a></div>
</div>

{*
Всего <a href="/tenders/by_customer?region_id={$region_id}">{$customers} госзаказчиков</a>, опубликовавших <a href="/tenders/?search[reg][{$region_id}]">{$tenders} тендеров</a> на сумму более {$sum} миллионов рублей.<br />
{$graph_type_counts}<br />
{$graph_bidders_count}<br />
Максимально было зарегистрировано <a href="/tenders/detail/{$max_bidders_id}">{$max_bidders_count}</a> участников в тендере <a href="/tenders/detail/{$max_bidders_id}">«{$max_bidders_name}»</a> .<br /><br />
{$graph_ddrop}<br />
{$graph_rubric_drops}<br />
Наибольшее падение составило <a href="/tenders/detail/{$max_ddrop_id}">{$max_ddrop}%</a> от начальной максимальной цены в тендере <a href="/tenders/detail/{$max_ddrop_id}">«{$max_ddrop_name}»</a>.<br />
<br />
<!--Всего {$total_distributors} поставщиков, выполнивших {$total_contracts} заказов на сумму более {$total_sum} миллионов рублей.
<br/>
{$graph_sum_year}<br/>
<br/>-->
<h3>Пятерка самых дорогих для {$regname} заказов</h3>
{$top_price}
<br />
<h3>Пятерка крупнейших заказчиков для {$regname}</h3>
{$top_customers}
<br />
<h3>Пятерка крупнейших поставщиков для {$regname}</h3>
{foreach from=$top_distributor item=item}
<a href="/tenders/distributor_list?id_distributor={$item.dist_id}">{$item.Name}</a> выполнил {$item.cnt} заказов на сумму более {$item.summa} миллионов рублей.<br/>
{/foreach}
<br />
<a href="/tenders/by_customer&region_id={$region_id}">Все заказчики {$regname}</a><br />
<a href="/tenders/?search[reg][{$region_id}]">Все тендеры {$regname}</a><br/>
<a href="/tenders/distributor_list?region_id={$region_id}">Все поставщики {$regname}</a>
*}