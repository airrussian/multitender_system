{*<link rel="stylesheet" href="/design/css/template.css" type="text/css" />*}

<h1>Тендеры и закупки{if !$mtime}: итоги торгов{/if}</h1>

{$form}

<div class="searcher-info">
    {if !$mtime}<span>На этой странице представлены результаты завершившихся торгов. Протоколы вы можете посмотреть на странице тендера.</span><br /><br />{/if}
    {if $mtime}<span style="display: inline-block; width: 200px;">Найдено: {$total}&nbsp;<small>(за {$mtime} сек.)</small></span>{/if}
    
    {if $user.id>0 && $user.type!='bot' && $id}
        {$fav_query}
        {$email_new}
    {/if}
    <a href="{$link_feed}" class="search-rss" title="Отслеживать результаты по средствам RSS">Отслеживать результаты</a>
   
</div>

<!-- <a href='http://www.rss2email.ru/?rss={$link_feed_encode}'>e-mail</a>. -->

<div class="mt_boxwhite">
{$list}
{$scrol}

{if $related_view}
<table id="related">
    <tr>
        {if $related_search}<td width="50%" valign="top">{$related_search}</td>{/if}
        {if $related_rubric}<td width="50%" valign="top">{$related_rubric}</td>{/if}
    </tr>
</table>
{/if}

</div>