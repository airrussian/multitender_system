<div id="freesubscribe" class="right-block" style="background-color: #daf2ff; padding: 20px;">
    <h3>Бесплатные рассылки</h3>
    <ol style="list-style: none; padding: 0; margin: 0">
        {foreach from=$freesearch key=key item=search}
            <li class="{if $search.status===true}del{else}add{/if}"><a href="/tenders/subscribe?task=freenewsletter&query={$key}&opt={if $search.status===true}del{else}add{/if}" onclick="yaCounter10080145.reachGoal('right_subscribe'); return true;">{$search.name}</a></li>
            {/foreach}
    </ol>
</div>

{literal}
    <style>
        #freesubscribe .add {
            background: transparent url('/images/mail_add.png') no-repeat left center;
            padding: 5px 0 5px 31px;
        }
        #freesubscribe .del {
            background: transparent url('/images/mail_check.png') no-repeat left center;
            padding: 5px 0 5px 31px;
        }
    </style>

    <script>
        jQuery("#freesubscribe ol li").click(function () {
            var cl = $(this).attr('class');
            if (cl == 'add') {
                if (!confirm("Добавить запрос в рассылку?")) {
                    return false;
                }
            } else {
                if (!confirm("Удалить из подписки?")) {
                    return false;
                }
            }
        });
    </script>
{/literal}
