<img class="y1" src="/templates/mt/img/sl.gif" alt="" /><div class="y2"><a class="c2" href="/tenders/distributor_list">Поставщики по регионам России</a></div>
<h1 class="y4i">Поставщики {$region.name_rp}<span class="c7"> &ndash; {$total}</span></h1><br/>

<div class="mt_boxwhite">
    <div class="o1" style="float: none">
        <div>Найдите заказчика по названию или ИНН</div>
        <form action="/tenders/distributor_list/?region_id={$region.id}" method="POST">
        <div class="s1"><input class="in in12"  name="filter" value="{$filter}" type="text"/><button class="iskat" type="submit"></button></div></form>
        <br/><br/><br/>{if $compl}<a href="/tenders/distributor_list?region_id={$region.id}">Отобразить всех поставщиков данного региона</a>{else}<a href="/tenders/distributor_list?region_id={$region.id}&complaints=only">Отобразить только поставщиков, находящихся в "Реестре Недобросовестных Поставщиков"</a>{/if}<br/><br/>
        {if $distributors}
        <table class="view distributors" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>Названия</th>
                    <th>ИНН</th>
                    <th>Количество контрактов</th>
                    <th>Количество жалоб</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$distributors item=item}
                <tr  onclick="go('{$item.link}');">
                    <td><a href="{$item.link}" title="Все контракты заказчика: «{$item.Name|escape}»">{if $item.Name}{$item.Name}{else}&ndash;{/if}</a></td>
                    <td><center>{$item.INN}</center></td>
                    <td><center><b>{if $item.count_items}{$item.count_items}{else}&ndash;{/if}</b></center></td>
                    <td><center><b>{if $item.qu_of_complaints}{$item.qu_of_complaints}{else}&ndash;{/if}</b></center></td>
                </tr>
                {/foreach}
            </tbody>
        </table>
        {$scrol}
        {else}
            {if $smarty.post.filter}
            <b>По данному фильтру: "{$filter}" не найдено поставщиков</b>
            {else}
            <b>Не обнаружено поставщиков для {$region.name_rp}</b>
            {/if}
        {/if}
    </div>
</div>