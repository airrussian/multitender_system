{if $list}
    <h3>Список запросов заказчиков за которыми вы следите</h3>
    
    <div>Состояние подписки {if $status==1}<b>Включена</b>&nbsp;<a href="/tenders/subscribe_customer?task=status&status=0">Выключить</a>{else}<b>Выключена</b>&nbsp;<a href="/tenders/subscribe_customer?task=status&status=1">Включить</a> {/if} </div>

    <form action="/tenders/subscribe_customer" id="addcustomer">    
        <input type="hidden" name="task" value="add" />
        <input type="text" maxlength="13" name="customer_inn" />
        <input type="submit" value="Добавить заказчика" />
    </form>

    {literal}
        <script>
            jQuery(document).ready(function () {
                jQuery("form#addcustomer").submit(function () {
                    alert(jQuery(this).serilize());
                    return false;
                });
            });
        </script>
    {/literal}

    <table class="view email_subscribe tbl_sort {if !$status}email_subscribe_off{/if}" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th>Заказчик</th>
                <th class="{literal}{sorter: false}{/literal}">Частота</th>
                <th class="{literal}{sorter: false}{/literal}">Удалить</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$list item=item}
                <tr>
                    <td>
                        <div>Название: {$item.customer_name}</div>
                        <div>ИНН: {$item.customer_inn}</div>
                        <div style="font-size:10px; color: #aaa">{if $search.datetime}время последней отправки: {$search.datetime|date_format:"%e.%m.%Y %H:%M"}{else}Рассылка по запросу не производилась{/if}</div>
                    </td>
                    <td>                
                        <select name="freq" class="subscribe" id="{$search.search_id}">
                            {*<option value="0" {if $search.freq==0}selected{/if}>Не отправлять</option>*}
                            <option value="1" {if $search.freq==1}selected{/if}>Раз в день</option>
                            <option value="3" {if $search.freq==3}selected{/if}>Каждые 3 дня</option>
                            <option value="7" {if $search.freq==7}selected{/if}>Каждую неделю</option>
                        </select>
                    </td>
                    <td>
                        <a href="/ajax.php?action=subscribe_customer&task=del&customer_inn={$item.customer_inn}" title="удалить запрос из подписки" class="delete"><span>удалить запрос из подписки</span></a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>

    {$scroll}

{else}
    <h3>Вы не пока не добавили не одно заказчика для отслеживания появления тендеров</h3>
{/if}