<div id='tab4' class="tabs" title="Заявка на кредит на исполнение госконтракта">
    <b>Заявка на кредит на исполнение госконтракта</b><br>
    {if !$send}   
    <form  name='form_mail4' action="/tenders/gosexpress?page={$smarty.get.page}" method="post" id="cForm">
           <input type="hidden" name="act" value="y4" />

           {if $messageError}<div class="error">{$messageError}</div>{/if}
           
        <p><label class="lbl">Наименование организации</label>
            <input id="name_org1" class="textbox" name="name_org1" type="text" value="{if $data.name_org1}{$data.name_org1}{/if}"></p>
        <p><label class="lbl">Сумма запрашиваемого кредита</label>
            <input id="summa_z1" class="textbox" name="summa_z1" type="text" value="{if $data.summa_z1}{$data.summa_z1}{/if}"></p>
        <p><label class="lbl">Срок кредита</label>
            <input id="srok_k1" class="textbox" name="srok_k1" type="text" value="{if $data.srok_k1}{$data.srok_k1}{/if}"></p>
        <p><label class="lbl">Сумма по госконтракту</label>
            <input id="summa_g1" class="textbox" name="summa_g1" type="text" value="{if $data.summa_g1}{$data.summa_g1}{/if}"></p>
        <p><label class="lbl">Срок госконтракта</label>
            <input id="srok_g1" class="textbox" name="srok_g1" type="text" value="{if $data.srok_g1}{$data.srok_g1}{/if}"></p>
        <p><label class="lbl">Контактное лицо<strong>*</strong></label>
            <input id="fio1" class="textbox" name="fio1" type="text" value="{if $data.fio1}{$data.fio1}{/if}"></p>
        <p><label class="lbl">Контактный телефон<strong>*</strong></label>
            <input id="tel1" class="textbox" name="tel1" type="text" value="{if $data.tel1}{$data.tel1}{/if}"></p>
        <p><label class="lbl">E-mail</label>
            <input id="email1" class="textbox" name="email1" type="text" value="{if $data.email1}{$data.email1}{/if}"></p>
        <p><label>Дополнительная информация</label><br />
            <textarea name="dop_info1" rows="" cols="">{if $data.dop_info1}{$data.dop_info1}{/if}</textarea></p>
        <br>
        <p><label><br></label><input class="button" type="submit" value="Отправить"></p>

        <div id="footer2">Поля, отмеченные <strong>*</strong>, обязательны к заполнению.</div>
    </form>
    {else}
    <p>Ваше сообщение успешно отправлено</p>
    {/if}
</div>