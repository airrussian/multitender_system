<?xml version="1.0" encoding="utf-8"?>
<Organization xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>{$data.Id}</Id>
  <Created>{$data.Created}</Created>
  <ServiceCenterId>{$data.ServiceCenterId}</ServiceCenterId>
  <Name>{$data.organization.name}</Name>
  <Inn>{$data.organization.INN}</Inn>
  {if $data.organization.KPP}<Kpp>{$data.organization.KPP}</Kpp>{/if}

  <RegionCode>{$RegionCode}</RegionCode> 
  <City>{$City}</City>

  <Comment>{$Comment}</Comment>

  <Persons>
    <Person>
      <Id>{$data.organization.personal.Id}</Id>
      <LastName>{$data.organization.personal.LastName}</LastName>
      <FirstName>{$data.organization.personal.FirstName}</FirstName>
      <IsNotResident>false</IsNotResident>
      <Email>{$data.organization.email}</Email>
      <Phone>{$data.organization.telephone}</Phone>
      <CryptoProInstalled>false</CryptoProInstalled>
      <IsDirector>false</IsDirector>
      <IsContactPerson>true</IsContactPerson>
      <RequestCertificate>false</RequestCertificate>
    </Person>
  </Persons>

  <ProductID>4f4028de-c960-4d7f-b6b9-402bd0668673</ProductID>
</Organization>