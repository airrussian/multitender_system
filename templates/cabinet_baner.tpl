<h1 class="y4y">Управление банерами</h1><br class="fix"/>

<div class='mt_boxwhite'>
    <div id="add_window">
        <h2><a href="/" onclick="jQuery('#doflag').val('add');jQuery('#banerlink').attr('disabled','');jQuery('#add_baner').css('display','block');jQuery('#banerlink').val('');jQuery('#region').val('0');jQuery('#type').val('0');jQuery('#onlymain').attr('checked', '');jQuery('#link').val('');jQuery('#published').attr('checked', 'checked');jQuery('#our').val('1');return false;">Добавить банер</a></h2>
        <div id="add_baner" style="display:none;">
            <form name="add" action="/tenders/cabinet_baner" method="POST">
                <b>Ссылка на банер:</b><br/>
                <input id="banerlink" name="pathway" type="text" style="border-color: #B2B2B2 #E5E5E5 #E5E5E5 #B2B2B2;border-style: solid;border-width: 1px;height: 22px;margin-right: 10px;margin-top: 1px;padding: 2px 0 2px 8px;"/><br/>
                <br/><b>Регион:</b><br/>
                <select id="region" name="region" style="border-color: #B2B2B2 #E5E5E5 #E5E5E5 #B2B2B2;border-style: solid;border-width: 1px;height: 22px;margin-right: 10px;margin-top: 1px;padding: 2px 0 2px 8px;">
                    <option value="0" selected>Для всех регионов</option>
                    {foreach from=$regions item=item}
                    {if $item.const!=0}<option value="{$item.const}">{$item.name}</option>{/if}
                    {/foreach}
                </select><br/>
                <br/><b>Тип банера:</b><br/>
                <select id="type" name="type" style="border-color: #B2B2B2 #E5E5E5 #E5E5E5 #B2B2B2;border-style: solid;border-width: 1px;height: 22px;margin-right: 10px;margin-top: 1px;padding: 2px 0 2px 8px;">
                    <option value="0" selected>950x120</option>
                    <option value="1">468x120(слева)</option>
                    <option value="2">468x120(справа)</option>
                    <option value="3">195x500</option>
                    <option value="4">195x235(сверху)</option>
                    <option value="5">195x235(снизу)</option>
                </select><br/>
                <br/><input id="onlymain" type="checkbox" name="onlymain" value="1"/>отображать только на главной<br/>
                <br/><b>Ссылка на рекламодателя:</b><br/>
                <input id="link" type="text" name="link" style="border-color: #B2B2B2 #E5E5E5 #E5E5E5 #B2B2B2;border-style: solid;border-width: 1px;height: 22px;margin-right: 10px;margin-top: 1px;padding: 2px 0 2px 8px;"/><br/>
                <br/><input id="published" type="checkbox" name="published" value="1" checked/>опубликовать<br/>
                <br/><b>Частота показа банера:</b><br/>
                <br/><input id="freq" type="text" name="freq" value="1" style="border-color: #B2B2B2 #E5E5E5 #E5E5E5 #B2B2B2;border-style: solid;border-width: 1px;height: 22px;margin-right: 10px;margin-top: 1px;padding: 2px 0 2px 8px;"/><br/>
                <br/><button onclick="this.form.submit();" type="button" class="save2"></button>
                <input type="hidden" name="do" value="add" id="doflag"/>
                <input type="hidden" name="id" id="id"/>
            </form>
            <br/><br/><a href="/" onclick="jQuery('#add_baner').css('display','none');return false;">Скрыть</a>
        </div>
    </div>

    <div id="direct">
        <h2><a href="/" onclick="jQuery('#editdirect').css('display', 'block');return false;">Директ</a></h2>
        <div id="editdirect" style="display:none;">
            <form action="/tenders/cabinet_baner" method="POST">
                Директ 468x120:<br/>
                <textarea name="468x120" style="border-color: #B2B2B2 #E5E5E5 #E5E5E5 #B2B2B2;border-style: solid;border-width: 1px;height: 120px;width:300px;margin-right: 10px;margin-top: 1px;padding: 2px 0 2px 8px;">{$direct1}</textarea><br/>
                <br/>Директ 950x120:<br/>
                <textarea name="950x120" style="border-color: #B2B2B2 #E5E5E5 #E5E5E5 #B2B2B2;border-style: solid;border-width: 1px;height: 120px;width:300px;margin-right: 10px;margin-top: 1px;padding: 2px 0 2px 8px;">{$direct2}</textarea><br/>
                <br/>Директ 195x500:<br/>
                <textarea name="195x500" style="border-color: #B2B2B2 #E5E5E5 #E5E5E5 #B2B2B2;border-style: solid;border-width: 1px;height: 120px;width:300px;margin-right: 10px;margin-top: 1px;padding: 2px 0 2px 8px;">{$direct3}</textarea><br/>
                <br/>Директ 195x235:<br/>
                <textarea name="195x235" style="border-color: #B2B2B2 #E5E5E5 #E5E5E5 #B2B2B2;border-style: solid;border-width: 1px;height: 120px;width:300px;margin-right: 10px;margin-top: 1px;padding: 2px 0 2px 8px;">{$direct4}</textarea><br/>
                <br/><button onclick="this.form.submit();" type="button" class="save2"></button>
                <input type="hidden" value="direct" name="do"/>
            </form>

            
            <br/><br/><a href="/" onclick="jQuery('#editdirect').css('display','none');return false;">Скрыть</a>
        </div>
    </div>

     <div id="directfreq">
        <h2><a href="/" onclick="jQuery('#editdirectfreq').css('display', 'block');return false;">Частота директа</a></h2>
        <div id="editdirectfreq" style="display:none;">
            <form action="/tenders/cabinet_baner" method="POST">
            <br/><b>Частота директа:</b><br/>
            <table>
            <tr><td>468x120(слева)</td>
                <td><input type="text" name="hleft" value="{$dfreq.hleft}" style="border-color: #B2B2B2 #E5E5E5 #E5E5E5 #B2B2B2;border-style: solid;border-width: 1px;height: 22px;margin-right: 10px;margin-top: 1px;padding: 2px 0 2px 8px;"/></td>
            </tr>
            <tr>
            <td>468x120(справа)</td>
                <td><input type="text" name="hright" value="{$dfreq.hright}" style="border-color: #B2B2B2 #E5E5E5 #E5E5E5 #B2B2B2;border-style: solid;border-width: 1px;height: 22px;margin-right: 10px;margin-top: 1px;padding: 2px 0 2px 8px;"/></td>
            </tr>
            <tr>
            <td>950x120</td>
                <td><input type="text" name="hwide" value="{$dfreq.hwide}" style="border-color: #B2B2B2 #E5E5E5 #E5E5E5 #B2B2B2;border-style: solid;border-width: 1px;height: 22px;margin-right: 10px;margin-top: 1px;padding: 2px 0 2px 8px;"/></td>
            </tr>
            <tr><td>195x235(сверху)</td>
                <td><input type="text" name="vtop" value="{$dfreq.vtop}" style="border-color: #B2B2B2 #E5E5E5 #E5E5E5 #B2B2B2;border-style: solid;border-width: 1px;height: 22px;margin-right: 10px;margin-top: 1px;padding: 2px 0 2px 8px;"/></td></tr>
            <tr><td>195x235(снизу)</td>
                <td><input type="text" name="vbot" value="{$dfreq.vbot}" style="border-color: #B2B2B2 #E5E5E5 #E5E5E5 #B2B2B2;border-style: solid;border-width: 1px;height: 22px;margin-right: 10px;margin-top: 1px;padding: 2px 0 2px 8px;"/></td></tr>
            <tr><td>195x500</td>
                <td><input type="text" name="vwide" value="{$dfreq.vwide}" style="border-color: #B2B2B2 #E5E5E5 #E5E5E5 #B2B2B2;border-style: solid;border-width: 1px;height: 22px;margin-right: 10px;margin-top: 1px;padding: 2px 0 2px 8px;"/></td>
                </tr>
            </table>
                <input type="hidden" value="directfreq" name="do"/>
                <br/><button type="submit" class="save2"></button>
            </form>
            <br/><br/><a href="/" onclick="jQuery('#editdirectfreq').css('display','none');return false;">Скрыть</a>
         </div>
        </div>

{if $list}
<table class="view" style="width:800px">
    <tr class="uunc">
        <td align="center"><b>Путь к банеру</b></td>
        <td align="center"><b>Регион</b></td>
        <td align="center"><b>Тип</b></td>
        <td align="center"><b>Отображать только на главной</b></td>
        <td align="center"><b>Ссылка</b></td>
        <td align="center"><b>Опубликован</b></td>
        <td align="center"><b>Частота</b></td>
        <td align="center"><b>Удалить</b></td>
        <td align="center"><b>Редактировать</b></td>
        <td align="center"><b>Статистика</b></td>
    </tr>
    {foreach from=$list item=item}
    <tr class="{cycle values='unc,uunc'}">
        <td><a href="{$item.pathway}" target="blank">{$item.pathway}</a></td>
        <td align="center">{$item.regionname}</td>
        <td align="center">{if $item.type==0}950x120{/if}{if $item.type==1}468x120(слева){/if}{if $item.type==2}468x120(справа){/if}{if $item.type==3}195x500{/if}{if $item.type==4}195x235(сверху){/if}{if $item.type==5}195x235(снизу){/if}</td>
        <td align="center">{if $item.onlymain==1}Да{else}Нет{/if}</td>
        <td>{$item.link}</td>
        <td align="center">{if $item.published == 1}Да{else}Нет{/if}</td>
        <td align="center">{$item.freq}</td>
        <td align="center"><a href="/tenders/cabinet_baner?do=remove&id={$item.id}">Удалить</a></td>
        <td align="center"><a href="/" onclick="jQuery('#doflag').val('edit');jQuery('#freq').val('{$item.freq}');jQuery('#banerlink').attr('disabled','disabled');jQuery('#id').val('{$item.id}');jQuery('#add_baner').css('display','block');jQuery('#banerlink').val('{$item.pathway}');jQuery('#region').val('{$item.region}');jQuery('#type').val('{$item.type}');{if $item.onlymain}jQuery('#onlymain').attr('checked', 'checked');{else}jQuery('#onlymain').attr('checked', '');{/if}jQuery('#link').val('{$item.link}');{if $item.published}jQuery('#published').attr('checked', 'checked');{else}jQuery('#published').attr('checked', '');{/if}return false;">Редактировать</a></td>
        <td align="center"><a href="/tenders/cabinet_baner_stat?id={$item.id}">Статистика</a></td>
    </tr>
    {/foreach}
</table>
{/if}

</div>