{literal}
<style type="text/css"  >
    #statistics_loaders .disable td {
        background-color: #EEE;
        color: #999;
    }
    #statistics_loaders .disable td a {
        color: #99F;
    }

    #statistics_loaders .error td {
        font-weight: bold;
    }
    #all_statics { font-size: 14px; margin: 10px 0; padding: 5px; border: 1px solid #CCC; }
    #all_statics label { width: 150px; display: inline-block; }
</style>
{/literal}

<h1>Статистика по сайтам</h1>

{include file="statistics_menu.tpl"}

<div class="rc-white-head"></div>
<div class="rc-white-cont">

    <div id="all_statics">
        <div><label>За историю:</label><b>{$total.count_all}</b></div>
        <div><label>За 7 дней:</label><b>{$total.count_week}</b></div>
        <div><label>За 3 дня:</label><b>{$total.count_day3}</b></div>
        <div><label>За сутки:</label><b>{$total.count_day}</b></div>
        <div><label>За 3 часа:</label><b>{$total.count_hour3}</b></div>
    </div>

    <div>
        <a href="#" onclick="javascript: $('#statistics_loaders').find('tr.disable').hide(); $(this).hide(); $('#statisctics-loader-show-disable').show(); return false;" id="statisctics-loader-hide-disable">Скрыть disabled</a>
        <a href="#" style="display: none" onclick="javascript: $('#statistics_loaders').find('tr.disable').show(); $(this).hide(); $('#statisctics-loader-hide-disable').show(); return false;" id="statisctics-loader-show-disable">Показать disabled</a>
        <span>|</span>
        <a href="#" onclick="javascript: $('#statistics_loaders').find('tr.error').hide(); $(this).hide(); $('#statisctics-loader-show-error').show(); return false;" id="statisctics-loader-hide-error">Скрыть с ошибками</a>
        <a href="#" style="display: none" onclick="javascript: $('#statistics_loaders').find('tr.error').show(); $(this).hide(); $('#statisctics-loader-hide-error').show(); return false;" id="statisctics-loader-show-error">Показать с ошибками</a>
    </div>

    <table class="view tbl_sort" id="statistics_loaders">
        <thead>
            <tr>
                <th class="{literal}{sorter: false}{/literal}">#</th>
                <th>URL</th>
                <th>Регион</th>
                <th>ПРОВЕРКА</th>
                <th class="{literal}{sorter: 'price'}{/literal}">Ошибки</th>
                <th>Всего</th>
                <th>За неделю</th>
                <th>3 Дня</th>
                <th>День</th>
                <th>3 Часа</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$items item=site}
            <tr class="{if $site.disable} disable {/if}{if ! ($site.count_week || $site.disable)} error {/if}" >
                <td>{counter}</td>
                <td>
                    <div>
                        <a href="{$site.url}">{$site.name}</a><br />
                        <a href="/tenders/?action=statistics&id={$site.id}">Подробнее</a><br />
                        <a href="/tenders/?search[site][{$site.id}]">Загруженные</a>
                    </div>
                    <span {if ! ($site.count_week || $site.disable)}style="font-weight: bold; text-decoration: blink; background-color: #000;
                        color: #FFF; cursor: help;" title="Нет тендеров неделю!!"
                        {elseif $site.disable}style="font-style: italic; text-decoration: line-through; cursor: help;" title="Отключена" {/if}> [{$site.id}]</span>
                    {if $site.t_comment}
                    <br />
                    {$site.t_comment}
                    {/if}
                </td>
                <td>{$site.region_name}{if $site.location}<b>/ {$site.location}</b>{/if}</td>
                <td>
                    {$site.recheck}
                </td>
                <td>
                    {if $site.error_counter > 5}
                    <b>{$site.error_counter}</b>
                    {else}
                    <i>{$site.error_counter}</i>
                    {/if}
                </td>
                <td align="right">{$site.count_all|default:"&mdash;"}</td>
                <td align="right">{$site.count_week|default:"&mdash;"}</td>
                <td align="right">{$site.count_day3|default:"&mdash;"}</td>
                <td align="right">{$site.count_day|default:"&mdash;"}</td>
                <td align="right">{$site.count_hour3|default:"&mdash;"}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
</div>
<div class="rc-white-foot"></div>