<div class="y2" style="float:left"><a class="c2" href="/tenders/by_customer">госзаказчики по регионам России</a>&nbsp;<br class="fix"/></div>
<img alt="" src="templates/mt/img/sl.gif" class="y1"/>
<div class="y2">Новые заказчики<br class="fix"/></div>
<h1 class="y4y2">Новые заказчики</h1><br class="fix"/>

<div class="mt_boxwhite">
<table class="view new_customer tbl_sort">
    <thead>
        <tr>
            <th width="62%">Заказчик</th>
            <th width="24%">Регион</th>
            <th class="{literal}{sorter: 'mtShortDate'}{/literal}">Дата (MSK)</th>
        </tr>
    </thead>
    <tbody>
        {foreach from=$items item=item}
        <tr class="{cycle values='even,odd'}" >
            <td class="name"><a href="{$conf.pref.link_base}search[customer][{$item.id}]" title="Все тендеры заказчика «{$item.name}»">{$item.name}</a></td>
            <td>
                {if $item.region_id}
                <a href="{$conf.pref.link_base}search[reg][{$item.region_id}]" title="Все тендеры региона «{$item.region_name}»">{$item.region_name|default:"&mdash;"}</a>
                {else}
                {$item.region_name|default:"&mdash;"}
                {/if}
            </td>
            <td>{$item.datetime|tc_date}</td>
        </tr>
        {/foreach}
    </tbody>
</table>
</div>
