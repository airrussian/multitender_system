<div id="search_form">
    <div class="sf_tabs" id="sf_tabs_simple_current">Простой поиск</div>
    <div class="sf_tabs" id="sf_tabs_ext">Расширенный поиск</div>
    {*<div class="sf_tabs" style="left: 300px;"><a href="/tenders/end_tenders/">Итоги торгов</a></div> *}
    <div class="sf_tabs" style="left: 300px;"><a href="/tenders/rubrics">Классификатор отраслей</a></div>

    <form method="get" action="{$action}" {$target}>

        <div id="sf_simple">
            <div id="sf_string" style="{if $items}position:relative;{/if}">
                {if $items}
                    <div id="sf_fq_button" onclick="javascript: $('#sf_favorite_queries').toggle();">
                        <div id="sf_favorite_queries">
                            <div class="title">Избранные запросы</div>
                            {foreach from=$items item=item}
                                <div class="item">
                                    <a href="{$action}{$item.search_url}">{$item.name|escape}</a><br />
                                    <small>дата обновления: {$item.total_update|tc_date}</small>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                {/if}
                <input type="text" name="search[main]" value='{$search.main}' id="smain" style="{if $items}width: 705px; padding-right: 32px;{/if}" />
                <input class='button' type="submit" value="Искать тендеры" />
            </div>
            <div id="sf_inform">
                <!--<span id="sf_searchin">
                    <input type="checkbox" name="search[myregion]" />
                    <label>Искать в Красноярском крае</label>
                </span>-->
                <span id="sf_helpsearch"><a href="/help-video#poisk">Как искать?</a></span>
                <span id="sf_examle">{$info}</span>
            </div>
            {* *** Выводим параметры поиска *** *}
            {*
            {if $search_info}
                <div id="sf_infosearch">
                    <div style="font-weight: bold;">Вы ищете:</div>
                    {if $search_info.reg}<div><span>Регионы</span>: {$search_info.reg}</div>{/if}
                    {if $search_info.type}<div><span>Типы</span>: {$search_info.type.list}</div>{/if}
                    {if $search_info.rubric}<div><span>Рубрики</span>: {$search_info.rubric.list}</div>{/if}
                    {if $search_info.other}<div><span>Дополнительные параметры</span>: {$search_info.other.list}</div>{/if}
                </div>
            {/if} *}
        </div>

        <div id="sf_ext">
            <div id="sf_location" class="col">
            <div class="hx"></div>
            {foreach from=$location item=country}
                <div class="sf_country{if !$pagereg && $country.open==1} open{else} close{/if}">
                <input type="checkbox" id="sf_country_{$country.id}" {if $country.id>1}name="search[reg][{$country.id*100}]"{/if} {if $country.on}checked="checked"{/if} /><label>{$country.name}</label>
                <div>
                    {if $country.okr}
                        {foreach from=$country.okr item=okr}
                            <div class="sf_okr{if !$pagereg}{if $okr.open==1} open{else} close{/if}{/if}">
                                <input type="checkbox" id="sf_okr_{$okr.id}" name="search[okr][{$okr.id}]" {if $okr.on}checked="checked"{/if} />
                                <label>{$okr.name}</label>
                                <div class="sf_regions">
                                    {foreach from=$okr.regs item=reg}
                                        <div class="sf_reg">
                                            <input type="checkbox" id="sf_reg_{$reg.id}" name="search[reg][{$reg.id}]" {if $reg.on}checked="checked"{/if} />
                                            <label for="sf_reg_{$reg.id}">{$reg.sname}</label>
                                        </div>
                                    {/foreach}
                                </div>
                            </div>
                        {/foreach}
                    {else}
                        {foreach from=$country.regs item=reg}
                            <div class="sf_reg">
                                <input type="checkbox" id="sf_reg_{$reg.id}" name="search[reg][{$reg.id}]" {if $reg.on}checked="checked"{/if} />
                                <label for="sf_reg_{$reg.id}">{$reg.sname}</label>
                            </div>
                        {/foreach}
                    {/if}
                </div>
            </div>
        {/foreach}
    </div>

    <div id="sf_types" class="col">
        <div class="hx">Тип тендера</div>
        <div class="sf_types_all">
            <input name="search[type][gozall]" class="form_types_all" type="checkbox" id="sf_types_all" {$form_types_all} /><label for="sf_types_all">Все типы гос. закупок</label>
            <div>
                {foreach from=$type item=type}
                    {if $type.id!=50}
                    <div class="sf_type">
                        <input type="checkbox" name="search[type][{$type.id}]" id="sf_types_{$type.id}" {if $type.on}checked="checked"{/if} />
                        <label for="sf_types_{$type.id}">{$type.name}</label>
                        {if $type.id==6}
                            <div id="sf_etps" style="margin-left: 10px;">
                                <div class='sf_etp'><input type="checkbox" name="search[site][1100101000]" id="roseltorg" /><label for="roseltorg">www.roseltorg.ru</label></div>
                                <div class='sf_etp'><input type="checkbox" name="search[site][1100201000]" id="sberbank" /><label for="sberbank">www.sberbank-ast.ru</label></div>
                                <div class='sf_etp'><input type="checkbox" name="search[site][1100301000]" id="zakazrf" /><label for="zakazrf">www.zakazrf.ru</label></div>
                                <div class='sf_etp'><input type="checkbox" name="search[site][1100401000]" id="micex" /><label for="micex">www.etp-micex.ru</label></div>
                                <div class='sf_etp'><input type="checkbox" name="search[site][1100501000]" id="rts" /><label for="rts">www.rts-tender.ru</label></div>
                            </div>
                        {/if}
                    </div>       
                    {/if}
                {/foreach}                
            </div>
        </div>
        <div>
            <input type="checkbox" id="sf_type_50" name="search[type][50]" /><label for="sf_type_50">ФЗ-223</label>
        </div>
        <div>
            <input class="form_types_com" type="checkbox" id="sf_types_com" name="search[type][100]" {$form_type_com} /><label for="sf_types_com">Коммерческие закупки</label>
        </div>
    </div>
    <div id="sf_other" class="col">
        <div id="sf_price">
            <div class="hx">Максимальная начальная цена тендера</div>
            <div class="interval">
                <label>от</label><input name="search[price_min]" value="{$search.price_min}" id="price_begin" class="int" type="text" />
                <label>до</label><input name="search[price_max]" value="{$search.price_max}" id="price_end"   class="int" type="text" />
                <label>рублей</label>
            </div>
            <div class="select">
                <input type="checkbox" name="search[param_no_null_price]" id="param_no_null_price" value="{$search.param_no_null_price}" {if isset($search.param_no_null_price)}checked="checked"{/if} />
                <label for="param_no_null_price">Показывать только с ценами</label>
                <img src="/templates/mt/img/!.png" title="Отображать в результатах поиска только тендеры, у которых указаны цены" />
            </div>
        </div>
        <div id="sf_dates">
            <div class="hx">Дата публикации тендера</div>
            <div class="interval">
                <input name="search[date_min]" value="{$search.date_min}" id="date_begin" type="text" />
                <span>&mdash;</span>
                <input name="search[date_max]" value="{$search.date_max}" id="date_end" type="text" />
            </div>
        </div>
        <div id="sf_param">
            <div>
                <input type="checkbox" name="search[param_no_customer]" id="param_no_customer"  value="{$search.param_no_customer}" {if isset($search.param_no_customer)}checked="checked"{/if} />
                <label for="param_no_customer">не искать в названии заказчика</label>
                <img src="/templates/mt/img/!.png" title="Не осуществлять поисковый запрос по наименованию заказчика" />
            </div>
            <div>
                <input type="checkbox" name="search[param_or]" id="param_or"  value="{$search.param_or}" {if isset($search.param_or)}checked="checked"{/if} />
                <label for="param_or">c любым из слов</label>
                <img src="/templates/mt/img/!.png" title="Отображать в результатах поиска тендеры, в которых найдено совпадение хотя бы с одним указанным словом" />
            </div>
        </div>
    </div>

    <div style="display: none">
    {* {foreach from=$hidden item=val key=key}<input name="{$key}" value="{$val}" type="hidden" />{/foreach} *}
{foreach from=$search.site item=val key=key}<input name="search[site][{$key}]" value="{$val}" type="hidden" />{/foreach}
</div>

<br class="fix" />
</div>
</form>
</div>

{if $items}
    {literal}
        <script>
            $("#search_form input").click(function(){ $('#sf_favorite_queries').hide(); });
            FSRC();
        </script>
    {/literal}
{/if}
