{if $error}
    <div class="error">{$error.email}</div>
{/if}

<form action="{$action}" method="POST">                                                
    <fieldset>
        <legend>
            Специалисты компании «АСПЕКТ» предоставят информацию по теме вашей заявки  и согласуют порядок действий. 
        </legend>          

    </fieldset>

    <div class="username text">
        <label>Ваше имя</label>
        <input value="{$data.username}" name="username" type="text">
    </div>

    <div class="email text">
        <label>Email для связи</label>
        <input value="{$data.email}" name="email" type="text">
    </div>

    <div class="telephone text">
        <label>Телефон</label>
        <span>+7</span>
        <input type="text" value="{$data.telephone.code}" name="telephone[code]" class="code">
        <span>‐</span>
        <input value="{$data.telephone.number}" name="telephone[number]" type="text" class="number">
    </div>        

    <div class="garant">Мы гарантируем, что ваши данные не будут разглашаться</div>

    <div class="textbox">
        <label>Комментарии</label>
        <textarea name="comment">{$data.comment}</textarea>
    </div>

    <input type="hidden" name="mtorder" value="1" />

    <input type="submit" name="mtorder" value="Отправить заявку" class="submit button">

    <div class="time">Наши специалисты свяжутся с вами в течение <span>30 минут</span></div>

    <div class="footp">
        «АСПЕКТ». Телефон: +7 495 9266720, или воспользуйтесь формой обратной связи на сайте: <a mce_href="http://www.sbaspect.ru/" href="http://www.sbaspect.ru/">www.sbaspect.ru</a>
    </div>
</form>
