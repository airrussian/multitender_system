<?php

function validata($data) {
    
    $result = array();
    
    if (empty($data['email'])) {
        $result['email'] = 'Заполните поле EMAIL';
    }
    
    if (!preg_match("#(.+?)@\S+?\.\S{2,}#si", $data['email'])) {
        $result['email'] = 'Введите корректный EMAIL';
    }
    
    return $result;
}