{if $error}
    <div class="error">{$error.email}</div>
{/if}
<form action="{$action}" method="POST">                                                
    <fieldset>
        <legend>
            Специалисты компании «АСПЕКТ» предоставят информацию по теме вашей заявки  и согласуют порядок действий. 
        </legend>          
        <table>
            <tr>
                <td class="col1"><div class="checkbox"><input type="checkbox" name="checkbox[1]" value="ОФормление электронной подписи" id="checkbox01s" {if $data.checkbox01}checked{/if} /></div></td>
                <td class="desc">Оформление электронной подписи</td>
                <td class="price"><div>от <span class="figure">3 500 р.</span></div></td>
            </tr>
            <tr>
                <td class="col1"><div class="checkbox"><input type="checkbox" name="checkbox[2]" value="Проведение аккредитации" id='checkbox02s' {if $data.checkbox02}checked{/if} /></div></td>
                <td class="desc">Проведение аккредитации</span></td>
                <td class="price"><div>от <span class="figure">5 000 р.</span></div></td>
            </tr>
            <tr>
                <td class="col1"><div class="checkbox"><input type="checkbox" name="checkbox[3]" value="Оформление тендерного кредита или займа для обеспечения заявки" id='checkbox03s' {if $data.checkbox03}checked{/if} /></div></td>
                <td class="desc">Оформление тендерного кредита, займа или гарантии для обеспечения заявки</td>
                <td class="price"><div>от <span class="figure">3%</span> от суммы обеспечения</div></td>
            </tr>
            <tr>
                <td class="col1"><div class="checkbox"><input type="checkbox" name="checkbox[4]" value="Подготовка заявки на торги" id='checkbox04s' {if $data.checkbox04}checked{/if} /></div></td>
                <td class="desc">Подготовка заявки на торги</td>
                <td class="price"><div>от <span class="figure">3 000 р.</span></div></td>
            </tr>
            <tr>
                <td class="col1"><div class="checkbox"><input type="checkbox" name="checkbox[5]" value="Оформление банковской гарантии для обеспечения исполнения контракта" id='checkbox05s' {if $data.checkbox05}checked{/if} /></div></td>
                <td class="desc">Оформление банковской гарантии для обеспечения исполнения контракта</td>
                <td class="price"><div>от <span class="figure">2%</span> от размера обеспечения</div></td>
            </tr>
        </table>     
    </fieldset>

    <div class="username text">
        <label>Ваше имя</label>
        <input value="{$data.username}" name="username" type="text">
    </div>

    <div class="email text">
        <label>Email для связи</label>
        <input value="{$data.email}" name="email" type="text">
    </div>

    <div class="telephone text">
        <label>Телефон</label>
        <span>+7</span>
        <input type="text" value="{$data.telephone.code}" name="telephone[code]" class="code">
        <span>‐</span>
        <input value="{$data.telephone.number}" name="telephone[number]" type="text" class="number">
    </div>        

    <div class="garant">Мы гарантируем, что ваши данные не будут разглашаться</div>

    <div class="textbox">
        <label>Комментарии</label>
        <textarea name="comment">{$data.comment}</textarea>
    </div>

    {if $data.item.id}<input type="hidden" name="item[id]" value="{$data.item.id}" />{/if}
    {if $data.item.name}<input type="hidden" name="item[name]" value="{$data.item.name}" />{/if}

    <input type="hidden" name="mtorder" value="1" />

    {if $data.item.id}<input type="hidden" name="item[id]" value="{$data.item.id}" />{/if}
    {if $data.item.name}<input type="hidden" name="item[name]" value="{$data.item.name}" />{/if}

    <input type="submit" value="Отправить заявку" class="submit button">

    <div class="time">Наши специалисты свяжутся с вами в течение <span>30 минут</span></div>

    <div class="footp">
        «АСПЕКТ». Телефон: +7 495 9266720, или воспользуйтесь формой обратной связи на сайте: <a mce_href="http://www.sbaspect.ru/" href="http://www.sbaspect.ru/">www.sbaspect.ru</a>
    </div>
</form>

{literal}<script>    
    
    $("#mt_order .checkbox input[type=checkbox]").each(function() {
        $(this).hide();
        $(this).after("<div class='status-view'></div>");
    });
    $("#mt_order .checkbox input[type=checkbox]:checked").each(function() {
        $(this).parent("div").addClass('checkbox-checked');
    });

    $("#mt_order .checkbox input[type=checkbox]").live('click', function() {
        $(this).parent("div").toggleClass('checkbox-checked');
        var s = "#" + $(this).attr("id") + "s";
        $(s).parent("div").toggleClass('checkbox-checked');
    });

    $('.status-view').click(function() {
        if ($(this).parent('.checkbox').hasClass('checkbox-checked')) {
            $(this).parent('.checkbox').removeClass('checkbox-checked')
        }
        else {
            $(this).parent('.checkbox').addClass('checkbox-checked')
        }
    });    
</script>{/literal}