{if !$send}
    <div id="online3">

        <h1 style="margin: 10px 0 20px 0; text-align: center; font-weight: normal; font-size: 20px;">Онлайн-заявка на Банковскую гарантию</h1>

    {if $messageError}<h2 style="color: red">{$messageError}</h2>{/if}

    <form method="post" action="{$link_base}action=gosexpress&page=bg" class="frmOnline">

        <p class="text">
            <label>Наименование организации:</label>
            <input type="text" name="orgName" value="">
            <br clear="all">
        </p>

        <p class="text">
            <label>Заказчик:</label>
            <input type="text" name="customer" value="">
            <br clear="all">
        </p>

        <p class="text">
            <label>Предмет контракта:</label>
            <input type="text" name="tender[name]" value="">
            <br clear="all">
        </p>

        <p class="text">
            <label>Цена контракта (с разбивкой по лотам):</label>
            <input type="text" name="tender[price]" value="">
            <br clear="all">
        </p>

        <p class="text" style="margin-bottom: 20px;">
            <label>Размер обеспечения исполнения контракта:</label>
            <input type="text" name="ob[size]" value="">
            <br clear="all">
        </p>

        <p class="text">
            <label>Срок действия контракта:</label>
            <input type="text" name="tender[period]" value="">
            <br clear="all">
        </p>

        <fieldset>
            <legend>Обеспечение гарантийного периода:</legend>
            <p class="radiobutton">
                <label for="f3g1s1">не требуется</label>
                <input type="radio" name="ob[period]" id="f3g1s1" value="none">
                <br clear="all">
            </p>
            <p class="radiobutton">
                <label for="f3g1s2">требуется</label>
                <input type="radio" id="f3g1s2" name="ob[period]" value="требуется">
                <br clear="all">
            </p>

            <p class="text" style="display: none">
                <label>Cроки гарантии:</label>
                <input type="text" name="ob[period]" value="">
                <br clear="all">
            </p>        
        </fieldset>

        <fieldset>
            <legend>Отношение к конкурсу (аукциону):</legend>
            <p class="radiobutton">
                <label for="f3g2s1">победитель</label>
                <input type="radio" name="tender[use]" id="f3g2s1" value="победитель">
                <br clear="all">
            </p>
            <p class="radiobutton">
                <label for="f3g2s2">участник</label>
                <input type="radio" name="tender[use]" id="f3g2s2" value="участник">
                <br clear="all">
            </p>

            <p class="radiobutton">
                <label for="f3g2s3">участие планируется</label>
                <input type="radio" name="tender[use]" id="f3g2s3" value="участие планируется">
                <br clear="all">

            </p>
        </fieldset>

        <p class="text">
            <label style="width: 190px; margin-right: 10px">Дата подведения итогов:</label>
            <input type="text" name="tender[date]" value="">
            <br clear="all">
        </p>

        <p class="text">
            <label>Контактное лицо:<strong>*</strong></label>
            <input type="text" name="contact[personal]">
            <br clear="all">
        </p>

        <p class="text phone">
            <label>Контактный телефон:<strong>*</strong></label>
            <input type="text" name="contact[telephone][plus]" class="plus" maxlength="2">
            <span>(</span>
            <input type="text" name="contact[telephone][code]" class="code" maxlength="4">
            <span>)</span>
            <input type="text" name="contact[telephone][numb]" class="numb">
            <br clear="all">
        </p>    

        <p class="text">
            <label>E-mail:<strong>*</strong></label>
            <input type="text" name="contact[email]">
            <br clear="all">
        </p>   

        <div class="buttonSubmit">
            <span>Все поля с * обязательны для заполнения</span>
            <input class="button" type="submit" value="Отправить заявку">
        </div>        
        <input type="hidden" name="online" value="1">

    </form>
</div>
{else}
    <div id='thank'>
        <h2>Спасибо, ваша заявка успешно отправлена.<br />Наши специалисты свяжутся с вами в ближайшее время.</h2>
        <a href='#online-return' class='a-return'>Отправить ещё одну заявку</a>
        <a href='#online-close' class='a-close'>Закрыть окно</a>
        <div id='banner'>
            <a href="http://www.gos-express.ru/" target='_blank'>
                <img src='http://www.gos-express.ru/images/banners/aspect.jpg' />
            </a>
        </div>
    </div>
{/if}