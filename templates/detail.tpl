{literal}
    <style>
        a.linkorder { padding: 0 24px 0 0; color: #61c290; display: inline-block; float: right; font-weight: bold; text-align: right; background: transparent url('/images/icon_arrowright_green.png') no-repeat right center; }
        a.linkorder span.ts { background: transparent url('/images/icon_file_green.png') no-repeat left center; padding: 3px 0 3px 20px; }
        a.linkorder span.bg { background: transparent url('/images/icon_path_green.png') no-repeat left center; padding: 3px 0 3px 20px; }
        a.follow { color: #61c290; font-weight: bold; text-decoration: none; background: transparent url('/images/follow.png') no-repeat left center; padding: 3px 0 3px 20px;}

        div.customers div .customer-title {   background: transparent url('/images/icon_arrowdown_green.png') no-repeat right 0px; padding-right: 25px; }
        div.customers div .customer-title:hover { background-color: #EEE; cursor: pointer; }
        div.customers div.open .customer-title { background-image: url('/images/icon_arrowup_green.png'); }

    </style>

    <script>
        jQuery(document).ready(function () {
            jQuery("div.customers .customer-title").click(function () {
                jQuery(this).parent().toggleClass("open");
                jQuery(this).parent().find(".customer-info").toggle();
            });
            
            jQuery("a.follow").click(function(){
                s = jQuery(this).attr('class');
                if (s.indexOf('add')) {
                    if (!confirm('Вы действительно, больше не желаете отслеживать закупки данного заказчика?')) {
                        return false;
                    }
                } 
                if (s.indexOf('del')) {
                    if (!confirm('Вы действительно, желаете отслеживать закупки данного заказчика?')) {
                        return false;
                    }                    
                }                
            });
        });
    </script>
{/literal}

<div id="detail">

    <div class="mt_boxwhite mt_boxwhitenew">

        <div class="item_main">
            <div class="top brawnbg cornerbg">
                <div class="item_header">
                    <div class="item_num">№ {$item.id}<span class="text">Внутренний номер тендера</span></div>                    
                </div>


                <h1 style="min-height: 45px;">{$item.name}</h1>                
                {if (($task!='print') && ($user.type!='bot') && ($user.id>0))}
                    <div class="item_menu item_menunew">
                        {$item.favorite}
                        <a href="#" title="Сообщить модератору: Неверное название тендера, не открывается официальный источник, прочие проблемы" class="error-tender" onclick="{literal}javascript:$('#dialog-error-tender').dialog('open');
                                return false;{/literal}">Сообщить об ошибке</a>
                        <!--noindex--><a href="/tenders/detail/{$item.id}?task=print&amp;no_html=1" target="_blank" rel="nofollow" title="Страница без оформления для вывода на принтер" class="version-print">Версия для печати</a><!--/noindex-->
                    </div>
                {/if}

            </div>


            <div class="item_select tabs">
                {if (($task!='print') && ($user.id))}
                    <ul>
                        <li><a href="#item_information"><span>Информация о закупке</span></a></li>
                        <li><a href="#item_comments"><span>Оставить комментарий</span></a></li>
                    </ul>
                {/if}

                {if ($task=='print')}<h3>Информация о закупке</h3>{/if}
                <div id="item_information">
                    <table>
                        <tbody>
                            <tr>
                                <th>Цена: </th>
                                <td>
                                    <span class="price">{$item.price|mt_price}</span>  
                                </td>
                            </tr>                    
                            <tr>
                                <th>Форма торгов: </th>
                                <td>{$item.type_name}</td>
                            </tr>

                            <tr>
                                <th>Номер извещения: </th>
                                <td>{$item.num|default:"&mdash;"}</td>
                            </tr>

                            {if $item.sector_id == 2}
                                <tr>
                                    <th>Сектор: </th>
                                    <td>
                                        <b>Коммерческие тендеры</b><br />
                                        <b>Внимание!</b> Информация о данном тендере находится
                                        на коммерческой площадке, которая может потребовать
                                        регистрации и дополнительной оплаты.
                                    </td>
                                </tr>
                            {/if}

                            <tr style="border: 0 none;">
                                <td colspan="2" style="padding: 0">                                    
                                    <table width="100%">
                                        <tr>                                                
                                            <th>Публикация извещения: </th>
                                            <td>{$item.date|tc_date}</td>                                                                                            
                                            <th>Окончание срока подачи заявок: </th>
                                            <td>{$item.date_end|tc_date_span}</td>                                                
                                        </tr>
                                    </table>                                    
                                </td>
                            </tr>                       

                            {if $item.type_id == 1 && $item.date_conf}
                                <tr>
                                    <th>Дата процедуры вскрытия конвертов: </th>
                                    <td>{$item.date_conf|tc_date_span}</td>
                                </tr>
                            {/if}

                            {if $item.type_id == 6 && $item.date_conf}
                                <tr>
                                    <th>Дата проведения торгов: </th>
                                    <td>{$item.date_conf|tc_date_span}</td>
                                </tr>
                            {/if}

                            <tr>
                                <th>Первоисточник: </th>
                                <td>                                    
                                    {if $user.id}   
                                        <a href="{$item.site_url}" rel="nofollow" target="_blank">{$item.site_desc}</a>, <br />
                                        <a rel="nofollow" href="{$item.away_link}" title="{$item.name|escape}" target="_blank"><b>{$item.link}</b></a>
                                            {else}
                                        <span>Для получения ссылки на первоисточник, пожалуйста авторизуйтесь или <a href="http://beta.multitender.ru/component/user/?task=register">зарегистрируйтесь</a></span>
                                    {/if}
                                </td>
                            </tr>    

                            {if $ext.lots}
                                {foreach from=$ext.lots key=key item=lot}
                                    <tr>
                                        <td>
                                            <div class="lot-number">{if $ext.lots|@count > 1}Лоты № {$key}{/if}</div>
                                            <div class="lot-name">{$lot.name}</div>
                                        </td>
                                        <td>
                                            <div class="lot-price">
                                                <h3>Начальная цена</h3>
                                                <div class="lot-price-value">{$lot.price/100|mt_price}
                                                    {if $actual}<a href="/ajax.php?action=order&amp;order=tenspr&amp;tender[name]={$item.name|escape}&amp;ajax" class="mt_order linkorder"><span class='ts'>Подготовка заявки на эти торги - 5 т.р.</span></a>{/if}
                                                </div>
                                            </div>
                                            <div class="customers" style="margin: 0; padding: 0; list-style: none;">
                                                {foreach from=$lot.customers item=customer}
                                                    <div>
                                                        <div class="customer-title">
                                                            <h3>Заказчик</h3>
                                                            <div class="title">ИНН: {$customer.inn}, Наименование: {$customer.name}</div>                                                
                                                        </div>
                                                        {if $user_id && $customer.inn}                                                    
                                                            <div class="customer-follow-button">
                                                                {if $customer.status}
                                                                    <a href="/tenders/subscribe_customer?task=del&customer_inn={$customer.inn}" class="del follow">Перестать отслеживать</a>
                                                                {else}
                                                                    <a href="/tenders/subscribe_customer?task=add&customer_inn={$customer.inn}" class="add follow">Отслеживать закупки</a>
                                                                {/if}
                                                            </div>
                                                        {/if}           
                                                        <div class="customer-info" style="display: none">
                                                            <div class="customer-applicationGuarantee">                                                        

                                                                <h3>Обеспечение заявки</h3>
                                                                <div>{if $customer.applicationGuarantee}{$customer.applicationGuarantee/100|mt_price}{else}Не уканано{/if}
                                                                    {if $customer.applicationGuarantee}<a href="/ajax.php?action=order&amp;order=tenkrd&amp;tender[name]={$item.name|escape}&amp;ajax" class="mt_order linkorder"><span class='ts'>Получить обеспечение</span></a>{/if}
                                                                </div>
                                                            </div>                                                                                                
                                                            <div class="customer-contractGuarantee">
                                                                <h3>Обеспечение контракта</h3>
                                                                <div>{if $customer.contractGuarantee}{$customer.contractGuarantee/100|mt_price}{else}Не указано{/if}
                                                                    {if $customer.contractGuarantee}<a href="/ajax.php?action=order&amp;order=bankgrt&amp;tender[name]={$item.name|escape}&amp;ajax" class="mt_order linkorder"><span class='ts'>Получить обеспечение</span></a>{/if}
                                                                </div>                                                    
                                                            </div>                 
                                                        </div>
                                                    </div>
                                                {/foreach}                                            
                                            </div>
                                            <div class="lot-winner">
                                                <h3>Победитель</h3>
                                                {if $lot.winner.inn}
                                                    <div>
                                                        <table>
                                                            <tr>
                                                                <td><b>{$lot.winner.name}</b><br />
                                                                    <b>ИНН: {$lot.winner.inn}</b>
                                                                </td>
                                                                <td>Подробная информация о победителе и истории участия в закупках <br /><a href="http://www.filetender.ru/info_new.php?inn={$lot.winner.inn}&num={$item.num|default:"&mdash;"}" target="_blank">Получить</a></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                {else}
                                                    {if $user_id}
                                                        <div>
                                                            <a href="/tenders/follow?task=add&purchaseNumber={$item.num|default:"&mdash;"}&lotNumber={$key}" class="follow">Информировать о победители</a>
                                                        </div>
                                                    {/if}
                                                {/if}
                                            </div>
                                        {/foreach}
                                    {/if}


                                    {if $item.customer}
                                <tr>
                                    <th>Наименование организатора:</th>
                                        {if $item.customer_id}
                                        <td><a href="/tenders/?search[customer][{$item.customer_id}]" title="Все тендеры заказчика: «{$item.customer|escape}»">{$item.customer}</a></td>
                                        {else}
                                        <td>{$item.customer}</td>
                                    {/if}
                                </tr>
                            {/if}

                            {if $item.customer_address}
                                <tr>
                                    <th>Адрес организатора:</th>
                                    <td>
                                        {$item.customer_address}, <a href="http://maps.yandex.ru/?text={$item.customer_address|escape:url}" target="_blank" rel="nofollow">Посмотреть на Яндекс.Картах</a>
                                    </td>
                                </tr>
                            {/if}

                            {if $item.customer_phone}
                                <tr>
                                    <th>Контакты организатора:</th>
                                    <td>{$item.customer_phone},
                                        {if $item.region_tz == 0}
                                            время Московское
                                        {else}
                                            разница во времени с Москвой {if $item.region_tz<0}&minus;{/if}{if $item.region_tz>0}+{/if}{math equation="abs(x)" x=$item.region_tz} ч.
                                        {/if}
                                    </td>
                                </tr>
                            {/if}

                            {if $item.customer_email}
                                <tr>
                                    <th>E-Mail организатора:</th>
                                    <td><a href="mailto:{$item.customer_email}?subject={'По тендеру'|escape:url}">{$item.customer_email}</a></td>
                                </tr>
                            {/if}

                            <tr>
                                <th>Регион: </th>
                                <td><a href="/tenders/?search[reg][{$item.region_id}]">{$item.region_name}</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                {if $user.id && $task!='print'}
                    <div id='item_comments'>{$comments}</div>
                {/if}
            </div>
        </div>

        <!-- Правая панель для информации -->
        {if ($task!='print')}
            <div id="detail-right">

                <!--noindex-->
                {*
                <div id='item_online_order' class="block">
                <h3>Онлайн-заявка</h3>
                <a href="/tenderservice/uslugi/consaltplaytorg?item_id={$item.id}" rel="nofollow">Получить консультацию</a>
                {*
                {if $item.type_id==2}<a href="/tenderservice/uslugi/createconcursorder?item_id={$item.id}" rel="nofollow">Подготовить конкурсную заявку</a>{/if}
                {if $item.type_id==6}<a href="/tenderservice/uslugi/createaucorder?item_id={$item.id}" rel="nofollow">Подготовить аукционную заявку</a>{/if}
                {if $item.type_id==3}<a href="/tenderservice/uslugi/createkotirovokorder?item_id={$item.id}" rel="nofollow">Подготовить котировочную заявку</a>{/if}
                
                </div>*}
                <!--/noindex-->        

                {literal}
                    <script>
                        $(function () {
                            $('.regbutton').click(function () {
                                document.location.href = '/component/user/?task=register';
                            });
                        });
                    </script>        
                {/literal}

                {*
                <div id="consalt" class="right-block">
                <h3 class="consalt">Как участвовать в выбранной закупке?</h3>
                <p>&nbsp;</p>
                <a href="/ajax.php?action=order&amp;order=cons_t&amp;ajax" data="item[id]={$item.id}&item[name]={$item.name}&checkbox[1]=true&checkbox[2]=true&checkbox[3]=true" class="mt_order button" style="padding: 7px 40px">Отправить заявку</a>
                </div>                        

                <div id="party" class="right-block">
                <h3 class="party">Для участия в торгах необходимо</h3>

                <div id="blockselected">

                <div class="checkbox">
                <input type="checkbox" id="checkbox01" checked />                        
                <label for="checkbox01">Оформление электронной подписи<span class="question"></span></label>                        
                </div>
                <div class="checkbox">
                <input type="checkbox" id="checkbox02" checked />                        
                <label for="checkbox02">Проведение аккредитации<span class="question"></span></label>                        
                </div>

                <div class="checkbox">
                <input type="checkbox" id='checkbox03' checked />
                <label for="checkbox03">Оформление тендерного обеспечения заявки<span class="question"></span></label>                        
                </div>

                <div class="checkbox">
                <input type="checkbox" id='checkbox04' checked/>
                <label for="checkbox04">Подготовка заявки на торги</label><span class="question"></span>                        
                </div>

                <div class="checkbox">
                <input type="checkbox" id='checkbox05' checked />
                <label for="checkbox05">Оформление банковской гарантии для обеспечения исполнения контракта<span class="question"></span></span></label>                        
                </div>    
                <a href="/ajax.php?action=order&amp;order=help&amp;ajax" data="item[id]={$item.id}&item[name]={$item.name}" class="mt_order button" style="padding: 7px 3+0px">Уточнить стоимость</a>
                </div>
                {literal}
                <script>
                $("#party a.mt_order").click(function () {
                var a = $(this);
                $("#party input").each(function () {
                var str = a.attr('data');
                var str = str.replace($(this).attr('id') + '=true', '');
                var str = str.replace($(this).attr('id') + '=false', '');
                a.attr('data', str);
                if ($(this).is(':checked')) {
                a.attr('data', a.attr('data') + '&' + $(this).attr('id') + '=true');
                }
                });
                return false;
                });
                </script>
                {/literal}
                </div>*}

                <div id="training" class="right-block">
                    <h3>Обучение закупкам</h3>
                    <div id="blockselected">
                        <a href="/webinar">Вебинары по 44-ФЗ и 223-ФЗ</a>

                    </div>
                </div>
                
                {$freesubscribe}

                {if !$user.id}
                <div class="info-buy right-block">
                    
                        <div class="warning">
                            <h3 class="warning">Зарегистрируйтесь!</h3>
                            <!--<p>
                                Став полноценным пользователем Мультитендера и оформив
                                подписку к сайту, вы сможете просматривать подробную
                                информацию о тендерах и сопроводительные документы,
                                читать и оставлять заметки и комментарии к тендерам
                            </p>-->
                            <div id="blockselected2">

                                <div class="checkbox">
                                    <div class="status-view noncheckbox"></div>                       
                                    <label for="checkbox01">Сохранение поисковых запросов и истории поиска</label>                        
                                </div>
                                <div class="checkbox">
                                    <div class="status-view noncheckbox"></div>
                                    <label for="checkbox02">Получение новых закупок по e-mail</label>                        
                                </div>

                                <div class="checkbox">
                                    <div class="status-view noncheckbox"></div>
                                    <label for="checkbox03">Возможность добавить закупку в &laquo;Избранное&raquo;</label>                        
                                </div>

                                <div class="checkbox">
                                    <div class="status-view noncheckbox"></div>
                                    <label for="checkbox04">Просмотр документов закупки</label>                        
                                </div>
                            </div>

                            <input type="button" value="Пройти регистрацию" class="button regbutton" />
                        </div>
                                                      
                </div>                                
                {/if}     

                {if $user.id && $docs}
                    <div id="item_docs" class="right-block">        
                        <h3>Документы тендера</h3>
                        {foreach from=$docs item=doc}
                            <a class="document {$doc.ext}" href="{$doc.link}" target="_blank" rel="nofollow">{$doc.name}</a><small>{$doc.size}&nbsp;{$doc.date}</small>                   
                            {/foreach}
                    </div>
                {/if}

                {* <!-- LIKES -->
                literal}
                <div id="likes">
                <h3 onclick="javascript: $(this).next('div').toggle(500);"><span>Cсылка на тендер</span></h3>
                <div style='display: none;'>
                <div class="likes"><div id="vk_like"></div><script type="text/javascript">VK.Widgets.Like("vk_like", {type: "button"});</script></div>
                <div class="likes"><fb:like layout="button_count" show_faces="true" width="135"></fb:like></div>
                <div class="likes"><a class="odkl-klass" expr:href='data:post.url' onclick="ODKL.Share(this);return false;" >Класс!</a></div>
                <div class="likes">
                <noindex><a target="_blank" class="mrc__plugin_like_button" href="http://connect.mail.ru/share" rel="{'type' : 'button', 'width' : '130', 'show_faces' : 'true'}">Нравится</a></noindex>
                <script type="text/javascript" src="http://cdn.connect.mail.ru/js/loader.js"></script>
                </div>
                <div class="likes">
                <a href="http://twitter.com/share" rel="nofollow" class="twitter-share-button" data-count="horizontal" data-via="multitender">Tweet</a>
                <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
                </div>
                </div>
                </div>
                {/literal 
                <!-- END LIKES -->*}
            {/if}       
        </div>
        <br class="fix" />
    </div>
</div>

{if $task!='print'}

    {if $similar.view}
        <div class="mt_boxwhite" style='margin-top: 10px'>
            <h2>5 похожих тендеров</h2>
            {$similar.view}
        </div>
    {/if}

    {if $customer.view }
        <div class="mt_boxwhite" style='margin-top: 10px'>
            <h2>Последние 5 тендеров заказчика <a href="{$like_customer_link}" title="Все тендеры заказчика: «{$item.customer|escape}»">«{$item.customer}»</a></h2>
            {$customer.view}
        </div>
    {/if}

    {if 1 }
        <div class="mt_boxwhite" style='margin-top: 10px'>
            <h2>Последние 5 тендеров региона <a href="{$region.link}" title="Все тендеры региона «{$item.region_name|escape}»">{$item.region_name}</a></h2>
                {$region.view}
        </div>
    {/if}

{/if}
{if (($task!='print') && ($user.id))}
    <div id="dialog-active-tender" title="Активация тендера">
        <p class="validateTips">Вы собираетесь получить полный доступ к подробной информации, комментариям и документам тендера.</p>
        {if $item.sector_id == 2}<p><b style="color: red">Внимание:</b>Информация о данном тендере находится на коммерческой площадке, которая может потребовать регистрации и дополнительной оплаты.</p>{/if}
        <p>Ваш баланс позволяет активировать <b>{$access.tenders} тендеров</b></p>
    </div>

    <div id="dialog-error-tender" title="Сообщить об ошибке на тендере">
        <p class="validateTips">Пожалуйста, укажите замеченную ошибку</p>
        <form action="/ajax.php?action=error_tenders&task=send" method="POST">
            <input type="hidden" name="item_id" value="{$item.id}" />
            <textarea name="comment" style="border: 1px solid #CCC; width: 300px;"></textarea>
        </form>
    </div>
{/if}

