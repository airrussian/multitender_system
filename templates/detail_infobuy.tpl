<div class="info-buy">
{if $conf.detail_public}
    <b>Доступ к полной информации о данном тендере в публичном доступе (прошло три месяца с момента публикации).</b>
{else}
    {if $user.id}
        {if $conf.full}
            <b>Доступ к полной информации о данном тендере приобретен: {$purches.date_purches}.
            {if $access.limit}
                Доступ к системе оплачен до {$access.todate}.
            {else}
                На балансе ещё {$access.tenders} просмотров.
            {/if}
        </b>
        {else}
            {if $access.tenders}
                <b>Для доступа к полной информации вы можете <span id="{$item.id}" class="buy-tender">приобрести</span> данный тендер, доступно еще: {$access.tenders} просмотров.</b>
            {else}
                <b>Для доступа к полной информации вы должны <a href="/tenders?action=billing">оплатить счёт</a></b>
            {/if}
        {/if}
    {else}
        <b>Для доступа к возможности просмотра полной информации <a href="/component/user/?task=register">пожалуйста зарегистрируйтесь</a>.</b>
    {/if}
{/if}
</div>
