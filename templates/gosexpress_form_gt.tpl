<div id='tab3' class="tabs" title="Заявка на поручительство">
    <b>Заявка на поручительство</b><br>
    {if !$send}
    
    <form  name='form_mail3' action="/tenders/gosexpress?page={$smarty.get.page}" method="post" id="cForm">

        {if $messageError}<div class="error">{$messageError}</div>{/if}

        <input type="hidden" name="act" value="y3" />
        <p><label class="lbl">Полное наименование организации</label>
            <input id="name_org1" class="textbox" name="name_org1" type="text" value="{if $data.name_org1}{$data.name_org1}{/if}"></p>
        <p><label class="lbl">Госзаказчик</label>
            <input id="gos_zak1" class="textbox" name="gos_zak1" type="text" value="{if $data.gos_zak1}{$data.gos_zak1}{/if}"></p>
        <p><label class="lbl">Предмет госконтракта</label>
            <input id="predmet1" class="textbox" name="predmet1" type="text" value="{if $data.predmet1}{$data.predmet1}{/if}"></p>
        <p><label class="lbl">Цена госконтракта (с разбивкой по лотам)</label>
            <input id="cenagos1" class="textbox" name="cenagos1" type="text" value="{if $data.cenagos1}{$data.cenagos1}{/if}"></p>
        <p><label class="lbl">Размер обеспечения исполнения госконтракта</label>
            <input id="razmer1" class="textbox" name="razmer1" type="text" value="{if $data.razmer1}{$data.razmer1}{/if}"></p>
        <p><label class="lbl">Срок действия госконтракта</label>
            <input id="srok1" class="textbox" name="srok1" type="text" value="{if $data.srok1}{$data.srok1}{/if}"></p>
        <p><label>Обеспечение гарантийного периода:</label></p>
        <p>
            <input style='width:25px' type='radio' id="obespech1" class="textbox" name="obespech1" value="не требуется"  {if $data.obespech1=='не требуется'}checked{/if}><label for="obespech1">не требуется</label>
            <input style='width:25px' type='radio' id="obespech2" class="textbox" name="obespech1" value="требуется"  {if $data.obespech1=='требуется'}checked{/if}><label for="obespech2">требуется</label>
        </p>
        <p><label>Отношение к конкурсу (аукциону)</label></p>
        <p>
            <input style='width:25px' type='radio' id="konkurs1" class="textbox" name="konkurs1" value="победитель" {if $data.konkurs1=='победитель'}checked{/if}><label for="konkurs1">победитель</label>
            <input style='width:25px' type='radio' id="konkurs2" class="textbox" name="konkurs1" value="участник" {if $data.konkurs1=='участник'}checked{/if}><label for="konkurs2">участник</label>
            <input style='width:25px' type='radio' id="konkurs3" class="textbox" name="konkurs1" value="участие планируется" {if $data.konkurs1=='участие планируется'}checked{/if}><label for="konkurs3">участие планируется</label>
        </p>
        <p><label class="lbl">Контактное лицо<strong>*</strong></label>
            <input id="fio1" class="textbox" name="fio1" type="text" value="{if $data.fio1}{$data.fio1}{/if}"></p>
        <p><label class="lbl">Контактный телефон<strong>*</strong></label>
            <input id="tel1" class="textbox" name="tel1" type="text" value="{if $data.tel1}{$data.tel1}{/if}"></p>
        <p><label class="lbl">E-mail</label>
            <input id="email1" class="textbox" name="email1" type="text" value="{if $data.email1}{$data.email1}{/if}"></p>
        <p><label>Дополнительная информация</label><br />
            <textarea name="dop_info1" rows="" cols=""></textarea></p>
        <br>
        <p><label><br></label><input class="button" type="submit" value="Отправить"></p>

        <div id="footer2">Поля, отмеченные <strong>*</strong>, обязательны к заполнению.</div>

    </form>
    {else}
    <p>Ваше сообщение успешно отправлено</p>
    {/if}
</div>