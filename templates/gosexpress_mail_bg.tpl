<h3>Онлайн-заявка на Банковскую гарантию</h3>
<table>
    {if $data.orgName}
    <tr>
        <td>Полное наименование организации:</td><td>{$data.orgName}</td>
    </tr>
    {/if}

    {if $data.customer}
    <tr>
        <td>Госзаказчик:</td><td>{$data.customer}</td>
    </tr>
    {/if}
    {if $data.tender.name}
    <tr>
        <td>Предмет госконтракта:</td>
        <td>{$data.tender.name}</td>
    </tr>
    {/if}
    {if $data.tender.price}
    <tr>
        <td>Цена госконтракта (с разбивкой по лотам):</td>
        <td>{$data.tender.price}</td>
    </tr>
    {/if}
    {if $data.ob.size}
    <tr>
        <td>Размер обеспечения исполнения госконтракта:</td>
        <td>{$data.ob.size}</td>
    </tr>        
    {/if}
    {if $data.tender.period}
    <tr>
        <td>Срок действия госконтракта:</td>
        <td>{$data.tender.period}</td>
    </tr>
    {/if}
    {if $data.ob.1}
    <tr>
        <td>Обеспечение гарантийного периода:</td>
        <td>$data.ob.1{if $data.ob.period}, Сроки гарантии:{$data.ob.period}{/if}</td>
    </tr>
    {/if}
    
    {if $data.tender.use}
    <tr>
        <td>Отношение к конкурсу (аукциону):</td>
        <td>{$data.tender.use}</td>
    </tr>
    {/if}
    
    {if $data.tender.date}
    <tr>
        <td>Дата подведения итогов:</td>
        <td>{$data.tender.date}</td>
    </tr>
    {/if}
            
    <tr>
        <td>Контактное лицо:</td><td>{$data.contact.personal}</td>
    </tr>
    <tr>
        <td>Контактный телефон:</td><td>{$data.contact.telephone.plus} ({$data.contact.telephone.code}) {$data.contact.telephone.numb}</td>
    </tr>
    
    <tr>
        <td>E-mail:</td><td>{$data.contact.email}</td>
    </tr>
    
    
</table>