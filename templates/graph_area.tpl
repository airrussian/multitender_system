{literal}<script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Year');
        data.addColumn('number', '{/literal}{$line_name}{literal}');
        data.addRows({/literal}{$values}{literal});

        var chart = new google.visualization.AreaChart(document.getElementById('{/literal}{$divid}{literal}'));
        chart.draw(data, {width: {/literal}{$width}{literal}, height: {/literal}{$height}{literal}, title: '{/literal}{$title}{literal}',
                          hAxis: {title: '{/literal}{$Xname}{literal}', titleTextStyle: {color: '#FF0000'}}
                         });
      }
    </script>

    <div id="{/literal}{$divid}{literal}"></div>
{/literal}