{include file="statistics_menu.tpl"}

<div class="mt_boxwhite">
    <div id="filter-at-search">
        <h5>Фильтр по поискам</h5>
        <p>
            {if $find==all}   <B>ВСЕ</B>   {else} <a href="{$link_base}{$link_act}&user={$user}&find=all">ВСЕ</a>       {/if}
            {if $find==search}<B>SEARCH</B>{else} <a href="{$link_base}{$link_act}&user={$user}&find=search">SEARCH</a> {/if}
            {if $find==feed}  <B>FEED</B>  {else} <a href="{$link_base}{$link_act}&user={$user}&find=feed">FEED</a>     {/if}
            {if $find==other} <B>OTHER</B> {else} <a href="{$link_base}{$link_act}&user={$user}&find=other">OTHER</a>   {/if}
        </p>
    </div>

    <div id="filter-at-users">
        <h5>фильтр по пользователям</h5>
        <p>

            {if $user==all}          <B>ВСЕ</B>                          {else} <a href="{$link_base}{$link_act}&find={$find}&user=all">ВСЕ</a> {/if}
            {if $user==anonym}       <B>АНОНИМЫ</B>                      {else} <a href="{$link_base}{$link_act}&find={$find}&user=anonym">АНОНИМЫ</a> {/if}
            {if $user==bot}          <B>БОТЫ</B>                         {else} <a href="{$link_base}{$link_act}&find={$find}&user=bot">БОТЫ</a> {/if}
            {if $user==user}         <B>ПОЛЬЗОВАТЕЛИ (не оплатившие)</B> {else} <a href="{$link_base}{$link_act}&find={$find}&user=user">ПОЛЬЗОВАТЕЛИ (не оплатившие)</a> {/if}
            {if $user==user_payment} <B>ПОЛЬЗОВАТЕЛИ (оплатившие)</B>    {else} <a href="{$link_base}{$link_act}&find={$find}&user=user_payment">ПОЛЬЗОВАТЕЛИ (оплатившие)</a> {/if}
        </p>
    </div>

    {if $total}

    <div>Всего: <b>{$total}</b></div>

    <table class="view">

        <thead>
            <tr>
                <th width=30%>Искали</th>
                <th width=20%>Доп. поиск</th>
                <th width=15%>Кто?</th>
                <th>Было найдено</th>
                <th>Когда</th>
                <th>Время запроса, msec<br />Sphinx/Mysql</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$items item=item}
            <tr>
                <td><a href="{$link_base}{$item.link}">{$item.search.main}</a></td>
                <td>{$item.search_txt}</td>
                <td>{$item.user.name} {$item.type_user}({$item.user.Id})</td>
                <td>{$item.search.total}</td>
                <td><b>{$item.date_add}</b><br />{$item.timestamp}</td>
                <td>{$item.mtime} ({$item.mtime_sphinx}/{$item.mtime_mysql})</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    {$scrol}
    {else}
    <div><b>Ничего нету</b></div>
    {/if}
</div>