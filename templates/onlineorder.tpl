<div id='online-order'>
    <a {if $item.id || $data.form}style='display: none'{/if} class='btm open' href="#" onclick="$(this).next('div').show();
            $(this).hide();
            return false;">Получить услугу</a>
    <div {if !$item.id && !$data.form}style='display: none'{/if}>
        <form id="dfContactForm" action="#dfContactForm" method="POST">
            <a href="#" onclick="$('a.btm').show();
            $('a.btm').next('div').hide();
            return false;" style='display: block; margin: 15px; color: black; text-align: right;'>Свернуть</a>
            {if $item.id}
                <p>
                    <label>Тендер: </label>
                    <span style="width: 325px; display: block; float: left;">&laquo;<a href='/tenders/detail/{$item.id}'>{$item.name}</a>&raquo;</span>
                </p>
                <input type='hidden' name='item[id]' value="{$item.id}" />
                <input type='hidden' name='item[name]' value="{$item.name}" />
                {if $price}
                    <p>
                        <label>Стоимость: </label>
                        <span><b>{$price} руб.</b></span>
                    </p>
                    <input type='hidden' name='price' value="{$price}" />
                {/if}
            {/if}

            {if $errorMsg}
                <div class="errorMsg">Ошибка: {$errorMsg}</div>
            {/if}

            <p>
                <label>Контактный email:</label>
                <input id="form_email" type='text' name="form[email]" value="{$data.form.email|escape}" />        
            </p>

            <p>
                <label>Наименование организации:</label>
                <input id="form_name" type='text' name="form[name]" value="{$data.form.name|escape}" />
            </p>       

            <p>
                <label>Контактное лицо:</label>
                <input id="form_contact" type='text' name="form[contact]" value="{$data.form.contact|escape}" />           
            </p>

            <p>
                <label>Контактный телефон:</label>
                <input id="form_telephone" type='text' name="form[telephone]" value="{$data.form.telephone|escape}" />           
            </p>

            <p>
                <label>Доп.информация:</label>
                <textarea id="form_other" name="form[other]">{$data.form.other}</textarea>
            </p>
            
            {if $user_id}<input type="hidden" name="user_id" value="{$user_id}" />{/if}
            
            {if $captcha}
                <p>
                    <label>Извините, сработала система борьбы со спамом, введите пожалуйста код изображенный справа.</label>
                <div style="margin-left: 215px;">{$captcha}</div>
                </p>
            {/if}
            <input class='btm' type="submit" name="send" value="Отправить заявку" />
        </form>     
    </div>
</div>


{literal}
    <style>
        .errorMsg {
        color: red;
        font-size: 15px;
        text-align: center;
        font-weight: bold;
        font-style: italic;
        }
    </style>
{/literal}