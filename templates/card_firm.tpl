{if $full_link}<div id="card_firm">{/if}
    <h3 style="font-weight: bold">Информация о фирме</h3>
    {if $firm}
    <table><col width="240px" /><col />
        <tr><td>Логин:</td><td>{$joomla.username}</td></tr>
        <tr><td>email:</td><td>{$joomla.email}</td></tr>
        <tr><td colspan="2"></td></tr>
        <tr><td>Название:</td><td>{$firm.name}</td></tr>
        <tr><td>ИНН:</td><td>{$firm.INN}</td></tr>
        <tr><td>КПП:</td><td>{if $firm.KPP}{$firm.KPP}{else}<i>Отсутствует</i>{/if}</td></tr>
        <tr><td>Юридический адрес:</td><td>{$firm.domicile_full}</td></tr>
        <tr><td>Почтовый адрес:</td><td>{if $firm.address_full}{$firm.address_full}{else}<i>Совпадает</i>{/if}</td></tr>
        <tr><td>Телефон:</td><td>{$firm.telephone}</td></tr>
        <tr><td>Контактное лицо:</td><td>{$firm.contact}</td></tr>
    </table>
    {else}
    <p>Информация отсутствует</p>
    {/if}
    {if $full_link}{if $user_id}
    <a href="{$link_base}action=card_user&task=fullcard_show&user_id={$user_id}">Подробная информация о пользователе</a>
    {/if}{/if}
{if $full_link}</div>{/if}
