<img class="y1" src="/templates/mt/img/sl.gif" alt="" /><div class="y2"><a class="c2" href="/tenders/distributor_list?id_distributor={$id_dist}">Вернуться к информации о поставщике</a></div>
<h1 class="y4i">Информация по контракту</h1><br/>

<div class="mt_boxwhite">

<small>Статус контракта: {$contract.execution_type}</small>
<div id="statistics">
    <dl>
        <dt><b>Дата заключения контракта</b></dt>
        <dd>{$contract.contract_date}</dd>
    </dl>
    <dl>
        <dt><b>Дата окончания контракта</b></dt>
        <dd>{$contract.contract_end_date}</dd>
    </dl>
    <dl>
        <dt><b>Сумма контракта (тыс. руб.)</b></dt>
        <dd>{$contract.contract_price}</dd>
    </dl>
</div>
<hr/>
<br/>
<h3>Поставщик: {$distributor.Name}</h3>
<hr/>
<div id="statistics">
    <dl>
        <dt><b>ИНН/КПП</b></dt>
        <dd>{$distributor.INN}/{$distributor.KPP}</dd>
    </dl>
    <dl>
        <dt><b>Адрес</b></dt>
        <dd>{$distributor.Address}</dd>
    </dl>
    <dl>
        <dt><b>Регион</b></dt>
        <dd>{$distributor.region_name}</dd>
    </dl>
</div>
<hr/>
<br/>
<h3>Заказчик: {$contract.customer_name}</h3>
<hr/>
<div id="statistics">
    <dl>
        <dt><b>ИНН/КПП</b></dt>
        <dd>{$contract.customer_inn}/{$contract.customer_kpp}</dd>
    </dl>
</div>
<hr/>
<br/>
{if $goods}
<h4>Товары по контракту</h4>
<small>Всего товаров: {$count}</small><br/>
<table class="view goods">
    <thead>
    <tr>
        <th align="left"><b>Наименование</b></th>
        <th align="left"><b>Цена, тыс. руб.</b></th>
        <th align="left"><b>Количество</b></th>
        <th align="left"><b>Тыс. руб.</b></th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$goods item=item}
    <tr class="{cycle values='unc,uunc'}">
        <td align="left">{$item.name}</td>
        <td align="left">{$item.price}</td>
        <td align="left">{$item.count}</td>
        <td align="left">{$item.sum}</td>
    </tr>
    {/foreach}
    <tr class="title">
        <td align="left"></td>
        <td align="left"></td>
        <td align="left"><b>Сумма контракта (тыс. руб.)</b></td>
        <td align="left">{$contract.contract_price}</td>
    </tr>
    </tbody>
</table>
{/if}
<br/>
<small>Данные получены из открытых источников: <a href="http://reestrgk.roskazna.ru/">Росказна</a> и <a href="http://rnp.fas.gov.ru/">Реестр недобросовестных поставщиков</a></small>
</div>