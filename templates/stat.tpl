<html>
    <head>
    </head>
    <body>
        <h1>По дням</h1>
        <i>Отсортированно в порядке убывания количества тендеров</i>
    <table border="0">
    {foreach from=$stat item=site key=site_id}
        <tr>
            <td colspan="{$site.Total|@sizeof|plus:1}" align="center"><h3>{$site_id}</h3></td>
        </tr>
        <tr bgcolor="{cycle values="#eeeeee,#dddddd"}">
            <td>&nbsp;</td>
            {foreach from=$site.Total item=Total key=date}
            <td>{$date}</td>
            {/foreach}
        </tr>

        {foreach from=$site item=dates key=region}
        <tr bgcolor="{cycle values="#eeeeee,#dddddd"}">
            {if $region == "Total"}
                <td><b>{$region}</b></td>
            {else}
                <td>{$region}</td>
            {/if}
            {foreach from=$dates item=total key=date}
            {if $region == "Total"}
                <td align="right"><b>{$total}</b></td>
            {else}
                <td align="right">{$total}</td>
            {/if}
            {/foreach}
        </tr>
        {/foreach}
    {/foreach}
    </table>
    </body>
</html>
