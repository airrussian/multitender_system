<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty price output
 *
 * Type:     modifier<br>
 * Name:     tc_price<br>
 * Purpose:  echo price
 * @author Paul Loginov
 * @param  string
 * @return string
 */
function smarty_modifier_tc_price($string) {
    $string = (int) $string;

    if (empty($string)) {
        return "—";
    }

    if ($string > 2000) {
        //FIXME: number_format - 1 char in replace
        $string = number_format($string/1000, 0, ',', ' ');
        $string = str_replace(' ', "\xC2\xA0", $string);
    } else {
        $string = number_format($string/1000, 3, ',', ' ');
    }

    return $string;
}
?>