<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty price output
 *
 * Type:     modifier<br>
 * Name:     tc_price<br>
 * Purpose:  echo price
 * @author Paul Loginov
 * @param  string
 * @return string
 */
function smarty_modifier_mt_price($string) {
    $string = (int) $string;

    if (empty($string)) {
        return "—";
    }

    $string = number_format($string, 2, ',', ' ');

    return $string;
}
?>