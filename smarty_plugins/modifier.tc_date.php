<?php
/**
 * Smarty date output
 *
 * Type:     modifier<br>
 * Name:     tc_date<br>
 * Purpose:  echo date with check date
 * @package Smarty
 * @subpackage plugins
 * @author Paul Loginov
 * @param  string
 * @return string
 */
function smarty_modifier_tc_date($string, $format = null) {
    $date_t = strtotime($string);
    if (empty($format)) {
        $format = 'd.m.Y';
    }
    if ( $date_t ) {
        $string = date($format, $date_t);
    } else {
        $string = "—";
    }
    return $string;
}
