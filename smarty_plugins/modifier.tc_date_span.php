<?php
/**
 * Smarty date output
 *
 * Type:     modifier<br>
 * Name:     tc_date<br>
 * Purpose:  echo date with check date
 * @package Smarty
 * @subpackage plugins
 * @author Paul Loginov
 * @param  string
 * @return string
 */
function smarty_modifier_tc_date_span($string, $format = null, $span='<span class="date_passed" title="прошедшая дата">#DATE#</span>') {
    $date_t = strtotime($string);
    if (empty($format)) {
        $format = 'd.m.Y';
    }
    $date = date($format, $date_t);
    if ($date_t) {
        if (strtotime('-1 day') > $date_t) {
            $string = str_ireplace('#DATE#', $date, $span);
        } else {
            $string = $date;
        }
    } else {
        $string = "—";
    }
    return $string;
}
